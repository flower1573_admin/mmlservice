CREATE DATABASE /*!32312 IF NOT EXISTS*/`portaldb` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `portaldb`;

/*Table structure for table `tbl_account_policy` */

DROP TABLE IF EXISTS `tbl_account_policy`;

CREATE TABLE `tbl_account_policy` (
  `id` varchar(50) DEFAULT NULL,
  `account_min_length` varchar(10) DEFAULT NULL,
  `account_max_length` varchar(10) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_audit_log` */

DROP TABLE IF EXISTS `tbl_audit_log`;

CREATE TABLE `tbl_audit_log` (
  `id` varchar(50) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `access_ip` varchar(50) DEFAULT NULL,
  `access_url` varchar(200) DEFAULT NULL,
  `response_code` varchar(10) DEFAULT NULL,
  `execute_time` varchar(50) DEFAULT NULL,
  `request_param` varchar(2000) DEFAULT NULL,
  `access_date_time` timestamp NULL DEFAULT NULL,
  `request_method` varchar(10) DEFAULT NULL,
  `request_module` varchar(100) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_log` */

DROP TABLE IF EXISTS `tbl_log`;

CREATE TABLE `tbl_log` (
  `id` varchar(50) NOT NULL,
  `level` varchar(10) DEFAULT NULL,
  `details` varchar(500) DEFAULT NULL,
  `filePath` varchar(500) DEFAULT NULL,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Table structure for table `tbl_login_address_policy` */

DROP TABLE IF EXISTS `tbl_login_address_policy`;

CREATE TABLE `tbl_login_address_policy` (
  `id` varchar(50) DEFAULT NULL,
  `start_ip` varchar(20) DEFAULT NULL,
  `end_ip` varchar(20) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_login_time_policy` */

DROP TABLE IF EXISTS `tbl_login_time_policy`;

CREATE TABLE `tbl_login_time_policy` (
  `id` varchar(50) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remark` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_menu` */

DROP TABLE IF EXISTS `tbl_menu`;

CREATE TABLE `tbl_menu` (
  `id` varchar(20) NOT NULL COMMENT 'ID',
  `name` varchar(50) NOT NULL COMMENT '权限名称',
  `permission` varchar(200) DEFAULT NULL COMMENT '权限标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='权限表';

/*Table structure for table `tbl_navigate_catalog` */

DROP TABLE IF EXISTS `tbl_navigate_catalog`;

CREATE TABLE `tbl_navigate_catalog` (
  `id` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_navigate_details` */

DROP TABLE IF EXISTS `tbl_navigate_details`;

CREATE TABLE `tbl_navigate_details` (
  `id` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `catalog_id` varchar(250) DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_password_policy` */

DROP TABLE IF EXISTS `tbl_password_policy`;

CREATE TABLE `tbl_password_policy` (
  `id` varchar(50) DEFAULT NULL,
  `min_length` varchar(10) DEFAULT NULL COMMENT '最小字符',
  `max_length` varchar(10) DEFAULT NULL COMMENT '最多字符',
  `min_upper_letter_length` varchar(10) DEFAULT NULL COMMENT '最少大写字符个数',
  `min_lower_letter_length` varchar(10) DEFAULT NULL COMMENT '最少小写字符个数',
  `min_special_letter_length` varchar(10) DEFAULT NULL COMMENT '最少特殊字符个数',
  `allow_incress` varchar(10) DEFAULT NULL COMMENT '密码不能是数字或字母递增或递减序列',
  `max_same_length` varchar(10) DEFAULT NULL COMMENT '密码中允许同一字符连续出现的次数',
  `max_use_time` varchar(10) DEFAULT NULL COMMENT '密码不予使用最长天数',
  `dict_letter` varchar(1000) DEFAULT NULL COMMENT '密码不能包含字典中的词汇',
  `first_login_update` varchar(10) DEFAULT NULL COMMENT '首次登录是否需要修改密码',
  `remark` varchar(10) DEFAULT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_role` */

DROP TABLE IF EXISTS `tbl_role`;

CREATE TABLE `tbl_role` (
  `id` varchar(50) DEFAULT NULL,
  `role_id` varchar(50) DEFAULT NULL COMMENT '角色ID',
  `role_name` varchar(50) NOT NULL COMMENT '角色名称',
  `remark` varchar(250) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色表';

/*Table structure for table `tbl_role_menu` */

DROP TABLE IF EXISTS `tbl_role_menu`;

CREATE TABLE `tbl_role_menu` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `role_id` varchar(50) DEFAULT NULL COMMENT '角色ID',
  `menu_id` varchar(50) DEFAULT NULL COMMENT '权限ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色与权限关系表';

/*Table structure for table `tbl_schedule` */

DROP TABLE IF EXISTS `tbl_schedule`;

CREATE TABLE `tbl_schedule` (
  `id` varchar(50) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `cron_expression` varchar(100) DEFAULT NULL,
  `cron_status` varchar(10) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `bean_name` varchar(250) DEFAULT NULL,
  `method_name` varchar(250) DEFAULT NULL,
  `method_params` varchar(250) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_uem` */

DROP TABLE IF EXISTS `tbl_uem`;

CREATE TABLE `tbl_uem` (
  `id` varchar(50) DEFAULT NULL,
  `request_method` varchar(10) DEFAULT NULL,
  `request_servlet_path` varchar(200) DEFAULT NULL,
  `request_page` varchar(500) DEFAULT NULL,
  `request_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_uem_analyze` */

DROP TABLE IF EXISTS `tbl_uem_analyze`;

CREATE TABLE `tbl_uem_analyze` (
  `id` varchar(50) DEFAULT NULL,
  `pv` varchar(50) DEFAULT NULL,
  `uv` varchar(50) DEFAULT NULL,
  `page_url` varchar(200) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `id` varchar(250) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户ID',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `status` varchar(10) DEFAULT NULL COMMENT '状态 PROHIBIT：禁用   NORMAL：正常',
  `account` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `user_icon_path` varchar(500) DEFAULT NULL,
  `nick_name` varchar(100) DEFAULT NULL,
  UNIQUE KEY `username` (`user_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统用户表';

/*Table structure for table `tbl_user_role` */

DROP TABLE IF EXISTS `tbl_user_role`;

CREATE TABLE `tbl_user_role` (
  `id` varchar(50) DEFAULT NULL COMMENT 'ID',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户ID',
  `role_id` varchar(50) DEFAULT NULL COMMENT '角色ID',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户与角色关系表';

/*操作日志配置*/
DROP TABLE IF EXISTS `tbl_operate_log_config`;
CREATE TABLE `tbl_operate_log_config` (
  `id` varchar(50) DEFAULT NULL,
  `record_level` varchar(10) DEFAULT NULL COMMENT '日志级别',
  `record_enable` tinyint(1) DEFAULT NULL COMMENT '手否开启日志记录'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert  into `tbl_operate_log_config`(`id`,`record_level`,`record_enable`) values ('log_b6296c36_da74_48b1_b332_8610191f7e69','warning','disable');

