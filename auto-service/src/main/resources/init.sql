/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.5.28 : Database - mmldb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mmldb` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `mmldb`;

/*Table structure for table `jmeter_generate_task` */

DROP TABLE IF EXISTS `jmeter_generate_task`;

CREATE TABLE `jmeter_generate_task` (
  `id` varchar(50) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `file_name` varchar(250) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `jmeter_task` */

DROP TABLE IF EXISTS `jmeter_task`;

CREATE TABLE `jmeter_task` (
  `id` varchar(50) DEFAULT NULL,
  `task_name` varchar(100) DEFAULT NULL,
  `task_status` varchar(10) DEFAULT NULL,
  `task_progress` varchar(10) DEFAULT NULL,
  `task_script_id` varchar(250) DEFAULT NULL,
  `task_script_name` text,
  `task_execute_details` text,
  `remark` varchar(250) DEFAULT NULL,
  `task_start_time` timestamp NULL DEFAULT NULL,
  `task_end_time` timestamp NULL DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_aduit_log` */

DROP TABLE IF EXISTS `tbl_aduit_log`;

CREATE TABLE `tbl_aduit_log` (
  `id` varchar(50) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `user_ip` varchar(50) DEFAULT NULL,
  `finger_print` varchar(100) DEFAULT NULL,
  `request_moudle` varchar(50) DEFAULT NULL,
  `request_clazz` varchar(50) DEFAULT NULL,
  `request_method` varchar(50) DEFAULT NULL,
  `request_type` varchar(50) DEFAULT NULL,
  `request_params` text,
  `request_consume` varchar(50) DEFAULT NULL,
  `request_path` text,
  `request_reault` varchar(200) DEFAULT NULL,
  `request_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Table structure for table `tbl_dictionary` */

DROP TABLE IF EXISTS `tbl_dictionary`;

CREATE TABLE `tbl_dictionary` (
  `id` varchar(50) DEFAULT NULL,
  `dictionary_key` varchar(250) DEFAULT NULL,
  `dictionary_value` text,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_file_analyze` */

DROP TABLE IF EXISTS `tbl_file_analyze`;

CREATE TABLE `tbl_file_analyze` (
  `id` varchar(50) DEFAULT NULL,
  `operate_type` varchar(50) DEFAULT NULL,
  `file_type` varchar(100) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_file_browser` */

DROP TABLE IF EXISTS `tbl_file_browser`;

CREATE TABLE `tbl_file_browser` (
  `id` varchar(50) DEFAULT NULL,
  `file_name` varchar(250) DEFAULT NULL,
  `file_type` varchar(50) DEFAULT NULL,
  `file_size` varchar(50) DEFAULT NULL,
  `bucket_code` varchar(250) DEFAULT NULL,
  `bucket_name` varchar(250) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_file_buckets` */

DROP TABLE IF EXISTS `tbl_file_buckets`;

CREATE TABLE `tbl_file_buckets` (
  `id` varchar(50) DEFAULT NULL,
  `buckets_name` varchar(250) DEFAULT NULL,
  `buckets_code` varchar(250) DEFAULT NULL,
  `file_num` varchar(50) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_file_catalog` */

DROP TABLE IF EXISTS `tbl_file_catalog`;

CREATE TABLE `tbl_file_catalog` (
  `id` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_file_records` */

DROP TABLE IF EXISTS `tbl_file_records`;

CREATE TABLE `tbl_file_records` (
  `id` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `size` varchar(50) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `file_path` varchar(100) DEFAULT NULL,
  `disk_path` text,
  `file_url` text,
  `catalog_code` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_jmeter_plan` */

DROP TABLE IF EXISTS `tbl_jmeter_plan`;

CREATE TABLE `tbl_jmeter_plan` (
  `id` varchar(50) DEFAULT NULL,
  `plan_name` varchar(250) DEFAULT NULL,
  `script_name` text,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_menu` */

DROP TABLE IF EXISTS `tbl_menu`;

CREATE TABLE `tbl_menu` (
  `id` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `permission` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_navigator` */

DROP TABLE IF EXISTS `tbl_navigator`;

CREATE TABLE `tbl_navigator` (
  `id` varchar(50) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `category_id` varchar(50) DEFAULT NULL,
  `link_href` text,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_navigator_category` */

DROP TABLE IF EXISTS `tbl_navigator_category`;

CREATE TABLE `tbl_navigator_category` (
  `id` varchar(50) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_notebook_article` */

DROP TABLE IF EXISTS `tbl_notebook_article`;

CREATE TABLE `tbl_notebook_article` (
  `id` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `remark` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `details` longtext,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Table structure for table `tbl_notebook_todo` */

DROP TABLE IF EXISTS `tbl_notebook_todo`;

CREATE TABLE `tbl_notebook_todo` (
  `id` varchar(50) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `details` text,
  `status` varchar(50) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_schedule_task` */

DROP TABLE IF EXISTS `tbl_schedule_task`;

CREATE TABLE `tbl_schedule_task` (
  `id` varchar(50) DEFAULT NULL,
  `task_name` varchar(100) DEFAULT NULL,
  `task_cron` varchar(100) DEFAULT NULL,
  `task_status` varchar(100) DEFAULT NULL,
  `task_bean` varchar(100) DEFAULT NULL,
  `task_method` varchar(100) DEFAULT NULL,
  `task_params` text,
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_system_log` */

DROP TABLE IF EXISTS `tbl_system_log`;

CREATE TABLE `tbl_system_log` (
  `id` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_test_case_file` */

DROP TABLE IF EXISTS `tbl_test_case_file`;

CREATE TABLE `tbl_test_case_file` (
  `id` varchar(50) DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  `file_size` varchar(20) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `id` varchar(50) DEFAULT NULL,
  `account` varchar(50) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Table structure for table `tbl_user_role` */

DROP TABLE IF EXISTS `tbl_user_role`;

CREATE TABLE `tbl_user_role` (
  `id` varchar(50) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `role_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_viedo_catalog` */

DROP TABLE IF EXISTS `tbl_viedo_catalog`;

CREATE TABLE `tbl_viedo_catalog` (
  `id` varchar(50) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Table structure for table `tbl_viedo_file` */

DROP TABLE IF EXISTS `tbl_viedo_file`;

CREATE TABLE `tbl_viedo_file` (
  `id` varchar(50) DEFAULT NULL,
  `file_name` varchar(250) DEFAULT NULL,
  `file_type` varchar(50) DEFAULT NULL,
  `file_size` varchar(100) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `catalog_id` varchar(50) DEFAULT NULL,
  `catalog_name` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
