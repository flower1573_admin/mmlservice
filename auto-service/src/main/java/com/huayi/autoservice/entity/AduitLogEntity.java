package com.huayi.autoservice.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AduitLogEntity implements Serializable {
    private String id;

    private String userId;

    private String userIp;

    private String fingerPrint;

    private String requestMoudle;

    private String requestClazz;

    private String requestMethod;

    private String requestType;

    private String requestParams;

    private String requestConsume;

    private String requestPath;

    private String requestReault;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date requestTime;
}
