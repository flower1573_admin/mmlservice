package com.huayi.autoservice.entity;


import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserRoleEntity {
    private String id;

    private String userId;

    private String roleId;
}
