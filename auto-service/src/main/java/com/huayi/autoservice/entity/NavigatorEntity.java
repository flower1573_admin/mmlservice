package com.huayi.autoservice.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class NavigatorEntity {
    @ExcelIgnore
    String id;

    @ExcelProperty("名称")
    String name;

    @ExcelProperty("名称")
    String categoryId;

    @ExcelProperty("链接")
    String linkHref;

    @ExcelProperty("备注")
    String remark;

    NavigatorCategoryEntity navigatorCategoryEntity;

    @ExcelIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    Date createTime;

    @ExcelIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    Date updateTime;
}
