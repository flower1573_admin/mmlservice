package com.huayi.autoservice.entity;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MenuEntity {
    private String id;

    private String name;

    private String permission;
}
