package com.huayi.autoservice.entity;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RoleMenuEntity {
    private String id;

    private String roleId;

    private String menuId;
}
