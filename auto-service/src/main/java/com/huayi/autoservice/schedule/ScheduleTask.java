
package com.huayi.autoservice.schedule;

import java.util.Objects;
import java.util.concurrent.ScheduledFuture;

public class ScheduleTask {
    volatile ScheduledFuture<?> future;

    public void cancel() {
        ScheduledFuture<?> scheduledFuture = this.future;
        if (Objects.nonNull(scheduledFuture)) {
            scheduledFuture.cancel(true);
        }
    }
}
