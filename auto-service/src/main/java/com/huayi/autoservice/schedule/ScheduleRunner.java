package com.huayi.autoservice.schedule;

import com.huayi.autoservice.entity.ScheduleTaskEntity;
import com.huayi.autoservice.service.ScheduleTaskService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ScheduleRunner implements CommandLineRunner {
    private static Logger logger = LoggerFactory.getLogger(ScheduleRunner.class);

    @Resource
    private ScheduleRegistrar cronTaskRegistrar;

    @Resource
    ScheduleTaskService scheduleService;

    @Override
    public void run(String... args) {
        logger.info("start execute schedule task start");

        try {
            Map<String, Object> params = new HashMap<>();
            params.put("cronStatus", "enable");

            List<ScheduleTaskEntity> jobList = scheduleService.getScheduleTaskList(params);
            if (CollectionUtils.isNotEmpty(jobList)) {
                jobList.forEach(job -> {
                    ScheduleRunnable task = new ScheduleRunnable(job.getTaskBean(), job.getTaskMethod(), job.getTaskParams(), job.getId());
                    cronTaskRegistrar.addCronTask(task, job.getTaskCron());
                });
                logger.info("start execute schedule task success");
            }
        } catch (Exception exception) {
            logger.error("execute schedule task error" + exception.getMessage());
        }
    }
}
