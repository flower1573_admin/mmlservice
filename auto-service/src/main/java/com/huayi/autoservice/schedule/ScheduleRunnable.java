package com.huayi.autoservice.schedule;

import com.huayi.autoservice.utils.SpringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.Objects;

public class ScheduleRunnable implements Runnable  {
    private final String beanName;

    private final String methodName;

    private final String params;

    private final String id;

    public ScheduleRunnable(String beanName, String methodName, String params, String id) {
        this.beanName = beanName;
        this.methodName = methodName;
        this.params = params;
        this.id = id;
    }

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();
        try {
            Object target = SpringUtils.getBean(beanName);

            Method method;
            if (StringUtils.isNotEmpty(params)) {
                method = target.getClass().getDeclaredMethod(methodName, String.class);
            } else {
                method = target.getClass().getDeclaredMethod(methodName);
            }

            ReflectionUtils.makeAccessible(method);
            if (StringUtils.isNotEmpty(params)) {
                method.invoke(target, params);
            } else {
                method.invoke(target);
            }
        } catch (Exception ex) {
        }
        long times = System.currentTimeMillis() - startTime;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (Objects.isNull(obj) || getClass() != obj.getClass()) {
            return false;
        }

        ScheduleRunnable that = (ScheduleRunnable) obj;
        if (Objects.isNull(params)) {
            return beanName.equals(that.beanName) &&
                    methodName.equals(that.methodName) &&
                    that.params == null;
        }

        return beanName.equals(that.beanName) &&
                methodName.equals(that.methodName) &&
                params.equals(that.params) &&
                id.equals(that.id);
    }

    @Override
    public int hashCode() {
        if (Objects.isNull(params)) {
            return Objects.hash(beanName, methodName, id);
        }
        return Objects.hash(beanName, methodName, params, id);
    }
}

