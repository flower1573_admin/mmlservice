package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.ViedoFileEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ViedoFileMapper {
    List<ViedoFileEntity> getDataList(Map<String, Object> params);

    ViedoFileEntity getDataById(@Param("id") String id);

    int getDataTotal(Map<String, Object> params);

    void createData(Map<String, Object> params);

    void deleteData(Map<String, Object> params);
}
