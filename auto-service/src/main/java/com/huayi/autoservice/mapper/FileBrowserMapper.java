package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.FileBucketsEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface FileBrowserMapper {
    List<FileBucketsEntity> getDataList(Map<String, Object> params);

    FileBucketsEntity getDataByParams(Map<String, Object> params);

    int getDataTotal(Map<String, Object> params);

    void createData(Map<String, Object> params);

    void updateData(Map<String, Object> params);

    void deleteData(Map<String, Object> params);
}
