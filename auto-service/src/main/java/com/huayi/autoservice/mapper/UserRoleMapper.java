package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.UserEntity;
import com.huayi.autoservice.entity.UserRoleEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserRoleMapper {
    int createUserRole(List<UserRoleEntity> userRoleList);
}
