package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.ScheduleTaskEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ScheduleTaskMapper {
    int getScheduleTaskCount(Map<String, Object> params);

    List<ScheduleTaskEntity> getScheduleTaskList(Map<String, Object> params);

    ScheduleTaskEntity getScheduleTaskSchedule(ScheduleTaskEntity scheduleEntity);

    int createScheduleTask(ScheduleTaskEntity scheduleEntity);

    int updateScheduleTask(ScheduleTaskEntity scheduleEntity);

    int deleteScheduleTask(List<String> params);
}
