package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.FileBucketsEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface FileBucketsMapper {
    List<FileBucketsEntity> getDataList(Map<String, Object> params);

    List<FileBucketsEntity> getAllDataList(Map<String, Object> params);

    FileBucketsEntity getDataByParams(Map<String, Object> params);

    int getDataTotal(Map<String, Object> params);

    void createData(Map<String, Object> params);

    void updateData(Map<String, Object> params);

    void deleteData(Map<String, Object> params);
}
