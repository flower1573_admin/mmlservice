package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.TestCaseFileEntity;
import com.huayi.autoservice.entity.ViedoCatalogEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ViedoCatalogMapper {
    List<ViedoCatalogEntity> getDataList(Map<String, Object> params);

    List<ViedoCatalogEntity> getAllDataList(Map<String, Object> params);

    int getDataTotal(Map<String, Object> params);

    void createData(Map<String, Object> params);

    void updateData(Map<String, Object> params);

    void deleteData(Map<String, Object> params);
}
