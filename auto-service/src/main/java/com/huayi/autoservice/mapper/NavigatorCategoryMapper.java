package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.NavigatorCategoryEntity;
import com.huayi.autoservice.entity.NavigatorEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface NavigatorCategoryMapper {
    List<NavigatorCategoryEntity> getNavigatorList(Map<String, Object> queryParams);

    List<NavigatorCategoryEntity> getAllNavigatorList(Map<String, Object> queryParams);

    int getNavigatorTotal(Map<String, Object> queryParams);

    int createNavigator(Map<String, Object> queryParams);

    int deleteNavigator(Map<String, Object> queryParams);

    int updateNavigator(Map<String, Object> queryParams);
}
