package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.FileAnalyzeEntity;
import com.huayi.autoservice.entity.FileBucketsEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FileAnalyzeMapper {
    List<FileBucketsEntity> getBucketsList();

    List<FileAnalyzeEntity> getUploadRecords();

    List<FileAnalyzeEntity> getDownloadRecords();

    List<FileAnalyzeEntity> getFileTypeList();
}
