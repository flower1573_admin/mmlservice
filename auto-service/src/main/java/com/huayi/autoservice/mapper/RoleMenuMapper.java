package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.RoleMenuEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleMenuMapper {
    int createRoleMenu(List<RoleMenuEntity> roleMenuEntityList);
}
