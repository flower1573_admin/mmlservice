package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.JmeterGenerateTaskEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface JmeterGenerateTaskMapper {
    List<JmeterGenerateTaskEntity> getTaskList(Map<String, Object> queryParams);

    int getTaskTotal(Map<String, Object> queryParams);

    int createTask(Map<String, Object> queryParams);

    int deleteTask(Map<String, Object> queryParams);

    int updateTask(Map<String, Object> queryParams);
}
