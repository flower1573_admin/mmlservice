package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.NoteBookArticleEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface NoteBookArticleMapper {
    List<NoteBookArticleEntity> getDataList(Map<String, Object> params);

    NoteBookArticleEntity getDataDetails(@Param("id") String id);

    int getDataTotal(Map<String, Object> params);

    void createData(Map<String, Object> params);

    void deleteData(Map<String, Object> params);

    void updateData(Map<String, Object> params);
}
