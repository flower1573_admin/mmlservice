package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.NavigatorEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface NavigatorMapper {
    List<NavigatorEntity> getNavigatorList(Map<String, Object> queryParams);

    int getNavigatorTotal(Map<String, Object> queryParams);

    int createNavigator(Map<String, Object> queryParams);

    int deleteNavigator(Map<String, Object> queryParams);

    int updateNavigator(Map<String, Object> queryParams);

    List<NavigatorEntity> getAllNavigatorList(Map<String, Object> queryParams);
}
