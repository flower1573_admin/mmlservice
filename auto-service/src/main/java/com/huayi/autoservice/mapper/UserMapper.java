package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Description 系统用户DAO
 * @Author Sans
 * @CreateTime 2019/9/14 15:57
 */
@Mapper
public interface UserMapper{
    UserEntity getUser(UserEntity userEntity);

    int getUserCount(Map<String, String> params);

    List<UserEntity> getUserList(Map<String, String> params);

    int registerUser(UserEntity userEntity);

    int createUser(UserEntity userEntity);

    int updateUser(UserEntity userEntity);

    int deleteUser(List<String> idList);
}
