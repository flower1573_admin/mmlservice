package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.FileRecordsEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface FileRecordsMapper {
    List<FileRecordsEntity> getFileRecordsList(Map<String, Object> params);

    List<FileRecordsEntity> getAllFileRecordsList(Map<String, Object> params);

    int getFileRecordsCount(Map<String, Object> params);

    void createFileRecords(FileRecordsEntity fileRecordsEntity);

    void deleteFileRecords(Map<String, Object> params);

    void deleteByCatalogCode(Map<String, Object> params);
}
