package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.RoleEntity;
import com.huayi.autoservice.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Description 角色DAO
 * @Author Sans
 * @CreateTime 2019/9/14 15:57
 */
@Mapper
public interface RoleMapper {
    List<RoleEntity> getRoleListByUser(UserEntity userRoleInfo);

    List<RoleEntity> getAllRoleList(Map<String, String> params);

    int getRoleCount(Map<String, String> params);

    List<RoleEntity> getRoleList(Map<String, String> params);

    RoleEntity getRole(RoleEntity roleEntity);

    int createRole(RoleEntity roleEntity);

    int updateRole(RoleEntity roleEntity);

    int deleteRole(List<String> idList);
}