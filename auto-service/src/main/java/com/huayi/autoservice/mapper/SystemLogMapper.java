package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.SystemLogEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface  SystemLogMapper {
    List<SystemLogEntity> getDataList(Map<String, Object> params);

    int getDataTotal(Map<String, Object> params);

    void createData(Map<String, Object> params);
}
