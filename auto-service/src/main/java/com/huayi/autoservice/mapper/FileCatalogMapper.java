package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.FileCatalogEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface FileCatalogMapper {
    List<FileCatalogEntity> getFileCatalogList(Map<String, Object> params);

    List<FileCatalogEntity> getAllFileCatalogList(Map<String, Object> params);

    void createFileCatalog(FileCatalogEntity fileCatalogEntity);

    void updateCatalog(FileCatalogEntity fileCatalogEntity);

    void deleteFileCatalog(Map<String, Object> params);
}
