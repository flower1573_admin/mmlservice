package com.huayi.autoservice.mapper;

import com.huayi.autoservice.entity.AduitLogEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AduitLogMapper {
    Integer getAduitLogCount(Map<String, Object> params);

    List<AduitLogEntity> getAduitLogList(Map<String, Object> params);

    void createAduitLog(AduitLogEntity aduitLogEntity);

    void deleteAduitLog(@Param("count") Integer count);
}