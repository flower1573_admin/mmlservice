package com.huayi.autoservice.mapper;


import com.huayi.autoservice.entity.MenuEntity;
import com.huayi.autoservice.entity.RoleEntity;
import com.huayi.autoservice.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 权限DAO
 * @Author Sans
 * @CreateTime 2019/9/14 15:57
 */
@Mapper
public interface MenuMapper {
    List<MenuEntity> getMenuListByUser(UserEntity userEntity);

    List<MenuEntity> getAllMenuList();

    List<MenuEntity> getMenuListByRole(RoleEntity roleEntity);
}