package com.huayi.autoservice.enmus;

/**
 * @author: jiangjs
 * @description:
 * @date: 2023/10/20 10:51
 **/
public enum MinioBucketEnum {

    /**
     * email
     */
    EMAIL("email");

    private final String bucket;

    MinioBucketEnum(String bucket){
        this.bucket = bucket;
    }

    public MinioBucketEnum getMinioBucket(String bucket){
        return MinioBucketEnum.valueOf(bucket);
    }

    public String getBucket(){
        return bucket;
    }
}
