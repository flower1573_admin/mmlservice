package com.huayi.autoservice.utils;

import org.springframework.http.MediaType;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class TimeUtils {
    public static Date convertZonedDateTimeToDate(ZonedDateTime  zonedDateTime) {
        ZoneId zoneId= ZoneId.systemDefault();
        ZonedDateTime localZonedDateTime = zonedDateTime.withZoneSameInstant(zoneId);
        Date date = Date.from(localZonedDateTime.toInstant());
        return date;
    }
}
