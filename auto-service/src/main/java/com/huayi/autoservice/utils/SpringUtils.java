package com.huayi.autoservice.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringUtils implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (SpringUtils.applicationContext == null) {
            SpringUtils.applicationContext = applicationContext;
        }
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static Object getBean(String name) {
        Object service = null;
        service = getApplicationContext().getBean(name);
        return service;
    }

    public static <T> T getBean(Class<T> clazz) {
        T service = null;
        try {
            service = getApplicationContext().getBean(clazz);
        } catch (BeansException beansException) {
            throw beansException;
        }
        return service;
    }

    public static <T> T getBean(String name, Class<T> clazz) {
        T service = null;
        service = getApplicationContext().getBean(name, clazz);
        return service;
    }
}
