package com.huayi.autoservice.utils;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class TransactionManagerBuilder {
    private PlatformTransactionManager transactionManager;

    public static TransactionManagerBuilder getTransactionContext() {
        return new TransactionManagerBuilder();
    }


    public TransactionManagerBuilder() {
        transactionManager = SpringUtils.getBean(PlatformTransactionManager.class);
    }

    public TransactionStatus createTransactionStatus(String name) {
        DefaultTransactionDefinition defaultTransaction = new DefaultTransactionDefinition();
        defaultTransaction.setName(name);
        defaultTransaction.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus transactionStatus = transactionManager.getTransaction(defaultTransaction);
        return transactionStatus;
    }

    public void commit(TransactionStatus transactionStatus) {
        if (transactionStatus != null) {
            transactionManager.commit(transactionStatus);
        }
    }

    public void rollback(TransactionStatus transactionStatus) {
        if (transactionStatus != null) {
            transactionManager.rollback(transactionStatus);
        }
    }
}
