package com.huayi.autoservice.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class CommonUtils {
    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "_");
    }

    public static String getTimeString() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String currentTime = simpleDateFormat.format(date);
        return currentTime;
    }

    public static String getUUID(String prefix) {
        String UUIDString = UUID.randomUUID().toString().replaceAll("-", "_");
        return prefix + "_" +UUIDString;
    }

    public static <T> List<T> castObjectToList(Object obj, Class<T> clazz) {
        List<T> result = new ArrayList<T>();
        if (obj instanceof List<?>) {
            for (Object o : (List<?>) obj) {
                result.add(clazz.cast(o));
            }
            return result;
        }
        return null;
    }
}
