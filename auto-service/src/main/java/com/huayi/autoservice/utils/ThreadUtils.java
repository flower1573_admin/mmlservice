package com.huayi.autoservice.utils;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ThreadUtils {
    public static Executor getAsyncExecutor() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                5,
                15,
                30,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(10),
                new BasicThreadFactory.Builder().namingPattern("async-pool").build()
        );
        threadPoolExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return threadPoolExecutor;
    }

    public static <V> List<Future<V>> asyncExecut(List<Callable<V>> callableList) {
        try {
            List<Future<V>> futureList = new ArrayList<>();
            if (CollectionUtils.isEmpty(callableList)) {
                for (Callable<V> callable : callableList) {
                    futureList.add(ThreadPoolUtils.submit(callable));
                }
            }
            return futureList;
        } catch (Exception exception) {
            throw exception;
        }
    }
}
