package com.huayi.autoservice.utils;

import net.sf.ehcache.util.NamedThreadFactory;

import java.util.concurrent.*;

public class ThreadPoolUtils {
    public static Integer cpuNumber = Runtime.getRuntime().availableProcessors();
    public static Integer coreSize = cpuNumber * 25;
    public static Integer threadNumber = cpuNumber * 125 + 1;
    public static ThreadPoolExecutor threadPool;

    public static void execute(Runnable runnable) {
        getThreadPool().execute(runnable);
    }

    public static <T> Future<T> submit(Callable<T> callable) {
        return getThreadPool().submit(callable);
    }

    public static ThreadPoolExecutor getThreadPool() {
        if (threadPool == null) {
            synchronized (ThreadPoolUtils.class) {
                if (threadPool == null) {
                    threadPool = new ThreadPoolExecutor(
                            coreSize,
                            threadNumber,
                            1,
                            TimeUnit.MINUTES,
                            new LinkedBlockingDeque<>(Integer.MAX_VALUE),
                            new NamedThreadFactory("portal_thread_pool_"),
                            new ThreadPoolExecutor.CallerRunsPolicy());
                }
            }
            return threadPool;
        } else {
            return threadPool;
        }
    }
}
