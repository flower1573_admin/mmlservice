package com.huayi.autoservice.utils;

import org.hyperic.sigar.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

public class SystemInfoUtils {
    private static Sigar sigarInstance = null;


    static {
        sigarInstance = new Sigar();
    }

    public static Map<String, String> getInetInfo() throws UnknownHostException {
        Map<String, String> resMap = new HashMap<>();

        Map<String, String> systemEnv = System.getenv();

        InetAddress inetAddress = InetAddress.getLocalHost();

        String hostName = inetAddress.getHostName();
        String userName = systemEnv.get("USERNAME");
        String computerName = systemEnv.get("COMPUTERNAME");
        String userDomain = systemEnv.get("USERDOMAIN");

        resMap.put("hostName", hostName);
        resMap.put("userName", userName);
        resMap.put("computerName", computerName);
        resMap.put("userDomain", userDomain);
        return resMap;
    }

    public static Map<String, String> getMemoryUsage() throws SigarException {
        Sigar sigar = sigarInstance;
        Mem mem = sigar.getMem();
        long memTotal = mem.getTotal();
        long memUsed = mem.getUsed();
        long memFree = mem.getFree();
        Map<String, String> cpuMap = new HashMap<>();
        cpuMap.put("memTotal", String.valueOf(memTotal));
        cpuMap.put("memUsed", String.valueOf(memUsed));
        cpuMap.put("memFree", String.valueOf(memFree));
        return cpuMap;
    }

    public static List<Map<String, String>> getCpuUsage() throws SigarException {
        Sigar sigar = sigarInstance;
        CpuInfo[] cpuInfoList = sigar.getCpuInfoList();
        CpuPerc[] cpuPercList = sigar.getCpuPercList();
        List<Map<String, String>> cpuList = new ArrayList<>();
        for (int i = 0; i < cpuInfoList.length; i++) {
            CpuInfo info = cpuInfoList[i];
            CpuPerc cpuPerc = cpuPercList[i];
            Map<String, String> cpuItem = new HashMap<>();
            cpuItem.put("size", String.valueOf(info.getMhz()));
            cpuItem.put("vendor", info.getVendor());
            cpuItem.put("model", info.getModel());
            cpuItem.put("cacheSize", String.valueOf(info.getCacheSize()));
            cpuItem.put("cpuUserPerc", CpuPerc.format(cpuPerc.getUser()));
            cpuItem.put("cpuSysPerc", CpuPerc.format(cpuPerc.getSys()));
            cpuItem.put("cpuWaitPerc", CpuPerc.format(cpuPerc.getWait()));
            cpuItem.put("cpuNicePerc", CpuPerc.format(cpuPerc.getNice()));
            cpuItem.put("cpuIdlePerc", CpuPerc.format(cpuPerc.getIdle()));
            cpuItem.put("cpuCombinedPerc", CpuPerc.format(cpuPerc.getCombined()));
            cpuList.add(cpuItem);
        }
        return cpuList;
    }

    public static Map<String, String> getOsInfos() throws SigarException {
        OperatingSystem operatingSystem = OperatingSystem.getInstance();
        Map<String, String> osMap = new HashMap<>();
        osMap.put("arch", operatingSystem.getArch());
        osMap.put("dataModel", operatingSystem.getDataModel());
        osMap.put("description", operatingSystem.getDescription());
        osMap.put("vendorName", operatingSystem.getVendorName());
        osMap.put("vendorVersion", operatingSystem.getVendorVersion());
        osMap.put("version", operatingSystem.getVersion());
        return osMap;
    }

    public static List<Map<String, String>> getDiskUsage() throws SigarException {
        Sigar sigar = sigarInstance;
        FileSystem[] fileSystemList = sigar.getFileSystemList();

        List<Map<String, String>> diskList = new ArrayList<>();

        for (int i = 0; i < fileSystemList.length; i++) {
            FileSystem fileSystem = fileSystemList[i];
            Map<String, String> diskItem = new HashMap<>();
            diskItem.put("devName", fileSystem.getDevName());
            diskItem.put("dirName", fileSystem.getDirName());
            diskItem.put("flags", String.valueOf(fileSystem.getFlags()));
            diskItem.put("sysTypeName", fileSystem.getSysTypeName());
            diskItem.put("typeName", fileSystem.getTypeName());
            diskItem.put("type", String.valueOf(fileSystem.getType()));

            FileSystemUsage usage = sigar.getFileSystemUsage(fileSystem.getDirName());

            switch (fileSystem.getType()) {
                case 2:
                    double usePercent = usage.getUsePercent() * 100D;
                    diskItem.put("useageTotal", String.valueOf(usage.getTotal()));
                    diskItem.put("useageFree", String.valueOf(usage.getFree()));
                    diskItem.put("useageAvail", String.valueOf(usage.getAvail()));
                    diskItem.put("useageUsed", String.valueOf(usage.getUsed()));
                    diskItem.put("useageUsedPerc", String.valueOf(usage.getUsed()) + "%");
                    break;
            }
            diskList.add(diskItem);
        }
        return diskList;
    }

    public static List<Map<String, String>> getPsList() throws SigarException {
        Sigar sigar = sigarInstance;
        List<Map<String, String>> psList = new ArrayList<>();

        long[] pidArray = org.hyperic.sigar.cmd.Shell.getPids(sigar, new String[]{});
        for (int i = 0; i < pidArray.length; i++) {
            Map<String, String> psItem = new HashMap<>();

            long pid = pidArray[i];
            ProcCpu cpu = sigar.getProcCpu(pid);

            psItem.put("pid", String.valueOf(pid));
            psItem.put("cpuPerc", CpuPerc.format(cpu.getPercent()));
            psList.add(psItem);
        }
        return psList;
    }

    public static List<Map<String, String>> getEthernet() throws SigarException {
        Sigar sigar = sigarInstance;
        List<Map<String, String>> psList = new ArrayList<>();

        String[] interfaceList = sigar.getNetInterfaceList();


        for (int i = 0; i < interfaceList.length; i++) {
            Map<String, String> psItem = new HashMap<>();

            NetInterfaceConfig interfaceConfig = sigar.getNetInterfaceConfig(interfaceList[i]);

            if (NetFlags.LOOPBACK_ADDRESS.equals(interfaceConfig.getAddress())
                    || (interfaceConfig.getFlags() & NetFlags.IFF_LOOPBACK) != 0
                    || NetFlags.NULL_HWADDR.equals(interfaceConfig.getHwaddr())) {
                continue;
            }

            psItem.put("address", interfaceConfig.getAddress());
            psItem.put("broadcast", interfaceConfig.getBroadcast());
            psItem.put("hwaddr", interfaceConfig.getHwaddr());
            psItem.put("netmask", interfaceConfig.getNetmask());
            psItem.put("description", interfaceConfig.getDescription());
            psItem.put("type", interfaceConfig.getType());
            psList.add(psItem);
        }
        return psList;
    }
}
