package com.huayi.autoservice.utils;

import com.alibaba.fastjson.JSON;
import com.huayi.autoservice.configuration.JWTConfig;
import com.huayi.autoservice.entity.UserEntity;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

@Component
public class JWTTokenUtil implements Serializable {
    public static String createAccessToken(UserEntity userEntity) {
        // 登陆成功生成JWT
        String token = Jwts.builder()
                // 放入用户名和用户ID
                .setId(userEntity.getId())
                // 主题
                .setSubject(userEntity.getUsername())
                // 签发时间
                .setIssuedAt(new Date())
                // 签发者
                .setIssuer("sans")
                // 自定义属性 放入用户拥有权限
                .claim("authorities", JSON.toJSONString(userEntity.getAuthorities()))
                // 失效时间
                .setExpiration(new Date(System.currentTimeMillis() + JWTConfig.expiration))
                // 签名算法和密钥
                .signWith(SignatureAlgorithm.HS512, JWTConfig.secret)
                .compact();
        return token;
    }

    public static boolean isTokenExpired(Claims claims){
        return claims.getExpiration().before(new Date());
    }
}
