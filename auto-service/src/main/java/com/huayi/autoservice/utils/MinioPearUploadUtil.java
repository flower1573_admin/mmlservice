package com.huayi.autoservice.utils;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.huayi.autoservice.configuration.PearMinioClient;
import com.huayi.autoservice.enmus.MinioBucketEnum;
import com.huayi.autoservice.vo.MinioPearVo;
import io.minio.*;
import io.minio.http.Method;
import io.minio.messages.Bucket;
import io.minio.messages.DeleteObject;
import io.minio.messages.Item;
import io.minio.messages.Part;
import lombok.SneakyThrows;
import okhttp3.HttpUrl;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
public class MinioPearUploadUtil {
    @Resource
    private PearMinioClient pearMinioClient;

    /**
     * 校验当前bucket是否存在，不存在则创建
     *
     * @param bucketName 桶
     */
    private void existBucket(String bucketName) {
        try {
            boolean isExist = pearMinioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
            if (!isExist) {
                pearMinioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建分片上传信息
     *
     * @param chunkNum    分片数量
     * @param fileName    文件名称
     * @param contentType 文件类型
     * @param bucketEnum  桶
     * @return 返回分片信息
     */
    @SneakyThrows
    public MinioPearVo createMultipartUploadUrl(Integer chunkNum, String fileName, String contentType, MinioBucketEnum bucketEnum) {
        //设置分片文件类型
        Multimap<String, String> headerMap = HashMultimap.create();
        headerMap.put("Content-Type", contentType);
        CreateMultipartUploadResponse uploadResponse = pearMinioClient.createMultipartUpload(bucketEnum.getBucket(), null, fileName,
                headerMap, null);
        Map<String, String> reqParams = new HashMap<>(2);
        reqParams.put("uploadId", uploadResponse.result().uploadId());
        MinioPearVo pearVo = new MinioPearVo();
        pearVo.setUploadId(uploadResponse.result().uploadId());
        List<MinioPearVo.PearUploadData> uploads = new ArrayList<>();
        for (int i = 1; i <= chunkNum; i++) {
            reqParams.put("partNumber", String.valueOf(i));
            String objectUrl = pearMinioClient.getPresignedObjectUrl(
                    GetPresignedObjectUrlArgs.builder()
                            .method(Method.PUT)
                            .bucket(MinioBucketEnum.EMAIL.getBucket())
                            .object(fileName)
                            .expiry(1, TimeUnit.DAYS)
                            .extraQueryParams(reqParams)
                            .build());
            MinioPearVo.PearUploadData uploadData = new MinioPearVo.PearUploadData();
            uploadData.setUploadUrl(objectUrl).setParkNum(i);
            uploads.add(uploadData);
        }
        pearVo.setParts(uploads);
        return pearVo;
    }

    /**
     * 合并文件分片
     *
     * @param chunkNum    分片数量
     * @param fileName    文件名称
     * @param contentType 文件类型
     * @param uploadId    分片上传时的Id
     * @param bucketEnum  桶
     * @return 合并结果
     */
    @SneakyThrows
    public Boolean completeMultipart(Integer chunkNum, String fileName, String contentType, String uploadId, MinioBucketEnum bucketEnum) {
        Multimap<String, String> headerMap = HashMultimap.create();
        headerMap.put("Content-Type", contentType);

        ListPartsResponse listMultipart = pearMinioClient.listMultipart(bucketEnum.getBucket(), null, fileName, chunkNum + 10,
                0, uploadId, headerMap, null);
        if (Objects.nonNull(listMultipart)) {
            Part[] parts = new Part[chunkNum + 10];
            int partNum = 1;
            for (Part part : listMultipart.result().partList()) {
                parts[partNum] = new Part(partNum, part.etag());
                partNum++;
            }
            pearMinioClient.completeMultipartUpload(MinioBucketEnum.EMAIL.getBucket(), null, fileName,
                    uploadId, parts, headerMap, null);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * 文件流上传
     *
     * @param ism        文件流
     * @param bucketName 桶名称
     * @param fileName   文件名称
     * @return 执行结果
     */
    public Boolean upLoadInputStream(InputStream ism, String bucketName, String fileName) {
        try {
            existBucket(bucketName);
            pearMinioClient.putObject(PutObjectArgs.builder()
                    .bucket(bucketName)
                    .object(fileName)
                    .stream(ism, ism.available(), -1)
                    .build());
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }

    /**
     * 下载文件
     *
     * @param fileName   文件名称
     * @param bucketName 桶名称
     * @return 文件流
     */
    public InputStream downLoadFile(String fileName, String bucketName) {
        InputStream ism = null;
        try {
            ism = pearMinioClient.getObject(GetObjectArgs.builder()
                    .bucket(bucketName)
                    .object(fileName).build());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ism;
    }

    /**
     * 获取文件链接
     *
     * @param bucketName
     * @param objectName
     * @return
     */
    @SneakyThrows(Exception.class)
    public String getPresignedObjectUrl(String bucketName, String objectName) {
        GetPresignedObjectUrlArgs args = GetPresignedObjectUrlArgs.builder()
                .bucket(bucketName)
                .object(objectName)
                .method(Method.GET)
                .build();
        return pearMinioClient.getPresignedObjectUrl(args);
    }

    /**
     * 创建桶
     *
     * @param bucketName
     * @return
     */
    @SneakyThrows(Exception.class)
    public void createBucket(String bucketName) {
        MakeBucketArgs makeBucketArgs = MakeBucketArgs.builder().bucket(bucketName).build();
        pearMinioClient.makeBucket(makeBucketArgs);
    }

    /**
     * 删除痛
     *
     * @param bucketName
     * @throws IOException
     */
    @SneakyThrows(Exception.class)
    public void deleteBucket(String bucketName) {
        pearMinioClient.removeBucket(RemoveBucketArgs.builder().bucket(bucketName).build());
    }

    /**
     * 删除痛
     *
     * @param bucketName
     * @throws IOException
     */
    @SneakyThrows(Exception.class)
    public Optional<Bucket> getBucketInfos(String bucketName) {
        List<Bucket> buckets = pearMinioClient.listBuckets();
        Optional<Bucket> bucket = buckets.stream().filter(b -> b.name().equals(bucketName)).findFirst();
        return bucket;
    }

    /**
     * 获得所有Bucket列表
     */
    @SneakyThrows(Exception.class)
    public List<Bucket> getAllBuckets() {
        List<Bucket> buckets = pearMinioClient.listBuckets();
        return buckets;
    }

    /**
     * 判断文件是否存在
     *
     * @param bucketName
     * @param objectName
     * @return
     */
    public boolean isObjectExist(String bucketName, String objectName) {
        boolean exist = true;
        try {
            pearMinioClient.statObject(StatObjectArgs.builder().bucket(bucketName).object(objectName).build());
        } catch (Exception e) {
            exist = false;
        }
        return exist;
    }

    /**
     * 判断文件夹是否存在
     *
     * @param bucketName
     * @param objectName
     * @return
     */
    public boolean isFolderExist(String bucketName, String objectName) {
        boolean exist = false;
        try {
            Iterable<Result<Item>> results = pearMinioClient.listObjects(ListObjectsArgs.builder().bucket(bucketName).prefix(objectName).recursive(false).build());
            for (Result<Item> result : results) {
                Item item = result.get();
                if (item.isDir() && objectName.equals(item.objectName())) {
                    exist = true;
                }
            }
        } catch (Exception e) {
            exist = false;
        }
        return exist;
    }

    /**
     * 根据文件前置查询文件
     *
     * @param bucketName 存储桶
     * @param prefix     前缀
     * @param recursive  是否使用递归查询
     * @return MinioItem 列表
     */
    @SneakyThrows(Exception.class)
    public List<Item> getAllObjectsByPrefix(String bucketName, String prefix, boolean recursive) {
        List<Item> list = new ArrayList<>();
        Iterable<Result<Item>> objectsIterator = pearMinioClient.listObjects(ListObjectsArgs.builder().bucket(bucketName).prefix(prefix).recursive(recursive).build());
        if (objectsIterator != null) {
            for (Result<Item> o : objectsIterator) {
                Item item = o.get();
                list.add(item);
            }
        }
        return list;
    }

    /**
     * 获取路径下文件列表
     *
     * @param bucketName 存储桶
     * @param prefix     文件名称
     * @param recursive  是否递归查找，false：模拟文件夹结构查找
     * @return 二进制流
     */
    public Iterable<Result<Item>> listObjects(String bucketName, String prefix, boolean recursive) {
        return pearMinioClient.listObjects(ListObjectsArgs.builder().bucket(bucketName).prefix(prefix).recursive(recursive).build());
    }

    /**
     * 创建文件夹或目录
     *
     * @param bucketName 存储桶
     * @param objectName 目录路径
     * @return
     */
    @SneakyThrows(Exception.class)
    public ObjectWriteResponse createDir(String bucketName, String objectName) {
        return pearMinioClient.putObject(PutObjectArgs.builder().bucket(bucketName).object(objectName).stream(new ByteArrayInputStream(new byte[]{}), 0, -1).build());
    }

    /**
     * 获取文件信息, 如果抛出异常则说明文件不存在
     *
     * @param bucketName 存储桶
     * @param objectName 文件名称
     * @return
     */
    @SneakyThrows(Exception.class)
    public StatObjectResponse getFileStatusInfo(String bucketName, String objectName) {
        return pearMinioClient.statObject(StatObjectArgs.builder().bucket(bucketName).object(objectName).build());
    }

    /**
     * 拷贝文件
     *
     * @param bucketName    存储桶
     * @param objectName    文件名
     * @param srcBucketName 目标存储桶
     * @param srcObjectName 目标文件名
     */
    @SneakyThrows(Exception.class)
    public ObjectWriteResponse copyFile(String bucketName, String objectName, String srcBucketName, String srcObjectName) {
        return pearMinioClient.copyObject(CopyObjectArgs.builder().source(CopySource.builder().bucket(bucketName).object(objectName).build()).bucket(srcBucketName).object(srcObjectName).build());
    }

    /**
     * 删除文件
     *
     * @param bucketName 存储桶
     * @param objectName 文件名称
     */
    @SneakyThrows(Exception.class)
    public void removeFile(String bucketName, String objectName) {
        pearMinioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
    }

    /**
     * 批量删除文件
     *
     * @param bucketName 存储桶
     * @param keys       需要删除的文件列表
     * @return
     */
    public void batchRemoveFiles(String bucketName, List<String> keys) {
        List<DeleteObject> objects = new LinkedList<>();
        keys.forEach(item -> {
            objects.add(new DeleteObject(item));
            try {
                removeFile(bucketName, item);
            } catch (Exception e) {
            }
        });
    }
}
