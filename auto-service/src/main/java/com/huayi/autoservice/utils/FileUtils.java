package com.huayi.autoservice.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.Java2DFrameUtils;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileUtils {
    public static String formatFileSize(long fileZize) {
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        String fileSizeString = "0B";
        if (fileZize == 0) {
            return fileSizeString;
        }
        if (fileZize < 1024) {
            fileSizeString = decimalFormat.format((double) fileZize) + "B";
        } else if (fileZize < 1048576) {
            fileSizeString = decimalFormat.format((double) fileZize / 1024) + "KB";
        } else if (fileZize < 1073741824) {
            fileSizeString = decimalFormat.format((double) fileZize / 1048576) + "MB";
        } else {
            fileSizeString = decimalFormat.format((double) fileZize / 1073741824) + "GB";
        }
        return fileSizeString;
    }

    public static String getFileContentType(String suffix) {
        switch (suffix) {
            case "jpg":
            case "jpeg":
                return "image/jpeg";
            case "png":
                return "image/png";
            case "gif":
                return "image/gif";
            case "xml":
                return "application/xml";
            case "pdf":
                return "application/pdf";
            case "xls":
                return "application/vnd.ms-excel";
            case "xlsx":
                return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            case "doc":
                return "application/msword";
            case "docx":
                return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            case "ppt":
                return "application/vnd.ms-powerpoint";
            case "pptx":
                return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
            default:
                return "";
        }
    }


    public static String getFileMediaType(String fileExtension) {
        MediaType mediaType = MediaType.parseMediaType(MediaType.APPLICATION_OCTET_STREAM.getType() + "/" + fileExtension);
        return mediaType.toString();
    }

    public static File transferMultipartFileToFile(MultipartFile multipartFile) {
        File file = null;
        try {
            String originalFilename = multipartFile.getOriginalFilename();
            String[] filename = originalFilename.split("\\.");
            file=File.createTempFile(filename[0], "." + filename[1]);
            multipartFile.transferTo(file);
            file.deleteOnExit();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static File convertInputStreamToFile(InputStream in) throws IOException {
        String PREFIX = "stream2file";//前缀字符串定义文件名；必须至少三个字符
        String SUFFIX = ".tmp";//后缀字符串定义文件的扩展名；如果为null，则将使用后缀".tmp"

        File tempFile = File.createTempFile(PREFIX, SUFFIX);
        tempFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in, out);
        }
        return tempFile;
    }

    public static void zipFile(String sourceFilePath, String zipFilePath) {
        try {
            FileOutputStream fos = null;
            ZipOutputStream zos = null;
            try {
                fos = new FileOutputStream(zipFilePath);
                zos = new ZipOutputStream(fos);
                addFolderToZip("", new File(sourceFilePath), zos);
            } finally {
                if (zos != null) {
                    zos.close();
                }
                if (fos != null) {
                    fos.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addFolderToZip(String parentPath, File folder, ZipOutputStream zos) throws IOException {
        try {
            for (File file : folder.listFiles()) {
                if (file.isDirectory()) {
                    addFolderToZip(parentPath + folder.getName() + "/", file, zos);
                } else {
                    FileInputStream fis = null;
                    try {
                        fis = new FileInputStream(file);
                        ZipEntry zipEntry = new ZipEntry(parentPath + folder.getName() + "/" + file.getName());
                        zos.putNextEntry(zipEntry);

                        byte[] bytes = new byte[1024];
                        int length;
                        while ((length = fis.read(bytes)) >= 0) {
                            zos.write(bytes, 0, length);
                        }
                    } finally {
                        if (fis != null) {
                            fis.close();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除文件夹
     *
     * @param folderPath
     * @throws IOException
     */
    public static void deleteFolder(String folderPath) throws IOException {
        try {
            Path path = Paths.get(folderPath);
            if (Files.exists(path)) {
                Files.walk(path)
                        .sorted(Comparator.reverseOrder())
                        .map(Path::toFile)
                        .forEach(File::delete);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 递归获取文件夹下所有的文件
     *
     */
    public static void listLogFiles(String fileDirPath, String fileType, List<String> fileList) {
        File fileDir = new File(fileDirPath);
        File[] files = fileDir.listFiles();

        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    // 递归调用
                    listLogFiles(file.getPath(), fileType, fileList);
                } else if (file.getName().endsWith(fileType)) {
                    fileList.add(file.getPath());
                }
            }
        }
    }
}
