package com.huayi.autoservice.utils;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentPBEConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JasyptUtils {
    private static String jasyptKey;

    private static String jasyptAlgorithm;

    @Value("${jasypt.jasyptKey}")
    public void setJasyptKey(String key) {
        jasyptKey = key;
    }

    @Value("${jasypt.jasyptAlgorithm}")
    public void setJasyptAlgorithm(String algorithm) {
        jasyptAlgorithm = algorithm;
    }

    public static String encodeJasypt(String dataString) throws Exception {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        EnvironmentPBEConfig config = new EnvironmentPBEConfig();

        config.setAlgorithm(jasyptAlgorithm);
        config.setPassword(jasyptKey);
        standardPBEStringEncryptor.setConfig(config);
        String resultText = standardPBEStringEncryptor.encrypt(dataString);
        return resultText;
    }

    public static String decodeJasypt(String dataString) throws Exception {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        EnvironmentPBEConfig config = new EnvironmentPBEConfig();

        config.setAlgorithm(jasyptAlgorithm);
        config.setPassword(jasyptKey);
        standardPBEStringEncryptor.setConfig(config);
        String resultText = standardPBEStringEncryptor.decrypt(dataString);
        return resultText;
    }
}
