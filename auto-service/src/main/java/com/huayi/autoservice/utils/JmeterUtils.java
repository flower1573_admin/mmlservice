package com.huayi.autoservice.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.config.gui.ArgumentsPanel;
import org.apache.jmeter.control.LoopController;
import org.apache.jmeter.control.gui.LoopControlPanel;
import org.apache.jmeter.control.gui.TestPlanGui;
import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.protocol.http.control.Header;
import org.apache.jmeter.protocol.http.control.HeaderManager;
import org.apache.jmeter.protocol.http.control.gui.HttpTestSampleGui;
import org.apache.jmeter.protocol.http.gui.HeaderPanel;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerProxy;
import org.apache.jmeter.protocol.http.util.HTTPArgument;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.reporters.Summariser;
import org.apache.jmeter.samplers.SampleSaveConfiguration;
import org.apache.jmeter.save.SaveService;
import org.apache.jmeter.testbeans.gui.TestBeanGUI;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.testelement.TestPlan;
import org.apache.jmeter.testelement.property.*;
import org.apache.jmeter.threads.ThreadGroup;
import org.apache.jmeter.threads.gui.ThreadGroupGui;
import org.apache.jmeter.timers.ConstantThroughputTimer;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.collections.HashTree;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.*;

@Component
public class JmeterUtils {
    private static String jmeterHomePath;

    private static String jmeterTempPath;

    public static final String JMETER_ENCODING = "UTF-8";

    public static String getJmeterHomePath() {
        return jmeterHomePath;
    }

    public static String getJmeterTempPath() {
        return jmeterTempPath;
    }

    @Value("${jmeter.jmeterHomePath}")
    public void setJmeterHomePath(String jmeterHomePath) {
        JmeterUtils.jmeterHomePath = jmeterHomePath;
    }

    @Value("${jmeter.jmeterHomePath}")
    public void setJmeterTempPath(String jmeterTempPath) {
        JmeterUtils.jmeterTempPath = jmeterTempPath;
    }

    public static Map<String, String> gengreatJmxFile(Map<String, Object> jmeterParam) throws IOException {
        String type = String.valueOf(jmeterParam.get("type"));

        Map<String, String> resMap = new HashMap<>();
        if (StringUtils.equals("1", type)) {
            resMap = gengreatScript(jmeterParam);
        } else if (StringUtils.equals("2", type)) {
            resMap = gengreatSingleScriptSingleThread(jmeterParam);
        } else if (StringUtils.equals("3", type)) {
            resMap = gengreatSingleScriptMultipleThread(jmeterParam);
        }
        return  resMap;
    }

    public static Map gengreatScript(Map<String, Object> jmeterParam) throws IOException {
        Map<String, String> resMap = new HashMap<>();
        List<Map> scriptList = CommonUtils.castObjectToList(jmeterParam.get("scriptList"), Map.class);
        String testPlanName =  String.valueOf(jmeterParam.get("name"));
        int threads =  Integer.parseInt(String.valueOf(jmeterParam.get("threads")));
        int rampUp =  Integer.parseInt(String.valueOf(jmeterParam.get("rampupPeriod")));
        long duration =  Long.parseLong(String.valueOf(jmeterParam.get("duration")));

        String timeString =  CommonUtils.getTimeString();
        String tempPath = "G:\\" + timeString + "\\";
        String zipFilePath = "G:\\"+ timeString + ".zip";

        resMap.put("fileName", timeString + ".zip");

        File jmxPath = new File(tempPath);
        if (!jmxPath.exists()) {
            jmxPath.mkdirs();
        }

        File jmeterHome = new File(jmeterHomePath);
        String slash = System.getProperty("file.separator");
        File jmeterProperties = new File(jmeterHome.getPath() + slash + "bin" + slash + "jmeter.properties");
        JMeterUtils.setJMeterHome(jmeterHome.getPath());
        JMeterUtils.loadJMeterProperties(jmeterProperties.getPath());
        JMeterUtils.initLogging();
        JMeterUtils.initLocale();


        for (Map item : scriptList) {
            String fileName = CommonUtils.getUUID();
            String name = String.valueOf(item.get("name"));
            String path = String.valueOf(item.get("url"));
            String domain = String.valueOf(item.get("domain"));
            String method = String.valueOf(item.get("method"));
            String params = String.valueOf(item.get("params"));

            if (StringUtils.equals("GET", method)) {
                path = path + params;
            }

            HashTree testPlanTree = new HashTree();

            // Test Plan 测试计划
            TestPlan testPlan = new TestPlan(testPlanName);
            testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName());
            testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
            testPlan.setUserDefinedVariables((Arguments) new ArgumentsPanel().createTestElement());


            HTTPSamplerProxy httpSamplerProxy = new HTTPSamplerProxy();
            httpSamplerProxy.setProtocol("https");
            httpSamplerProxy.setContentEncoding("utf-8");
            httpSamplerProxy.setName(name);
            httpSamplerProxy.setDomain(domain);
            httpSamplerProxy.setPath(path);
            httpSamplerProxy.setMethod(method);
            httpSamplerProxy.setProperty(TestElement.TEST_CLASS, HTTPSamplerProxy.class.getName());
            httpSamplerProxy.setProperty(TestElement.GUI_CLASS, HttpTestSampleGui.class.getName());


            if (StringUtils.equals("POST", method)) {
                httpSamplerProxy.setPostBodyRaw(true);
                httpSamplerProxy.addArgument("", params);
            }

            LoopController loopController = new LoopController();
            loopController.setLoops(1);
            loopController.setFirst(false);
            loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
            loopController.setProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName());
            loopController.initialize();

            // Thread Group 线程组
            ThreadGroup threadGroup = new ThreadGroup();
            threadGroup.setName("线程组");
            threadGroup.setNumThreads(threads);
            threadGroup.setRampUp(rampUp);
            threadGroup.setDuration(duration);
            threadGroup.setScheduler(true);
            threadGroup.setSamplerController(loopController);
            threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
            threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());

            // 测试计划
            testPlanTree.add(testPlan);
            HashTree threadGroupHashTree = testPlanTree.add(testPlan, threadGroup);
            threadGroupHashTree.add(httpSamplerProxy);

            SaveService.saveTree(testPlanTree, new FileOutputStream(tempPath + fileName + ".jmx"));
        }
        FileUtils.zipFile(tempPath, zipFilePath);
        FileUtils.deleteFolder("G:\\"+ timeString);
        return  resMap;
    }

    /**
     * 单文件单线程组
     *
     * @param jmeterParam
     * @return
     * @throws IOException
     */
    public static Map<String, String> gengreatSingleScriptSingleThread(Map<String, Object> jmeterParam) throws IOException {
        Map<String, String> resMap = new HashMap<>();
        List<Map> scriptList = CommonUtils.castObjectToList(jmeterParam.get("scriptList"), Map.class);
        String testPlanName =  String.valueOf(jmeterParam.get("name"));
        int threads =  Integer.parseInt(String.valueOf(jmeterParam.get("threads")));
        int rampUp =  Integer.parseInt(String.valueOf(jmeterParam.get("rampupPeriod")));
        long duration =  Long.parseLong(String.valueOf(jmeterParam.get("duration")));

        String timeString =  CommonUtils.getTimeString();
        String tempPath = "G:\\" + timeString + "\\";
        String zipFilePath = "G:\\"+ timeString + ".zip";

        resMap.put("fileName", timeString + ".zip");

        File jmxPath = new File(tempPath);
        if (!jmxPath.exists()) {
            jmxPath.mkdirs();
        }

        File jmeterHome = new File(jmeterHomePath);
        String slash = System.getProperty("file.separator");
        File jmeterProperties = new File(jmeterHome.getPath() + slash + "bin" + slash + "jmeter.properties");
        JMeterUtils.setJMeterHome(jmeterHome.getPath());
        JMeterUtils.loadJMeterProperties(jmeterProperties.getPath());
        JMeterUtils.initLogging();
        JMeterUtils.initLocale();

        HashTree testPlanTree = new HashTree();

        // Test Plan 测试计划
        TestPlan testPlan = new TestPlan(testPlanName);
        testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName());
        testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
        testPlan.setUserDefinedVariables((Arguments) new ArgumentsPanel().createTestElement());
        String fileName = CommonUtils.getUUID();

        LoopController loopController = new LoopController();
        loopController.setLoops(1);
        loopController.setFirst(false);
        loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
        loopController.setProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName());
        loopController.initialize();

        // Thread Group 线程组
        ThreadGroup threadGroup = new ThreadGroup();
        threadGroup.setName("线程组");
        threadGroup.setNumThreads(threads);
        threadGroup.setRampUp(rampUp);
        threadGroup.setDuration(duration);
        threadGroup.setScheduler(true);
        threadGroup.setSamplerController(loopController);
        threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
        threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());

        for (Map item : scriptList) {
            String name = String.valueOf(item.get("name"));
            String path = String.valueOf(item.get("url"));
            String domain = String.valueOf(item.get("domain"));
            String method = String.valueOf(item.get("method"));
            String params = String.valueOf(item.get("params"));

            if (StringUtils.equals("GET", method)) {
                path = path + params;
            }

            HTTPSamplerProxy httpSamplerProxy = new HTTPSamplerProxy();
            httpSamplerProxy.setProtocol("https");
            httpSamplerProxy.setContentEncoding("utf-8");
            httpSamplerProxy.setName(name);
            httpSamplerProxy.setDomain(domain);
            httpSamplerProxy.setPath(path);
            httpSamplerProxy.setMethod(method);
            httpSamplerProxy.setProperty(TestElement.TEST_CLASS, HTTPSamplerProxy.class.getName());
            httpSamplerProxy.setProperty(TestElement.GUI_CLASS, HttpTestSampleGui.class.getName());


            if (StringUtils.equals("POST", method)) {
                httpSamplerProxy.setPostBodyRaw(true);
                httpSamplerProxy.addArgument("", params);
            }

            // 测试计划
            testPlanTree.add(testPlan);
            HashTree threadGroupHashTree = testPlanTree.add(testPlan, threadGroup);
            threadGroupHashTree.add(httpSamplerProxy);

        }

        SaveService.saveTree(testPlanTree, new FileOutputStream(tempPath + fileName + ".jmx"));

        FileUtils.zipFile(tempPath, zipFilePath);
        FileUtils.deleteFolder("G:\\"+ timeString);
        return  resMap;
    }

    /**
     * 单文件多线程组
     * @param jmeterParam
     * @return
     * @throws IOException
     */
    public static Map<String, String> gengreatSingleScriptMultipleThread(Map<String, Object> jmeterParam) throws IOException {
        Map<String, String> resMap = new HashMap<>();
        List<Map> scriptList = CommonUtils.castObjectToList(jmeterParam.get("scriptList"), Map.class);
        String testPlanName =  String.valueOf(jmeterParam.get("name"));
        int threads =  Integer.parseInt(String.valueOf(jmeterParam.get("threads")));
        int rampUp =  Integer.parseInt(String.valueOf(jmeterParam.get("rampupPeriod")));
        long duration =  Long.parseLong(String.valueOf(jmeterParam.get("duration")));

        String timeString =  CommonUtils.getTimeString();
        String tempPath = "G:\\" + timeString + "\\";
        String zipFilePath = "G:\\"+ timeString + ".zip";

        resMap.put("fileName", timeString + ".zip");

        File jmxPath = new File(tempPath);
        if (!jmxPath.exists()) {
            jmxPath.mkdirs();
        }

        File jmeterHome = new File(jmeterHomePath);
        String slash = System.getProperty("file.separator");
        File jmeterProperties = new File(jmeterHome.getPath() + slash + "bin" + slash + "jmeter.properties");
        JMeterUtils.setJMeterHome(jmeterHome.getPath());
        JMeterUtils.loadJMeterProperties(jmeterProperties.getPath());
        JMeterUtils.initLogging();
        JMeterUtils.initLocale();

        HashTree testPlanTree = new HashTree();

        // Test Plan 测试计划
        TestPlan testPlan = new TestPlan(testPlanName);
        testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName());
        testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
        testPlan.setUserDefinedVariables((Arguments) new ArgumentsPanel().createTestElement());
        String fileName = CommonUtils.getUUID();

        for (Map item : scriptList) {
            String name = String.valueOf(item.get("name"));
            String path = String.valueOf(item.get("url"));
            String domain = String.valueOf(item.get("domain"));
            String method = String.valueOf(item.get("method"));
            String params = String.valueOf(item.get("params"));

            if (StringUtils.equals("GET", method)) {
                path = path + params;
            }

            HTTPSamplerProxy httpSamplerProxy = new HTTPSamplerProxy();
            httpSamplerProxy.setProtocol("https");
            httpSamplerProxy.setContentEncoding("utf-8");
            httpSamplerProxy.setName(name);
            httpSamplerProxy.setDomain(domain);
            httpSamplerProxy.setPath(path);
            httpSamplerProxy.setMethod(method);
            httpSamplerProxy.setProperty(TestElement.TEST_CLASS, HTTPSamplerProxy.class.getName());
            httpSamplerProxy.setProperty(TestElement.GUI_CLASS, HttpTestSampleGui.class.getName());


            if (StringUtils.equals("POST", method)) {
                httpSamplerProxy.setPostBodyRaw(true);
                httpSamplerProxy.addArgument("", params);
            }

            LoopController loopController = new LoopController();
            loopController.setLoops(1);
            loopController.setFirst(false);
            loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
            loopController.setProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName());
            loopController.initialize();

            // Thread Group 线程组
            ThreadGroup threadGroup = new ThreadGroup();
            threadGroup.setName("线程组");
            threadGroup.setNumThreads(threads);
            threadGroup.setRampUp(rampUp);
            threadGroup.setDuration(duration);
            threadGroup.setScheduler(true);
            threadGroup.setSamplerController(loopController);
            threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
            threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());

            // 测试计划
            testPlanTree.add(testPlan);
            HashTree threadGroupHashTree = testPlanTree.add(testPlan, threadGroup);
            threadGroupHashTree.add(httpSamplerProxy);

        }

        SaveService.saveTree(testPlanTree, new FileOutputStream(tempPath + fileName + ".jmx"));

        FileUtils.zipFile(tempPath, zipFilePath);
        FileUtils.deleteFolder("G:\\"+ timeString);
        return  resMap;
    }

    public static Map gengreatSJmxFile(Map<String, String> jmeterParam) throws IOException {
        HashMap result = new HashMap();
        String data = "";
        System.out.println(jmeterHomePath);
        File jmeterHome = new File(jmeterHomePath);
        String slash = System.getProperty("file.separator");
        try {
            if (jmeterHome.exists()) {
                File jmeterProperties = new File(jmeterHome.getPath() + slash + "bin" + slash + "jmeter.properties");
                if (jmeterProperties.exists()) {
                    StandardJMeterEngine jmeter = new StandardJMeterEngine();
                    JMeterUtils.setJMeterHome(jmeterHome.getPath());
                    JMeterUtils.loadJMeterProperties(jmeterProperties.getPath());
                    JMeterUtils.initLogging();
                    JMeterUtils.initLocale();

                    // JMeter测试计划
                    HashTree testPlanTree = new HashTree();

                    //开始组装HashTree
                    URL uri = new URL(jmeterParam.get("url"));
                    String protocol = uri.getProtocol();
                    String host = uri.getHost();
                    String path = uri.getPath();
                    String query = uri.getQuery();
                    String method = jmeterParam.get("method");
                    int port = uri.getPort();

                    if (port == -1) {
                        if ("http".equals(protocol)) {
                            port = 80;
                        }
                        if ("https".equals(protocol)) {
                            port = 443;
                        }
                    }
                    HTTPSamplerProxy httpSamplerProxy = new HTTPSamplerProxy();
                    httpSamplerProxy.setDomain(host);
                    httpSamplerProxy.setPort(port);
                    if (Objects.nonNull(query) && !"".equals(query)) {
                        httpSamplerProxy.setPath(path + "?" + query);
                    } else {
                        httpSamplerProxy.setPath(path);
                    }
                    httpSamplerProxy.setMethod(method);
                    httpSamplerProxy.setName("JmeterTest" + System.currentTimeMillis());
                    httpSamplerProxy.setProperty(TestElement.TEST_CLASS, HTTPSamplerProxy.class.getName());
                    httpSamplerProxy.setProperty(TestElement.GUI_CLASS, HttpTestSampleGui.class.getName());

                    // Loop Controller 循环控制
                    LoopController loopController = new LoopController();
                    loopController.setLoops(jmeterParam.get("loop"));
                    loopController.setFirst(true);
                    loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
                    loopController.setProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName());
                    loopController.initialize();

                    // Thread Group 线程组
                    ThreadGroup threadGroup = new ThreadGroup();
                    threadGroup.setName("ThreadGroup");
                    threadGroup.setNumThreads(Integer.parseInt(jmeterParam.get("threads")));
                    threadGroup.setRampUp(Integer.parseInt(jmeterParam.get("rampUp")));
                    threadGroup.setSamplerController(loopController);
                    threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
                    threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());

                    // Test Plan 测试计划
                    TestPlan testPlan = new TestPlan("JMeterPlan");
                    testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName());
                    testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
                    testPlan.setUserDefinedVariables((Arguments) new ArgumentsPanel().createTestElement());

                    // 从以上初始化的元素构造测试计划
                    testPlanTree.add(testPlan);
                    HashTree threadGroupHashTree = testPlanTree.add(testPlan, threadGroup);
                    threadGroupHashTree.add(httpSamplerProxy);
                    String jmeterTestName = UUID.randomUUID().toString();

                    String savePath = jmeterTempPath;
                    File jmxPath = new File(savePath);
                    if (!jmxPath.exists()) {
                        jmxPath.mkdirs();
                    }
                    // 将生成的测试计划保存为JMeter的.jmx文件格式
                    SaveService.saveTree(testPlanTree, new FileOutputStream(savePath + jmeterTestName + ".jmx"));

                    // 在stdout中添加summary输出，得到测试进度，如:
                    Summariser summer = null;
                    String summariserName = JMeterUtils.getPropDefault("summariser.name", "summary");
                    if (summariserName.length() > 0) {
                        summer = new Summariser(summariserName);
                    }

                    // 将执行结果存储到.jtl文件中
                    String logFile = savePath + jmeterTestName + ".jtl";
                    ResultCollector resultCollector = new ResultCollector(summer);
                    resultCollector.setFilename(logFile);
                    testPlanTree.add(testPlanTree.getArray()[0], resultCollector);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        result.put("data", data);
        return result;
    }

    public static void runJmeter(Map<String, String> jmeterParam) throws IOException {
        String jmxPath = jmeterParam.get("jmxPath");
        String replayLogPath = jmeterParam.get("replayLogPath");
        String threads = jmeterParam.get("threads");
        String url = jmeterParam.get("url");
        String port = jmeterParam.get("port");
        String api = jmeterParam.get("api");
        String request = jmeterParam.get("request");

        // 获取TestPlan
        TestPlan testPlan = getTestPlan();

        // 获取设置循环控制器
        LoopController loopController = getLoopController();

        // 获取线程组
        ThreadGroup threadGroup = getThreadGroup(loopController, Integer.parseInt(threads));

        // 获取Http请求信息
        HTTPSamplerProxy httpSamplerProxy = getHttpSamplerProxy(url, port, api, request);

        // 获取结果：如汇总报告、察看结果树
        List<ResultCollector> resultCollector = getResultCollector(replayLogPath);

        // 获取设置吞吐量
        ConstantThroughputTimer constantThroughputTimer = getConstantThroughputTimer(20);

        // 获取请求头信息
        HeaderManager headerManager = getHeaderManager();
        HashTree fourHashTree = new HashTree();
        resultCollector.stream().forEach(item -> fourHashTree.add(item));
        fourHashTree.add(headerManager);

        HashTree thirdHashTree = new HashTree();

        // 注意：设置吞吐量需要和Http请求同一级，否则无效
        thirdHashTree.add(constantThroughputTimer);
        thirdHashTree.add(httpSamplerProxy, fourHashTree);

        HashTree secondHashTree = new HashTree();
        secondHashTree.add(threadGroup, thirdHashTree);

        HashTree firstTreeTestPlan = new HashTree();
        firstTreeTestPlan.add(testPlan, secondHashTree);

        StandardJMeterEngine jMeterEngine = new StandardJMeterEngine();
        jMeterEngine.configure(firstTreeTestPlan);
        try {
            SaveService.saveTree(firstTreeTestPlan, new FileOutputStream(jmxPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        jMeterEngine.run();

        // 使用命令
       /* String command = JMeterUtils.getJMeterBinDir() + "/jmeter -n -t " + jmxPath + " -l /Users/liufei/Downloads/jmter/replay_result.jtl";
        Runtime.getRuntime().exec(command);
        System.out.println(command);*/
    }

    public static void commandRunJmeter(Map<String, String> jmeterParam) throws IOException {
        String jmxPath = jmeterParam.get("jmxPath");
        String jtlPath = jmeterParam.get("jtlPath");
        String reportPath = jmeterParam.get("reportPath");
        String command = JMeterUtils.getJMeterBinDir() + "/jmeter -n -t " + jmxPath + " -l" + jtlPath + "replay_result.jtl" + "-e -o " + reportPath;
        Runtime.getRuntime().exec(command);
        System.out.println(command);
    }

    private static List<ResultCollector> getResultCollector(String replayLogPath) {
        // 察看结果数
        List<ResultCollector> resultCollectors = new ArrayList<>();
        Summariser summariser = new Summariser("速度");
        ResultCollector resultCollector = new ResultCollector(summariser);
        resultCollector.setProperty(new BooleanProperty("ResultCollector.error_logging", false));
        resultCollector.setProperty(new ObjectProperty("saveConfig", getSampleSaveConfig()));
        resultCollector.setProperty(new StringProperty("TestElement.gui_class", "org.apache.jmeter.visualizers.ViewResultsFullVisualizer"));
        resultCollector.setProperty(new StringProperty("TestElement.name", "察看结果树"));
        resultCollector.setProperty(new StringProperty("TestElement.enabled", "true"));
        resultCollector.setProperty(new StringProperty("filename", replayLogPath));
        resultCollectors.add(resultCollector);

        // 结果汇总
        ResultCollector resultTotalCollector = new ResultCollector();
        resultTotalCollector.setProperty(new BooleanProperty("ResultCollector.error_logging", false));
        resultTotalCollector.setProperty(new ObjectProperty("saveConfig", getSampleSaveConfig()));
        resultTotalCollector.setProperty(new StringProperty("TestElement.gui_class", "org.apache.jmeter.visualizers.SummaryReport"));
        resultTotalCollector.setProperty(new StringProperty("TestElement.name", "汇总报告"));
        resultTotalCollector.setProperty(new StringProperty("TestElement.enabled", "true"));
        resultTotalCollector.setProperty(new StringProperty("filename", ""));
        resultCollectors.add(resultTotalCollector);

        return resultCollectors;
    }

    private static LoopController getLoopController() {
        LoopController loopController = new LoopController();
        loopController.setContinueForever(false);
        loopController.setProperty(new StringProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName()));
        loopController.setProperty(new StringProperty(TestElement.TEST_CLASS, LoopController.class.getName()));
        loopController.setProperty(new StringProperty(TestElement.NAME, "循环控制器"));
        loopController.setProperty(new StringProperty(TestElement.ENABLED, "true"));
        loopController.setProperty(new StringProperty(LoopController.LOOPS, "1"));
        return loopController;
    }

    /***
     * 创建线程组
     * @param loopController 循环控制器
     * @param numThreads 线程数量
     * @return
     */
    private static ThreadGroup getThreadGroup(LoopController loopController, int numThreads) {
        ThreadGroup threadGroup = new ThreadGroup();
        threadGroup.setNumThreads(numThreads);
        threadGroup.setRampUp(1);
        threadGroup.setDelay(0);
        threadGroup.setDuration(0);
        threadGroup.setProperty(new StringProperty(ThreadGroup.ON_SAMPLE_ERROR, "continue"));
        threadGroup.setScheduler(false);
        threadGroup.setName("回放流量");
        threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
        threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());
        threadGroup.setProperty(new BooleanProperty(TestElement.ENABLED, true));
        threadGroup.setProperty(new TestElementProperty(ThreadGroup.MAIN_CONTROLLER, loopController));
        return threadGroup;
    }

    /***
     * 创建http请求信息
     * @param url ip地址
     * @param port 端口
     * @param api url
     * @param request 请求参数（请求体）
     * @return
     */
    private static HTTPSamplerProxy getHttpSamplerProxy(String url, String port, String api, String request) {
        HTTPSamplerProxy httpSamplerProxy = new HTTPSamplerProxy();
        Arguments HTTPsamplerArguments = new Arguments();
        HTTPArgument httpArgument = new HTTPArgument();
        httpArgument.setProperty(new BooleanProperty("HTTPArgument.always_encode", false));
        httpArgument.setProperty(new StringProperty("Argument.value", request));
        httpArgument.setProperty(new StringProperty("Argument.metadata", "="));
        ArrayList<TestElementProperty> list1 = new ArrayList<>();
        list1.add(new TestElementProperty("", httpArgument));
        HTTPsamplerArguments.setProperty(new CollectionProperty("Arguments.arguments", list1));
        httpSamplerProxy.setProperty(new TestElementProperty("HTTPsampler.Arguments", HTTPsamplerArguments));
        httpSamplerProxy.setProperty(new StringProperty("HTTPSampler.domain", url));
        httpSamplerProxy.setProperty(new StringProperty("HTTPSampler.port", port));
        httpSamplerProxy.setProperty(new StringProperty("HTTPSampler.protocol", "http"));
        httpSamplerProxy.setProperty(new StringProperty("HTTPSampler.path", api));
        httpSamplerProxy.setProperty(new StringProperty("HTTPSampler.method", "POST"));
        httpSamplerProxy.setProperty(new StringProperty("HTTPSampler.contentEncoding", JMETER_ENCODING));
        httpSamplerProxy.setProperty(new BooleanProperty("HTTPSampler.follow_redirects", true));
        httpSamplerProxy.setProperty(new BooleanProperty("HTTPSampler.postBodyRaw", true));
        httpSamplerProxy.setProperty(new BooleanProperty("HTTPSampler.auto_redirects", false));
        httpSamplerProxy.setProperty(new BooleanProperty("HTTPSampler.use_keepalive", true));
        httpSamplerProxy.setProperty(new BooleanProperty("HTTPSampler.DO_MULTIPART_POST", false));
        httpSamplerProxy.setProperty(new StringProperty("TestElement.gui_class", "org.apache.jmeter.protocol.http.control.gui.HttpTestSampleGui"));
        httpSamplerProxy.setProperty(new StringProperty("TestElement.test_class", "org.apache.jmeter.protocol.http.sampler.HTTPSamplerProxy"));
        httpSamplerProxy.setProperty(new StringProperty("TestElement.name", "HTTP Request"));
        httpSamplerProxy.setProperty(new StringProperty("TestElement.enabled", "true"));
        httpSamplerProxy.setProperty(new BooleanProperty("HTTPSampler.postBodyRaw", true));
        httpSamplerProxy.setProperty(new StringProperty("HTTPSampler.embedded_url_re", ""));
        httpSamplerProxy.setProperty(new StringProperty("HTTPSampler.connect_timeout", ""));
        httpSamplerProxy.setProperty(new StringProperty("HTTPSampler.response_timeout", ""));
        return httpSamplerProxy;
    }

    private static TestPlan getTestPlan() {
        TestPlan testPlan = new TestPlan("Test Plan");
        testPlan.setFunctionalMode(false);
        testPlan.setSerialized(false);
        testPlan.setTearDownOnShutdown(true);
        testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName());
        testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
        testPlan.setProperty(new BooleanProperty(TestElement.ENABLED, true));
        testPlan.setProperty(new StringProperty(TestElement.COMMENTS, ""));
        testPlan.setTestPlanClasspath("");
        Arguments arguments = new Arguments();
        testPlan.setUserDefinedVariables(arguments);
        return testPlan;
    }

    private static SampleSaveConfiguration getSampleSaveConfig() {
        SampleSaveConfiguration sampleSaveConfiguration = new SampleSaveConfiguration();
        sampleSaveConfiguration.setTime(true);
        sampleSaveConfiguration.setLatency(true);
        sampleSaveConfiguration.setTimestamp(true);
        sampleSaveConfiguration.setSuccess(true);
        sampleSaveConfiguration.setLabel(true);
        sampleSaveConfiguration.setCode(true);
        sampleSaveConfiguration.setMessage(true);
        sampleSaveConfiguration.setThreadName(true);
        sampleSaveConfiguration.setDataType(true);
        sampleSaveConfiguration.setEncoding(false);
        sampleSaveConfiguration.setAssertions(true);
        sampleSaveConfiguration.setSubresults(true);
        sampleSaveConfiguration.setResponseData(false);
        sampleSaveConfiguration.setSamplerData(false);
        sampleSaveConfiguration.setAsXml(false);
        sampleSaveConfiguration.setFieldNames(true);
        sampleSaveConfiguration.setResponseHeaders(false);
        sampleSaveConfiguration.setRequestHeaders(false);
        sampleSaveConfiguration.setAssertionResultsFailureMessage(true);
        sampleSaveConfiguration.setBytes(true);
        sampleSaveConfiguration.setSentBytes(true);
        sampleSaveConfiguration.setUrl(true);
        sampleSaveConfiguration.setThreadCounts(true);
        sampleSaveConfiguration.setIdleTime(true);
        sampleSaveConfiguration.setConnectTime(true);
        return sampleSaveConfiguration;
    }

    private static HeaderManager getHeaderManager() {
        ArrayList<TestElementProperty> headerMangerList = new ArrayList<>();
        HeaderManager headerManager = new HeaderManager();
        Header header = new Header("Content-Type", "application/json");
        TestElementProperty HeaderElement = new TestElementProperty("", header);
        headerMangerList.add(HeaderElement);

        headerManager.setEnabled(true);
        headerManager.setName("HTTP Header Manager");
        headerManager.setProperty(new CollectionProperty(HeaderManager.HEADERS, headerMangerList));
        headerManager.setProperty(new StringProperty(TestElement.TEST_CLASS, HeaderManager.class.getName()));
        headerManager.setProperty(new StringProperty(TestElement.GUI_CLASS, HeaderPanel.class.getName()));
        return headerManager;
    }

    /***
     * 限制QPS设置
     * @param throughputTimer
     * @return
     */
    private static ConstantThroughputTimer getConstantThroughputTimer(int throughputTimer) {
        ConstantThroughputTimer constantThroughputTimer = new ConstantThroughputTimer();
        constantThroughputTimer.setEnabled(true);
        constantThroughputTimer.setName("常数吞吐量定时器");
        constantThroughputTimer.setProperty(TestElement.TEST_CLASS, ConstantThroughputTimer.class.getName());
        constantThroughputTimer.setProperty(TestElement.GUI_CLASS, TestBeanGUI.class.getName());
        constantThroughputTimer.setCalcMode(ConstantThroughputTimer.Mode.AllActiveThreads.ordinal());

        constantThroughputTimer.setProperty(new IntegerProperty("calcMode", ConstantThroughputTimer.Mode.AllActiveThreads.ordinal()));
        DoubleProperty doubleProperty = new DoubleProperty();
        doubleProperty.setName("throughput");
        doubleProperty.setValue(throughputTimer * 60f);
        constantThroughputTimer.setProperty(doubleProperty);
        return constantThroughputTimer;
    }
}