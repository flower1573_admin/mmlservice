package com.huayi.autoservice.utils;

import com.huayi.autoservice.entity.ScheduleTaskEntity;
import com.huayi.autoservice.schedule.ScheduleRegistrar;
import com.huayi.autoservice.schedule.ScheduleRunnable;
import com.huayi.autoservice.service.ScheduleTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component
public class ScheduleTaskUtils {
    private static Logger logger = LoggerFactory.getLogger(ScheduleTaskUtils.class);

    @Resource
    private ScheduleRegistrar cronTaskRegistrar;

    @Autowired
    ScheduleTaskService scheduleService;

    public Boolean createTask(ScheduleTaskEntity scheduleEntity) {
        Integer insert = scheduleService.createScheduleTask(scheduleEntity);
        if (insert == null) {
            return false;
        }
        // 添加成功, 并且状态是启用, 则直接放入任务器
        if (scheduleEntity.getTaskStatus().equals("enable")) {
            ScheduleRunnable task = new ScheduleRunnable(scheduleEntity.getTaskName(), scheduleEntity.getTaskMethod(), scheduleEntity.getTaskParams(), scheduleEntity.getId());
            cronTaskRegistrar.addCronTask(task, scheduleEntity.getTaskCron());
            logger.info("insertTaskJob success");
        }
        return true;
    }

    public boolean updateTask(ScheduleTaskEntity scheduleEntity) {
        ScheduleTaskEntity queryParams = ScheduleTaskEntity.builder().id(scheduleEntity.getId()).build();
        ScheduleTaskEntity existSchedule = scheduleService.getScheduleTaskSchedule(queryParams);

        ScheduleTaskEntity updateParams = ScheduleTaskEntity.builder()
                .id(scheduleEntity.getId())
                .taskName(scheduleEntity.getTaskName())
                .taskBean(scheduleEntity.getTaskBean())
                .taskMethod(scheduleEntity.getTaskMethod())
                .taskParams(scheduleEntity.getTaskParams())
                .taskStatus(scheduleEntity.getTaskStatus())
                .taskCron(scheduleEntity.getTaskCron())
                .updateTime(scheduleEntity.getUpdateTime())
                .remark(scheduleEntity.getRemark())
                .build();

        // 修改任务
        Integer update = scheduleService.updateScheduleTask(updateParams);
        if (update == null) {
            return false;
        }
        // 修改成功, 则先删除任务器中的任务, 并重新添加
        ScheduleRunnable preTask = new ScheduleRunnable(existSchedule.getTaskName(), existSchedule.getTaskMethod(), existSchedule.getTaskParams(), existSchedule.getId());
        cronTaskRegistrar.removeCronTask(preTask);
        // 如果修改后的任务状态是启用, 就加入任务器
        if (scheduleEntity.getTaskStatus().equals("enable")) {
            ScheduleRunnable task = new ScheduleRunnable(scheduleEntity.getTaskName(), scheduleEntity.getTaskMethod(), scheduleEntity.getTaskParams(), scheduleEntity.getId());
            cronTaskRegistrar.addCronTask(task, existSchedule.getTaskCron());
            logger.info("updateTaskJob success");
        }
        return true;
    }

    public boolean deleteTask(String id) {
        // 先查询要删除的任务信息
        ScheduleTaskEntity queryParams = ScheduleTaskEntity.builder().id(id).build();
        ScheduleTaskEntity existedSchedule = scheduleService.getScheduleTaskSchedule(queryParams);

        List<String> deleteParams = new ArrayList<>();
        deleteParams.add(id);
        // 删除
        Integer delete = scheduleService.deleteScheduleTask(deleteParams);
        if (delete == null) {
            return false;
        }
        // 删除成功, 并删除定时任务器中的对应任务
        ScheduleRunnable task = new ScheduleRunnable(existedSchedule.getTaskName(), existedSchedule.getTaskMethod(), existedSchedule.getTaskParams(), existedSchedule.getId());
        cronTaskRegistrar.removeCronTask(task);
        return true;
    }

    public boolean updateTaskStatus(String id, String status) {
        // 修改任务状态
        ScheduleTaskEntity scheduleSetting = ScheduleTaskEntity.builder().id(id).taskStatus(status).build();
        Integer update = scheduleService.updateScheduleTask(scheduleSetting);
        if (update == null) {
            return false;
        }
        // 查询修改后的任务信息
        ScheduleTaskEntity queryParams = ScheduleTaskEntity.builder().id(id).taskStatus(status).build();

        ScheduleTaskEntity existSchedule = scheduleService.getScheduleTaskSchedule(queryParams);

        // 如果状态是启用, 则添加任务
        ScheduleRunnable schedulingRunnable = new ScheduleRunnable(existSchedule.getTaskName(), existSchedule.getTaskMethod(), existSchedule.getTaskParams(), existSchedule.getId());
        if (existSchedule.getTaskStatus().equals("enable")) {
            cronTaskRegistrar.addCronTask(schedulingRunnable, existSchedule.getTaskCron());
            logger.info("changeStatus success");
        } else {
            cronTaskRegistrar.removeCronTask(schedulingRunnable);
            logger.info("changeStatus success");
        }
        return true;
    }
}
