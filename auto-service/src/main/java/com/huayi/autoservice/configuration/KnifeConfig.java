package com.huayi.autoservice.configuration;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import com.github.xiaoymin.knife4j.spring.extension.OpenApiExtensionResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableKnife4j
public class KnifeConfig {
    /**
     * 分组名称
     */
    private String groupName = "knife4j";
    /**
     * 主机名
     */
    private String host = "http://127.0.0.1";
    /**
     * 标题
     */
    private String title = "API在线文档工具";
    /**
     * 简介
     */
    private String description = "API在线文档工具";
    /**
     * 服务条款URL
     */
    private String termsOfServiceUrl = "http://127.0.0.1";
    /**
     * 联系人
     */
    private String contactName = "portalservice";
    /**
     * 联系网址
     */
    private String contactUrl = "http://java.qingtian.cn";
    /**
     * 联系邮箱
     */
    private String contactEmail = "chennan@xx.com";
    /**
     * 版本号
     */
    private String version = "1.0.0";

    @Autowired
    private OpenApiExtensionResolver openApiExtensionResolver;

    @Bean
    public Docket docket() {
        String groupName = "1.0.0";
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .host(host)
                .apiInfo(apiInfo())
                .groupName(groupName)
                .select()
                .paths(PathSelectors.any())
                .build()
                .extensions(openApiExtensionResolver.buildExtensions(groupName));
        return docket;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(title)
                .description(description)
                .termsOfServiceUrl(termsOfServiceUrl)
                .contact(new Contact(contactName, contactUrl, contactEmail))
                .version(version)
                .build();
    }
}