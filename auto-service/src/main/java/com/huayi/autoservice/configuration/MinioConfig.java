package com.huayi.autoservice.configuration;

import io.minio.MinioClient;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author: jiangjs
 * @description: minio相关配置
 * @date: 2023/10/20 10:46
 **/
@Configuration
@EnableConfigurationProperties(MinioEndPointInfo.class)
public class MinioConfig {
    @Resource
    private MinioEndPointInfo minioEndPointInfo;

    @Bean
    public PearMinioClient createPearMinioClient() {
        MinioClient minioClient = MinioClient.builder()
                .endpoint(minioEndPointInfo.getEndpoint())
                .credentials(minioEndPointInfo.getAccessKey(), minioEndPointInfo.getSecretKey())
                .build();
        return new PearMinioClient(minioClient);
    }
}
