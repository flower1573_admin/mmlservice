package com.huayi.autoservice.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author: jiangjs
 * @description:
 * @date: 2023/10/20 10:27
 **/
@Component
@Data
@ConfigurationProperties(prefix = "minio")
public class MinioEndPointInfo {
    /**
     * minio节点地址
     */
    private String endpoint;
    /**
     * 登录用户名
     */
    private String accessKey;
    /**
     * 密码
     */
    private String secretKey;

}
