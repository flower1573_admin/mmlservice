package com.huayi.autoservice.scheduleTask;

import com.huayi.autoservice.service.AduitLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("AduitLogScheduleTask")
public class AduitLogScheduleTask {
    @Autowired
    AduitLogService aduitLogService;

    public void deleteAduitLog(String count) {
        aduitLogService.deleteAduitLog(Integer.parseInt(count));
    }
}
