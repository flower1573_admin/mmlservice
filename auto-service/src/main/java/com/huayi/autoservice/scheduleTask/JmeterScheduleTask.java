package com.huayi.autoservice.scheduleTask;

import com.huayi.autoservice.service.AduitLogService;
import com.huayi.autoservice.service.JmeterPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component("JmeterScheduleTask")
public class JmeterScheduleTask {
    @Autowired
    JmeterPlanService jmeterPlanService;

    public void executeTask(String string) throws IOException {
        jmeterPlanService.executeTask(string);
    }
}
