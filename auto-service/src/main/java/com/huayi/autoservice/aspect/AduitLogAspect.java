package com.huayi.autoservice.aspect;


import com.huayi.autoservice.annotation.AduitLog;
import com.huayi.autoservice.entity.AduitLogEntity;
import com.huayi.autoservice.service.AduitLogService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.HttpUtils;
import com.huayi.autoservice.utils.ThreadPoolUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Aspect
@Component
public class AduitLogAspect {
    private static Logger LOGGER = LoggerFactory.getLogger(AduitLogAspect.class);

    @Autowired
    private AduitLogService aduitLogService;

    // 声明AOP切入点
    @Pointcut("@annotation(com.huayi.autoservice.annotation.AduitLog)")
    public void aduitLog() {
    }

    @Before("aduitLog()")
    public void beforeExec(JoinPoint joinPoint) {
    }

    @After("aduitLog()")
    public void afterExec(JoinPoint joinPoint) {
    }

    @Around("aduitLog()")
    public Object aroundExec(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        LocalDateTime accessDate = LocalDateTime.now();

        Object proceed = proceedingJoinPoint.proceed();

        LocalDateTime processEndDate = LocalDateTime.now();

        handleAduitLog(proceedingJoinPoint, proceed, accessDate, processEndDate);
        return proceed;
    }

    private void handleAduitLog(ProceedingJoinPoint proceedingJoinPoint, Object proceed, LocalDateTime accessDate, LocalDateTime processEndDate) {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

            // 获取用户IP
            String clientIp = HttpUtils.getClientIp(request);

            // 请求类
            Signature signature = proceedingJoinPoint.getSignature();
            String requestFullClazz = signature.getDeclaringTypeName();
            String[] requestFullClazzArr = requestFullClazz.split("\\.");
            String requestClazz = requestFullClazzArr[requestFullClazzArr.length - 1];

            // 获取请求路径
            String requestPath = request.getServletPath();

            // 请求方法
            String requestType = request.getMethod();

            // 模块名称
            MethodSignature methodSignature = (MethodSignature) signature;
            Method method = methodSignature.getMethod();
            AduitLog aduitLog = method.getAnnotation(AduitLog.class);
            String moudleCode = aduitLog.moudleCode();

            // 请求方法
            String requestMethod = signature.getName();

            // 接口耗时
            String timeConsume = Duration.between(accessDate, processEndDate).toMillis() + "";

            String finalResponseCode = "__";

            // 请求参数
            List<Object> requestList = Arrays.asList(proceedingJoinPoint.getArgs()).stream().filter(new Predicate<Object>() {
                @Override
                public boolean test(Object obj) {
                    if (obj instanceof ServletRequest || obj instanceof ServletResponse || obj instanceof MultipartFile) {
                        return false;
                    }
                    return true;
                }
            }).collect(Collectors.toList());
            String requestParams = StringUtils.join(requestList, "_");

            // 浏览器指纹
            String fingerprint = ((ServletRequestAttributes) Objects
                    .requireNonNull(RequestContextHolder.getRequestAttributes()))
                    .getRequest()
                    .getHeader("x-fingerprint");

            String finalResponseCode1 = finalResponseCode;
            ThreadPoolUtils.execute(new Runnable() {
                @Override
                public void run() {
                    AduitLogEntity aduitLogEntity = AduitLogEntity.builder()
                            .id(CommonUtils.getUUID("aduit_log"))
                            .userIp(clientIp)
                            .fingerPrint(fingerprint)
                            .requestMoudle(moudleCode)
                            .requestClazz(requestClazz)
                            .requestMethod(requestMethod)
                            .requestType(requestType)
                            .requestParams(requestParams)
                            .requestConsume(timeConsume)
                            .requestPath(requestPath)
                            .requestReault("")
                            .requestTime(new Date())
                            .build();

                    aduitLogService.createAduitLog(aduitLogEntity);
                }
            });
        } catch (Exception exception) {
            LOGGER.error("Error occured while auditing, cause by: ", exception);
        }
    }

    /**
     * 不带参返回
     */
    @AfterReturning(pointcut = "aduitLog()")
    public void doAfterReturning(JoinPoint joinPoint) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

    }

    @AfterThrowing(pointcut = "aduitLog()", throwing = "throwable")
    public void doAfterThrowing(JoinPoint joinPoint, Throwable throwable) {
        LOGGER.error("AuditLogAspect doAfterThrowing, cause by {}", throwable);
    }
}

