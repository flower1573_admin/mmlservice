package com.huayi.autoservice.controller;

import com.huayi.autoservice.annotation.AduitLog;
import com.huayi.autoservice.entity.NavigatorEntity;
import com.huayi.autoservice.service.NavigatorService;
import com.huayi.autoservice.vo.ResultVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class NavigatorController {
    private static Logger logger = LoggerFactory.getLogger(NavigatorController.class);

    @Autowired
    NavigatorService navigatorService;

    @RequestMapping(value = "/navigator/getNavigatorList", method = RequestMethod.POST)
    public ResultVo getNavigatorList(@RequestBody Map<String, String> params) {
        int pageStart = 0;
        int size = 15;
        if (StringUtils.isNotEmpty(params.get("page")) && StringUtils.isNotEmpty(params.get("size"))) {
            int page = Integer.parseInt(params.get("page"));
            size = Integer.parseInt(params.get("size"));
            pageStart = (page - 1) * size;
        }

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("name", params.get("name"));
        queryParams.put("pageStart", pageStart);
        queryParams.put("pageSize", size);

        List<NavigatorEntity> userList = navigatorService.getNavigatorList(queryParams);
        int userCont = navigatorService.getNavigatorTotal(queryParams);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", userList);
        resultMap.put("total", userCont);
        return ResultVo.success(resultMap);
    }

    @AduitLog(moudleCode = "ADULT", clazz = NavigatorController.class)
    @RequestMapping(value = "/navigator/createNavigator", method = RequestMethod.POST)
    public ResultVo createUser(@RequestBody Map<String, Object> params) {
        logger.info("start createUser");
        navigatorService.createNavigator(params);
        return ResultVo.success();
    }

    @AduitLog(moudleCode = "ADULT", clazz = NavigatorController.class)
    @RequestMapping(value = "/navigator/updateNavigator", method = RequestMethod.POST)
    public ResultVo updateUser(@RequestBody Map<String, Object> params) {
        logger.info("start updateUser");
        navigatorService.updateNavigator(params);
        return ResultVo.success();
    }

    @AduitLog(moudleCode = "ADULT", clazz = NavigatorController.class)
    @RequestMapping(value = "/navigator/deleteNavigator", method = RequestMethod.POST)
    public ResultVo deleteNavigator(@RequestBody Map<String, Object> params) {
        navigatorService.deleteNavigator(params);
        return ResultVo.success();
    }

    @RequestMapping(value = "/navigator/exportNavigator", method = RequestMethod.POST)
    public void exportNavigator(HttpServletResponse response, @RequestBody Map<String, Object> params) throws IOException {
        navigatorService.exportNavigator(response, params);
    }
}
