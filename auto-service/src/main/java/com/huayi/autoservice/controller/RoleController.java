package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.RoleEntity;
import com.huayi.autoservice.entity.UserEntity;
import com.huayi.autoservice.service.RoleService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.vo.ResultVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
public class RoleController {
    private static Logger logger = LoggerFactory.getLogger(RoleController.class);

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/role/getDataList", method = RequestMethod.POST)
    public ResultVo getRoleList(@RequestBody Map<String, String> params) {
        List<RoleEntity> roleLis = roleService.getRoleLis(params);
        int dataCount = roleService.getRoleCount(params);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", roleLis);
        resultMap.put("total", dataCount);
        return ResultVo.success(resultMap);
    }

    @RequestMapping(value = "/role/getAllRoleList", method = RequestMethod.POST)
    public ResultVo getAllRoleList(@RequestBody Map<String, String> params) {
        List<RoleEntity> resultVo = roleService.getAllRoleList(params);
        return  ResultVo.success(resultVo);
    }

    @RequestMapping(value = "/role/getData", method = RequestMethod.POST)
    public ResultVo getRole(@RequestBody RoleEntity params) {
        RoleEntity res = roleService.getRole(params);
        return ResultVo.success(res);
    }

    @RequestMapping(value = "/role/getRoleListByUser", method = RequestMethod.POST)
    public ResultVo getRoleListByUser(@RequestBody UserEntity userRoleInfo) {
        List<RoleEntity> roleEntityList = roleService.getRoleListByUser(userRoleInfo);
        return ResultVo.success(roleEntityList);
    }

    @RequestMapping(value = "/role/createData", method = RequestMethod.POST)
    public ResultVo createRole(@RequestBody Map<String, Object> params) {
        logger.info("start createRole");
        ResultVo resultVo = roleService.createRole(params);
        return ResultVo.success(resultVo);
    }

    @RequestMapping(value = "/role/updateData", method = RequestMethod.POST)
    public ResultVo updateRole(@RequestBody RoleEntity roleEntity) {
        logger.info("start updateRole");
        return roleService.updateRole(roleEntity);
    }

    @RequestMapping(value = "/role/deleteData", method = RequestMethod.POST)
    public ResultVo deleteRole(@RequestBody Map<String, Object> params) {
        logger.info("start deleteRole");

        List<String> idList = CommonUtils.castObjectToList(params.get("data"), String.class);
        roleService.deleteRole(idList);
        return ResultVo.success();
    }
}
