package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.TestCaseFileEntity;
import com.huayi.autoservice.entity.ViedoCatalogEntity;
import com.huayi.autoservice.service.ViedoCatalogService;
import com.huayi.autoservice.vo.ResultVo;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class ViedoCatalogController {
    @Autowired
    ViedoCatalogService viedoCatalogService;

    @RequestMapping(value = "/viedoCatalog/getDataList", method = RequestMethod.POST)
    public ResultVo getEthernetInfo(@RequestBody Map<String, String> params) {
        int pageIndex = (Integer.parseInt(params.get("page")) - 1) * Integer.parseInt(params.get("size"));
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("fileName", params.get("fileName"));
        queryParams.put("pageIndex", pageIndex);
        queryParams.put("pageSize", params.get("size"));

        Date createTime = null;
        if (StringUtils.isNotEmpty(params.get("createTime"))){
            createTime = new Date(Long.valueOf(params.get("createTime")));
        }

        queryParams.put("createTime", createTime);

        List<ViedoCatalogEntity> dataList = viedoCatalogService.getDataList(queryParams);
        int dataCount = viedoCatalogService.getDataTotal(queryParams);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", dataList);
        resultMap.put("total", dataCount);
        return ResultVo.success(resultMap);
    }


    @RequestMapping(value = "/viedoCatalog/getAllDataList", method = RequestMethod.POST)
    public ResultVo getAllDataList(@RequestBody Map<String, Object> params)  {
        List<ViedoCatalogEntity> allDataList = viedoCatalogService.getAllDataList(params);
        return ResultVo.success(allDataList);
    }

    @RequestMapping(value = "/viedoCatalog/createData", method = RequestMethod.POST)
    public ResultVo createData(@RequestBody Map<String, Object> params)  {
        viedoCatalogService.createData(params);
        return ResultVo.success();
    }

    @RequestMapping(value = "/viedoCatalog/updateData", method = RequestMethod.POST)
    public ResultVo updateData(@RequestBody Map<String, Object> params)  {
        viedoCatalogService.updateData(params);
        return ResultVo.success();
    }


    @RequestMapping(value = "/viedoCatalog/deleteData", method = RequestMethod.POST)
    public ResultVo deleteData(@RequestBody Map<String, Object> params)  {
        viedoCatalogService.deleteData(params);
        return ResultVo.success();
    }
}
