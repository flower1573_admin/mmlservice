package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.AduitLogEntity;
import com.huayi.autoservice.entity.SystemLogEntity;
import com.huayi.autoservice.service.SystemLogService;
import com.huayi.autoservice.vo.ResultVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class SystemLogController {
    private static Logger logger = LoggerFactory.getLogger(SystemLogController.class);

    @Autowired
    SystemLogService systemLogService;

    @RequestMapping(value = "/systemLog/getDataList", method = RequestMethod.POST)
    public ResultVo getDataList(@RequestBody Map<String, String> params) {
        int pageStart = 0;
        int size = 15;
        if (StringUtils.isNotEmpty(params.get("page")) && StringUtils.isNotEmpty(params.get("size"))) {
            int page = Integer.parseInt(params.get("page"));
            size = Integer.parseInt(params.get("size"));
            pageStart = (page - 1) * size;
        }

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("pageStart", pageStart);
        queryParams.put("pageSize", size);

        List<SystemLogEntity> aduitLogList = systemLogService.getDataList(queryParams);
        Integer aduitLogCount = systemLogService.getDataTotal(queryParams);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", aduitLogList);
        resultMap.put("total", aduitLogCount);
        return ResultVo.success(resultMap);
    }

    @RequestMapping(value = "/systemLog/collectData", method = RequestMethod.POST)
    public ResultVo collectData(@RequestBody Map<String, Object> params) {
        systemLogService.collectData(params);
        return ResultVo.success();
    }

    @RequestMapping(value = "/systemLog/exportData", method = RequestMethod.POST)
    public void exportData(HttpServletResponse response, @RequestBody Map<String, Object> params) {
        systemLogService.exportData(response, params);
    }
}
