package com.huayi.autoservice.controller;

import com.alibaba.fastjson.JSONObject;
import com.huayi.autoservice.entity.FileRecordsEntity;
import com.huayi.autoservice.service.FileService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.FileUtils;
import com.huayi.autoservice.utils.MinioPearUploadUtil;
import com.huayi.autoservice.vo.MinioPearVo;
import com.huayi.autoservice.vo.ResultVo;
import io.minio.GetObjectArgs;
import io.minio.StatObjectArgs;
import io.minio.StatObjectResponse;
import io.minio.errors.*;
import lombok.AllArgsConstructor;
import org.apache.catalina.connector.ClientAbortException;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.huayi.autoservice.utils.FileUtils.convertInputStreamToFile;

@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class FileController {
    private static Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileService fileService;
    private final MinioPearUploadUtil minioPearUploadUtil;

    @RequestMapping(value = "/fileRecords/getFilemultipart", method = RequestMethod.POST)
    public ResultVo createMultipartFile(@RequestBody Map<String, String> params) {
        int chunkNum = Integer.parseInt(params.get("chunkNum"));
        String fileName = params.get("fileName");
        String contentType = params.get("contentType");
        MinioPearVo multipartUploadUrl = fileService.createMultipartUploadUrl(chunkNum, fileName, contentType);
        return ResultVo.success(multipartUploadUrl);
    }

    @RequestMapping(value = "/fileRecords/completeFilemultipart", method = RequestMethod.POST)
    public ResultVo completeMultipart(@RequestBody Map<String, String> params) {
        int chunkNum = Integer.parseInt(params.get("chunkNum"));
        String fileName = params.get("fileName");
        String contentType = params.get("contentType");
        String uploadId = params.get("uploadId");
        String fileMd5 = params.get("fileMd5");

        fileService.completeMultipart(chunkNum, fileName, contentType, uploadId, fileMd5);
        return ResultVo.success();
    }

    @RequestMapping(value = "/fileRecords/uploadSingleFile", method = RequestMethod.POST)
    public ResultVo uploadSingleFile(@RequestBody MultipartFile file) {
        fileService.uploadSingleFile(file);
        return ResultVo.success();
    }

    @RequestMapping(value = "/fileRecords/downloadSingleFile", method = RequestMethod.POST)
    public void downloadSingleFile(HttpServletResponse response, @RequestBody Map<String, String> params) {
        String fileName = params.get("fileName");
        try (InputStream inputStream = minioPearUploadUtil.downLoadFile(fileName, "email")) {
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            response.setHeader("Content-disposition", "inline; filename=" + URLEncoder.encode(fileName, "UTF-8"));
            byte[] bytes = new byte[1024];
            OutputStream osm = response.getOutputStream();
            int count;
            while ((count = inputStream.read(bytes)) != -1) {
                osm.write(bytes, 0, count);
            }
            osm.flush();
            osm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/fileRecords/getFIleUrl", method = RequestMethod.POST)
    public ResultVo getFIleUrl(HttpServletResponse response, @RequestBody Map<String, String> params) throws IOException, InvalidResponseException, InvalidKeyException, NoSuchAlgorithmException, ServerException, ErrorResponseException, XmlParserException, InsufficientDataException, InternalException {
        String fileName = params.get("fileName");
        String email = minioPearUploadUtil.getPresignedObjectUrl("email", fileName);
        return ResultVo.success(email);
    }

    @RequestMapping(value = "/fileRecords/createBucket", method = RequestMethod.POST)
    public ResultVo createBucket(@RequestBody Map<String, String> params) throws IOException, InvalidResponseException, InvalidKeyException, NoSuchAlgorithmException, ServerException, ErrorResponseException, XmlParserException, InsufficientDataException, InternalException {
        String bucketName = params.get("bucketName");
        minioPearUploadUtil.createBucket(bucketName);
        return ResultVo.success();
    }

    @RequestMapping(value = "/fileRecords/deleteBucket", method = RequestMethod.POST)
    public ResultVo deleteBucket(@RequestBody Map<String, String> params) throws IOException, InvalidResponseException, InvalidKeyException, NoSuchAlgorithmException, ServerException, ErrorResponseException, XmlParserException, InsufficientDataException, InternalException {
        String bucketName = params.get("bucketName");
        minioPearUploadUtil.deleteBucket(bucketName);
        return ResultVo.success();
    }


    @RequestMapping(value = "/fileRecords/showViedo", method = RequestMethod.GET)
    public void showViedo2(HttpServletRequest request, HttpServletResponse response) throws IOException {
        InputStream inputStream = minioPearUploadUtil.downLoadFile("学习三元龙以及用法.mp4", "email");
        long fileSize = minioPearUploadUtil.getFileStatusInfo("email", "学习三元龙以及用法.mp4").size();
        File file = convertInputStreamToFile(inputStream);
        fileChunkDownload(file, inputStream, (int) fileSize, request, response);
    }

    /**
     * 文件支持分块下载和断点续传
     *
     * @param request  请求
     * @param response 响应
     */
    private void fileChunkDownload(File file, InputStream inputStream, int fileSize, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String range = request.getHeader("Range");
        //开始下载位置
        long startByte = 0;
        //结束下载位置
        long endByte = fileSize - 1;
        if (range != null && range.contains("bytes=") && range.contains("-")) {
            range = range.substring(range.lastIndexOf("=") + 1).trim();
            String[] ranges = range.split("-");
            try {
                //判断range的类型
                if (ranges.length == 1) {
                    //类型一：bytes=-2343
                    if (range.startsWith("-")) {
                        endByte = Long.parseLong(ranges[0]);
                    }
                    //类型二：bytes=2343-
                    else if (range.endsWith("-")) {
                        startByte = Long.parseLong(ranges[0]);
                    }
                } else if (ranges.length == 2) {
                    //类型三：bytes=22-2343
                    startByte = Long.parseLong(ranges[0]);
                    endByte = Long.parseLong(ranges[1]);
                }
            } catch (NumberFormatException e) {
                startByte = 0;
                endByte = fileSize - 1;
            }
            //要下载的长度
            long contentLength = endByte - startByte + 1;
            //文件名
            String fileName = file.getName();
            //文件类型
            String contentType = request.getServletContext().getMimeType(fileName);
            byte[] fileNameBytes = fileName.getBytes(StandardCharsets.UTF_8);
            fileName = new String(fileNameBytes, 0, fileNameBytes.length, StandardCharsets.ISO_8859_1);
            //各种响应头设置
            //支持断点续传，获取部分字节内容：
            response.setHeader("Accept-Ranges", "bytes");
            //http状态码要为206：表示获取部分内容
            response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
            response.setContentType(contentType);
            response.setHeader("Content-Type", contentType);
            //inline表示浏览器直接使用，attachment表示下载，fileName表示下载的文件名
            response.setHeader("Content-Disposition", "inline;filename=" + fileName);
            response.setHeader("Content-Length", String.valueOf(contentLength));
            // Content-Range，格式为：[要下载的开始位置]-[结束位置]/[文件总大小]
            response.setHeader("Content-Range", "bytes " + startByte + "-" + endByte + "/" + file.length());
            response.setHeader("Access-Control-Allow-Origin", "*");

            BufferedOutputStream outputStream = null;
            RandomAccessFile randomAccessFile = null;
            //已传送数据大小
            long transmitted = 0;
            try {
                randomAccessFile = new RandomAccessFile(file, "r");
                outputStream = new BufferedOutputStream(response.getOutputStream());
                byte[] buff = new byte[4096];
                int len = 0;
                randomAccessFile.seek(startByte);
                //坑爹地方四：判断是否到了最后不足4096（buff的length）个byte这个逻辑（(transmitted + len) <= contentLength）要放前面！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！
                //不然会会先读取randomAccessFile，造成后面读取位置出错，找了一天才发现问题所在
                //此处的原作者意思逻辑就是  (len = randomAccessFile.read(buff)) 每次读取4096个字节 eg 文件剩余2000 读4096 意味着 有2096
                //是空的  那么前端解析的时候就会出错  所以此处作者加了(transmitted + len) <= contentLength
                //条件判断
                while ((transmitted + len) <= contentLength && (len = randomAccessFile.read(buff)) != -1) {
                    outputStream.write(buff, 0, len);
                    transmitted += len;
                }
                //处理不足buff.length部分
                if (transmitted < contentLength) {
                    len = randomAccessFile.read(buff, 0, (int) (contentLength - transmitted));
                    outputStream.write(buff, 0, len);
                    transmitted += len;
                }

                outputStream.flush();
                response.flushBuffer();
                randomAccessFile.close();
            } catch (Exception e) {

            }finally {
                randomAccessFile.close();
            }
        }
    }
}


