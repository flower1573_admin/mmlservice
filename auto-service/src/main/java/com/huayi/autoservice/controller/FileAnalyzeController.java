package com.huayi.autoservice.controller;

import com.huayi.autoservice.service.FileAnalyzeService;
import com.huayi.autoservice.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class FileAnalyzeController {
    @Autowired
    FileAnalyzeService fileAnalyzeService;

    @RequestMapping(value = "/fileHome/getAnalyzeInfos", method = RequestMethod.POST)
    public ResultVo getDataList(@RequestBody Map<String, String> params) {
        Map<String, Object> analyzeInfos = fileAnalyzeService.getAnalyzeInfos();
        return ResultVo.success(analyzeInfos);
    }
}
