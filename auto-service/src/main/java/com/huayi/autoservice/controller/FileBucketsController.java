package com.huayi.autoservice.controller;

import com.huayi.autoservice.annotation.AduitLog;
import com.huayi.autoservice.entity.FileBucketsEntity;
import com.huayi.autoservice.service.FileBucketsService;
import com.huayi.autoservice.vo.ResultVo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class FileBucketsController {
    private static Logger logger = LoggerFactory.getLogger(FileBucketsController.class);

    @Autowired
    FileBucketsService fileBucketsService;

    @RequestMapping(value = "/fileBuckets/getDataList", method = RequestMethod.POST)
    public ResultVo getDataList(@RequestBody Map<String, String> params) {
        int pageIndex = (Integer.parseInt(params.get("page")) - 1) * Integer.parseInt(params.get("size"));
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("fileName", params.get("fileName"));
        queryParams.put("pageIndex", pageIndex);
        queryParams.put("pageSize", params.get("size"));

        Date createTime = null;
        if (StringUtils.isNotEmpty(params.get("createTime"))){
            createTime = new Date(Long.valueOf(params.get("createTime")));
        }

        queryParams.put("createTime", createTime);

        List<FileBucketsEntity> dataList = fileBucketsService.getDataList(queryParams);
        int dataCount = fileBucketsService.getDataTotal(queryParams);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", dataList);
        resultMap.put("total", dataCount);
        return ResultVo.success(resultMap);
    }

    @AduitLog(moudleCode = "FileBuckets", clazz = FileBucketsController.class)
    @RequestMapping(value = "/fileBuckets/createData", method = RequestMethod.POST)
    public ResultVo createData(@RequestBody Map<String, Object> params) {
        logger.info("start fileBuckets create");
        List<FileBucketsEntity> dataList = fileBucketsService.getAllDataList(params);

        if (CollectionUtils.isNotEmpty(dataList)) {
            return ResultVo.error(400, "文件桶编码重复");
        }

        fileBucketsService.createData(params);
        return ResultVo.success();
    }


    @AduitLog(moudleCode = "FileBuckets", clazz = FileBucketsController.class)
    @RequestMapping(value = "/fileBuckets/updateData", method = RequestMethod.POST)
    public ResultVo updateData(@RequestBody Map<String, Object> params) {
        logger.info("start fileBuckets update");
        fileBucketsService.updateData(params);
        return ResultVo.success();
    }

    @AduitLog(moudleCode = "FileBuckets", clazz = FileBucketsController.class)
    @RequestMapping(value = "/fileBuckets/deleteData", method = RequestMethod.POST)
    public ResultVo deleteData(@RequestBody  Map<String, Object> params) {
        logger.info("start fileBuckets delete");
        fileBucketsService.deleteData(params);
        return ResultVo.success();
    }
}
