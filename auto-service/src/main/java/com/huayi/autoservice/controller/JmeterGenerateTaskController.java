package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.JmeterGenerateTaskEntity;
import com.huayi.autoservice.entity.JmeterPlanEntity;
import com.huayi.autoservice.service.JmeterGenerateTaskService;
import com.huayi.autoservice.service.JmeterPlanService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.vo.ResultVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class JmeterGenerateTaskController {
    @Autowired
    JmeterGenerateTaskService jmeterGenerateTaskService;

    @RequestMapping(value = "/jmeterGenerateTask/getTaskList", method = RequestMethod.POST)
    public ResultVo getTaskList(@RequestBody Map<String, String> params) {
        int pageStart = 0;
        int size = 15;
        if (StringUtils.isNotEmpty(params.get("page")) && StringUtils.isNotEmpty(params.get("size"))) {
            int page = Integer.parseInt(params.get("page"));
            size = Integer.parseInt(params.get("size"));
            pageStart = (page - 1) * size;
        }

        Date startTime = null;
        Date endTime = null;
        if (StringUtils.isNotEmpty(params.get("startTime"))){
            startTime = new Date(Long.valueOf(params.get("startTime")));
        }
        if (StringUtils.isNotEmpty(params.get("endTime"))){
            endTime =new Date(Long.valueOf(params.get("endTime")));
        }

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("pageStart", pageStart);
        queryParams.put("pageSize", size);
        queryParams.put("name", params.get("name"));
        queryParams.put("startTime", startTime);
        queryParams.put("endTime", endTime);

        List<JmeterGenerateTaskEntity> fileRecordsList = jmeterGenerateTaskService.getTaskList(queryParams);
        int count = jmeterGenerateTaskService.getTaskTotal(queryParams);

        Map<String, Object> resMap = new HashMap<>();
        resMap.put("data", fileRecordsList);
        resMap.put("total", count);
        return ResultVo.success(resMap);
    }

    @RequestMapping(value = "/jmeterGenerateTask/generateScript", method = RequestMethod.POST)
    public ResultVo generateScript(@RequestBody Map<String, Object> params) throws IOException {
        jmeterGenerateTaskService.createTask(params);
        return ResultVo.success();
    }

    @RequestMapping(value = "/jmeterGenerateTask/exportScript", method = RequestMethod.POST)
    public void exportScript(HttpServletResponse response, @RequestBody Map<String, Object> params) {
        jmeterGenerateTaskService.exportScript(response, params);
    }
}
