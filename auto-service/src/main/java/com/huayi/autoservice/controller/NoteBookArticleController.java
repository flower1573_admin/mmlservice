package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.NoteBookArticleEntity;
import com.huayi.autoservice.service.NoteBookArticleService;
import com.huayi.autoservice.vo.ResultVo;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class NoteBookArticleController {
    @Autowired
    NoteBookArticleService noteBookArticleService;

    @RequestMapping(value = "/noteBookArticle/getDataList", method = RequestMethod.POST)
    public ResultVo getDataList(@RequestBody Map<String, String> params) {
        int pageIndex = (Integer.parseInt(params.get("page")) - 1) * Integer.parseInt(params.get("size"));
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("fileName", params.get("fileName"));
        queryParams.put("pageIndex", pageIndex);
        queryParams.put("pageSize", params.get("size"));

        Date createTime = null;
        if (StringUtils.isNotEmpty(params.get("createTime"))){
            createTime = new Date(Long.valueOf(params.get("createTime")));
        }

        queryParams.put("createTime", createTime);

        List<NoteBookArticleEntity> dataList = noteBookArticleService.getDataList(queryParams);
        int dataCount = noteBookArticleService.getDataTotal(queryParams);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", dataList);
        resultMap.put("total", dataCount);
        return ResultVo.success(resultMap);
    }

    @RequestMapping(value = "/noteBookArticle/getDataDetails", method = RequestMethod.POST)
    public ResultVo getDataDetails(@RequestBody Map<String, Object> params) {
        NoteBookArticleEntity data = noteBookArticleService.getDataDetails(params);
        return ResultVo.success(data);
    }

    @RequestMapping(value = "/noteBookArticle/createData", method = RequestMethod.POST)
    public ResultVo createData(@RequestBody Map<String, Object> params) {
        noteBookArticleService.createData(params);
        return ResultVo.success();
    }

    @RequestMapping(value = "/noteBookArticle/updateData", method = RequestMethod.POST)
    public ResultVo updateData(@RequestBody Map<String, Object> params) {
        noteBookArticleService.updateData(params);
        return ResultVo.success();
    }

    @RequestMapping(value = "/noteBookArticle/deleteData", method = RequestMethod.POST)
    public ResultVo deleteData(@RequestBody  Map<String, Object> params) {
        noteBookArticleService.deleteData(params);
        return ResultVo.success();
    }
}
