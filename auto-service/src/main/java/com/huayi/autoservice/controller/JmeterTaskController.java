package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.JmeterTaskEntity;
import com.huayi.autoservice.service.JmeterTaskService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.vo.ResultVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class JmeterTaskController {
    @Autowired
    JmeterTaskService jmeterTaskService;

    @RequestMapping(value = "/jmeterTask/getTaskList", method = RequestMethod.POST)
    public ResultVo getTaskList(@RequestBody Map<String, String> params) {
        int pageStart = 0;
        int size = 15;
        if (StringUtils.isNotEmpty(params.get("page")) && StringUtils.isNotEmpty(params.get("size"))) {
            int page = Integer.parseInt(params.get("page"));
            size = Integer.parseInt(params.get("size"));
            pageStart = (page - 1) * size;
        }

        Date startTime = null;
        Date endTime = null;
        if (StringUtils.isNotEmpty(params.get("startTime"))){
            startTime = new Date(Long.valueOf(params.get("startTime")));
        }
        if (StringUtils.isNotEmpty(params.get("endTime"))){
            endTime =new Date(Long.valueOf(params.get("endTime")));
        }

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("pageStart", pageStart);
        queryParams.put("pageSize", size);
        queryParams.put("name", params.get("name"));
        queryParams.put("startTime", startTime);
        queryParams.put("endTime", endTime);

        List<JmeterTaskEntity> fileRecordsList = jmeterTaskService.getTaskList(queryParams);
        int count = jmeterTaskService.getTaskTotal(queryParams);

        Map<String, Object> resMap = new HashMap<>();
        resMap.put("data", fileRecordsList);
        resMap.put("total", count);
        return ResultVo.success(resMap);
    }

    @RequestMapping(value = "/jmeterTask/deleteTask", method = RequestMethod.POST)
    public ResultVo deleteTask(@RequestBody Map<String, Object> params) {
        jmeterTaskService.deleteTask(params);
        return ResultVo.success();
    }
}
