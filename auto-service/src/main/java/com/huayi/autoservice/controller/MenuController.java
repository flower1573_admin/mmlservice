package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.MenuEntity;
import com.huayi.autoservice.entity.RoleEntity;
import com.huayi.autoservice.service.MenuService;
import com.huayi.autoservice.vo.ResultVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MenuController {
    private static Logger logger = LoggerFactory.getLogger(MenuController.class);

    @Autowired
    private MenuService menuService;

    @RequestMapping(value = "/menu/getMenuList",method = RequestMethod.POST)
    public ResultVo getMenuList(){
        List<MenuEntity> menuEntityList = menuService.getAllMenuList();
        return ResultVo.success(menuEntityList);
    }

    @RequestMapping(value = "/menu/getMenuListByRole",method = RequestMethod.POST)
    public ResultVo getMenuListByRole(@RequestBody RoleEntity roleEntity){
        List<MenuEntity> menuEntityList = menuService.getMenuListByRole(roleEntity);
        return ResultVo.success(menuEntityList);
    }
}
