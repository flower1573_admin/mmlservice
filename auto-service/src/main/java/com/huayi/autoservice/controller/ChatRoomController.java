package com.huayi.autoservice.controller;

import com.huayi.autoservice.service.ChatRoomService;
import com.huayi.autoservice.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ChatRoomController {
    @Autowired
    ChatRoomService chatRoomService;

    @RequestMapping(value = "/chatroom/sendMessageToAllUser", method = RequestMethod.POST)
    public ResultVo sendMessageToAllUser(@RequestBody Map<String, String> params) {
        String message = params.get("message");
        chatRoomService.sendMessageToAllUser(message);
        return ResultVo.success();
    }
}
