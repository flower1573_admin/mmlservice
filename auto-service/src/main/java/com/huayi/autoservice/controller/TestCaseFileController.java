package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.TestCaseFileEntity;
import com.huayi.autoservice.service.TestCaseFileService;
import com.huayi.autoservice.vo.ResultVo;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class TestCaseFileController {
    @Autowired
    TestCaseFileService testCaseFileService;

    @RequestMapping(value = "/testCaseFile/getDataList", method = RequestMethod.POST)
    public ResultVo getEthernetInfo(@RequestBody Map<String, String> params) {
        int pageIndex = (Integer.parseInt(params.get("page")) - 1) * Integer.parseInt(params.get("size"));
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("fileName", params.get("fileName"));
        queryParams.put("pageIndex", pageIndex);
        queryParams.put("pageSize", params.get("size"));

        Date createTime = null;
        if (StringUtils.isNotEmpty(params.get("createTime"))){
            createTime = new Date(Long.valueOf(params.get("createTime")));
        }

        queryParams.put("createTime", createTime);

        List<TestCaseFileEntity> dataList = testCaseFileService.getDataList(queryParams);
        int dataCount = testCaseFileService.getDataTotal(queryParams);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", dataList);
        resultMap.put("total", dataCount);
        return ResultVo.success(resultMap);
    }

    @RequestMapping(value = "/testCaseFile/uploadData", method = RequestMethod.POST)
    public ResultVo createData(@RequestBody MultipartFile file)  {
        testCaseFileService.uploadData(file);
        return ResultVo.success();
    }


    @RequestMapping(value = "/testCaseFile/deleteData", method = RequestMethod.POST)
    public ResultVo deleteDate(@RequestBody Map<String, Object> params)  {
        testCaseFileService.deleteData(params);
        return ResultVo.success();
    }
}
