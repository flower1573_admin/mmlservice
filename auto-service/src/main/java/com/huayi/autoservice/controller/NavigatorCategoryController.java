package com.huayi.autoservice.controller;

import com.huayi.autoservice.annotation.AduitLog;
import com.huayi.autoservice.entity.NavigatorCategoryEntity;
import com.huayi.autoservice.service.NavigatorCategoryService;
import com.huayi.autoservice.vo.ResultVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class NavigatorCategoryController {
    private static Logger logger = LoggerFactory.getLogger(NavigatorCategoryController.class);

    @Autowired
    NavigatorCategoryService navigatorCategoryService;

    @RequestMapping(value = "/navigatorCategory/getNavigatorList", method = RequestMethod.POST)
    public ResultVo getNavigatorList(@RequestBody Map<String, String> params) {
        int pageStart = 0;
        int size = 15;
        if (StringUtils.isNotEmpty(params.get("page")) && StringUtils.isNotEmpty(params.get("size"))) {
            int page = Integer.parseInt(params.get("page"));
            size = Integer.parseInt(params.get("size"));
            pageStart = (page - 1) * size;
        }

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("name", params.get("name"));
        queryParams.put("pageStart", pageStart);
        queryParams.put("pageSize", size);

        List<NavigatorCategoryEntity> userList = navigatorCategoryService.getNavigatorList(queryParams);
        int userCont = navigatorCategoryService.getNavigatorTotal(queryParams);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", userList);
        resultMap.put("total", userCont);
        return ResultVo.success(resultMap);
    }

    @RequestMapping(value = "/navigatorCategory/getAllNavigatorList", method = RequestMethod.POST)
    public ResultVo getAllNavigatorList(@RequestBody Map<String, Object> params) {
        List<NavigatorCategoryEntity> userList = navigatorCategoryService.getAllNavigatorList(params);
        return ResultVo.success(userList);
    }

    @AduitLog(moudleCode = "NAVIGATO", clazz = NavigatorCategoryController.class)
    @RequestMapping(value = "/navigatorCategory/createNavigator", method = RequestMethod.POST)
    public ResultVo createUser(@RequestBody Map<String, Object> params) {
        logger.info("start createUser");
        navigatorCategoryService.createNavigator(params);
        return ResultVo.success();
    }

    @AduitLog(moudleCode = "NAVIGATO", clazz = NavigatorCategoryController.class)
    @RequestMapping(value = "/navigatorCategory/updateNavigator", method = RequestMethod.POST)
    public ResultVo updateUser(@RequestBody Map<String, Object> params) {
        logger.info("start updateUser");
        navigatorCategoryService.updateNavigator(params);
        return ResultVo.success();
    }

    @AduitLog(moudleCode = "NAVIGATO2", clazz = NavigatorCategoryController.class)
    @RequestMapping(value = "/navigatorCategory/deleteNavigator", method = RequestMethod.POST)
    public ResultVo deleteNavigator(@RequestBody Map<String, Object> params) {
        navigatorCategoryService.deleteNavigator(params);
        return ResultVo.success();
    }
}
