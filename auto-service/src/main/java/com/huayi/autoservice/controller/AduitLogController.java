package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.AduitLogEntity;
import com.huayi.autoservice.service.AduitLogService;
import com.huayi.autoservice.vo.ResultVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class AduitLogController {
    private static Logger logger = LoggerFactory.getLogger(AduitLogController.class);

    @Autowired
    AduitLogService aduitLogService;

    @RequestMapping(value = "/aduitLog/getAduitLogList", method = RequestMethod.POST)
    public ResultVo getAduitLogList(@RequestBody Map<String, String> params) {
        logger.info("start getAduitLogList");
        int pageStart = 0;
        int size = 15;
        if (StringUtils.isNotEmpty(params.get("page")) && StringUtils.isNotEmpty(params.get("size"))) {
            int page = Integer.parseInt(params.get("page"));
            size = Integer.parseInt(params.get("size"));
            pageStart = (page - 1) * size;
        }

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("pageStart", pageStart);
        queryParams.put("pageSize", size);

        List<AduitLogEntity> aduitLogList = aduitLogService.getAduitLogList(queryParams);
        Integer aduitLogCount = aduitLogService.getAduitLogCount(queryParams);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", aduitLogList);
        resultMap.put("total", aduitLogCount);
        return ResultVo.success(resultMap);
    }
}
