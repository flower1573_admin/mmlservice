package com.huayi.autoservice.controller;

import com.huayi.autoservice.configuration.JWTConfig;
import com.huayi.autoservice.entity.RoleEntity;
import com.huayi.autoservice.entity.UserEntity;
import com.huayi.autoservice.service.RoleService;
import com.huayi.autoservice.service.UserService;
import com.huayi.autoservice.utils.JWTTokenUtil;
import com.huayi.autoservice.utils.ResultUtil;
import com.huayi.autoservice.vo.ResultVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.UnknownHostException;
import java.util.*;


@RestController
public class UserController {
    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/user/loginIn",method = RequestMethod.POST)
    public void loginIn(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String, String> params){
        Map<String,Object> result = new HashMap<>();
        String userName = params.get("userAccount");
        String password = params.get("password");

        UserEntity userQuery = UserEntity.builder().userAccount(userName).build();
        UserEntity userInfo = userService.getUser(userQuery);
        if (userInfo == null) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        if (!new BCryptPasswordEncoder().matches(password, userInfo.getPassword())) {
            throw new BadCredentialsException("密码不正确");
        }

        Set<GrantedAuthority> authorities = new HashSet<>();
        // 查询用户角色

        UserEntity userRoleInfo = UserEntity.builder().id(userInfo.getId()).build();
        List<RoleEntity> sysRoleEntityList = roleService.getRoleListByUser(userRoleInfo);
        for (RoleEntity sysRoleEntity: sysRoleEntityList){
            authorities.add(new SimpleGrantedAuthority("ROLE_" + sysRoleEntity.getRoleName()));
        }
        userInfo.setAuthorities(authorities);
        Authentication authentication = new UsernamePasswordAuthenticationToken(userInfo, password, authorities);

        UserEntity selfUserEntity = (UserEntity) authentication.getPrincipal();
        String token = JWTTokenUtil.createAccessToken(selfUserEntity);
        token = JWTConfig.tokenPrefix + token;
        // 封装返回参数
        Map<String, Object> resultData = new HashMap<>();
        resultData.put("code", 200);
        resultData.put("msg", "登录成功");
        resultData.put("data", token);
        response.addHeader("Authorization", token);
        ResultUtil.responseJson(response, resultData);
    }

    @RequestMapping(value = "/user/getUserInfo",method = RequestMethod.POST)
    public ResultVo getUserInfo(Authentication authentication) throws UnknownHostException {
        if (authentication == null) {
            return ResultVo.error(401, "请登录账号");
        }
        UserEntity userEntity = (UserEntity) authentication.getPrincipal();

        UserEntity queryEntity = UserEntity.builder()
                .id(userEntity.getId())
                .build();
        UserEntity user = userService.getUser(queryEntity);

        UserEntity resultEntity = UserEntity.builder()
                .id(user.getId())
                .username(user.getUsername())
                .userAccount(user.getUserAccount())
                .phone(user.getPhone())
                .email(user.getEmail())
                .remark(user.getRemark())
                .build();
        return ResultVo.success(resultEntity);
    }


    @RequestMapping(value = "/user/getUserList",method = RequestMethod.POST)
    public ResultVo getUserList(@RequestBody Map<String, String> params){
        List<UserEntity> userList = userService.getUserList(params);
        int userCount = userService.getUserCount(params);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", userList);
        resultMap.put("total", userCount);

        return ResultVo.success(resultMap);
    }

    @RequestMapping(value = "/user/getUser",method = RequestMethod.POST)
    public ResultVo getUser(@RequestBody UserEntity params){
        UserEntity userEntity = userService.getUser(params);

        UserEntity res = UserEntity.builder()
                .id(userEntity.getId())
                .username(userEntity.getUsername())
                .userAccount(userEntity.getUserAccount())
                .email(userEntity.getUserAccount())
                .phone(userEntity.getPhone())
                .remark(userEntity.getRemark())
                .createTime(userEntity.getCreateTime())
                .updateTime(userEntity.getUpdateTime())
                .build();
        return ResultVo.success(res);
    }

    @RequestMapping(value = "/user/updateUserInfo",method = RequestMethod.POST)
    public ResultVo updateUserInfo(Authentication authentication, @RequestBody Map<String, String> params) {
        String userId = params.get("id");
        String userName = params.get("userName");
        String email = params.get("email");
        String phone = params.get("phone");
        String userAccount = params.get("userAccount");
        String password = params.get("password");
        if (StringUtils.isNotEmpty(password)) {
            password = new BCryptPasswordEncoder().encode(password);
        }

        UserEntity updateEntity = UserEntity.builder()
                .id(userId)
                .username(userName)
                .userAccount(userAccount)
                .email(email)
                .phone(phone)
                .password(password)
                .build();
        return userService.updateUser(updateEntity);
    }

    @RequestMapping(value = "/user/resetPassword", method = RequestMethod.POST)
    public ResultVo resetPassword(@RequestBody Map<String, String> params){
        ResultVo resultVo = userService.resetPassword(params);
        return ResultVo.success(resultVo);
    }

    @RequestMapping(value = "/user/registerUser", method = RequestMethod.POST)
    public ResultVo registerUser(@RequestBody Map<String, String> params){
        ResultVo resultVo = userService.registerUser(params);
        return ResultVo.success(resultVo);
    }

    @RequestMapping(value = "/user/createUser", method = RequestMethod.POST)
    public ResultVo createUsert(@RequestBody Map<String, Object> params){
        ResultVo resultVo = userService.createUser(params);
        return ResultVo.success(resultVo);
    }

    @RequestMapping(value = "/user/deleteUser", method = RequestMethod.POST)
    public ResultVo deleteUser(@RequestBody Map<String, Object> params){
        ResultVo resultVo = userService.deleteUser(params);
        return ResultVo.success(resultVo);
    }
}
