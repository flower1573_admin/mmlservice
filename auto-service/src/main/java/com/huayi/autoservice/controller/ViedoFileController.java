package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.ViedoFileEntity;
import com.huayi.autoservice.service.ViedoFileService;
import com.huayi.autoservice.vo.ResultVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ViedoFileController {
    @Autowired
    ViedoFileService viedoFileService;

    @RequestMapping(value = "/viedoFile/getDataList", method = RequestMethod.POST)
    public ResultVo getDataList(@RequestBody Map<String, String> params) {
        int pageIndex = (Integer.parseInt(params.get("page")) - 1) * Integer.parseInt(params.get("size"));
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("fileName", params.get("fileName"));
        queryParams.put("pageIndex", pageIndex);
        queryParams.put("pageSize", params.get("size"));

        Date createTime = null;
        if (StringUtils.isNotEmpty(params.get("createTime"))){
            createTime = new Date(Long.valueOf(params.get("createTime")));
        }

        queryParams.put("createTime", createTime);

        List<ViedoFileEntity> dataList = viedoFileService.getDataList(queryParams);
        int dataCount = viedoFileService.getDataTotal(queryParams);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", dataList);
        resultMap.put("total", dataCount);
        return ResultVo.success(resultMap);
    }

    @RequestMapping(value = "/viedoFile/getDataDetails", method = RequestMethod.POST)
    public ResultVo getDataDetails(@RequestBody Map<String, Object> params) {
        Map<String, String> data = viedoFileService.getDataDetails(params);
        return ResultVo.success(data);
    }


    @RequestMapping(value = "/viedoFile/uploadFile", method = RequestMethod.POST)
    public ResultVo uploadFile(@RequestBody MultipartFile file, @RequestParam(name="catalogId") String catalogId, @RequestParam(name="catalogName") String catalogName) {
        viedoFileService.uploadFile(file, catalogId, catalogName);
        return ResultVo.success();
    }

    @RequestMapping(value = "/viedoFile/deleteData", method = RequestMethod.POST)
    public ResultVo deleteData(@RequestBody  Map<String, Object> params) {
        viedoFileService.deleteData(params);
        return ResultVo.success();
    }

}
