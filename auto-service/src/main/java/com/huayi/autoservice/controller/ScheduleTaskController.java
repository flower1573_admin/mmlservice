package com.huayi.autoservice.controller;

import com.huayi.autoservice.annotation.AduitLog;
import com.huayi.autoservice.entity.ScheduleTaskEntity;
import com.huayi.autoservice.service.ScheduleTaskService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.ScheduleTaskUtils;
import com.huayi.autoservice.vo.ResultVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ScheduleTaskController {
    private static Logger logger = LoggerFactory.getLogger(ScheduleTaskController.class);

    @Autowired
    ScheduleTaskService scheduleTaskService;

    @Resource
    private ScheduleTaskUtils scheduleUtils;

    @AduitLog(moudleCode = "ADULT", clazz = UserController.class)
    @RequestMapping(value = "/scheduleTask/getScheduleTaskList", method = RequestMethod.POST)
    public ResultVo getScheduleList(@RequestBody Map<String, String> params) {
        try {
            int pageIndex = (Integer.parseInt(params.get("page")) - 1) * Integer.parseInt(params.get("size"));
            Map<String, Object> queryParams = new HashMap<>();
            queryParams.put("taskName", params.get("name"));
            queryParams.put("cronStatus", params.get("cronStatus"));
            queryParams.put("pageIndex", pageIndex);
            queryParams.put("pageSize", params.get("size"));
            queryParams.put("order", params.get("order"));
            queryParams.put("orderKey", params.get("orderKey"));

            Date startTime = null;
            Date endTime = null;
            if (StringUtils.isNotEmpty(params.get("startTime"))){
                startTime = new Date(Long.valueOf(params.get("startTime")));
            }
            if (StringUtils.isNotEmpty(params.get("endTime"))){
                endTime =new Date(Long.valueOf(params.get("endTime")));
            }
            queryParams.put("startTime", startTime);
            queryParams.put("endTime", endTime);

            List<ScheduleTaskEntity> dataList = scheduleTaskService.getScheduleTaskList(queryParams);
            int dataCount = scheduleTaskService.getScheduleTaskCount(queryParams);

            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("data", dataList);
            resultMap.put("total", dataCount);
            return ResultVo.success(resultMap);
        } catch (Exception e) {
            System.out.println(e);
        }
        return ResultVo.error(400,null);
    }

    /**
     * 创建定时任务
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/scheduleTask/createScheduleTask", method = RequestMethod.POST)
    public ResultVo createSchedu(@RequestBody Map<String, String> params) {
        ScheduleTaskEntity scheduleEntity = ScheduleTaskEntity.builder()
                .id(CommonUtils.getUUID())
                .taskName(params.get("taskName"))
                .taskBean(params.get("taskBean"))
                .taskMethod(params.get("taskMethod"))
                .taskParams(params.get("taskParams"))
                .taskCron(params.get("taskCron"))
                .taskStatus(params.get("taskStatus"))
                .createTime(new Date())
                .updateTime(new Date())
                .remark(params.get("remark"))
                .build();
        scheduleUtils.createTask(scheduleEntity);
        return ResultVo.success();
    }

    /**
     * 更新定时任务
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/scheduleTask/updateScheduleTask", method = RequestMethod.POST)
    public ResultVo updateSchedule(@RequestBody Map<String, String> params) {
        ScheduleTaskEntity scheduleEntity = ScheduleTaskEntity.builder()
                .id(params.get("id"))
                .taskName(params.get("taskName"))
                .taskBean(params.get("taskBean"))
                .taskMethod(params.get("taskMethod"))
                .taskParams(params.get("taskParams"))
                .taskCron(params.get("taskCron"))
                .taskStatus(params.get("taskStatus"))
                .createTime(new Date())
                .updateTime(new Date())
                .remark(params.get("remark"))
                .build();
        scheduleUtils.updateTask(scheduleEntity);
        return ResultVo.success();
    }

    /**
     * 修改定时任务状态
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/scheduleTask/updateScheduleTaskStatus", method = RequestMethod.POST)
    public ResultVo updateScheduleStatus(@RequestBody Map<String, String> params) {
        scheduleUtils.updateTaskStatus(params.get("id"), params.get("taskStatus"));
        return ResultVo.success();
    }

    /**
     * 修改定时任务状态
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/scheduleTask/deleteScheduleTask", method = RequestMethod.POST)
    public ResultVo deleteSchedule(@RequestBody Map<String, String> params) {
        String taskId = params.get("id");
        scheduleUtils.updateTaskStatus(taskId, "disable");

        scheduleUtils.deleteTask(taskId);

        return ResultVo.success();
    }

    @RequestMapping(value = "/scheduleTask/executeScheduleTask", method = RequestMethod.POST)
    public ResultVo executeScheduleTask(@RequestBody Map<String, String> params) {


        return ResultVo.success();
    }
}
