package com.huayi.autoservice.controller;

import com.huayi.autoservice.service.ServeService;
import com.huayi.autoservice.service.SystemInfoService;
import com.huayi.autoservice.vo.ResultVo;
import lombok.RequiredArgsConstructor;
import org.hyperic.sigar.SigarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class ServeController {
    @Autowired
    ServeService serveService;

    @RequestMapping(value = "/serve/getEthernetInfo", method = RequestMethod.POST)
    public ResultVo getEthernetInfo() throws SigarException, UnknownHostException {
        List<Map<String, String>> ethernetInfo = serveService.getEthernetInfo();
        return ResultVo.success(ethernetInfo);
    }

    @RequestMapping(value = "/serve/getDiskInfo", method = RequestMethod.POST)
    public ResultVo getDiskInfo() throws SigarException, UnknownHostException {
        List<Map<String, String>> ethernetInfo = serveService.getDiskInfo();
        return ResultVo.success(ethernetInfo);
    }

    @RequestMapping(value = "/serve/getCpuInfo", method = RequestMethod.POST)
    public ResultVo getCpuInfo() throws SigarException, UnknownHostException {
        List<Map<String, String>> ethernetInfo = serveService.getCpuInfo();
        return ResultVo.success(ethernetInfo);
    }
}
