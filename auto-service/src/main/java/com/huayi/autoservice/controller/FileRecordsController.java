package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.FileRecordsEntity;
import com.huayi.autoservice.service.FileRecordsService;
import com.huayi.autoservice.vo.ResultVo;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.*;

@RestController
@RequiredArgsConstructor
public class FileRecordsController {
    @Autowired
    FileRecordsService fileRecordsService;

    @RequestMapping(value = "/fileRecords/getDataList", method = RequestMethod.POST)
    public ResultVo getDataList(@RequestBody Map<String, String> params) {
        int pageStart = 0;
        int size = 15;
        if (StringUtils.isNotEmpty(params.get("page")) && StringUtils.isNotEmpty(params.get("size"))) {
            int page = Integer.parseInt(params.get("page"));
            size = Integer.parseInt(params.get("size"));
            pageStart = (page - 1) * size;
        }

        Date startTime = null;
        Date endTime = null;
        if (StringUtils.isNotEmpty(params.get("startTime"))){
            startTime = new Date(Long.valueOf(params.get("startTime")));
        }
        if (StringUtils.isNotEmpty(params.get("endTime"))){
            endTime =new Date(Long.valueOf(params.get("endTime")));
        }

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("pageStart", pageStart);
        queryParams.put("pageSize", size);
        queryParams.put("name", params.get("name"));
        queryParams.put("startTime", startTime);
        queryParams.put("endTime", endTime);

        List<FileRecordsEntity> fileRecordsList = fileRecordsService.getFileRecordsList(queryParams);
        int count = fileRecordsService.getFileRecordsCount(queryParams);

        Map<String, Object> resMap = new HashMap<>();
        resMap.put("data", fileRecordsList);
        resMap.put("total", count);
        return ResultVo.success(resMap);
    }

    @RequestMapping(value = "/fileRecords/uploadFile", method = RequestMethod.POST)
    public ResultVo syncFileRecords(@RequestBody Map<String, String> params) {
        try {
            System.out.println(111);
        } catch (Exception e) {
            return ResultVo.error(400, "同步失败");
        }
        return ResultVo.success();
    }

    @RequestMapping(value = "/fileRecords/deleteFile", method = RequestMethod.POST)
    public ResultVo deleteFile(@RequestBody Map<String, Object> params) {
        try {
            fileRecordsService.deleteFileRecords(params);
        } catch (Exception e) {
            return ResultVo.error(400, "删除失败");
        }
        return ResultVo.success();
    }

    @RequestMapping(value = "/fileRecords/getFileLink", method = RequestMethod.POST)
    public ResultVo getFileLink(@RequestBody Map<String, Object> params) {
        String fileLink = fileRecordsService.getFileLink(params);
        return ResultVo.success(fileLink);
    }
}
