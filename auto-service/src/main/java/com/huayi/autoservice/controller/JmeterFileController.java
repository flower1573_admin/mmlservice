package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.FileRecordsEntity;
import com.huayi.autoservice.service.FileCatalogService;
import com.huayi.autoservice.service.JmeterFileService;
import com.huayi.autoservice.utils.JmeterUtils;
import com.huayi.autoservice.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class JmeterFileController {
    @Autowired
    JmeterFileService jmeterFileService;

    @RequestMapping(value = "/jmeterFile/generaFile", method = RequestMethod.POST)
    public ResultVo getDataList(@RequestBody Map<String, Object> params) throws IOException {
        Map<String, String> jmeterParams = new HashMap<>();
        jmeterParams.put("url", "https://blog.51cto.com/u_16175499/11002869");
        jmeterParams.put("method", "GET");
        jmeterParams.put("loop", "1");
        jmeterParams.put("threads", "1");
        jmeterParams.put("rampUp", "1");
/*
        JmeterUtils.gengreatJmxFile(jmeterParams);
*/
        return ResultVo.success();
    }
}
