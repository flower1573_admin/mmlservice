package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.FileBucketsEntity;
import com.huayi.autoservice.service.FileBrowserService;
import com.huayi.autoservice.service.FileBucketsService;
import com.huayi.autoservice.vo.ResultVo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class FileBrowserController {
    private static Logger logger = LoggerFactory.getLogger(FileBrowserController.class);

    @Autowired
    FileBrowserService fileBrowserService;

    @RequestMapping(value = "/fileBrowser/getDataList", method = RequestMethod.POST)
    public ResultVo getDataList(@RequestBody Map<String, String> params) {
        int pageIndex = (Integer.parseInt(params.get("page")) - 1) * Integer.parseInt(params.get("size"));
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("fileName", params.get("fileName"));
        queryParams.put("pageIndex", pageIndex);
        queryParams.put("pageSize", params.get("size"));

        Date createTime = null;
        if (StringUtils.isNotEmpty(params.get("createTime"))){
            createTime = new Date(Long.valueOf(params.get("createTime")));
        }

        queryParams.put("createTime", createTime);

        List<FileBucketsEntity> dataList = fileBrowserService.getDataList(queryParams);
        int dataCount = fileBrowserService.getDataTotal(queryParams);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", dataList);
        resultMap.put("total", dataCount);
        return ResultVo.success(resultMap);
    }

    @RequestMapping(value = "/fileBrowser/createData", method = RequestMethod.POST)
    public ResultVo createData(@RequestBody Map<String, Object> params) {
        logger.info("start fileBuckets create");
        FileBucketsEntity fileBucketsEntity = fileBrowserService.getDataByParams(params);

        if (fileBucketsEntity != null) {
            return ResultVo.error(400, "文件名称重复");
        }

        fileBrowserService.createData(params);
        return ResultVo.success();
    }


    @RequestMapping(value = "/fileBrowser/updateData", method = RequestMethod.POST)
    public ResultVo updateData(@RequestBody Map<String, Object> params) {
        logger.info("start fileBuckets update");
        fileBrowserService.updateData(params);
        return ResultVo.success();
    }

    @RequestMapping(value = "/fileBrowser/deleteData", method = RequestMethod.POST)
    public ResultVo deleteData(@RequestBody  Map<String, Object> params) {
        logger.info("start fileBuckets delete");
        fileBrowserService.deleteData(params);
        return ResultVo.success();
    }

}
