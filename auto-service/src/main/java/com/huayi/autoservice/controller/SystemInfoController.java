package com.huayi.autoservice.controller;

import com.huayi.autoservice.service.SystemInfoService;
import com.huayi.autoservice.vo.ResultVo;
import lombok.RequiredArgsConstructor;
import org.hyperic.sigar.SigarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class SystemInfoController {
    @Autowired
    SystemInfoService systemInfoService;

    @RequestMapping(value = "/systemInfo/getInfos", method = RequestMethod.POST)
    public ResultVo getInfos() throws SigarException, UnknownHostException {
        Map<String, Object> infos = systemInfoService.getInfos();
        return ResultVo.success(infos);
    }
}
