package com.huayi.autoservice.controller;

import com.huayi.autoservice.entity.FileCatalogEntity;
import com.huayi.autoservice.service.FileCatalogService;
import com.huayi.autoservice.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class FileCatalogController {
    @Autowired
    FileCatalogService fileCatalogService;

    @RequestMapping(value = "/fileCatalog/getCatalogList", method = RequestMethod.POST)
    public ResultVo getDataList(@RequestBody Map<String, String> params) {
        Map<String, Object> filterParams = new HashMap<>();
        List<FileCatalogEntity> fileCatalogList = fileCatalogService.getFileCatalogList(filterParams);
        return ResultVo.success(fileCatalogList);
    }

    @RequestMapping(value = "/fileCatalog/createCatalog", method = RequestMethod.POST)
    public ResultVo createCatalog(@RequestBody Map<String, String> params) {
        fileCatalogService.createFileCatalog(params);
        return ResultVo.success();
    }

    @RequestMapping(value = "/fileCatalog/updateCatalog", method = RequestMethod.POST)
    public ResultVo updateCatalog(@RequestBody Map<String, String> params) {
        fileCatalogService.updateCatalog(params);
        return ResultVo.success();
    }

    @RequestMapping(value = "/fileCatalog/deleteCatalog", method = RequestMethod.POST)
    public ResultVo deleteCatalog(@RequestBody Map<String, Object> params) {
        fileCatalogService.deleteCatalog(params);
        return ResultVo.success();
    }
}
