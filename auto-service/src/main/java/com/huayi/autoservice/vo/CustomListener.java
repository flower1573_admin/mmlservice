package com.huayi.autoservice.vo;

import com.alibaba.fastjson.JSON;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.reporters.Summariser;
import org.apache.jmeter.samplers.SampleEvent;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.SampleSaveConfiguration;

public class CustomListener extends ResultCollector {
    public static final String REQUEST_COUNT = "requestCount";
    public CustomListener() {
        super();
    }
    public CustomListener(Summariser summer) {
        super(summer);
    }

    @Override
    public void sampleOccurred(SampleEvent event) {
        super.sampleOccurred(event);
        SampleResult result = event.getResult();
        System.out.println(result);
    }
}
