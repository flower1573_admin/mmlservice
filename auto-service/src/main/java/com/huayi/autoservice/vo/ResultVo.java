package com.huayi.autoservice.vo;

import lombok.*;
import org.springframework.http.HttpStatus;

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ResultVo<T> {
    private Integer code;

    private String message;

    private T data;

    public static ResultVo success() {
        return new ResultVo(HttpStatus.OK.value(), "success", null);
    }

    public static <T> ResultVo<T> success(T data) {
        return new ResultVo<>(HttpStatus.OK.value(), "success", data);
    }

    public static ResultVo error(Integer code, String message) {
        return new ResultVo(code, message, null);
    }
}
