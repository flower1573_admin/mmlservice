package com.huayi.autoservice.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author: jiangjs
 * @description:
 * @date: 2023/10/30 9:36
 **/
@Data
public class MinioPearVo {
    /**
     * 上传Id
     */
    private String uploadId;
    /**
     * 获取分片上传URL
     */
    private List<PearUploadData> parts;

    @Data
    @Accessors(chain = true)
    public static class PearUploadData {
        /**
         * 分片编号，从1开始
         */
        private int parkNum;
        /**
         * 分片上传Url
         */
        private String uploadUrl;
    }

}
