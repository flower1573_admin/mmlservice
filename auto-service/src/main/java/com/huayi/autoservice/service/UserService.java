package com.huayi.autoservice.service;


import com.huayi.autoservice.entity.UserEntity;
import com.huayi.autoservice.vo.ResultVo;

import java.util.List;
import java.util.Map;


public interface UserService {
    UserEntity getUser(UserEntity userEntity);

    List<UserEntity> getUserList(Map<String, String> params);

    int getUserCount(Map<String, String> params);

    ResultVo resetPassword(Map<String, String> params);

    ResultVo registerUser(Map<String, String> params);

    ResultVo createUser(Map<String, Object> params);

    ResultVo updateUser(UserEntity updateEntity);

    ResultVo deleteUser(Map<String, Object> params);
}