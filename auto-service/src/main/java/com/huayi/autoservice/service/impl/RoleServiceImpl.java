package com.huayi.autoservice.service.impl;


import com.huayi.autoservice.entity.RoleEntity;
import com.huayi.autoservice.entity.RoleMenuEntity;
import com.huayi.autoservice.entity.UserEntity;
import com.huayi.autoservice.mapper.RoleMapper;
import com.huayi.autoservice.mapper.RoleMenuMapper;
import com.huayi.autoservice.service.RoleService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.TransactionManagerBuilder;
import com.huayi.autoservice.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import java.util.*;

@Service("RoleServiceImpl")
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleMapper roleMapper;

    @Autowired
    RoleMenuMapper roleMenuMapper;


    @Override
    public List<RoleEntity> getRoleListByUser(UserEntity userRoleInfo) {
        return roleMapper.getRoleListByUser(userRoleInfo);
    }

    @Override
    public List<RoleEntity> getAllRoleList(Map<String, String> params) {
        return roleMapper.getAllRoleList(params);
    }

    @Override
    public List<RoleEntity> getRoleLis(Map<String, String> params) {
        List<RoleEntity> dataList = roleMapper.getRoleList(params);
        return dataList;
    }

    @Override
    public int getRoleCount(Map<String, String> params) {
        return roleMapper.getRoleCount(params);
    }

    @Override
    public RoleEntity getRole(RoleEntity roleEntity) {
        return roleMapper.getRole(roleEntity);
    }

    @Override
    public ResultVo createRole(Map<String, Object> params) {
        String roleId = CommonUtils.getUUID();
        RoleEntity roleEntity  = RoleEntity.builder()
                .id(roleId)
                .remark(params.get("remark").toString())
                .createTime(new Date())
                .updateTime(new Date())
                .roleName(params.get("roleName").toString())
                .build();

        List<String> menuIdList = CommonUtils.castObjectToList(params.get("menuIdList"), String.class);

        List<RoleMenuEntity> roleMenuEntityList = new ArrayList<>();
        for (int i = 0; i < menuIdList.size(); i++) {
            RoleMenuEntity roleMenuEntity = RoleMenuEntity.builder()
                    .id(CommonUtils.getUUID())
                    .menuId(menuIdList.get(i))
                    .roleId(roleId)
                    .build();
            roleMenuEntityList.add(roleMenuEntity);
        }

        TransactionManagerBuilder transactionManagerBuilder = TransactionManagerBuilder.getTransactionContext();

        TransactionStatus transactionStatus = transactionManagerBuilder.createTransactionStatus("transaction_createRole");

        try {
            roleMapper.createRole(roleEntity);
            roleMenuMapper.createRoleMenu(roleMenuEntityList);
            transactionManagerBuilder.commit(transactionStatus);
        } catch (Exception exception) {
            transactionManagerBuilder.rollback(transactionStatus);
            return ResultVo.error(400, exception.getMessage());
        }
        return ResultVo.success();
    }

    @Override
    public ResultVo updateRole(RoleEntity params) {
        RoleEntity roleEntity  = RoleEntity.builder()
                .id(params.getId())
                .updateTime(new Date())
                .roleName(params.getRoleName())
                .remark(params.getRemark())
                .build();
        roleMapper.updateRole(roleEntity);
        return ResultVo.success();    }

    @Override
    public int deleteRole(List<String> idList) {
        return roleMapper.deleteRole(idList);
    }
}
