package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.NavigatorCategoryEntity;
import com.huayi.autoservice.mapper.NavigatorCategoryMapper;
import com.huayi.autoservice.service.NavigatorCategoryService;
import com.huayi.autoservice.utils.CommonUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("NavigatorCategoryServiceImpl")
public class NavigatorCategoryServiceImpl implements NavigatorCategoryService {
    @Resource
    private NavigatorCategoryMapper navigatorCategoryMapper;

    @Override
    public List<NavigatorCategoryEntity> getNavigatorList(Map<String, Object> queryParams) {
        return navigatorCategoryMapper.getNavigatorList(queryParams);
    }

    @Override
    public List<NavigatorCategoryEntity> getAllNavigatorList(Map<String, Object> queryParams) {
        return navigatorCategoryMapper.getAllNavigatorList(queryParams);
    }

    @Override
    public int getNavigatorTotal(Map<String, Object> queryParams) {
        return navigatorCategoryMapper.getNavigatorTotal(queryParams);
    }

    @Override
    public int createNavigator(Map<String, Object> queryParams) {
        queryParams.put("id", CommonUtils.getUUID("navigator"));
        queryParams.put("createTime", new Date());
        queryParams.put("updateTime", new Date());
        return navigatorCategoryMapper.createNavigator(queryParams);
    }

    @Override
    public int deleteNavigator(Map<String, Object> queryParams) {
        return navigatorCategoryMapper.deleteNavigator(queryParams);
    }

    @Override
    public int updateNavigator(Map<String, Object> queryParams) {
        return navigatorCategoryMapper.updateNavigator(queryParams);
    }
}
