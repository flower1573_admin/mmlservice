package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.NavigatorCategoryEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface NavigatorCategoryService {
    List<NavigatorCategoryEntity> getNavigatorList(Map<String, Object> queryParams);

    List<NavigatorCategoryEntity> getAllNavigatorList(Map<String, Object> queryParams);

    int getNavigatorTotal(Map<String, Object> queryParams);

    int createNavigator(Map<String, Object> queryParams);

    int deleteNavigator(Map<String, Object> queryParams);

    int updateNavigator(Map<String, Object> queryParams);

}
