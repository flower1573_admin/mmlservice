package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.ViedoFileEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface ViedoFileService {
    List<ViedoFileEntity> getDataList(Map<String, Object> params);

    Map<String, String> getDataDetails(Map<String, Object> params);

    int getDataTotal(Map<String, Object> params);

    void uploadFile(@RequestBody MultipartFile file, String catalogId, String cacalogName);

    void deleteData(Map<String, Object> params);
}
