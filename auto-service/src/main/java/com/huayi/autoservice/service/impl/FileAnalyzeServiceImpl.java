package com.huayi.autoservice.service.impl;


import com.huayi.autoservice.entity.FileAnalyzeEntity;
import com.huayi.autoservice.entity.FileBucketsEntity;
import com.huayi.autoservice.mapper.FileAnalyzeMapper;
import com.huayi.autoservice.service.FileAnalyzeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("FileAnalyzeServiceImpl")
public class FileAnalyzeServiceImpl implements FileAnalyzeService {
    @Resource
    private FileAnalyzeMapper fileAnalyzeMapper;

    @Override
    public Map<String, Object> getAnalyzeInfos() {
        List<FileBucketsEntity> bucketsList = fileAnalyzeMapper.getBucketsList();
        List<FileAnalyzeEntity> downloadRecords = fileAnalyzeMapper.getDownloadRecords();
        List<FileAnalyzeEntity> uploadRecords = fileAnalyzeMapper.getUploadRecords();
        List<FileAnalyzeEntity> fileTypeList = fileAnalyzeMapper.getFileTypeList();
        Map<String, Object> resMap = new HashMap<>();
        resMap.put("bucketsList", bucketsList);
        resMap.put("downloadRecords", downloadRecords);
        resMap.put("uploadRecords", uploadRecords);
        resMap.put("fileTypeList", fileTypeList);
        return resMap;
    }
}
