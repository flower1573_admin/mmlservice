package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.DictionaryEntity;
import com.huayi.autoservice.mapper.DictionaryMapper;
import com.huayi.autoservice.service.DictionaryService;
import com.huayi.autoservice.utils.CommonUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("DictionaryServiceImpl")
public class DictionaryServiceImpl implements DictionaryService {
    @Resource
    private DictionaryMapper dictionaryMapper;


    @Override
    public List<DictionaryEntity> getTaskList(Map<String, Object> queryParams) {
        return dictionaryMapper.getTaskList(queryParams);
    }

    @Override
    public int getTaskTotal(Map<String, Object> queryParams) {
        return dictionaryMapper.getTaskTotal(queryParams);
    }

    @Override
    public int createTask(Map<String, Object> queryParams) {
        queryParams.put("id", CommonUtils.getUUID("dictionary"));
        queryParams.put("createTime", new Date());
        queryParams.put("updateTime", new Date());
        return dictionaryMapper.createTask(queryParams);
    }

    @Override
    public int deleteTask(Map<String, Object> queryParams) {
        return dictionaryMapper.deleteTask(queryParams);
    }

    @Override
    public int updateTask(Map<String, Object> queryParams) {
        queryParams.put("updateTime", new Date());
        return dictionaryMapper.updateTask(queryParams);
    }
}
