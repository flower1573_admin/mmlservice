package com.huayi.autoservice.service;



import com.huayi.autoservice.entity.RoleEntity;
import com.huayi.autoservice.entity.UserEntity;
import com.huayi.autoservice.vo.ResultVo;

import java.util.List;
import java.util.Map;

/**
 * @Description 角色业务接口
 * @Author Sans
 * @CreateTime 2019/9/14 15:57
 */
public interface RoleService {
    List<RoleEntity> getRoleListByUser(UserEntity userRoleInfo);

    List<RoleEntity> getAllRoleList(Map<String, String> params);

    List<RoleEntity> getRoleLis(Map<String, String> params);

    int getRoleCount(Map<String, String> params);

    RoleEntity getRole(RoleEntity roleEntity);

    ResultVo createRole(Map<String, Object> params);

    ResultVo updateRole(RoleEntity roleEntity);

    int deleteRole(List<String> idList);
}