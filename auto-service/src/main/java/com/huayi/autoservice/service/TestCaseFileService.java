package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.TestCaseFileEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface TestCaseFileService {
    List<TestCaseFileEntity> getDataList(Map<String, Object> params);

    int getDataTotal(Map<String, Object> params);

    void uploadData(MultipartFile file);

    void deleteData(Map<String, Object> params);
}
