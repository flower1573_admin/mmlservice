package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.FileRecordsEntity;
import org.hyperic.sigar.SigarException;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

public interface SystemInfoService {
    Map<String, Object> getInfos() throws UnknownHostException, SigarException;
}
