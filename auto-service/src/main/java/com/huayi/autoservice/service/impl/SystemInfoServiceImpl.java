package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.service.SystemInfoService;
import com.huayi.autoservice.utils.SystemInfoUtils;
import org.hyperic.sigar.SigarException;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("SystemInfoServiceImpl")
public class SystemInfoServiceImpl implements SystemInfoService {
    @Override
    public Map<String, Object> getInfos() throws UnknownHostException, SigarException {
        Map<String, Object> resMap = new HashMap<>();
        Map<String, String> inetInfo = SystemInfoUtils.getInetInfo();
        List<Map<String, String>> cpuUsage = SystemInfoUtils.getCpuUsage();
        Map<String, String> memoryUsage = SystemInfoUtils.getMemoryUsage();
        List<Map<String, String>> diskUsage = SystemInfoUtils.getDiskUsage();
        Map<String, String> osInfos = SystemInfoUtils.getOsInfos();
        List<Map<String, String>> psList = SystemInfoUtils.getPsList();
        List<Map<String, String>> ethernetList = SystemInfoUtils.getEthernet();

        resMap.put("inetInfo", inetInfo);
        resMap.put("cpuUsage", cpuUsage);
        resMap.put("memoryUsage", memoryUsage);
        resMap.put("diskUsage", diskUsage);
        resMap.put("osInfos", osInfos);
        resMap.put("psList", psList);
        resMap.put("ethernetList", ethernetList);
        return resMap;
    }
}
