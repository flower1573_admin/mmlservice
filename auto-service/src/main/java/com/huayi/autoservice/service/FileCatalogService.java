package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.FileCatalogEntity;

import java.util.List;
import java.util.Map;

public interface FileCatalogService {
    List<FileCatalogEntity> getFileCatalogList(Map<String, Object> params);

    void createFileCatalog(Map<String, String> filterParams);

    void updateCatalog(Map<String, String> filterParams);

    void deleteCatalog(Map<String, Object> params);
}
