package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.JmeterGenerateTaskEntity;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface JmeterGenerateTaskService {
    List<JmeterGenerateTaskEntity> getTaskList(Map<String, Object> queryParams);

    int getTaskTotal(Map<String, Object> queryParams);

    int createTask(Map<String, Object> queryParams) throws IOException;

    void exportScript(HttpServletResponse response, Map<String, Object> params);

    int deleteTask(Map<String, Object> queryParams);
}
