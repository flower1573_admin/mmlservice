package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.NavigatorEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface NavigatorService {
    List<NavigatorEntity> getNavigatorList(Map<String, Object> queryParams);

    int getNavigatorTotal(Map<String, Object> queryParams);

    int createNavigator(Map<String, Object> queryParams);

    int deleteNavigator(Map<String, Object> queryParams);

    int updateNavigator(Map<String, Object> queryParams);

    void exportNavigator(HttpServletResponse response, Map<String, Object> queryParams) throws IOException;
}
