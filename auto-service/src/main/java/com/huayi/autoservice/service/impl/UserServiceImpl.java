package com.huayi.autoservice.service.impl;


import com.huayi.autoservice.entity.UserEntity;
import com.huayi.autoservice.entity.UserRoleEntity;
import com.huayi.autoservice.mapper.UserMapper;
import com.huayi.autoservice.mapper.UserRoleMapper;
import com.huayi.autoservice.service.UserService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.TransactionManagerBuilder;
import com.huayi.autoservice.vo.ResultVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import java.util.*;

@Service("UserServiceImpl")
public class UserServiceImpl implements UserService {
    private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserRoleMapper userRoleMapper;

    @Override
    public UserEntity getUser(UserEntity userEntity) {
        return userMapper.getUser(userEntity);
    }

    @Override
    public List<UserEntity> getUserList(Map<String, String> params) {
        List<UserEntity> dataList = userMapper.getUserList(params);
        return dataList;
    }

    @Override
    public int getUserCount(Map<String, String> params) {
        return userMapper.getUserCount(params);
    }

    @Override
    public ResultVo resetPassword(Map<String, String> params) {
        Map<String, Object> result = new HashMap<>();

        UserEntity checkEntity = UserEntity.builder()
                .username(params.get("accountName"))
                .build();
        UserEntity user = userMapper.getUser(checkEntity);
        if (user == null) {
            return ResultVo.error(400, "账号不存在");
        }

        String password = new BCryptPasswordEncoder().encode(params.get("accountPassword").toString());
        UserEntity updateEntity = UserEntity.builder()
                .id(user.getId())
                .password(password)
                .updateTime(new Date())
                .build();
        userMapper.updateUser(updateEntity);

        return ResultVo.success(result);
    }

    @Override
    public ResultVo registerUser(Map<String, String> params) {
        UserEntity checkEntity = UserEntity.builder()
                .username(params.get("userAccount"))
                .build();
        UserEntity user = userMapper.getUser(checkEntity);
        if (user != null) {
            return ResultVo.error(400, "用户名重复");
        }

        String password = new BCryptPasswordEncoder().encode(params.get("password").toString());
        UserEntity userEntity  = UserEntity.builder()
                .id(CommonUtils.getUUID("user_"))
                .username(params.get("userName"))
                .userAccount(params.get("userAccount"))
                .password(password)
                .email(params.get("email"))
                .phone(params.get("phone"))
                .createTime(new Date())
                .updateTime(new Date())
                .build();
        userMapper.registerUser(userEntity);

        return ResultVo.success();
    }

    @Override
    public ResultVo createUser(Map<String, Object> params) {
        logger.info("createUser start");

        String userPassword = new BCryptPasswordEncoder().encode(params.get("password").toString());
        String userId = CommonUtils.getUUID();

        UserEntity userEntity  = UserEntity.builder()
                .id(userId)
                .username(params.get("userName").toString())
                .userAccount(params.get("userAccount").toString())
                .password(userPassword)
                .email(params.get("email").toString())
                .phone(params.get("phone").toString())
                .phone(params.get("remark").toString())
                .createTime(new Date())
                .updateTime(new Date())
                .build();

        List<String> userRoleIdList = CommonUtils.castObjectToList(params.get("roleIdList"), String.class);

        List<UserRoleEntity> userRoleList = new ArrayList<>();
        for (int i = 0; i < userRoleIdList.size(); i++) {
            UserRoleEntity roleEntity = new UserRoleEntity();
            roleEntity.setId(CommonUtils.getUUID());
            roleEntity.setUserId(userId);
            roleEntity.setRoleId(userRoleIdList.get(i));
            userRoleList.add(roleEntity);
        }

        TransactionManagerBuilder transactionManagerBuilder = TransactionManagerBuilder.getTransactionContext();

        TransactionStatus transactionStatus = transactionManagerBuilder.createTransactionStatus("transaction_createUser");

        try {
            userMapper.createUser(userEntity);

            userRoleMapper.createUserRole(userRoleList);
            transactionManagerBuilder.commit(transactionStatus);
            logger.info("createUser success");
        } catch (Exception exception) {
            logger.error("createUser fail");
            transactionManagerBuilder.rollback(transactionStatus);
            return ResultVo.error(400, exception.getMessage());
        }

        return ResultVo.success();
    }

    @Override
    public ResultVo updateUser(UserEntity updateEntity) {
        try {
            userMapper.updateUser(updateEntity);
            ResultVo.success();
        } catch (Exception exception) {
            logger.error("updateUser fail");
            return ResultVo.error(400, exception.getMessage());
        }
        return ResultVo.success();
    }

    @Override
    public ResultVo deleteUser(Map<String, Object> params) {
        List<String> idList = CommonUtils.castObjectToList(params.get("data"), String.class);
        userMapper.deleteUser(idList);
        return ResultVo.success();
    }
}
