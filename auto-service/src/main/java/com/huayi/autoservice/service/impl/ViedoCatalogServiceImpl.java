package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.TestCaseFileEntity;
import com.huayi.autoservice.entity.ViedoCatalogEntity;
import com.huayi.autoservice.mapper.ViedoCatalogMapper;
import com.huayi.autoservice.service.ViedoCatalogService;
import com.huayi.autoservice.utils.CommonUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("ViedoCatalogServiceImpl")
public class ViedoCatalogServiceImpl implements ViedoCatalogService {
    @Resource
    private ViedoCatalogMapper viedoCatalogMapper;

    @Override
    public List<ViedoCatalogEntity> getDataList(Map<String, Object> params) {
        return viedoCatalogMapper.getDataList(params);
    }

    @Override
    public List<ViedoCatalogEntity> getAllDataList(Map<String, Object> params) {
        return viedoCatalogMapper.getAllDataList(params);
    }

    @Override
    public int getDataTotal(Map<String, Object> params) {
        return viedoCatalogMapper.getDataTotal(params);
    }

    @Override
    public void createData(Map<String, Object> params) {
        params.put("id", CommonUtils.getUUID());
        params.put("createTime", new Date());
        params.put("updateTime", new Date());
        viedoCatalogMapper.createData(params);
    }

    @Override
    public void updateData(Map<String, Object> params) {
        params.put("updateTime", new Date());
        viedoCatalogMapper.updateData(params);
    }

    @Override
    public void deleteData(Map<String, Object> params) {
        viedoCatalogMapper.deleteData(params);
    }
}
