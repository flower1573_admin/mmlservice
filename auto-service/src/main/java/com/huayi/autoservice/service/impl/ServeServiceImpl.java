package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.service.ServeService;
import com.huayi.autoservice.utils.SystemInfoUtils;
import org.hyperic.sigar.SigarException;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

@Service("ServeServiceImpl")
public class ServeServiceImpl implements ServeService {
    @Override
    public List<Map<String, String>> getEthernetInfo() throws SigarException {
        List<Map<String, String>> ethernet = SystemInfoUtils.getEthernet();
        return ethernet;
    }

    @Override
    public List<Map<String, String>> getDiskInfo() throws UnknownHostException, SigarException {
        List<Map<String, String>> diskUsage = SystemInfoUtils.getDiskUsage();
        return diskUsage;
    }

    @Override
    public List<Map<String, String>> getCpuInfo() throws UnknownHostException, SigarException {
        List<Map<String, String>> cpuInfo = SystemInfoUtils.getCpuUsage();
        return cpuInfo;
    }
}
