package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.FileCatalogEntity;
import com.huayi.autoservice.entity.FileRecordsEntity;
import com.huayi.autoservice.mapper.FileCatalogMapper;
import com.huayi.autoservice.mapper.FileRecordsMapper;
import com.huayi.autoservice.service.FileRecordsService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.MinioPearUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("FileRecordsServiceImpl")
public class FileRecordsServiceImpl implements FileRecordsService {
    @Autowired
    FileRecordsMapper fileRecordsMapper;

    @Resource
    private MinioPearUploadUtil minioPearUploadUtil;

    @Override
    public int getFileRecordsCount(Map<String, Object> params) {
        return fileRecordsMapper.getFileRecordsCount(params);
    }

    @Override
    public List<FileRecordsEntity> getFileRecordsList(Map<String, Object> params) {
        return fileRecordsMapper.getFileRecordsList(params);
    }

    @Override
    public void createFileRecords(FileRecordsEntity fileRecordsEntity) {
        fileRecordsMapper.createFileRecords(fileRecordsEntity);
    }

    @Override
    public void deleteFileRecords(Map<String, Object> params) {
        List<FileRecordsEntity> allFileRecordsList = fileRecordsMapper.getAllFileRecordsList(params);
        List<String> nameList = allFileRecordsList.stream()
                .map(FileRecordsEntity::getName)
                .collect(Collectors.toList());

        minioPearUploadUtil.batchRemoveFiles("email", nameList);
        fileRecordsMapper.deleteFileRecords(params);
    }

    @Override
    public String getFileLink(Map<String, Object> params) {
        String fileName = String.valueOf(params.get("fileNam"));
        String fileLink = minioPearUploadUtil.getPresignedObjectUrl("email", fileName);
        return fileLink;
    }
}
