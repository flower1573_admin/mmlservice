package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.ScheduleTaskEntity;
import com.huayi.autoservice.mapper.ScheduleTaskMapper;
import com.huayi.autoservice.service.ScheduleTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("ScheduleTaskServiceImpl")
public class ScheduleTaskServiceImpl implements ScheduleTaskService {
    @Autowired
    ScheduleTaskMapper scheduleTaskMapper;

    @Override
    public int getScheduleTaskCount(Map<String, Object> params) {
        return scheduleTaskMapper.getScheduleTaskCount(params);
    }

    @Override
    public List<ScheduleTaskEntity> getScheduleTaskList(Map<String, Object> params) {
        return scheduleTaskMapper.getScheduleTaskList(params);
    }

    @Override
    public ScheduleTaskEntity getScheduleTaskSchedule(ScheduleTaskEntity scheduleEntity) {
        return scheduleTaskMapper.getScheduleTaskSchedule(scheduleEntity);
    }

    @Override
    public int createScheduleTask(ScheduleTaskEntity scheduleEntity) {
        return scheduleTaskMapper.createScheduleTask(scheduleEntity);
    }

    @Override
    public int updateScheduleTask(ScheduleTaskEntity scheduleEntity) {
        return scheduleTaskMapper.updateScheduleTask(scheduleEntity);
    }

    @Override
    public int deleteScheduleTask(List<String> params) {
        return scheduleTaskMapper.deleteScheduleTask(params);
    }
}
