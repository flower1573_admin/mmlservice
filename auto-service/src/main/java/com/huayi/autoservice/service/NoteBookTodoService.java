package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.NoteBookArticleEntity;

import java.util.List;
import java.util.Map;

public interface NoteBookTodoService {
    List<NoteBookArticleEntity> getDataList(Map<String, Object> params);

    NoteBookArticleEntity getDataDetails(Map<String, Object> params);

    int getDataTotal(Map<String, Object> params);

    void createData(Map<String, Object> params);

    void updateData(Map<String, Object> params);

    void deleteData(Map<String, Object> params);
}
