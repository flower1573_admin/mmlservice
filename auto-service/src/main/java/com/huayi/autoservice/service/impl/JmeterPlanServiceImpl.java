package com.huayi.autoservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.huayi.autoservice.entity.JmeterPlanEntity;
import com.huayi.autoservice.entity.ScheduleTaskEntity;
import com.huayi.autoservice.mapper.JmeterPlanMapper;
import com.huayi.autoservice.mapper.JmeterTaskMapper;
import com.huayi.autoservice.service.JmeterPlanService;
import com.huayi.autoservice.utils.*;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor(onConstructor = @__(@Autowired))
@Service("JmeterPlanServiceImpl")
public class JmeterPlanServiceImpl implements JmeterPlanService {
    private final MinioPearUploadUtil minioPearUploadUtil;

    @Resource
    private ScheduleTaskUtils scheduleUtils;

    @Resource
    private JmeterPlanMapper jmeterPlanMapper;

    @Resource
    private JmeterTaskMapper jmeterTaskMapper;


    @Override
    public List<JmeterPlanEntity> getTaskList(Map<String, Object> queryParams) {
        return jmeterPlanMapper.getTaskList(queryParams);
    }

    @Override
    public int getTaskTotal(Map<String, Object> queryParams) {
        return jmeterPlanMapper.getTaskTotal(queryParams);
    }

    @Override
    public int createTask(Map<String, Object> queryParams) {
        queryParams.put("id", CommonUtils.getUUID());
        queryParams.put("createTime", new Date());
        queryParams.put("updateTime", new Date());
        return jmeterPlanMapper.createTask(queryParams);
    }

    @Override
    public int deleteTask(Map<String, Object> queryParams) {
        return jmeterPlanMapper.deleteTask(queryParams);
    }

    @Override
    public int updateTask(Map<String, Object> queryParams) {
        return jmeterPlanMapper.updateTask(queryParams);
    }

    @Override
    public void executePlan(Map<String, Object> queryParams) {
        String planName = String.valueOf(queryParams.get("planName"));
        String taskType = String.valueOf(queryParams.get("type"));
        String scriptName = String.valueOf(queryParams.get("scriptName"));
        Map<String, Object> taskParams = new HashMap<>();

        String  taskCron= "";

        if (StringUtils.equals("date", taskType)){
            String executeTime = String.valueOf(queryParams.get("executeTime"));
            SimpleDateFormat dateFormat = new SimpleDateFormat("ss mm HH dd MM ? yyyy");
            taskCron = dateFormat.format(new Date(Long.valueOf(executeTime)));
        } else {
            taskCron = String.valueOf(queryParams.get("executeCron"));
        }

        JmeterPlanEntity  jmeterPlanEntity = JmeterPlanEntity.builder()
                .scriptName(scriptName)
                .planName(planName)
                .build();

        String params = JSON.toJSONString(jmeterPlanEntity);
        ScheduleTaskEntity scheduleEntity = ScheduleTaskEntity.builder()
                .id(CommonUtils.getUUID())
                .taskName(planName)
                .taskBean("JmeterScheduleTask")
                .taskMethod("POST")
                .taskParams(params)
                .taskCron(taskCron)
                .taskStatus("enable")
                .createTime(new Date())
                .updateTime(new Date())
                .remark("jmeter定时任务")
                .build();
        scheduleUtils.createTask(scheduleEntity);

        taskParams.put("id", CommonUtils.getUUID());
        taskParams.put("taskName", planName);
        taskParams.put("taskStatus", "waitting");
        taskParams.put("taskProgress", "0");
        jmeterTaskMapper.createTask(taskParams);
    }

    @Override
    public void executeTask(String string) throws IOException {
        JmeterPlanEntity jmeterPlanEntity = JSON.parseObject(string, JmeterPlanEntity.class);
        String scriptName = jmeterPlanEntity.getScriptName();

        String bucketName = "email";
        InputStream inputStream = minioPearUploadUtil.downLoadFile(scriptName, bucketName);

        String jmxPath = "G:\\" + scriptName;

        File tempFile = new File(jmxPath);
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(inputStream, out);
        }

        String timeString =  CommonUtils.getTimeString();

        String jtlPath = "G:\\" + timeString + "\\jtl";
        String reportPath = "G:\\" + timeString + "\\report";

        File jmxDir = new File(jtlPath);
        if (!jmxDir.exists()) {
            jmxDir.mkdirs();
        }

        String command = JMeterUtils.getJMeterBinDir() + "/jmeter -n -t " + jmxPath + " -l" + jtlPath + "replay_result.jtl" + " -e -o " + reportPath;
        Runtime.getRuntime().exec(command);

        String commandPath = "G:\\" + timeString + ".bat";

        try (PrintWriter writer = new PrintWriter(new FileWriter(commandPath))) {
            writer.println(command);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Process process = Runtime.getRuntime().exec("cmd /c start " + commandPath);
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        Runtime run = Runtime.getRuntime();
        try {
            Process process = run.exec("cmd.exe /k start " + commandPath);
            InputStream in = process.getInputStream();
            while (in.read() != -1) {
                System.out.println(in.read());
            }
            in.close();
            process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }
}
