package com.huayi.autoservice.service;


import com.huayi.autoservice.enmus.MinioBucketEnum;
import com.huayi.autoservice.vo.MinioPearVo;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    MinioPearVo createMultipartUploadUrl(Integer chunkNum, String fileName, String contentType);
    Boolean completeMultipart(Integer chunkNum, String fileName, String contentType,String uploadId,String fileMd5);

    void uploadSingleFile(MultipartFile file);
}
