package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.FileCatalogEntity;
import com.huayi.autoservice.mapper.FileCatalogMapper;
import com.huayi.autoservice.mapper.FileRecordsMapper;
import com.huayi.autoservice.service.FileCatalogService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.MinioPearUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("FileCatalogServiceImpl")
public class FileCatalogServiceImpl implements FileCatalogService {
    @Autowired
    FileCatalogMapper fileCatalogMapper;

    @Autowired
    FileRecordsMapper fileRecordsMapper;

    @Resource
    private MinioPearUploadUtil minioPearUploadUtil;

    @Override
    public List<FileCatalogEntity> getFileCatalogList(Map<String, Object> params) {
        return fileCatalogMapper.getFileCatalogList(params);
    }

    @Override
    public void createFileCatalog(Map<String, String> filterParams) {
        String catalogName = filterParams.get("catalogName");
        String catalogCode = filterParams.get("catalogCode");

        minioPearUploadUtil.createBucket(catalogCode);

        FileCatalogEntity fileCatalogEntity = FileCatalogEntity.builder()
                .id(CommonUtils.getUUID("file"))
                .name(catalogName)
                .code(catalogCode)
                .createTime(new Date())
                .updateTime(new Date())
                .build();
        fileCatalogMapper.createFileCatalog(fileCatalogEntity);
    }

    @Override
    public void updateCatalog(Map<String, String> filterParams) {
        String catalogName = filterParams.get("catalogName");
        String id = filterParams.get("id");

        FileCatalogEntity fileCatalogEntity = FileCatalogEntity.builder()
                .id(id)
                .name(catalogName)
                .updateTime(new Date())
                .build();
        fileCatalogMapper.updateCatalog(fileCatalogEntity);
    }

    @Override
    public void deleteCatalog(Map<String, Object> params) {
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("idList", params.get("idList"));

        List<FileCatalogEntity> allFileCatalogList = fileCatalogMapper.getAllFileCatalogList(queryParams);
        List<String> codeList = allFileCatalogList.stream()
                .map(FileCatalogEntity::getCode)
                .collect(Collectors.toList());

        for(String item: codeList) {
            minioPearUploadUtil.deleteBucket(item);
        }

        fileCatalogMapper.deleteFileCatalog(params);

        Map<String, Object> catalogMap = new HashMap<>();
        catalogMap.put("idList", codeList);

        fileRecordsMapper.deleteByCatalogCode(catalogMap);
    }
}
