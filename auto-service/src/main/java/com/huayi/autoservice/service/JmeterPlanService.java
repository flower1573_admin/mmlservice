package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.JmeterPlanEntity;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface JmeterPlanService {
    List<JmeterPlanEntity> getTaskList(Map<String, Object> queryParams);

    int getTaskTotal(Map<String, Object> queryParams);

    int createTask(Map<String, Object> queryParams);

    int deleteTask(Map<String, Object> queryParams);

    int updateTask(Map<String, Object> queryParams);

    void executePlan(Map<String, Object> queryParams);

    void executeTask(String string) throws IOException;
}
