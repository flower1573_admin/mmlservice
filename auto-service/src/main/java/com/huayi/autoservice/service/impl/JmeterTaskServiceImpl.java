package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.JmeterTaskEntity;
import com.huayi.autoservice.mapper.JmeterTaskMapper;
import com.huayi.autoservice.service.JmeterTaskService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("JmeterTaskServiceImpl")
public class JmeterTaskServiceImpl implements JmeterTaskService {
    @Resource
    private JmeterTaskMapper jmeterTaskMapper;

    @Override
    public List<JmeterTaskEntity> getTaskList(Map<String, Object> queryParams) {
        return jmeterTaskMapper.getTaskList(queryParams);
    }

    @Override
    public int getTaskTotal(Map<String, Object> queryParams) {
        return jmeterTaskMapper.getTaskTotal(queryParams);
    }

    @Override
    public int createTask(Map<String, Object> queryParams) {
        return jmeterTaskMapper.createTask(queryParams);
    }

    @Override
    public int deleteTask(Map<String, Object> queryParams) {
        return jmeterTaskMapper.deleteTask(queryParams);
    }

    @Override
    public int updateTask(Map<String, Object> queryParams) {
        return jmeterTaskMapper.updateTask(queryParams);
    }
}
