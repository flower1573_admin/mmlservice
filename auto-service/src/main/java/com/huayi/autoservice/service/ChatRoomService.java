package com.huayi.autoservice.service;

public interface ChatRoomService {
    void sendMessageToAllUser(String message);
}
