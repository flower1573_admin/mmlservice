package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.controller.FileBucketsController;
import com.huayi.autoservice.entity.FileBucketsEntity;
import com.huayi.autoservice.mapper.FileBucketsMapper;
import com.huayi.autoservice.service.FileBucketsService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.MinioPearUploadUtil;
import com.huayi.autoservice.utils.TransactionManagerBuilder;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import javax.annotation.Resource;
import java.util.*;

@Service("FileBucketsServiceImpl")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class FileBucketsServiceImpl implements FileBucketsService {
    private static Logger logger = LoggerFactory.getLogger(FileBucketsServiceImpl.class);

    private final MinioPearUploadUtil minioPearUploadUtil;

    @Resource
    private FileBucketsMapper fileBucketsMapper;

    @Override
    public List<FileBucketsEntity> getDataList(Map<String, Object> params) {
        return fileBucketsMapper.getDataList(params);
    }

    @Override
    public List<FileBucketsEntity> getAllDataList(Map<String, Object> params) {
        return fileBucketsMapper.getAllDataList(params);
    }

    @Override
    public int getDataTotal(Map<String, Object> params) {
        return fileBucketsMapper.getDataTotal(params);
    }

    @Override
    public void createData(Map<String, Object> params) {
        try {
            String bucketsCode = String.valueOf(params.get("bucketsCode"));
            params.put("id", CommonUtils.getUUID());
            params.put("createTime", new Date());
            params.put("updateTime", new Date());
            minioPearUploadUtil.createBucket(bucketsCode);
            fileBucketsMapper.createData(params);
        } catch (Exception e) {
            logger.error("create buckets error", e);
        }
    }

    @Override
    public void updateData(Map<String, Object> params) {
        params.put("updateTime", new Date());
        fileBucketsMapper.updateData(params);
    }

    @Override
    public void deleteData(Map<String, Object> params) {
        TransactionManagerBuilder transactionManagerBuilder = TransactionManagerBuilder.getTransactionContext();
        TransactionStatus transactionStatus = transactionManagerBuilder.createTransactionStatus("transaction_delete_buckets");

        try {
            List<String> idList = CommonUtils.castObjectToList(params.get("idList"), String.class);
            List<String> bucketsCodeList = new ArrayList<>();
            for (String item : idList) {
                Map<String, Object> queryParams = new HashMap<>();
                queryParams.put("id", item);
                FileBucketsEntity fileBucketsEntity = fileBucketsMapper.getDataByParams(queryParams);
                String bucketsCode = fileBucketsEntity.getBucketsCode();
                bucketsCodeList.add(bucketsCode);
            }

            for (String item : bucketsCodeList) {
                minioPearUploadUtil.deleteBucket(item);
            }
            fileBucketsMapper.deleteData(params);
            transactionManagerBuilder.commit(transactionStatus);
        } catch (Exception exception) {
            logger.error("delete buckets error", exception);
            transactionManagerBuilder.rollback(transactionStatus);
        }
    }
}
