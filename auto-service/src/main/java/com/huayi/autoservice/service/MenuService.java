package com.huayi.autoservice.service;



import com.huayi.autoservice.entity.MenuEntity;
import com.huayi.autoservice.entity.RoleEntity;
import com.huayi.autoservice.entity.UserEntity;

import java.util.List;

/**
 * @Description 权限业务接口
 * @Author Sans
 * @CreateTime 2019/9/14 15:57
 */
public interface MenuService {
    List<MenuEntity> getMenuListByUser(UserEntity userEntity);

    List<MenuEntity> getAllMenuList();

    List<MenuEntity> getMenuListByRole(RoleEntity roleEntity);
}