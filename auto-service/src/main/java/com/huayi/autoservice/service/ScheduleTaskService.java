package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.ScheduleTaskEntity;

import java.util.List;
import java.util.Map;

public interface ScheduleTaskService {
    int getScheduleTaskCount(Map<String, Object> params);

    List<ScheduleTaskEntity> getScheduleTaskList(Map<String, Object> params);

    ScheduleTaskEntity getScheduleTaskSchedule(ScheduleTaskEntity scheduleEntity);

    int createScheduleTask(ScheduleTaskEntity scheduleEntity);

    int updateScheduleTask(ScheduleTaskEntity scheduleEntity);

    int deleteScheduleTask(List<String> params);
}
