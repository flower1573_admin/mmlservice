package com.huayi.autoservice.service.impl;


import com.huayi.autoservice.entity.MenuEntity;
import com.huayi.autoservice.entity.RoleEntity;
import com.huayi.autoservice.entity.UserEntity;
import com.huayi.autoservice.mapper.MenuMapper;
import com.huayi.autoservice.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("MenuServiceImpl")
public class MenuServiceImpl implements MenuService {
    @Autowired
    MenuMapper menuMapper;


    @Override
    public List<MenuEntity> getMenuListByUser(UserEntity userEntity) {
        return menuMapper.getMenuListByUser(userEntity);
    }

    @Override
    public List<MenuEntity> getAllMenuList() {
        return menuMapper.getAllMenuList();
    }

    @Override
    public List<MenuEntity> getMenuListByRole(RoleEntity roleEntity) {
        return menuMapper.getMenuListByRole(roleEntity);
    }
}
