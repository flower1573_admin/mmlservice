package com.huayi.autoservice.service;

import org.hyperic.sigar.SigarException;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

public interface ServeService {
    List<Map<String, String>> getEthernetInfo() throws UnknownHostException, SigarException;

    List<Map<String, String>> getDiskInfo() throws UnknownHostException, SigarException;

    List<Map<String, String>> getCpuInfo() throws UnknownHostException, SigarException;
}
