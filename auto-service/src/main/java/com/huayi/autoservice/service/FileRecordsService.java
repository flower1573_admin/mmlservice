package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.FileRecordsEntity;

import java.util.List;
import java.util.Map;

public interface FileRecordsService {
    List<FileRecordsEntity> getFileRecordsList(Map<String, Object> params);

    int getFileRecordsCount(Map<String, Object> params);

    void createFileRecords(FileRecordsEntity fileRecordsEntity);

    void deleteFileRecords(Map<String, Object> params);

    String getFileLink(Map<String, Object> params);
}
