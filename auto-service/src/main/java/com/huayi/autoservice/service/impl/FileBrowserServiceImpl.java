package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.FileBucketsEntity;
import com.huayi.autoservice.mapper.FileBrowserMapper;
import com.huayi.autoservice.service.FileBrowserService;
import com.huayi.autoservice.utils.CommonUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("FileBrowserServiceImpl")
public class FileBrowserServiceImpl implements FileBrowserService {
    @Resource
    private FileBrowserMapper fileBrowserMapper;

    @Override
    public List<FileBucketsEntity> getDataList(Map<String, Object> params) {
        return fileBrowserMapper.getDataList(params);
    }

    @Override
    public FileBucketsEntity getDataByParams(Map<String, Object> params) {
        return fileBrowserMapper.getDataByParams(params);
    }

    @Override
    public int getDataTotal(Map<String, Object> params) {
        return fileBrowserMapper.getDataTotal(params);
    }

    @Override
    public void createData(Map<String, Object> params) {
        params.put("id", CommonUtils.getUUID());
        params.put("createTime", new Date());
        params.put("updateTime", new Date());
        fileBrowserMapper.createData(params);
    }

    @Override
    public void updateData(Map<String, Object> params) {
        params.put("updateTime", new Date());
        fileBrowserMapper.updateData(params);
    }

    @Override
    public void deleteData(Map<String, Object> params) {
        fileBrowserMapper.deleteData(params);
    }
}
