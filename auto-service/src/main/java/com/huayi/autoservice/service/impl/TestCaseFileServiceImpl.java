package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.TestCaseFileEntity;
import com.huayi.autoservice.mapper.TestCaseFileMapper;
import com.huayi.autoservice.service.TestCaseFileService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("TestCaseFileServiceImpl")
public class TestCaseFileServiceImpl implements TestCaseFileService {
    @Resource
    private TestCaseFileMapper testCaseFileMapper;

    @Override
    public List<TestCaseFileEntity> getDataList(Map<String, Object> params) {
        return testCaseFileMapper.getDataList(params);
    }

    @Override
    public int getDataTotal(Map<String, Object> params) {
        return testCaseFileMapper.getDataTotal(params);
    }

    @Override
    public void uploadData(MultipartFile file)  {
        try {
            String originalFilename = file.getOriginalFilename();
            String contentType = file.getContentType();
            String fileSize = FileUtils.formatFileSize(file.getSize());
            file.transferTo(new File("E:\\images\\" + originalFilename));

            Map<String, Object> params = new HashMap<>();
            params.put("id", CommonUtils.getUUID());
            params.put("fileName", originalFilename);
            params.put("fileType", contentType);
            params.put("fileSize", fileSize);
            testCaseFileMapper.createData(params);
        } catch (Exception e) {

        }
    }

    @Override
    public void deleteData(Map<String, Object> params) {
        testCaseFileMapper.deleteData(params);
    }
}
