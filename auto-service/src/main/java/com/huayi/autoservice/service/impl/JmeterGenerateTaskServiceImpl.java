package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.JmeterGenerateTaskEntity;
import com.huayi.autoservice.entity.JmeterPlanEntity;
import com.huayi.autoservice.mapper.JmeterGenerateTaskMapper;
import com.huayi.autoservice.mapper.JmeterPlanMapper;
import com.huayi.autoservice.service.JmeterGenerateTaskService;
import com.huayi.autoservice.service.JmeterPlanService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.JmeterUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("JmeterGenerateTaskServiceImpl")
public class JmeterGenerateTaskServiceImpl implements JmeterGenerateTaskService {
    @Resource
    private JmeterGenerateTaskMapper jmeterGenerateTaskMapper;


    @Override
    public List<JmeterGenerateTaskEntity> getTaskList(Map<String, Object> queryParams) {
        return jmeterGenerateTaskMapper.getTaskList(queryParams);
    }

    @Override
    public int getTaskTotal(Map<String, Object> queryParams) {
        return jmeterGenerateTaskMapper.getTaskTotal(queryParams);
    }

    @Override
    public int createTask(Map<String, Object> queryParams) throws IOException {
        Map<String, String> map = JmeterUtils.gengreatJmxFile(queryParams);
        String fileName = map.get("fileName");

        Map<String, Object> taskParams = new HashMap<>();
        taskParams.put("id", CommonUtils.getUUID("jmeter"));
        taskParams.put("name", queryParams.get("name"));
        taskParams.put("fileName", fileName);
        taskParams.put("remark", queryParams.get("remark"));
        taskParams.put("createTime", new Date());

        return jmeterGenerateTaskMapper.createTask(taskParams);
    }

    @Override
    public void exportScript(HttpServletResponse response, Map<String, Object> params) {
        String fileName = String.valueOf(params.get("fileName"));
        try  {
            String tempPath = "G:\\";
            String filePath = tempPath + fileName;
            File file = new File(filePath);

            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            // 写入输出流
            FileInputStream inputStream = new FileInputStream(file);
            OutputStream outputStream = response.getOutputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int deleteTask(Map<String, Object> queryParams) {
        return jmeterGenerateTaskMapper.deleteTask(queryParams);
    }
}
