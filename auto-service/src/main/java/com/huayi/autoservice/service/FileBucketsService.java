package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.FileBucketsEntity;

import java.util.List;
import java.util.Map;

public interface FileBucketsService {
    List<FileBucketsEntity> getDataList(Map<String, Object> params);

    List<FileBucketsEntity> getAllDataList(Map<String, Object> params);

    int getDataTotal(Map<String, Object> params);

    void createData(Map<String, Object> params);

    void updateData(Map<String, Object> params);

    void deleteData(Map<String, Object> params);
}
