package com.huayi.autoservice.service;


import java.util.Map;

public interface FileAnalyzeService {
    Map<String, Object> getAnalyzeInfos();
}
