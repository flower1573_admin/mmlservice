package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.handler.WebsocketHandler;
import com.huayi.autoservice.service.ChatRoomService;
import com.huayi.autoservice.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("ChatRoomServiceImpl")
public class ChatRoomServiceImpl implements ChatRoomService {
    @Autowired
    private WebsocketHandler websocketHandler;

    @Override
    public void sendMessageToAllUser(String message) {
        websocketHandler.sendMessageToAllUsers(message);
    }
}
