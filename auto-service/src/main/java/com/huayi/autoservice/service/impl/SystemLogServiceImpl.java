package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.controller.SystemLogController;
import com.huayi.autoservice.entity.SystemLogEntity;
import com.huayi.autoservice.mapper.SystemLogMapper;
import com.huayi.autoservice.service.SystemLogService;
import com.huayi.autoservice.utils.FileUtils;
import com.huayi.autoservice.utils.MinioPearUploadUtil;
import io.minio.GetObjectArgs;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service("SystemLogServiceImpl")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SystemLogServiceImpl implements SystemLogService {
    private static Logger logger = LoggerFactory.getLogger(SystemLogServiceImpl.class);

    private final MinioPearUploadUtil minioPearUploadUtil;

    @Resource
    private SystemLogMapper systemLogMapper;

    @Override
    public List<SystemLogEntity> getDataList(Map<String, Object> params) {
        return systemLogMapper.getDataList(params);
    }

    @Override
    public int getDataTotal(Map<String, Object> params) {
        return systemLogMapper.getDataTotal(params);
    }

    @Async
    @Override
    public void collectData(Map<String, Object> params) {
        Date startTime = new Date(Long.valueOf(String.valueOf(params.get("startTime"))));
        Date endTime = new Date(Long.valueOf(String.valueOf(params.get("endTime"))));
        String fileType = String.valueOf(params.get("type"));

        Instant endZoned = Instant.ofEpochMilli(Long.valueOf(String.valueOf(params.get("endTime"))));
        Instant startZoned = Instant.ofEpochMilli(Long.valueOf(String.valueOf(params.get("startTime"))));

        try {
            String infoPath = "C:\\Users\\Administrator\\Desktop\\最新项目\\mmlservice\\logs\\";

            List<String> fileList = new ArrayList<>();
            FileUtils.listLogFiles(infoPath, ".log", fileList);

            List<String> validFileList = new ArrayList<>();
            for (String filePath : fileList) {
                Path path = Paths.get(filePath);
                BasicFileAttributes attrs = Files.readAttributes(path, BasicFileAttributes.class);
                Instant createTime = attrs.creationTime().toInstant();
                Instant updateTime = attrs.lastModifiedTime().toInstant();
                boolean isTypeValid = false;

                if ("all".equals(fileType)) {
                    isTypeValid = true;
                } else {
                    isTypeValid = !filePath.contains(".zip") && filePath.contains(fileType);
                }
                if (isTypeValid && createTime.isAfter(startZoned) && updateTime.isBefore(endZoned)) {
                    validFileList.add(filePath);
                }
            }

            if (validFileList.size() > 0) {
                String fileName = String.valueOf(System.currentTimeMillis()) + ".zip";
                String zipFileName = infoPath + fileName;
                ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipFileName));
                for (String filePath : validFileList) {
                    Path path = Paths.get(filePath);
                    FileInputStream fileIn = new FileInputStream(path.toFile());
                    ZipEntry zipEntry = new ZipEntry(path.getFileName().toString());
                    zipOut.putNextEntry(zipEntry);
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = fileIn.read(buffer)) >= 0) {
                        zipOut.write(buffer, 0, length);
                    }
                    zipOut.closeEntry();
                }
                zipOut.close();

                InputStream inputStream = new FileInputStream(new File(zipFileName));
                minioPearUploadUtil.upLoadInputStream(inputStream, "test", fileName);

                params.put("id", new Date());
                params.put("name", fileName);
                params.put("startTime", startTime);
                params.put("endTime", endTime);
                systemLogMapper.createData(params);
            }
        } catch (Exception e) {
            logger.error("failed collect log", e);
        }
    }

    @Override
    public void exportData(HttpServletResponse response, Map<String, Object> params) {
        String fileName = String.valueOf(params.get("name"));
        InputStream inputStream = null;
        try {
            inputStream = minioPearUploadUtil.downLoadFile(fileName, "test");

            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            OutputStream outputStream = response.getOutputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
