package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.ViedoCatalogEntity;
import com.huayi.autoservice.entity.ViedoFileEntity;

import java.util.List;
import java.util.Map;

public interface ViedoCatalogService {
    List<ViedoCatalogEntity> getDataList(Map<String, Object> params);

    List<ViedoCatalogEntity> getAllDataList(Map<String, Object> params);

    int getDataTotal(Map<String, Object> params);

    void createData(Map<String, Object> params);

    void updateData(Map<String, Object> params);

    void deleteData(Map<String, Object> params);
}
