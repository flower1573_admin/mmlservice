package com.huayi.autoservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.huayi.autoservice.entity.NavigatorEntity;
import com.huayi.autoservice.mapper.NavigatorMapper;
import com.huayi.autoservice.service.NavigatorService;
import com.huayi.autoservice.utils.CommonUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

@Service("NavigatorServiceImpl")
public class NavigatorServiceImpl implements NavigatorService {
    @Resource
    private NavigatorMapper navigatorMapper;

    @Override
    public List<NavigatorEntity> getNavigatorList(Map<String, Object> queryParams) {
        return navigatorMapper.getNavigatorList(queryParams);
    }

    @Override
    public int getNavigatorTotal(Map<String, Object> queryParams) {
        return navigatorMapper.getNavigatorTotal(queryParams);
    }

    @Override
    public int createNavigator(Map<String, Object> queryParams) {
        queryParams.put("id", CommonUtils.getUUID("navigator"));
        return navigatorMapper.createNavigator(queryParams);
    }

    @Override
    public int deleteNavigator(Map<String, Object> queryParams) {
        return navigatorMapper.deleteNavigator(queryParams);
    }

    @Override
    public int updateNavigator(Map<String, Object> queryParams) {
        return navigatorMapper.updateNavigator(queryParams);
    }

    @Override
    public void exportNavigator(HttpServletResponse response, Map<String, Object> queryParams) throws IOException {
        List<NavigatorEntity> allNavigatorList = navigatorMapper.getAllNavigatorList(queryParams);
        String fileName = CommonUtils.getTimeString();

        response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        response.setHeader("Content-disposition", "inline; filename=" + URLEncoder.encode(fileName + ".xlsx", "UTF-8"));
        EasyExcel.write(response.getOutputStream())
                .autoCloseStream(false)
                .head(NavigatorEntity.class)
                .excelType(ExcelTypeEnum.XLSX)
                .sheet("Sheet1")
                .doWrite(allNavigatorList);
    }
}
