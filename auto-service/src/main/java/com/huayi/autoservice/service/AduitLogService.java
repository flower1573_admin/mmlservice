package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.AduitLogEntity;

import java.util.List;
import java.util.Map;

public interface AduitLogService {
    Integer getAduitLogCount(Map<String, Object> params);

    List<AduitLogEntity> getAduitLogList(Map<String, Object> params);

    void createAduitLog(AduitLogEntity aduitLogEntity);

    void deleteAduitLog(Integer count);
}
