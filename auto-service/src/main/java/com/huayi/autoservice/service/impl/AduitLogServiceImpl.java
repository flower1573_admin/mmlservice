package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.AduitLogEntity;
import com.huayi.autoservice.mapper.AduitLogMapper;
import com.huayi.autoservice.service.AduitLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("AduitLogServiceImpl")
public class AduitLogServiceImpl implements AduitLogService {
    @Autowired
    AduitLogMapper aduitLogMapper;

    @Override
    public Integer getAduitLogCount(Map<String, Object> params) {
        return aduitLogMapper.getAduitLogCount(params);
    }

    @Override
    public List<AduitLogEntity> getAduitLogList(Map<String, Object> params) {
        return aduitLogMapper.getAduitLogList(params);
    }

    @Override
    public void createAduitLog(AduitLogEntity aduitLogEntity) {
        aduitLogMapper.createAduitLog(aduitLogEntity);
    }

    @Override
    public void deleteAduitLog(Integer count) {
        aduitLogMapper.deleteAduitLog(count);
    }
}
