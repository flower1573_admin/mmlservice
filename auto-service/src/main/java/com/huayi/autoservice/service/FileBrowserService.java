package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.FileBucketsEntity;

import java.util.List;
import java.util.Map;

public interface FileBrowserService {
    List<FileBucketsEntity> getDataList(Map<String, Object> params);

    FileBucketsEntity getDataByParams(Map<String, Object> params);

    int getDataTotal(Map<String, Object> params);

    void createData(Map<String, Object> params);

    void updateData(Map<String, Object> params);

    void deleteData(Map<String, Object> params);
}
