package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.NoteBookArticleEntity;
import com.huayi.autoservice.mapper.NoteBookArticleMapper;
import com.huayi.autoservice.service.NoteBookArticleService;
import com.huayi.autoservice.utils.CommonUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("NoteBookArticleServiceImpl")
public class NoteBookArticleServiceImpl implements NoteBookArticleService {
    @Resource
    private NoteBookArticleMapper noteBookArticleMapper;

    @Override
    public List<NoteBookArticleEntity> getDataList(Map<String, Object> params) {
        return noteBookArticleMapper.getDataList(params);
    }

    @Override
    public NoteBookArticleEntity getDataDetails(Map<String, Object> params) {
        String id = String.valueOf(params.get("id"));
        return noteBookArticleMapper.getDataDetails(id);
    }

    @Override
    public int getDataTotal(Map<String, Object> params) {
        return noteBookArticleMapper.getDataTotal(params);
    }

    @Override
    public void createData(Map<String, Object> params) {
        params.put("id", CommonUtils.getUUID());
        params.put("createTime", new Date());
        params.put("updateTime", new Date());
        noteBookArticleMapper.createData(params);
    }

    @Override
    public void updateData(Map<String, Object> params) {
        params.put("updateTime", new Date());
        noteBookArticleMapper.updateData(params);
    }

    @Override
    public void deleteData(Map<String, Object> params) {
        noteBookArticleMapper.deleteData(params);
    }
}
