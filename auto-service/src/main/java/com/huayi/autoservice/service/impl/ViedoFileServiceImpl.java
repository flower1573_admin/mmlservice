package com.huayi.autoservice.service.impl;

import com.huayi.autoservice.entity.FileRecordsEntity;
import com.huayi.autoservice.entity.ViedoFileEntity;
import com.huayi.autoservice.mapper.ViedoFileMapper;
import com.huayi.autoservice.service.ViedoFileService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.FileUtils;
import com.huayi.autoservice.utils.MinioPearUploadUtil;
import com.huayi.autoservice.utils.ViedoUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("ViedoFileServiceImpl")
public class ViedoFileServiceImpl implements ViedoFileService {
    @Resource
    private MinioPearUploadUtil minioPearUploadUtil;

    @Resource
    private ViedoFileMapper viedoFileMapper;

    @Override
    public List<ViedoFileEntity> getDataList(Map<String, Object> params) {
        return viedoFileMapper.getDataList(params);
    }

    @Override
    public Map<String, String> getDataDetails(Map<String, Object> params) {
        String fileId = String.valueOf(params.get("fileId"));

        ViedoFileEntity fileData = viedoFileMapper.getDataById(fileId);
        String fileName = fileData.getFileName();

        String fileLink = minioPearUploadUtil.getPresignedObjectUrl("email", fileName);

        Map<String, String> res = new HashMap<>();
        res.put("fileLink", fileLink);
        res.put("fileName", fileName);
        res.put("fileType", fileData.getFileType());
        return res;
    }

    @Override
    public int getDataTotal(Map<String, Object> params) {
        return viedoFileMapper.getDataTotal(params);
    }

    @Override
    public void uploadFile(MultipartFile multipartFile, String catalogId, String cacalogName) {
        try (InputStream ism = multipartFile.getInputStream()) {
            String fileName = multipartFile.getOriginalFilename();
            String fileMediaType = multipartFile.getContentType();
            String fileSize = FileUtils.formatFileSize(multipartFile.getSize());

            File file = FileUtils.transferMultipartFileToFile(multipartFile);
            ViedoUtils.getViedoImage(1, file);

            minioPearUploadUtil.upLoadInputStream(ism, "email", fileName);

            Map<String, Object> params = new HashMap<>();
            params.put("id", CommonUtils.getUUID("file"));
            params.put("fileName",fileName );
            params.put("fileSize",fileSize );
            params.put("fileType",fileMediaType );
            params.put("catalogId", catalogId);
            params.put("catalogName", cacalogName);
            params.put("createTime", new Date());
            viedoFileMapper.createData(params);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteData(Map<String, Object> params) {
        viedoFileMapper.deleteData(params);
    }
}
