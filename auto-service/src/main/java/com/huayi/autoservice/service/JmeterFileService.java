package com.huayi.autoservice.service;

import java.util.Map;

public interface JmeterFileService {
    void generaFile(Map<String, Object> params);
}
