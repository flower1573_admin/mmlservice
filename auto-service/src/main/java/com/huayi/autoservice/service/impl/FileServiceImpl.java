package com.huayi.autoservice.service.impl;


import com.huayi.autoservice.enmus.MinioBucketEnum;
import com.huayi.autoservice.entity.FileRecordsEntity;
import com.huayi.autoservice.mapper.FileCatalogMapper;
import com.huayi.autoservice.mapper.FileRecordsMapper;
import com.huayi.autoservice.service.FileService;
import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.FileUtils;
import com.huayi.autoservice.utils.MinioPearUploadUtil;
import com.huayi.autoservice.vo.MinioPearVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.Date;

@Service("FileServiceImpl")
public class FileServiceImpl implements FileService {
    @Resource
    private MinioPearUploadUtil minioPearUploadUtil;

    @Autowired
    FileRecordsMapper fileRecordsMapper;

    @Override
    public MinioPearVo createMultipartUploadUrl(Integer chunkNum, String fileName, String contentType) {
        MinioPearVo multipartUploadUrl = minioPearUploadUtil.createMultipartUploadUrl(chunkNum, fileName, contentType, MinioBucketEnum.EMAIL);
        return multipartUploadUrl;
    }

    @Override
    public Boolean completeMultipart(Integer chunkNum, String fileName, String contentType, String uploadId, String fileMd5) {
        Boolean aBoolean = minioPearUploadUtil.completeMultipart(chunkNum, fileName, contentType, uploadId, MinioBucketEnum.EMAIL);
        return aBoolean;
    }

    @Override
    public void uploadSingleFile(MultipartFile file) {
        try (InputStream ism = file.getInputStream()) {
            String md5Str = DigestUtils.md5DigestAsHex(ism);
            String fileName = md5Str + "_" + file.getOriginalFilename();
            Boolean uploadFlag = minioPearUploadUtil.upLoadInputStream(ism, "email", fileName);
            String fileMediaType = file.getContentType();
            String fileSize = FileUtils.formatFileSize(file.getSize());

            FileRecordsEntity fileRecordsEntity = FileRecordsEntity.builder()
                    .id(CommonUtils.getUUID("file"))
                    .name(fileName)
                    .catalogCode("email")
                    .type(fileMediaType)
                    .size(fileSize)
                    .createTime(new Date())
                    .build();
            fileRecordsMapper.createFileRecords(fileRecordsEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
