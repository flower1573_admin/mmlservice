package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.SystemLogEntity;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface SystemLogService {
    List<SystemLogEntity> getDataList(Map<String, Object> params);

    int getDataTotal(Map<String, Object> params);

    void collectData(Map<String, Object> params);

    void exportData(HttpServletResponse response, Map<String, Object> params);
}
