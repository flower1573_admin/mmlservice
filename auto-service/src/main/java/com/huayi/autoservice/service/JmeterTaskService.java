package com.huayi.autoservice.service;

import com.huayi.autoservice.entity.JmeterTaskEntity;

import java.util.List;
import java.util.Map;

public interface JmeterTaskService {
    List<JmeterTaskEntity> getTaskList(Map<String, Object> queryParams);

    int getTaskTotal(Map<String, Object> queryParams);

    int createTask(Map<String, Object> queryParams);

    int deleteTask(Map<String, Object> queryParams);

    int updateTask(Map<String, Object> queryParams);
}
