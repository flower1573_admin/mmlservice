package com.huayi.autoservice;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableCaching
@EnableAspectJAutoProxy(exposeProxy = true)
@EnableEncryptableProperties
@SpringBootApplication
@EnableAsync
public class PortalserviceApplication {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SpringApplication.run(PortalserviceApplication.class, args);
            }
        }).start();
    }
}
