package com.huayi.autoservice;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentPBEConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class JasyptUtils {

    private final static String SECRECT = "123";
    private final static String ALGORITHM = "PBEWithMD5AndDES";

    @Test
    public void testEncrypt() throws Exception {
        System.out.println("密文密码：" + encrypt("minioadmin"));
    }

    private String encrypt(String text){
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        EnvironmentPBEConfig config = new EnvironmentPBEConfig();
        config.setAlgorithm(ALGORITHM);
        config.setPassword(SECRECT);
        standardPBEStringEncryptor.setConfig(config);
        return standardPBEStringEncryptor.encrypt(text);
    }

    public String decrypt(String text) {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        EnvironmentPBEConfig config = new EnvironmentPBEConfig();
        config.setAlgorithm(ALGORITHM);
        config.setPassword(SECRECT);
        standardPBEStringEncryptor.setConfig(config);
        return standardPBEStringEncryptor.decrypt(text);
    }

}
