package com.huayi.autoservice;

import com.huayi.autoservice.utils.CommonUtils;
import com.huayi.autoservice.utils.JmeterUtils;
import com.huayi.autoservice.vo.CustomListener;
import org.apache.jmeter.JMeter;
import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.report.dashboard.ReportGenerator;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.reporters.Summariser;
import org.apache.jmeter.samplers.SampleEvent;
import org.apache.jmeter.save.SaveService;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.collections.HashTree;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.InputStream;

import static org.apache.jmeter.JMeter.JMETER_REPORT_OUTPUT_DIR_PROPERTY;

@SpringBootTest
public class CmdUtils {
    public static void main(String[] args) {
        try {
            StandardJMeterEngine engine = new StandardJMeterEngine();
            JMeterUtils.loadJMeterProperties("G:\\jmeter\\apache-jmeter-5.2\\bin\\jmeter.properties");
            JMeterUtils.setJMeterHome("G:\\jmeter\\apache-jmeter-5.2");
            JMeterUtils.initLogging();// you can comment this line out to see extra log messages of i.e. DEBUG level
            JMeterUtils.initLocale();            // Initialize JMeter SaveService
            SaveService.loadProperties();            //jmeter结果相关设置
            Summariser summer = null;
            String summariserName = JMeterUtils.getPropDefault("summariser.name", "summary");
            if (summariserName.length() > 0) {
                summer = new Summariser(summariserName);
            }

            ResultCollector resultCollector = new CustomListener(summer);

            String logFile = "G:\\20240921211254\\22\\" + "23322.jtl";
            File file = new File("G:\\20240921211254\\111.jmx");
            HashTree jmxTree = SaveService.loadTree(file);
            jmxTree.add(jmxTree.getArray()[0], resultCollector);
            resultCollector.setFilename(logFile);
            JMeter.convertSubTree(jmxTree, false);
            engine.configure(jmxTree);
            engine.run();

            // 生成报告
            ReportGenerator generator = new ReportGenerator(logFile, null);
            JMeterUtils.setProperty(JMETER_REPORT_OUTPUT_DIR_PROPERTY, "G:\\20240921211254\\33");
            generator.generate();
        } catch (Exception e) {
            System.out.println(">>>5>>>>>" + CommonUtils.getTimeString());
            e.printStackTrace();
        } finally {
            System.out.println(">>>>6>>>>" + CommonUtils.getTimeString());
        }
    }
}
