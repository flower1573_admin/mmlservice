import {useLocation} from 'react-router-dom';

import menuList from "./moudle"
import "./index.less"

export default function HeaderLayout() {
    const navigate = useNavigate()
    const [currentMenus, setCurrentMenus] = useState([])
    const location = useLocation();

    useEffect(() => {
        initRouterParams();
    }, []);

    const initRouterParams = () => {
        const routeKey = location.pathname.split("/")
        if (routeKey && routeKey.length) {
            const currentMenu = menuList.find(item => item.key.includes(routeKey[1]))
            document.title = currentMenu?.label

            setCurrentMenus(currentMenu?.key)
        }
    }

    const onClickMenu = (event) => {
        const targetPath = event.key
        navigate(targetPath)
    };

    return (
        <Menu
            items={menuList}
            onClick={onClickMenu}
            mode="horizontal"
            selectedKeys={currentMenus}
            className="header-layout-container"
        />
    )
}