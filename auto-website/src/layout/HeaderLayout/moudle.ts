const menuList = [
    {
        label: '首页',
        key: '/',
    },
    {
        label: '系统',
        key: '/systemManag/systemManagHomeView',
    },
    {
        label: '网盘',
        key: '/fileManage',
    }, {
        label: '测试',
        key: '/testBox/jmeterGenerateView',
    }, {
        label: '工具箱',
        key: '/toolBox/dataFlushView',
    }, {
        label: '导航',
        key: '/navigator/navigatorLinkView',
    }, {
        label: '娱乐',
        key: '/playRoom/viedoFileView',
    }, {
        label: '笔记本',
        key: '/notebook/notebookArticleView',
    }, {
        label: '客服',
        key: '/chatRoomView',
    },
]

export default menuList