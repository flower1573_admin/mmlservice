import HeaderLayout from "../HeaderLayout";
const { Header, Footer, Sider, Content } = Layout;
import "./index.less"

const MainLayout: React.FC = () => {
    return (
        <Layout className="common-layout-container">
            <Header className="header-wrap">
                <HeaderLayout/>
            </Header>
            <Content className="content-wrap">
                <Outlet/>
            </Content>
        </Layout>
    )
}

export default MainLayout