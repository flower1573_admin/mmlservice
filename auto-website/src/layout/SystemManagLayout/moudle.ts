export const menuList = [
    {
        key: "systemManagHomeView",
        label: "总览"
    }, {
        key: "serveView",
        label: "服务器",
        children: [
            {
                key: 'serveCpuView',
                label: 'CPU监控'
            }, {
                key: 'serveDiskView',
                label: '磁盘监控'
            }, {
                key: 'serveEthernetView',
                label: '网卡监控'
            },
        ],
    }, {
        key: "authView",
        label: "权限",
        children: [
            {
                key: 'userView',
                label: '用户'
            }
        ],
    }, {
        key: "scheduledView",
        label: "定时任务"
    }, {
        key: "logView",
        label: "日志",
        children: [
            {
                key: 'aduitLogView',
                label: '操作日志'
            }, {
                key: 'systemLogView',
                label: '系统日志'
            }
        ],
    }, {
        key: "dictionaryView",
        label: "数据字典"
    }, {
        key: "fileBucketsManage",
        label: "文件管理",
        children: [
            {
                key: 'fileBucketsView',
                label: '文件桶'
            }, {
                key: 'fileBrowserView',
                label: '文件列表'
            },
        ],
    },
]