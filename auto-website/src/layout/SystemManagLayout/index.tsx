import {useLocation} from 'react-router-dom';

import HeaderLayout from "@/layout/HeaderLayout";
const { Header, Footer, Sider, Content } = Layout;
import "./index.less"
import React from "react";
import {menuList} from "./moudle";

const MainLayout: React.FC = () => {
    const navigate = useNavigate()
    const [currentMenus, setCurrentMenus] = useState([])
    const [currentOpenedMenus, setCurrentOpenedMenus] = useState([])
    const location = useLocation();

    useEffect(() => {
        initRouterParams();
    }, [location]);

    const initRouterParams = () => {
        const currentMenu = location.pathname.split("/")
        if (currentMenu && currentMenu.length) {
            setCurrentMenus(currentMenu[2])
        }
    }

    const renderSidePanel = () => {
        const clickMenu = (event) => {
            const targetPath = event.key
            navigate(targetPath)
        }

        const onOpenChange = (event) => {
            setCurrentOpenedMenus(event)
        }

        return (
            <>
                <Menu className="menu-wrap"
                    onClick={clickMenu}
                    items={menuList}
                    mode="inline"
                    selectedKeys={currentMenus}
                />
            </>
        )
    }

    return (
        <Layout className="system-manage-layout-container">
            <Header className="header-wrap">
                <HeaderLayout/>
            </Header>
            <Layout className="main-layout">
                <Sider className="side-panel">
                    {renderSidePanel()}
                </Sider>
                <Content className="content-panel">
                    <Content className="content-wrap">
                        <Outlet/>
                    </Content>
                </Content>
            </Layout>
        </Layout>
    )
}

export default MainLayout