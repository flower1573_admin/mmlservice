import HeaderLayout from "@/layout/HeaderLayout";
const { Header, Footer, Sider, Content } = Layout;
import "./index.less"
import React from "react";
import {menuList} from "./moudle";

const PlayRoomLayout: React.FC = () => {
    const navigate = useNavigate()

    const renderSidePanel = () => {
        const clickMenu = (event) => {
            const targetPath = event.key
            navigate(targetPath)
        }
        return (
            <>
                <Menu className="menu-wrap"
                    onClick={clickMenu}
                    items={menuList}
                    mode="inline"
                />
            </>
        )
    }

    return (
        <Layout className="playroom-layout-container">
            <Header className="header-wrap">
                <HeaderLayout/>
            </Header>
            <Layout className="main-layout">
                <Sider className="side-panel">
                    {renderSidePanel()}
                </Sider>
                <Content className="content-panel">
                    <Content className="content-wrap">
                        <Outlet/>
                    </Content>
                </Content>
            </Layout>
        </Layout>
    )
}

export default PlayRoomLayout