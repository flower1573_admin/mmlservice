export const menuList = [
    {
        key: "jmeterGenerateView",
        label: "用例生成"
    }, {
        key: "jmeterRunnerView",
        label: "用例执行",
        children: [
            {
                key: 'testCaseFileView',
                label: '用例管理'
            }, {
                key: 'jmeterPlanView',
                label: '执行计划'
            }, {
                key: 'jmeterTaskView',
                label: '执行历史'
            }
        ],
    }
]