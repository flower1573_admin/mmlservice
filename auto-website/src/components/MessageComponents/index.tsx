import React, { useEffect } from 'react';
import { message } from 'antd';
import {createRoot} from "react-dom/client";
import { v4 as uuidv4  } from 'uuid';

export const Message = (props) => {
    const {type, content}= props

    useEffect(() => {
        message.open({
            type: type,
            content: content,
        });
    }, []);
    return (
        <>
        </>
    );
}

const renderComponents = (props) => {
    const divId = uuidv4()
    const div = document.createElement('div') as any;
    div.id = divId;
    document.body.appendChild(div);
    const root = createRoot(div);
    root.render(<Message  {...props} />);

    setTimeout(() => {
        const div = document.getElementById(divId);
        if (div) {
            document.body.removeChild(div);
        }
    }, 200);
}

const MessageComponents =  {
    success(content) {
        const parms = {
            type: "success",
            content: content
        }
        renderComponents(parms)
    },
    error(content) {
        const parms = {
            type: "error",
            content: content
        }
        renderComponents(parms)
    },
    warning(content) {
        const parms = {
            type: "warning",
            content: content
        }
        renderComponents(parms)
    },
    info(content) {
        const parms = {
            type: "info",
            content: content
        }
        renderComponents(parms)
    }
}

export default MessageComponents