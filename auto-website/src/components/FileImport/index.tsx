import React, {useState} from 'react';
import {createRoot} from "react-dom/client";
import "./index.less"
import {Button} from "antd";
import {getFileSize, getFileType} from "@/utils/fileUtils";
import axios from 'axios'
import {post} from "@/request";

export const generateUUID = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const ModalRender = (props: any) => {
    const [open, setOpen] = useState(true);
    const [check, setCheck] = useState(false);

    const [visibleLoading, setVisibleLoading] = useState(false);
    const [tableSelectedList, setTableSelectedList] = useState([]);
    const [tableList, setTableList] = useState([])
    const [forbidUpload, setForbidUpload] = useState(false)
    const [fileList, setFileList] = useState([])

    const handleOk = () => {
        setOpen(false);
        if (props.onOk) {
            props.onOk()
        }
    }

    // 点击取消
    const handleCancel = () => {
        setOpen(false);
        props.onCancel?.();
        setTimeout(() => {
            const div = document.getElementById(props.id);
            if (div) {
                document.body.removeChild(div);
            }
        }, 300);
    }

    const handleProgress = (resList) => {
        for (let i = 0; i < resList.length; i++) {
            const newFileList = tableList.map(item => {
                item.status = resList[i] == "error" ? "failed" : "success"
                return {
                    ...item
                }
            })
            setTableList(newFileList)
        }
    }

    const clickImport = () => {
        const uploadList = []
        for (let i = 0; i < tableList.length; i++) {
            const fileItem = tableList[i]
            const {id, file} = fileItem
            const formData = new FormData();
            formData.append('file', file);

            const fileRequest = new Promise((resolve, reject) => {
                axios.post('/fileRecords/uploadSingleFile', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                } as any ).then((res) => {
                    resolve(res)
                }).catch(err => {
                    resolve("error")
                })
            })
            uploadList.push(fileRequest)
        }
        setVisibleLoading(true)
      Promise.all(uploadList).then(res => {
          handleProgress(res)
        }).catch(err => {
      }).finally(() => {
          setForbidUpload(true)
            setVisibleLoading(false)
        })
    }

    const clickDelete = () => {
        const newTableList = tableList.filter(item => !tableSelectedList.includes(item.id))
        setTableList(newTableList)
        setTableSelectedList([])
    }

    const rowSelection = () => {
        return {
            selectedRowKeys: tableSelectedList,
            onChange: event => {
                setTableSelectedList(event);
            }
        }
    }

    const convertStatus = (status) => {
        if (status == "waitting") {
           return "等待上传"
        } else if (status == "success") {
            return "上传成功"
        } else if (status == "failed") {
            return "上传失败"
        } else if (status == "uploading") {
            return "上传中"
        }
        return "--"
    }

    const tableColumns = [
        {
            title: "序号",
            dataIndex: "number",
            key: "number",
            render: (_, record, index) => index + 1,
            align: "center",
        }, {
            title: "名称",
            dataIndex: "name",
            key: "name",
        }, {
            title: "类型",
            dataIndex: "type",
            key: "type",
        }, {
            title: "大小(MB)",
            dataIndex: "size",
            key: "size",
        }, {
            title: "状态",
            dataIndex: "status",
            key: "status",
            render: (event, event2) => (
                <div className="table-operate-column-wrap">
                    {convertStatus(event2.status)}
                </div>
            )
        }
    ]

    const uploadProps = {
        showUploadList: false,
        maxCount: 8,
        multiple: true,
        fileList: fileList,
        beforeUpload: () => {
            return false;
        },
        onChange: (event) => {
            const fileList = event.fileList

            const newFileList = []
            for (let i = 0; i < fileList.length; i++) {
                const file = fileList[i]
                const {name, size}= file
                const fileItem = {
                    key: generateUUID(),
                    id: generateUUID(),
                    name: name,
                    file: file,
                    type: getFileType(name),
                    size: getFileSize(size),
                    status: "waitting"
                }
                newFileList.push(fileItem)
            }
            setTableList([...newFileList, ...tableList])
            setFileList([])
        }
    }

    return <Modal
        key={props.id}
        open={open}
        title="上传"
        onOk={handleOk}
        onCancel={handleCancel}
        className="components-file-import-modal"
        width="50%"
        footer={() => (
            <div className="operate-btn-group">
                <Space>
                    <Button onClick={handleCancel}>关闭</Button>
                </Space>
            </div>
        )}
    >
        <div className="operate-panel">
            <Space>
                <Upload {...uploadProps}>
                    <Button>添加</Button>
                </Upload>
                <Button onClick={clickImport} disabled={tableList.length == 0 || forbidUpload}>上传</Button>
                <Button onClick={clickDelete} disabled={tableSelectedList.length == 0}>删除</Button>
            </Space>
            <Divider/>
        </div>
        <Table className="table-cls" loading={visibleLoading}
            rowSelection={rowSelection()}
            dataSource={tableList}
            columns={tableColumns}
            rowKey={row => row.id}
            pagination={false}
        />
    </Modal>
}


const FileImport = (props: any) => {
    const divId = generateUUID()
    const modalProps = {
        ...props,
        id: divId
    }
    const div = document.createElement('div') as any;
    div.id = divId;
    document.body.appendChild(div);
    const root = createRoot(div);

    root.render(<ModalRender  {...props} />);
}

export default FileImport

