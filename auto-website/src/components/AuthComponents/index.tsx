import React from "react";
import { Navigate } from "react-router-dom";

const hasLogin = true;

const AuthComponents = (props) => {
  console.log(props)
  const { components: Components } = props;

  if (hasLogin)  {
    return Components
  };

  return <Navigate to="/login" replace/>;
};

export default AuthComponents;
