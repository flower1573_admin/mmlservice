import React, { useState } from 'react';
import { createRoot } from "react-dom/client";
import dangerImg from "./images/danger.svg"
import "./index.less"
import {Button} from "antd";


const ModalRender = (props: any) => {
    const [open, setOpen] = useState(true);
    const [check, setCheck] = useState(false);
    const handleOk = () => {
        setOpen(false);
        if (props.onOk) {
            props.onOk()
        }
    }

    // 点击取消
    const handleCancel = () => {
        setOpen(false);
        props.onCancel?.();
        setTimeout(() => {
            const div = document.getElementById(props.id);
            if (div) {
                document.body.removeChild(div);
            }
        }, 300);
    }

    const onChange = (event) => {
        setCheck(!check)
    }

    return <Modal
        key={props.id}
        open={open}
        title="高危"
        onOk={handleOk}
        onCancel={handleCancel}
        className="components-hightrisk-modal"
        footer={() => (
            <div className="operate-btn-group">
                <Space>
                    <Button onClick={handleCancel}>取消</Button>
                    <Button type="primary" onClick={handleOk} disabled={!check}>确定</Button>
                </Space>
            </div>
        )}
    >
        <div className="modal-wrap">
            <div className="icon-wrap">
                <img src={dangerImg}/>
            </div>
            <div className="label-cls">
                此操作为高危操作，请确认是否继续该操作？
            </div>
        </div>
        <div className="context-wrap">
            <Checkbox onChange={onChange} value={check}>我已明确该操作的风险，确定继续操作。</Checkbox>
        </div>
    </Modal>
}


const HightRisk = (props: any) => {
    const divId = generateUUID()
    const modalProps = {
        ...props,
        id: divId
    }
    const div = document.createElement('div') as any;
    div.id = divId;
    document.body.appendChild(div);
    const root = createRoot(div);

    root.render(<ModalRender  {...props} />);
}

export default HightRisk

