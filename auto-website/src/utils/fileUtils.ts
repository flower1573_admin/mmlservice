import SparkMD5 from 'spark-md5';

/**
 * 计算文件md5
 * @param file
 */
export const getFileMd5 = (file) => {
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    const sparkMD5 = new SparkMD5();
    return new Promise((resolve) => {
        reader.onload = (e) => {
            sparkMD5.append(e.target.result);
            resolve(sparkMD5.end());
        }
    })
}

export const getFileType = (fileName) => {
    let ext = "--";
    const name = fileName.toLowerCase();
    const i = name.lastIndexOf(".");
    if(i > -1){
         ext = name.substring(i);
    }
    return ext;
}

export const  getFileSize =(bytes) =>{
    return (bytes / (1024 * 1024)).toFixed(3)
}
