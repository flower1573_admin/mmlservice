import { get, post } from '@/request'

export function getDataList(params: any) {
    return post('/viedoCatalog/getDataList', {
        data: params,
    })
}

export function getAllDataList(params: any) {
    return post('/viedoCatalog/getAllDataList', {
        data: params,
    })
}

export function createData(params: any) {
    return post('/viedoCatalog/createData', {
        data: params,
    })
}

export function updateData(params: any) {
    return post('/viedoCatalog/updateData', {
        data: params,
    })
}

export function deleteData(params: any) {
    return post('/viedoCatalog/deleteData', {
        data: params,
    })
}