import { get, post } from '@/request'

export function getDataList(params: any) {
    return post('/viedoFile/getDataList', {
        data: params,
    })
}

export function deleteData(params: any) {
    return post('/viedoFile/deleteData', {
        data: params,
    })
}

export function uploadData(params: any) {
    return post("/viedoFile/uploadData", {
        data: params,
        headers: {
            'Content-Type': "multipart/form-data"
        }
    })
}

export function getDataDetails(params: any) {
    return post("/viedoFile/getDataDetails", {
        data: params
    })
}