import { get, post } from '@/request'

export function getDataList(params: any) {
    return post('/noteBookArticle/getDataList', {
        data: params,
    })
}

export function getDataDetails(params: any) {
    return post('/noteBookArticle/getDataDetails', {
        data: params,
    })
}

export function createData(params: any) {
    return post('/noteBookArticle/createData', {
        data: params,
    })
}

export function updateData(params: any) {
    return post('/noteBookArticle/updateData', {
        data: params,
    })
}

export function deleteData(params: any) {
    return post('/noteBookArticle/deleteData', {
        data: params,
    })
}