import { get, post } from '@/request'

export function getDataList(params: any) {
    return post('/noteBookTodo/getDataList', {
        data: params,
    })
}

export function getDataDetails(params: any) {
    return post('/noteBookTodo/getDataDetails', {
        data: params,
    })
}

export function createData(params: any) {
    return post('/noteBookTodo/createData', {
        data: params,
    })
}

export function updateData(params: any) {
    return post('/noteBookTodo/updateData', {
        data: params,
    })
}

export function deleteData(params: any) {
    return post('/noteBookTodo/deleteData', {
        data: params,
    })
}