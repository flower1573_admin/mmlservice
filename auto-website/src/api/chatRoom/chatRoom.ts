import {get, post, put} from '@/request'

export const sendMessage =  (params: any) => {
    return  post('/chatroom/sendMessageToAllUser', {
        data: params,
    })
}