import { get, post } from '@/request'
import axios from "axios";

export function getTaskList(params: any) {
    return post('/jmeterGenerateTask/getTaskList', {
        data: params,
    })
}

export function generateScript(params: any) {
    return post('/jmeterGenerateTask/generateScript', {
        data: params,
    })
}

export function exportScript(params: any) {
    return axios({
        data: params,
        url: "http://127.0.0.1:31986/jmeterGenerateTask/exportScript",
        method:'POST',
        responseType: 'blob' // 确保响应类型是blob，以便处理文件下载
    })
}
