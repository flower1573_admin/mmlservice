import { get, post } from '@/request'
import axios from "axios";

export function getNavigatorList(params: any) {
    return post('/navigator/getNavigatorList', {
        data: params,
    })
}

export function createNavigator(params: any) {
    return post('/navigator/createNavigator', {
        data: params,
    })
}

export function deleteNavigator(params: any) {
    return post('/navigator/deleteNavigator', {
        data: params,
    })
}

export function updateNavigator(params: any) {
    return post('/navigator/updateNavigator', {
        data: params,
    })
}

export function exportNavigator(params: any) {
    return axios({
        data: params,
        url: "http://127.0.0.1:31986/navigator/exportNavigator",
        method:'POST',
        responseType: 'blob' // 确保响应类型是blob，以便处理文件下载
    })
}