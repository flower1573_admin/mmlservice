import {get, post} from '@/request'
import axios from "axios";

export const getNavigatorCategoryList = (params: any) => {
    return post('/navigatorCategory/getNavigatorList', {
        data: params,
    })
}

export const createNavigatorCategory = (params: any) => {
    return post('/navigatorCategory/createNavigator', {
        data: params,
    })
}

export const updateNavigatorCategory = (params: any) => {
    return post('/navigatorCategory/updateNavigator', {
        data: params,
    })
}

export const deleteNavigatorCategory = (params: any) => {
    return post('/navigatorCategory/deleteNavigator', {
        data: params,
    })
}

export const getAllNavigatorList = (params: any) => {
    return post('/navigatorCategory/getAllNavigatorList', {
        data: params,
    })
}