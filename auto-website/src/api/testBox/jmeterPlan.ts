import { get, post } from '@/request'

export function getPlanList(params: any) {
    return post('/jmeterPlan/getTaskList', {
        data: params,
    })
}

export function createPlan(params: any) {
    return post('/jmeterPlan/createTask', {
        data: params,
    })
}

export function deletePlan(params: any) {
    return post('/jmeterPlan/deleteTask', {
        data: params,
    })
}

export function updatePlan(params: any) {
    return post('/jmeterPlan/updateTask', {
        data: params,
    })
}

export function executePlan(params: any) {
    return post('/jmeterPlan/executePlan', {
        data: params,
    })
}