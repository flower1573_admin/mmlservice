import { get, post } from '@/request'

export function getTableList(params: any) {
    return post('/jmeterTask/getTaskList', {
        data: params,
    })
}

export function deleteTask(params: any) {
    return post('/jmeterTask/deleteTask', {
        data: params,
    })
}