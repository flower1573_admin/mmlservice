import { get, post } from '@/request'

export function getCaseFileList(params: any) {
    return post('/testCaseFile/getDataList', {
        data: params,
    })
}

export function deleteTestCase(params: any) {
    return post('/testCaseFile/deleteData', {
        data: params,
    })
}

export function importTestCase(params: any) {
    return post('/testCaseFile/importTestCase', {
        data: params,
    })
}

export function exportTestCase(params: any) {
    return post('/testCaseFile/exportTestCase', {
        data: params,
    })
}

export function importTestCaseTemplate(params: any) {
    return post('/testCaseFile/importTestCaseTemplate', {
        data: params,
    })
}