import { get, post } from '@/request'

export function generateJmeterScript(params: any) {
    return post('/jmeterGenerate/generateJmeterScript', {
        data: params,
    })
}