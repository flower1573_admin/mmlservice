import { get, post } from '@/request'

export function getTableList(params: any) {
    return post('/scheduleTask/getScheduleTaskList', {
        data: params,
    })
}

export function createSchedule(params: any) {
    return post('/scheduleTask/createScheduleTask', {
        data: params,
    })
}

export function deleteSchedule(params: any) {
    return post('/scheduleTask/deleteScheduleTask', {
        data: params,
    })
}

export function updateSchedule(params: any) {
    return post('/scheduleTask/updateScheduleTask', {
        data: params,
    })
}

export function updateScheduleStatus(params: any) {
    return post('/scheduleTask/updateScheduleTaskStatus', {
        data: params,
    })
}

export function testSchedule(params: any) {
    return post('/scheduleTask/executeScheduleTask', {
        data: params,
    })
}


