import { get, post, put } from '@/request'
import axios from 'axios'
import {async} from "q";

export function getFileRecordsList(params: any) {
    return post('/fileRecords/getDataList', {
        data: params,
    })
}

/**
 * 获取分片上传信息
 *
 * @param params
 */
export const  getFilemultipart = async (params: any) => {
     return await post('/fileRecords/getFilemultipart', {
        data: params,
    })
}

/**
 * 分片上传
 *
 * @param params
 */
export function uploadFilemultipart(params: any) {
    const {uploadUrl, chunkFile, fileType} = params
    return put(uploadUrl, {
        data: chunkFile,
        headers: {
            'Content-Type': fileType
        }
    })
}

/**
 * 合并分片上传
 *
 * @param params
 */
export function completeFilemultipart(params: any) {
    return post('/fileRecords/completeFilemultipart', {
        data: params,
    })
}

/**
 * 上传单文件
 *
 * @param params
 */
export function uploadSingleFile(params: any) {
    return post("/fileRecords/uploadSingleFile", {
        data: params,
        headers: {
            'Content-Type': "multipart/form-data"
        }
    })
}

export function downloadSingleFile(params: any) {
    return axios({
        data: params,
        url: "http://127.0.0.1:31986/fileRecords/downloadSingleFile",
        method:'POST',
        responseType: 'blob' // 确保响应类型是blob，以便处理文件下载
    })
}

export function getFIleUrl(params: any) {
    return post("/fileRecords/getFIleUrl", {
        data: params
    })
}

/**
 * 创建目录
 *
 * @param data
 */
export const createCatalog = (data: any) => {
    return post("/fileCatalog/createCatalog", {
        data: data
    })
}

export const updateCatalog = (data: any) => {
    return post("/fileCatalog/updateCatalog", {
        data: data
    })
}

export const deleteCatalog = (data: any) => {
    return post("/fileCatalog/deleteCatalog", {
        data: data
    })
}

export const deleteFile = (data: any) => {
    return post("/fileRecords/deleteFile", {
        data: data
    })
}

export const getFileLink = (data: any) => {
    return post("/fileRecords/getFileLink", {
        data: data
    })
}


/**
 * 查询目录
 *
 * @param params
 */
export function getCategoryList(params: any) {
    return post('/fileCatalog/getCatalogList', {
        data: params,
    })
}

export function getSystemInfo() {
    return post('/systemInfo/getInfos')
}