import { get, post } from '@/request'

export function getCpuInfo() {
    return post('/serve/getCpuInfo')
}