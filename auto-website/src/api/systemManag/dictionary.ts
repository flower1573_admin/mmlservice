import { get, post } from '@/request'

export function getDictionaryList(params) {
    return post('/dictionary/getTaskList', {
        data: params,
    })
}

export function createDictionary(params) {
    return post('/dictionary/createTask', {
        data: params,
    })
}

export function deleteDictionary(params) {
    return post('/dictionary/deleteTask', {
        data: params,
    })
}

export function updateDictionary(params) {
    return post('/dictionary/updateTask', {
        data: params,
    })
}