import { get, post } from '@/request'

export function getBucketsInfos(params) {
    return post('/fileHome/getAnalyzeInfos', {
        data: params,
    })
}