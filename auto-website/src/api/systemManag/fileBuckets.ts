import { get, post } from '@/request'

export function getDataList(params) {
    return post('/fileBuckets/getDataList', {
        data: params,
    })
}

export function createData(params) {
    return post('/fileBuckets/createData', {
        data: params,
    })
}

export function updateData(params) {
    return post('/fileBuckets/updateData', {
        data: params,
    })
}

export function deleteData(params) {
    return post('/fileBuckets/deleteData', {
        data: params,
    })
}