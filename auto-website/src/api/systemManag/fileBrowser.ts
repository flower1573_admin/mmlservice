import { get, post } from '@/request'

export function getDataList(params) {
    return post('/fileBrowser/getDataList', {
        data: params,
    })
}

export function createData(params) {
    return post('/fileBrowser/createData', {
        data: params,
    })
}

export function updateData(params) {
    return post('/fileBrowser/updateData', {
        data: params,
    })
}

export function deleteData(params) {
    return post('/fileBrowser/deleteData', {
        data: params,
    })
}