import { get, post } from '@/request'

export function getEthernetInfo() {
    return post('/serve/getEthernetInfo')
}