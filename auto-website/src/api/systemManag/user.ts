import { get, post } from '@/request'

export function getUserList(params) {
    return post('/user/getUserList', {
        data: params,
    })
}

export function createUser(params) {
    return post('/user/createUser', {
        data: params,
    })
}

export function deleteUser(params) {
    return post('/user/deleteUser', {
        data: params,
    })
}

export function updateUser(params) {
    return post('/user/updateUser', {
        data: params,
    })
}