import { get, post } from '@/request'

export function getServeInfo() {
    return post('/systemInfo/getInfos')
}