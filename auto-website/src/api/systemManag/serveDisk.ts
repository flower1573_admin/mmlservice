import { get, post } from '@/request'

export function getDiskInfo() {
    return post('/serve/getDiskInfo')
}