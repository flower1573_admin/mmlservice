import { get, post } from '@/request'

export function getTableList(params: any) {
    return post('/aduitLog/getAduitLogList', {
        data: params,
    })
}
