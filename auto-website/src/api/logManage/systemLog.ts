import { get, post } from '@/request'
import axios from 'axios'

export function getDataList(params: any) {
    return post('/systemLog/getDataList', {
        data: params,
    })
}

export function collectData(params: any) {
    return post('/systemLog/collectData', {
        data: params,
    })
}

export function exportData(params: any) {
    return axios({
        data: params,
        url: "http://127.0.0.1:31986/systemLog/exportData",
        method:'POST',
        responseType: 'blob'
    })
}