import axios, {AxiosRequestConfig, AxiosInstance} from 'axios'

const service: AxiosInstance = axios.create({
    baseURL: "http://127.0.0.1:31986/"
} as any)

service.interceptors.request.use((config) => {
    return config
}, (error) => {
    return Promise.reject(error)
})

service.interceptors.response.use((response) => {
    return response.data
}, (error) => {
    return Promise.reject(error)
})

function request<T = any>(config: AxiosRequestConfig<T>): Promise<T> {
    return new Promise((resolve, reject) => {
        service.request<any, T>(config).then((res: any) => {
            if (res.code !== 200) {
                return reject(res)
            }
            resolve(res.data)
        }).catch((err) => {
            return reject(err)
        })
    })
}

export function get<T = any>(url: string, config?: AxiosRequestConfig<T>): Promise<T> {
    return request<T>({...config, method: 'GET', url})
}

export function post<T = any>(url: string, config?: AxiosRequestConfig<T>): Promise<T> {
    return request<T>({...config, method: 'POST', url})
}

export function put<T = any>(url: string, config?: AxiosRequestConfig<T>): Promise<T> {
    console.log(config)
    return request<T>({...config, method: 'PUT', url})
}

export function del<T = any>(url: string, config?: AxiosRequestConfig<T>): Promise<T> {
    return request<T>({...config, method: 'Delete', url})
}

export default service
