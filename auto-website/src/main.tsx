import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";

import 'virtual:svg-icons-register'
import 'virtual:uno.css'

import '@/locales/index'
import '@/assets/styles/index.less'

ReactDOM.createRoot(document.getElementById("root")).render(
  <App />
);

