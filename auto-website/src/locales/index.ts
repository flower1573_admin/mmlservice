import i18n from 'i18next'
import zhCnTrans from './lang/zh-cn'
import enUsTrans from './lang/en'

i18n.use(initReactI18next).init({
  resources: {
    en: {
      translation: enUsTrans,
    },
    zhCn: {
      translation: zhCnTrans,
    },
  },
  fallbackLng: 'zhCn',
  debug: false,
  interpolation: {
    escapeValue: false,
  },
})

export default i18n
