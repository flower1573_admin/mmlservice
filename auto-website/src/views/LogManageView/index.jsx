import React from "react";

import "./index.less"
import {tableBaseColumns} from "./moudle"
import {getTableList} from "@/api/logManage/logManage";

export function LogManageView() {
  const {RangePicker} = DatePicker;

  let renderRef = useRef(true);
  const [filterParams, setFilterParams] = useState({
    timeRange: []
  })
  const [tableList, setTableList] = useState([])
  const [tableCurrent, setTableCurrent] = useState(1)
  const [tableSize, setTableSize] = useState(15)
  const [tableTotal, setTableTotal] = useState(0)

  useEffect(() => {
    if (renderRef.current) {
      renderRef.current = false;
      return;
    }
    initTableList();
  }, []);

  const initTableList = () => {
    const startTime = filterParams.timeRange[0]
    const endTime = filterParams.timeRange[1]
    const params = {
      startTime: startTime,
      endTime: endTime,
    }
    getTableList(params).then(res => {
     const  {data, total} = res
      setTableList(data)
      setTableTotal(total)
    }).catch(err => {
    })
  }


  const onPaginationChange = (page, size) => {
    if (tableSize != size) {
      setTableCurrent(1)
      setTableSize(size)
    } else {
      setTableCurrent(page)
    }
    initTableList()
  }

  const onTimeChange = (event) => {
    setFilterParams({
      ...filterParams,
      timeRange: event
    })
  }

  const clickFilter = () => {
    initTableList()
  }

  const clickReset = () => {
    setFilterParams({
      timeRange: []
    })
    initTableList()
  }

  const clickExport = () => {

  }

  const renderTablePanel = () => {
    const tableColumns = [
      ...tableBaseColumns
    ]
    return (
      <div className="table-panel">
        <Table
          dataSource={tableList}
          columns={tableColumns}
          rowKey={row => row.id}
          bordered
        />
        <Pagination
          current={tableCurrent}
          pageSize={tableSize}
          total={tableTotal}
          className="pagination-cls"
          showQuickJumper
          pageSizeOptions={[15, 30, 50, 100, 500]}
          onChange={onPaginationChange}
        />
      </div>
    )
  }

  const renderFilterPanel = () => {
    return (
      <div className="filter-panel">
        <div className="filter-right">
          <div className="filter-item">
            <div className="label-cls">时间</div>
            <RangePicker value={filterParams.timeRange} onChange={onTimeChange} showTime />
          </div>
          <Button className="filter-item" onClick={clickFilter}>搜索</Button>
          <Button className="filter-item" onClick={clickReset}>重置</Button>
        </div>
        <div className="filter-left">
          <Button className="filter-item" onClick={clickExport}>导出</Button>
        </div>
      </div>
    )
  }

  return <div className='log-manage-view-container'>
  </div>
}

export default LogManageView;