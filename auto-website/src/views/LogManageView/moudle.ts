export const tableBaseColumns = [
    {
        title: '序号',
        dataIndex: 'number',
        key: 'number',
    }, {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '参数',
        dataIndex: 'params',
        key: 'params',
    }, {
        title: '模块',
        dataIndex: 'moudle',
        key: 'moudle',
    }, {
        title: '客户端地址',
        dataIndex: 'ipAddress',
        key: 'ipAddress',
    }, {
        title: '指纹',
        dataIndex: 'fingerPrint',
        key: 'fingerPrint',
    }, {
        title: '操作时间',
        dataIndex: 'requestTime',
        key: 'requestTime',
    },
]