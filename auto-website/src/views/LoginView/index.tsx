import React from "react";
import "./index.less"

export function LoginView() {
    const clickLoginIn = () => {
        console.log(">>>>>>>>>>>")
    }


    return <div className='login-container'>
        <div className="login-outer-wrap">
            <div className="login-wrap">
                <div className="login-item">
                    登录
                </div>
                <div className="login-item">
                    <Input size="large" placeholder="账号"/>
                </div>
                <div className="login-item">
                    <Input.Password size="large" placeholder="密码"/>
                </div>
                <div className="login-item">
                    <Button className="login-btn-item"  type="primary" danger size="large" onClick={clickLoginIn}>登录</Button>
                </div>
            </div>
        </div>

    </div>

}

export default LoginView;
