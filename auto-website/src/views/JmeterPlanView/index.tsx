import React from "react";

import "./index.less"
import {tableBaseColumns} from "./moudle"
import {getPlanList, createPlan, deletePlan, updatePlan, executePlan} from "@/api/testBox/jmeterPlan";
const { TextArea } = Input;

export function LogManageView() {
    const {RangePicker} = DatePicker;

    let renderRef = useRef(true);
    const [filterParams, setFilterParams] = useState({
        timeRange: [],
        name: "",
        taskStatus: ""
    })
    const filterParamsRef  = useRef({
        timeRange: [],
        name: "",
        taskStatus: ""
    })

    const [tableList, setTableList] = useState([])
    const [tableCurrent, setTableCurrent] = useState(1)
    const [tableSize, setTableSize] = useState(15)
    const [tableTotal, setTableTotal] = useState(0)
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);

    const [visibleConfig, setVisibleConfig] = useState(false)
    const [visibleExecute, setVisibleExecute] = useState(false)
    const [executeParams, setExecuteParams] = useState({
        type: "date",
        executeTime: "",
        executeCron: "",
        planName: "",
        scriptName: "",
    })
    const executeParamsRef = useRef({
        type: "date",
        executeTime: "",
        executeCron: "",
        planName: "",
        scriptName: "",
    })

    const [configParams, setConfigParams] = useState({
        operateType: "",
        id: "",
        planName: "",
        scriptName: "",
        taskCron: "",
        remark: "",
    })
    const configParamsRef  = useRef({
        operateType: "",
        id: "",
        planName: "",
        scriptName: "",
        taskCron: "",
        remark: "",
    })

    useEffect(() => {
        if (renderRef.current) {
            renderRef.current = false;
            return;
        }
        initTableList();
    }, []);

    const initTableList = () => {
        const params = {
            page: tableCurrent,
            size: tableSize,
            name: filterParamsRef.current.name,
            taskStatus: filterParamsRef.current.taskStatus,
        }
        if (filterParamsRef.current.timeRange.length) {
            const startTime = filterParamsRef.current.timeRange[0].valueOf()
            const endTime = filterParamsRef.current.timeRange[1].valueOf()
            params.startTime = startTime
            params.endTime = endTime
        }
        getPlanList(params).then(res => {
            const {data, total} = res
            setTableList(data)
            setTableTotal(total)
        }).catch(err => {
        })
    }


    const onPaginationChange = (page, size) => {
        if (tableSize != size) {
            setTableCurrent(1)
            setTableSize(size)
        } else {
            setTableCurrent(page)
        }
        initTableList()
    }

    const onTimeChange = (event) => {
        setFilterParams({
            ...filterParams,
            timeRange: event
        })
    }

    const clickFilter = () => {
        initTableList()
    }

    const clickReset = () => {
        setFilterParams({
            timeRange: []
        })
        initTableList()
    }

    const clickExport = () => {

    }

    const renderTablePanel = () => {
        const clickConfig = (event) => {
            const {id, planName, scriptName,  taskCron, remark} = event
            configParamsRef.current = {
                operateType: "update",
                id: id,
                planName: planName,
                scriptName: scriptName,
                taskCron: taskCron,
                remark: remark,
            }
            setVisibleConfig(true)
        }
        const clickExecute = (event) => {
            const {planName, scriptName} = event
            executeParamsRef.current = {
                ...executeParamsRef.current,
                planName: planName,
                scriptName: scriptName,
            }
            setVisibleExecute(true)
        }
        const clickDelete = (event) => {
            const params = {
                idList: event.id
            }

            deletePlan(params).then(res=> {
                initTableList()
            }).catch(err => {

            })
        }

        const onSelectChange = (event) => {
            setSelectedRowKeys(event);
        }

        const rowSelection =  {
            selectedRowKeys,
            onChange: onSelectChange,
        };

        const tableColumns = [
            ...tableBaseColumns,
            {
                title: '操作',
                key: 'operate',
                dataIndex: 'operate',
                align: 'center', //头部单元格和内容区水平居中
                render: (event, event2) => (
                    <div className="table-operate-column-wrap">
                        <div className="column-cls" onClick={tags => clickExecute(event2)}>启动</div>
                        <div className="column-cls" onClick={tags => clickConfig(event2)}>编辑</div>
                        <div className="column-cls" onClick={tags => clickDelete(event2)}>删除</div>
                    </div>
                )
            }
        ]
        return (
            <div className="table-panel">
                <Table
                    pagination={false}
                    rowSelection={rowSelection}
                    dataSource={tableList}
                    columns={tableColumns}
                    rowKey={row => row.id}
                />
                <Pagination
                    current={tableCurrent}
                    pageSize={tableSize}
                    total={tableTotal}
                    className="pagination-cls"
                    showQuickJumper
                    pageSizeOptions={[15, 30, 50, 100, 500]}
                    onChange={onPaginationChange}
                    showTotal={(total) => `共 ${total} 条`}
                />
            </div>
        )
    }

    const renderFilterPanel = () => {
        const clickCreate = () => {
            configParamsRef.current = {
                ...configParamsRef.current,
                operateType: "create",
            }
            setVisibleConfig(true)
        }

        const onFilterChange = (type, event) => {
            console.log(event)
            const newParams = {
                ...filterParamsRef.current,
            }
            if (type == "name"){
                newParams.name = event.target.value
            }
            if (["taskStatus", "timeRange"].includes(type)){
                newParams[type] = event
            }
            filterParamsRef.current = newParams
            setFilterParams(newParams)
        }

        return (
            <div className="filter-panel">
                <div className="filter-left">
                    <Space>
                        <Button className="filter-item" onClick={clickCreate}>创建</Button>
                        <Button className="filter-item" onClick={clickFilter}>删除</Button>
                    </Space>
                </div>
                <div className="filter-right">
                    <div className="filter-item">
                        <div className="label-cls">名称</div>
                        <Input className="filter-value-item" value={filterParams.name} onChange={event => onFilterChange("name", event)}/>
                    </div>
                    <div className="filter-item">
                        <div className="label-cls">状态</div>
                        <Select className="filter-value-item"
                                value={filterParams.type}
                                onSelect={event => onFilterChange("taskStatus", event)}
                                options={[
                                    { value: 'enable', label: '启用' },
                                    { value: 'disable', label: '禁用' },
                                ]}
                        />
                    </div>
                    <div className="filter-item">
                        <div className="label-cls">更新时间</div>
                        <RangePicker value={filterParams.timeRange} onChange={event => onFilterChange("timeRange", event)} showTime/>
                    </div>
                    <Button className="filter-item" onClick={clickFilter}>搜索</Button>
                    <Button className="filter-item" onClick={clickReset}>重置</Button>
                </div>
            </div>
        )
    }


    const renderConfigModal = () => {
        const clickCancel = () => {
            setVisibleConfig(false)
        }
        const clickConfirm = () => {
            const {operateType, id, planName, scriptName, taskCron, remark} = configParamsRef.current
            const params = {
                id: id,
                planName: planName,
                scriptName: scriptName,
                taskCron: taskCron,
                remark: remark,
            }

            if (operateType == "create"){
                createPlan(params).then(res=> {
                    clickCancel()
                }).catch(err => {

                })
            } else {
                updatePlan(params).then(res=> {
                    clickCancel()
                }).catch(err => {

                })
            }
        }
        const onInputChange = (key, event) => {
            const value = event.target.value
            const params = configParamsRef.current
            params[key] = value
        }
        return (
            <Modal
                title="配置"
                open={visibleConfig}
                onOk={clickConfirm}
                onCancel={clickCancel}
                keyboard={true}
                classNames="scheduled-config-modal"
                width="35%"
            >
                <Form
                    name="basic"
                    autoComplete="off"
                    labelCol={{ span: 2 }}
                >
                    <Form.Item
                        label="名称"
                        name="planName"
                        rules={[{ required: true, message: '请输入名称' }]}
                    >
                        <Input defaultValue={configParamsRef.current.planName} value={configParamsRef.current.planName} onChange={event => onInputChange("planName", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="类名"
                        name="scriptName"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Input defaultValue={configParamsRef.current.scriptName} value={configParamsRef.current.scriptName} onChange={event => onInputChange("scriptName", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="备注"
                        name="remark"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <TextArea
                            showCount
                            style={{ height: 100 }}
                            defaultValue={configParamsRef.current.remark}
                            value={configParamsRef.current.remark}
                            onChange={event => onInputChange("remark", event)}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        )
    }

    const renderExecuteModal = () => {
        const onInputChange = (key, event) => {
            let changeValue = ""

            console.log(key)
            if (["executeTime",  "type"].includes(key)){
                changeValue = event
            } else {
                changeValue = event.target.value
            }
            executeParamsRef.current = {
                ...executeParamsRef.current,
                [key]: changeValue
            }
            setExecuteParams({
                ...executeParams,
                [key]: changeValue
            })
        }

        const clickCancel = () => {
            setVisibleExecute(false)
        }

        const clickConfirm = () => {
            const {type, executeTime, executeCron, planName, scriptName} = executeParamsRef.current
            const params = {
                type: type,
                planName: planName,
                scriptName: scriptName,
                executeTime: executeTime.valueOf(),
                executeCron: executeCron,
            }
            executePlan(params).then(res=> {
                clickCancel()
            }).catch(err => {

            })
        }

        return (
            <Modal
                title="配置"
                open={visibleExecute}
                onOk={clickConfirm}
                onCancel={clickCancel}
                keyboard={true}
                className="jmeter-runner-execute-modal"
                width="30%"
            >
                <Form
                    name="basic"
                    autoComplete="off"
                    labelCol={{ span: 2 }}
                >
                    <Form.Item
                        label="类型"
                        name="type"
                    >
                        <Select className="filter-value-item"
                            value={executeParams.type}
                            defaultValue={executeParams.type}
                            onChange={event => onInputChange("type", event)}
                            options={[
                                { value: 'date', label: '固定时间' },
                                { value: 'cron', label: 'CRON' },
                            ]}
                        />
                    </Form.Item>
                    <Form.Item className={executeParams.type == "date" ? "column-show" : "column-hidden"}
                        label="时间"
                        name="executeTime"
                        rules={[{ required: true, message: '请输入名称' }]}
                    >

                        <DatePicker className="datepicker-item"
                            showTime
                            onChange={event => onInputChange("executeTime", event)}
                        />
                    </Form.Item>
                    <Form.Item className={executeParams.type == "cron" ? "column-show" : "column-hidden"}
                        label="CRON"
                        name="executeCron"
                        rules={[{ required: true, message: '请输入名称' }]}
                    >
                        <Input defaultValue={executeParams.executeCron} value={executeParams.executeCron} onChange={event => onInputChange("executeCron", event)}/>
                    </Form.Item>
                </Form>
            </Modal>
        )
    }

    return <div className='jmeter-runner--container'>
        {renderFilterPanel()}
        {renderTablePanel()}
        {renderConfigModal()}
        {renderExecuteModal()}
    </div>
}

export default LogManageView;