import React from "react";
import "./index.less"
import {getDataList, createData, updateData, deleteData} from "@/api/notebook/notebookArticle";
import {Button} from "antd";
import {tableBaseColumns} from "./moudle";

export function NotebookHomeView() {
    const navigate = useNavigate()

    const [filterParams, setFilterParams] = useState(() => {
        return {
            name: "",
            timeRange: [],
            tableCurrent: 1,
            tableSize: 15,
        }
    });
    const filterParamsRef = useRef({
        name: "",
        timeRange: [],
        tableCurrent: 1,
        tableSize: 15,
    })
    const [tableList, setTableList] = useState([]);
    const [tableSelectedList, setTableSelectedList] = useState([]);
    const [tableTotal, setTableTotal] = useState(0);

    useEffect(() => {
        initDataList();
    }, []);

    const initDataList = () => {
        const {name, tableCurrent, tableSize, timeRange} = filterParamsRef.current
        const params = {
            name: name,
            page: tableCurrent,
            size: tableSize,
        } as any
        if (timeRange.length > 0) {
            params.startTime = timeRange[0].valueOf()
            params.endTime = timeRange[1].valueOf()
        }
        getDataList(params).then((res: any) => {
            const {data, total} = res
            setTableList(data)
            setTableTotal(total)
        }).catch(err => {

        })
    }

    const renderFilterPanel = () => {
        const clickImport = () => {
            navigate("/notebook/noteBookArticleEditorView")
        }

        const clickExport = () => {

        }

        const clickDelete = () => {
            const params = {
                idList: tableSelectedList
            }
            deleteData(params).then(res => {
                initDataList()
            }).catch(err => {

            })
        }

        const clickExportTemplate = () => {

        }

        const onFilterChange = (key, event) => {
            const params = {
                ...filterParams,
                [key]: event
            }
            setFilterParams(params)
            filterParamsRef.current = params
        }

        const clickFilter = (type) => {
            if (type == "reset") {
                const params = {
                    ...filterParams,
                    name: "",
                    timeRange: [],
                    tableCurrent: 1,
                    tableSize: 15,
                }
                setFilterParams(params)
                filterParamsRef.current = params
            }
        }

        return (
            <div className="filter-panel">
                <div className="panel-left">
                    <Space>
                        <Button onClick={clickImport}>新增</Button>
                        <Button onClick={clickDelete}>删除</Button>
                    </Space>
                </div>

                <div className="panel-right">
                    <Space>
                        <div>名称</div>
                        <Input value={filterParams.name}
                               onChange={(event) => onFilterChange("name", event.target.value)} allowClear/>
                        <div>更新时间</div>
                        <DatePicker.RangePicker value={filterParams.timeRange}
                                                onChange={event => onFilterChange("timeRange", event)} showTime
                                                allowClear/>
                        <Button onClick={() => clickFilter("filter")}>搜索</Button>
                        <Button onClick={() => clickFilter("reset")}>重置</Button>
                    </Space>
                </div>
            </div>
        )
    }

    const renderTablePanel = () => {
        const tableColumns = [
            ...tableBaseColumns,
            {
                title: '操作',
                key: 'operate',
                dataIndex: 'operate',
                align: 'center',
                render: (event, event2) => (
                    <div className="table-operate-column-wrap">
                        <Space className="column-cls">
                            <Link target="_blank" to={`/notebook/noteBookArticleDetailsView?id=${event2.id}`}>详情</Link>
                            <Link target="_blank" to={`/notebook/noteBookArticleEditorView?id=${event2.id}&type=config`}>编辑</Link>
                        </Space>
                    </div>
                )
            }
        ]

        const rowSelection = () => {
            return {
                selectedRowKeys: tableSelectedList,
                onChange: event => {
                    setTableSelectedList(event)
                },
            }
        }

        const onPaginationChange = (page, size) => {
            const params = {
                ...filterParams,
                tableCurrent: page,
                tableSize: size
            }
            setFilterParams()
            filterParamsRef.current = params
        }
        return (
            <div className="table-panel">
                <Table className="table-cls"
                       rowSelection={rowSelection()}
                       dataSource={tableList}
                       columns={tableColumns}
                       rowKey={row => row.id}
                       pagination={false}
                />
                <Pagination
                    current={filterParams.tableCurrent}
                    pageSize={filterParams.tableSize}
                    total={tableTotal}
                    className="pagination-cls"
                    showQuickJumper
                    pageSizeOptions={[15, 30, 50, 100, 500]}
                    onChange={onPaginationChange}
                    showTotal={(total) => `共 ${total} 条`}
                />
            </div>
        )
    }

    return (
        <div className="notebook-article-view">
            {renderFilterPanel()}
            <Divider/>
            {renderTablePanel()}
        </div>
    )
}

export default NotebookHomeView