import "./index.less"
import {getDataDetails} from "@/api/notebook/notebookArticle";
import {useSearchParams} from "react-router-dom";
import ReactMarkdown  from 'react-markdown';

export function NoteBookArticleDetailsView() {
    const [ routerParams, setRouterParams ] = useSearchParams()
    const [articleText, setArticleText] = useState("");
    const [articleName, setArticleName] = useState("");

    useEffect(() => {
        initRouterParams();
    }, []);

    const initRouterParams = () => {
        const fileId = routerParams.get("id")
        const params = {
            id: fileId
        }
        getDataDetails(params).then(res => {
            const {name, details} = res
            setArticleName(name)
            setArticleText(details)
        }).catch(err => {

        })
    }

    const renderArticlePanel = () => {
        return (
            <div className="article-panel">
                <div className="name-wrap">{articleName}</div>
                <div dangerouslySetInnerHTML={{ __html: articleText}}>
                </div>
            </div>
        )
    }

    return(
        <div className="notebook-article-details-view">
            {renderArticlePanel()}
        </div>
    )
}

export default NoteBookArticleDetailsView