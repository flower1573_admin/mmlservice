import React from "react";
import "./index.less"
import {getDataList, createData, updateData, deleteData} from "@/api/notebook/noteBookTodo";
import {Button} from "antd";

export function NotebookHomeView() {
    const filterParamsRef = useRef({
        name: "",
        timeRange: [],
        tableCurrent: 1,
        tableSize: 5,
    })
    const [visibleConfig, setVisibleConfig] = useState(false);
    const configParamsRef = useRef({
        operateType: "",
        id: "",
        name: "",
        details: "",
    });

    const [tableList, setTableList] = useState([]);
    const [tableTotal, setTableTotal] = useState([]);


    useEffect(() => {
        initDataList();
    }, []);

    const initDataList = () => {
        const {name, tableCurrent, tableSize, timeRange} = filterParamsRef.current
        const params = {
            name: name,
            page: tableCurrent,
            size: tableSize,
        } as any
        if (timeRange.length > 0) {
            params.startTime = timeRange[0].valueOf()
            params.endTime = timeRange[1].valueOf()
        }
        getDataList(params).then((res: any) => {
            const {data, total} = res
            console.log(data)
            setTableList(data)
            setTableTotal(total)
        }).catch(err => {

        })
    }

    const renderFilterPanel = () => {
        const clickCreate = () => {
            configParamsRef.current = {
                ...configParamsRef.current,
                operateType: "create"
            }
            setVisibleConfig(true)
        }

        const onFilterChange = (key, event) => {
            const params = {
                ...filterParamsRef.current,
                [key]: event
            }
            filterParamsRef.current = params
        }

        const clickFilter = (type) => {
            if (type == "reset") {
                const params = {
                    name: "",
                    timeRange: [],
                    tableCurrent: 1,
                    tableSize: 15,
                }
                filterParamsRef.current = params
            }
        }

        return (
            <div className="filter-panel">
                <div className="panel-left">
                    <Space>
                        <Button onClick={clickCreate}>新增</Button>
                    </Space>
                </div>

                <div className="panel-right">
                    <Space>
                        <div>名称</div>
                        <Input value={filterParamsRef.current.name}
                               onChange={(event) => onFilterChange("name", event.target.value)} allowClear/>
                        <div>更新时间</div>
                        <DatePicker.RangePicker value={filterParamsRef.current.timeRange}
                                                onChange={event => onFilterChange("timeRange", event)} showTime
                                                allowClear/>
                        <Button onClick={() => clickFilter("filter")}>搜索</Button>
                        <Button onClick={() => clickFilter("reset")}>重置</Button>
                    </Space>
                </div>
            </div>
        )
    }

    const renderConfigModal = () => {
        const clickCancel = () => {
            setVisibleConfig(false)
        }
        const clickConfirm = () => {
            const {operateType, id, name, details} = configParamsRef.current
            const executeCreate = () => {
                const params = {
                    name: name,
                    details: details,
                }
                createData(params).then(res => {
                    clickCancel()
                    initDataList()
                }).catch(err => {

                })
            }

            const executeUpdate = () => {
                const params = {
                    id: id,
                    name: name,
                    details: details,
                }
                updateData(params).then(res => {
                    clickCancel()
                    initDataList()
                }).catch(err => {

                })
            }
            if (configParamsRef.current.operateType == "create") {
                executeCreate()
            } else {
                executeUpdate()
            }
        }
        const onInputChange = (key, event) => {
            const params = configParamsRef.current
            configParamsRef.current = {
                ...params,
                [key]: event
            }
        }
        return (
            <Modal
                title="配置"
                open={visibleConfig}
                onOk={clickConfirm}
                onCancel={clickCancel}
                keyboard={true}
                classNames="user-config-modal"
                width="30%"
            >
                <Form
                    name="basic"
                    autoComplete="off"
                    labelCol={{ span: 2 }}
                >
                    <Form.Item
                        label="名称"
                        name="name"
                        rules={[{ required: true, message: '请输入名称' }]}
                    >
                        <Input defaultValue={configParamsRef.current.name} value={configParamsRef.current.name} onChange={event => onInputChange("name", event.target.value)}/>
                    </Form.Item>
                    <Form.Item
                        label="备注"
                        name="details"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Input.TextArea
                            showCount
                            style={{ height: 100 }}
                            defaultValue={configParamsRef.current.details}
                            value={configParamsRef.current.details}
                            onChange={event => onInputChange("details", event.target.value)}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        )
    }

    const renderCardPanel = () => {
        const clickUpdate = (event) => {
            const {id, name, details} = event
            configParamsRef.current = {
                ...configParamsRef.current,
                operateType: "update",
                id: id,
                name: name,
                details: details,
            }
            setVisibleConfig(true)
        }

        const clickDelete = (event) => {
            const params = {
                idList: [event]
            }
            deleteData(params).then(res => {
                initDataList()
            }).catch(err => {

            })
        }

        const onPaginationChange = (page, size) => {
            filterParamsRef.current = {
                ...filterParamsRef.current,
                tableCurrent: page,
                tableSize: size,
            }
            initDataList()
        }

        return (
            <div className="card-panel">
                <div className="card-details">
                    {
                        tableList.map((item, index) => {
                            return <Card
                                title={item.name}
                                key={item.id}
                                extra={
                                    <Space>
                                        <Button type="link" onClick={event => clickUpdate(item)}>编辑</Button>
                                        <Button type="link" onClick={event => clickDelete(item.id)}>删除</Button>
                                    </Space>
                                }
                                className="crad-item-wrap"
                            >
                                <p>{item.details}</p>
                                <div>{item.createTime}</div>
                            </Card>
                        })
                    }
                </div>
                <Pagination
                    current={filterParamsRef.current.tableCurrent}
                    pageSize={filterParamsRef.current.tableSize}
                    total={tableTotal}
                    className="pagination-cls"
                    showQuickJumper
                    pageSizeOptions={[5, 10, 30, 50, 100]}
                    onChange={onPaginationChange}
                    showTotal={(total) => `共 ${total} 条`}
                />
            </div>
        )
    }

    return (
        <div className="notebook-todo-view">
            {renderFilterPanel()}
            {renderCardPanel()}
            {renderConfigModal()}
        </div>
    )
}

export default NotebookHomeView