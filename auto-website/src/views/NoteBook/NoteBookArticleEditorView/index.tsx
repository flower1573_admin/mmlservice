import React from "react";
import {useSearchParams} from "react-router-dom";
import BraftEditor from 'braft-editor';
import 'braft-editor/dist/index.css';
import {createData, updateData, getDataDetails} from "@/api/notebook/notebookArticle";

import "./index.less"

export function NoteBookArticleEditorView() {
    const [routerParams, setRouterParams] = useSearchParams()
    const [articleText, setArticleText] = useState(BraftEditor.createEditorState(null));
    const editorRef = useRef(null);
    const configParamsRef = useRef({
        operateType: "",
        id: "",
        name: "",
        remark: "",
        details: ""
    });
    const [configParams, setConfigParams] = useState({
        name: "",
        remark: "",
        details: ""
    });

    useEffect(() => {
        initRouterParams();
    }, []);

    const initRouterParams = () => {
        const fileId = routerParams.get("id")
        const type = routerParams.get("type")
        if (type == "config") {
            const params = {
                id: fileId
            }
            getDataDetails(params).then(res => {
                const {name, details} = res
                const resParams = {
                    ...configParamsRef.current,
                    id: fileId,
                    name: name,
                    operateType: "config"
                }
                configParamsRef.current = resParams

                setArticleText(BraftEditor.createEditorState(details));
            }).catch(err => {
            })
        }
    }

    const renderEditorPanel = () => {
        const onEditorChange = (event) => {
            if (!event.isEmpty()) {
                const htmlValue = event.toHTML()
                setArticleText(event);
            }
        }

        const onInputChange = (key, value) => {
            configParamsRef.current = {
                ...configParamsRef.current,
                name: value
            }
        }

        const clickSave = () => {
            const {operateType, id, name} = configParamsRef.current
            const params = {
                id: id,
                name: name,
                details: articleText.toHTML()
            }

            if (operateType == "config") {
                updateData(params).then(res => {

                }).catch(err => {

                })
            } else {
                createData(params).then(res => {

                }).catch(err => {

                })
            }
        }

        return (
            <div className="editor-wrap">
                <Form className="form-cls"
                      autoComplete="off"
                >
                    <div className="form-wrap">
                        <Form.Item  className="name-input-form"
                            label="名称"
                            name="name"
                            rules={[{required: true, message: '请输入名称'}]}
                        >
                            <Input className="name-input-cls" key={configParamsRef.current.name}
                                   defaultValue={configParamsRef.current.name}
                                   value={configParamsRef.current.name}
                                   onChange={event => onInputChange("name", event.target.value)}
                            />
                        </Form.Item>
                        <Form.Item className="save-form"
                            label=""
                            name="save"
                            rules={[{required: true, message: '请输入名称'}]}
                        >
                            <Button type="primary" onClick={clickSave}>保存</Button>
                        </Form.Item>
                    </div>
                </Form>
                <BraftEditor className="editor-cls"
                    ref={editorRef}
                    value={articleText}
                    onChange={onEditorChange}
                />
            </div>
        )
    }
    return (
        <div className="notebook-article-editor-view">
            {renderEditorPanel()}
        </div>
    )
}

export default NoteBookArticleEditorView