export const tableBaseColumns = [
    {
        title: '序号',
        dataIndex: 'number',
        key: 'number',
        render: (_, record, index) => index + 1,
        align: 'center',
    }, {
        title: '大小',
        dataIndex: 'size',
        key: 'size',
    }, {
        title: '使用率',
        dataIndex: 'cpuUserPerc',
        key: 'cpuUserPerc',
    }, {
        title: '制造商',
        dataIndex: 'vendor',
        key: 'vendor',
    }, {
        title: '描述',
        dataIndex: 'model',
        key: 'model',
    }, {
        title: '指纹',
        dataIndex: 'fingerPrint',
        key: 'fingerPrint',
    }, {
        title: '操作时间',
        dataIndex: 'requestTime',
        key: 'requestTime',
    },
]