import React from "react";

import "./index.less"
import {tableBaseColumns} from "./moudle"
import {getDiskInfo} from "@/api/systemManag/serveDisk";

export function LogManageView() {
  const {RangePicker} = DatePicker;

  let renderRef = useRef(true);
  const [tableList, setTableList] = useState([])

  useEffect(() => {
    initTableList();
  }, []);

  const initTableList = () => {
    getDiskInfo().then(res => {
      const newList = res.map((item, index) => {
        return {
          id: index,
          ...item
        }
      })
      setTableList(newList)
    }).catch(err => {
    })
  }

  const renderTablePanel = () => {
    const tableColumns = [
      ...tableBaseColumns
    ]
    return (
      <div className="table-panel">
        <Table
          dataSource={tableList}
          columns={tableColumns}
          rowKey={row => row.id}
          pagination={false}
        />
      </div>
    )
  }

  return <div className='system-manage-view-servediskview-view-container'>
    {renderTablePanel()}
  </div>
}

export default LogManageView;