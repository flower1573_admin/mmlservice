export const tableBaseColumns = [
    {
        title: '序号',
        dataIndex: 'number',
        key: 'number',
        render: (_, record, index) => index + 1,
        align: 'center',
    }, {
        title: '盘符名称',
        dataIndex: 'devName',
        key: 'devName',
    }, {
        title: '盘符类型',
        dataIndex: 'sysTypeName',
        key: 'sysTypeName',
    }, {
        title: '可用大小',
        dataIndex: 'useageUsed',
        key: 'useageUsed',
    }, {
        title: '总大小',
        dataIndex: 'useageTotal',
        key: 'useageTotal',
    }, {
        title: '利用率',
        dataIndex: 'useageUsedPerc',
        key: 'useageUsedPerc',
    }
]