export const tableBaseColumns = [
    {
        title: "序号",
        dataIndex: "number",
        key: "number",
        render: (_, record, index) => index + 1,
        align: "center",
    }, {
        title: "名称",
        dataIndex: "fileName",
        key: "fileName",
    }, {
        title: "类型",
        dataIndex: "fileType",
        key: "fileType",
    }, {
        title: "大小(MB)",
        dataIndex: "fileSize",
        key: "fileSize",
    }, {
        title: "创建时间",
        dataIndex: "createTime",
        key: "createTime",
    }, {
        title: "更新时间",
        dataIndex: "updateTime",
        key: "updateTime",
    }
]