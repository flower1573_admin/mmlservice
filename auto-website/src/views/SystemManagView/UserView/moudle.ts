export const tableBaseColumns = [
    {
        title: '序号',
        dataIndex: 'number',
        key: 'number',
        render: (_, record, index) => index + 1,
        align: 'center',
    }, {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '账号',
        dataIndex: 'account',
        key: 'account',
    }, {
        title: '创建时间',
        dataIndex: 'createTime',
        key: 'createTime',
    }, {
        title: '修改时间',
        dataIndex: 'updateTime',
        key: 'updateTime',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }
]