import React from "react";

import "./index.less"
import {tableBaseColumns} from "./moudle"
import {getUserList, createUser, updateUser, deleteUser} from "@/api/systemManag/user";

const {RangePicker} = DatePicker;
const { TextArea } = Input;

export function LogManageView() {
    let renderRef = useRef(true);

    const [tableList, setTableList] = useState([])
    const [tableCurrent, setTableCurrent] = useState(1)
    const [tableSize, setTableSize] = useState(15)
    const [tableTotal, setTableTotal] = useState(0)
    const [visibleConfig, setVisibleConfig] = useState(false)
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);

    const configParamsRef  = useRef({
        id: "",
        name: "",
        account: "",
        phone: "",
        email: "",
        password: "",
        remark: "",
    })

    const filterParamsRef  = useRef({
        name: "",
        account: "",
        timeRange: []
    })

    useEffect(() => {
        if (renderRef.current) {
            renderRef.current = false;
            return;
        }
        initTableList();
    }, []);

    const initTableList = () => {
        const params = {}
        getUserList(params).then(res => {
            const {data, total} = res
            setTableList(data)
            setTableTotal(total)
        }).catch(err => {
        })
    }

    const renderFilterPanel = () => {
        const clickCreate = () => {
            configParamsRef.current = {
                ...configParamsRef.current,
                operateType: "create",
            }
            setVisibleConfig(true)
        }

        const onFilterChange = (type, event) => {
            const newParams = {
                ...filterParamsRef.current,
            }
            if (["name", "account","password", "remark"].includes(type)){
                newParams[type] = event.target.value
            }
            if ([ "timeRange"].includes(type)){
                newParams[type] = event
            }
            filterParamsRef.current = newParams
        }

        const clickFilter = () => {
            initTableList()
        }

        const clickReset = () => {
            initTableList()
        }

        return (
            <div className="filter-panel">
                <div className="filter-left">
                    <Space>
                        <Button className="filter-item" onClick={clickCreate}>创建</Button>
                        <Button className="filter-item" onClick={clickFilter}>删除</Button>
                    </Space>
                </div>
                <div className="filter-right">
                    <div className="filter-item">
                        <div className="label-cls">名称</div>
                        <Input className="filter-value-item" value={filterParamsRef.current.name} onChange={event => onFilterChange("name", event)}/>
                    </div>
                    <div className="filter-item">
                        <div className="label-cls">名称</div>
                        <Input className="filter-value-item" value={filterParamsRef.current.account} onChange={event => onFilterChange("account", event)}/>
                    </div>
                    <div className="filter-item">
                        <div className="label-cls">更新时间</div>
                        <RangePicker value={filterParamsRef.current.timeRange} onChange={event => onFilterChange("timeRange", event)} showTime/>
                    </div>
                    <Button className="filter-item" onClick={clickFilter}>搜索</Button>
                    <Button className="filter-item" onClick={clickReset}>重置</Button>
                </div>
            </div>
        )
    }

    const renderTablePanel = () => {
        const onSelectChange = (event) => {
            setSelectedRowKeys(event);
        }

        const rowSelection =  {
            selectedRowKeys,
            onChange: onSelectChange,
        };

        const clickConfig = (event) => {
            const {id, name, account, email, phone, remark} = event
            configParamsRef.current = {
                operateType: "update",
                id: id,
                name: name,
                email: email,
                phone: phone,
                account: account,
                remark: remark,
            }
            setVisibleConfig(true)
        }
        const clickDelete = (event) => {
            const params = {
                idList: [event.id]
            }

            deleteUser(params).then(res=> {

            }).catch(err => {

            })
        }

        const onPaginationChange = (page, size) => {
            setTableCurrent(page)
            setTableSize(size)
        }

        const tableColumns = [
            ...tableBaseColumns,
            {
                title: '操作',
                key: 'operate',
                dataIndex: 'operate',
                align: 'center',
                render: (event, event2) => (
                    <div className="table-operate-column-wrap">
                        <div className="column-cls" onClick={tags => clickConfig(event2)}>编辑</div>
                        <div className="column-cls" onClick={tags => clickDelete(event2)}>删除</div>
                    </div>
                )
            }
        ]
        return (
            <div className="table-panel">
                <Table
                    rowSelection={rowSelection}
                    dataSource={tableList}
                    columns={tableColumns}
                    rowKey={row => row.id}
                    pagination={false}
                />
                <Pagination
                    current={tableCurrent}
                    pageSize={tableSize}
                    total={tableTotal}
                    className="pagination-cls"
                    showQuickJumper
                    pageSizeOptions={[15, 30, 50, 100, 500]}
                    onChange={onPaginationChange}
                    showTotal={(total) => `共 ${total} 条`}
                />
            </div>
        )
    }

    const renderConfigModal = () => {
        const clickCancel = () => {
            setVisibleConfig(false)
        }
        const clickConfirm = () => {
            const {operateType, id, name, account, phone, email, password, remark} = configParamsRef.current
            const params = {
                id: id,
                name: name,
                account: account,
                phone: phone,
                email: email,
                password: password,
                remark: remark,
            }

            if (operateType == "create"){
                createUser(params).then(res=> {
                    clickCancel()
                }).catch(err => {

                })
            } else {
                updateUser(params).then(res=> {
                    clickCancel()
                }).catch(err => {

                })
            }
        }
        const onInputChange = (key, event) => {
            const value = event.target.value
            const params = configParamsRef.current
            params[key] = value
        }
        return (
            <Modal
                title="配置"
                open={visibleConfig}
                onOk={clickConfirm}
                onCancel={clickCancel}
                keyboard={true}
                classNames="user-config-modal"
                width="30%"
            >
                <Form
                    name="basic"
                    autoComplete="off"
                    labelCol={{ span: 2 }}
                >
                    <Form.Item
                        label="名称"
                        name="name"
                        rules={[{ required: true, message: '请输入名称' }]}
                    >
                        <Input defaultValue={configParamsRef.current.name} value={configParamsRef.current.name} onChange={event => onInputChange("name", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="账号"
                        name="account"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Input defaultValue={configParamsRef.current.account} value={configParamsRef.current.account} onChange={event => onInputChange("account", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="电话"
                        name="phone"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Input defaultValue={configParamsRef.current.phone} value={configParamsRef.current.phone} onChange={event => onInputChange("phone", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="邮箱"
                        name="email"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Input defaultValue={configParamsRef.current.email} value={configParamsRef.current.email} onChange={event => onInputChange("email", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="密码"
                        name="password"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Input.Password defaultValue={configParamsRef.current.password} value={configParamsRef.current.password} onChange={event => onInputChange("password", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="备注"
                        name="remark"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <TextArea
                            showCount
                            style={{ height: 100 }}
                            defaultValue={configParamsRef.current.remark}
                            value={configParamsRef.current.remark}
                            onChange={event => onInputChange("remark", event)}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        )
    }

    return <div className='system-manage-view-user-view-container'>
        {renderFilterPanel()}
        {renderTablePanel()}
        {renderConfigModal()}
    </div>
}

export default LogManageView;