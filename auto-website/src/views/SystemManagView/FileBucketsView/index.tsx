import "./index.less"
import {getDataList, createData, updateData, deleteData} from "@/api/systemManag/fileBuckets";
import MessageComponents from "@/components/MessageComponents";
import React from "react";
import {tableBaseColumns} from "./moudle";
import {AiOutlineEdit} from "react-icons/ai";

export function FileBucketsView() {
    const [filterParams, setFilterParams] = useState(() => {
        return {
            name: "",
            timeRange: [],
            tableCurrent: 1,
            tableSize: 15,
        }
    });
    const filterParamsRef = useRef({
        name: "",
        timeRange: [],
        tableCurrent: 1,
        tableSize: 15,
    })
    const [tableList, setTableList] = useState([]);
    const [tableSelectedList, setTableSelectedList] = useState([]);
    const [tableTotal, setTableTotal] = useState(0);

    const [visibleConfig, setVisibleConfig] = useState(false);
    const [configForm] = Form.useForm();
    const configParamsRef = useRef({
        operateType: "",
        id: "",
        bucketsName: "",
        bucketsCode: "",
        remark: "",
    });

    useEffect(() => {
        initDataList();
    }, []);

    const initDataList = () => {
        const {name, tableCurrent, tableSize, timeRange} = filterParamsRef.current
        const params = {
            name: name,
            page: tableCurrent,
            size: tableSize,
        } as any
        if (timeRange.length > 0) {
            params.startTime = timeRange[0].valueOf()
            params.endTime = timeRange[1].valueOf()
        }
        getDataList(params).then((res: any) => {
            const {data, total} = res
            setTableList(data)
            setTableTotal(total)
        }).catch(err => {

        })
    }

    const renderFilterPanel = () => {
        const clickImport = () => {
            configParamsRef.current = {
                ...configParamsRef.current,
                operateType: "create"
            }
            setVisibleConfig(true)
        }

        const clickExport = () => {

        }

        const clickDelete = () => {
            const params = {
                idList: tableSelectedList
            }
            deleteData(params).then(res => {
                MessageComponents.success("删除成功")
                initDataList()
            }).catch(err => {

            })
        }

        const clickExportTemplate = () => {

        }

        const onFilterChange = (key, event) => {
            const params = {
                ...filterParams,
                [key]: event
            }
            setFilterParams(params)
            filterParamsRef.current = params
        }

        const clickFilter = (type) => {
            if (type == "reset") {
                const params = {
                    ...filterParams,
                    name: "",
                    timeRange: [],
                    tableCurrent: 1,
                    tableSize: 15,
                }
                setFilterParams(params)
                filterParamsRef.current = params
            }
            initDataList()
        }

        return (
            <div className="filter-panel">
                <div className="panel-left">
                    <Space>
                        <Button onClick={clickImport}>新增</Button>
                        <Button onClick={clickDelete}>删除</Button>
                    </Space>
                </div>

                <div className="panel-right">
                    <Space>
                        <div>名称</div>
                        <Input value={filterParams.name}
                               onChange={(event) => onFilterChange("name", event.target.value)} allowClear/>
                        <div>更新时间</div>
                        <DatePicker.RangePicker value={filterParams.timeRange}
                                                onChange={event => onFilterChange("timeRange", event)} showTime
                                                allowClear/>
                        <Button onClick={() => clickFilter("filter")}>搜索</Button>
                        <Button onClick={() => clickFilter("reset")}>重置</Button>
                    </Space>
                </div>
            </div>
        )
    }

    const renderTablePanel = () => {
        const tableColumns = [
            ...tableBaseColumns,
            {
                title: '操作',
                key: 'operate',
                dataIndex: 'operate',
                align: 'center',
                render: (event, event2) => (
                    <div className="table-operate-column-wrap">
                        <AiOutlineEdit onClick={tags => clickConfig(event2)} className="operate-icon"/>
                    </div>
                )
            }
        ]

        const clickConfig = (event) => {
            const {id, bucketsName, bucketsCode, remark} = event
            configParamsRef.current = {
                operateType: "update",
                id: id,
                bucketsName: bucketsName,
                bucketsCode: bucketsCode,
                remark: remark,
            }
            setVisibleConfig(true)
        }

        const rowSelection = () => {
            return {
                selectedRowKeys: tableSelectedList,
                onChange: event => {
                    setTableSelectedList(event)
                },
            }
        }

        const onPaginationChange = (page, size) => {
            const params = {
                ...filterParams,
                tableCurrent: page,
                tableSize: size
            }
            setFilterParams()
            filterParamsRef.current = params
        }
        return (
            <div className="table-panel">
                <Table className="table-cls"
                       rowSelection={rowSelection()}
                       dataSource={tableList}
                       columns={tableColumns}
                       rowKey={row => row.id}
                       pagination={false}
                />
                <Pagination
                    current={filterParams.tableCurrent}
                    pageSize={filterParams.tableSize}
                    total={tableTotal}
                    className="pagination-cls"
                    showQuickJumper
                    pageSizeOptions={[15, 30, 50, 100, 500]}
                    onChange={onPaginationChange}
                    showTotal={(total) => `共 ${total} 条`}
                />
            </div>
        )
    }

    const renderConfigModal = () => {
        const clickCancel = () => {
            setVisibleConfig(false)
        }
        const clickConfirm = () => {
            const {operateType, id, bucketsName, bucketsCode, remark} = configParamsRef.current
            const executeCreate = () => {
                const params = {
                    bucketsName: bucketsName,
                    bucketsCode: bucketsCode,
                    remark: remark,
                }
                createData(params).then(res => {
                    MessageComponents.success("新增成功")
                    clickCancel()
                    initDataList()
                }).catch(err => {
                    MessageComponents.error(err.message)
                })
            }

            const executeUpdate = () => {
                const params = {
                    id: id,
                    bucketsName: bucketsName,
                    bucketsCode: bucketsCode,
                    remark: remark,
                }
                updateData(params).then(res => {
                    MessageComponents.success("编辑成功")
                    clickCancel()
                    initDataList()
                }).catch(err => {
                    MessageComponents.error("编辑失败")
                })
            }

            configForm.validateFields().then(values => {
                if (configParamsRef.current.operateType == "create") {
                    executeCreate()
                } else {
                    executeUpdate()
                }
            }).catch(err => {
            });
        }
        const onInputChange = (key, event) => {
            const params = configParamsRef.current
            configParamsRef.current = {
                ...params,
                [key]: event
            }
        }

        const validatorBucketsCode = (rules, value, callback) => {
            const regex = /^[A-Za-z]+$/;
            if (!regex.test(value)) {
                callback(new Error("只允许输入英文字符"))
            } else {
                callback()
            }
        }

        return (
            <Modal
                title="配置"
                open={visibleConfig}
                onOk={clickConfirm}
                onCancel={clickCancel}
                keyboard={true}
                classNames="user-config-modal"
                width="30%"
            >
                <Form
                    form={configForm}
                    name="basic"
                    autoComplete="off"
                    labelCol={{span: 2}}
                >
                    <Form.Item
                        label="名称"
                        name="bucketsName"
                        rules={[{required: true, message: '请输入名称'}]}
                        initialValue={configParamsRef.current.bucketsName}
                    >
                        <Input
                            value={configParamsRef.current.bucketsName}
                            onChange={event => onInputChange("bucketsName", event.target.value)}
                        />
                    </Form.Item>
                    <Form.Item
                        label="编码"
                        name="bucketsCode"
                        initialValue={configParamsRef.current.bucketsCode}
                        rules={[
                            {
                                required: true, message: '请输入编码'
                            }, {
                                validator: (rules, value, callback) => {
                                    validatorBucketsCode(rules, value, callback)
                                }
                            }
                        ]}
                    >
                        <Input
                            disabled={configParamsRef.current.operateType == "update"}
                            value={configParamsRef.current.bucketsCode}
                            onChange={event => onInputChange("bucketsCode", event.target.value)}
                        />
                    </Form.Item>
                    <Form.Item
                        label="备注"
                        name="remark"
                        initialValue={configParamsRef.current.remark}
                    >
                        <Input.TextArea
                            showCount
                            style={{height: 100}}
                            value={configParamsRef.current.remark}
                            onChange={event => onInputChange("remark", event.target.value)}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        )
    }

    return (
        <div className="file-buckets-view">
            {renderFilterPanel()}
            <Divider/>
            {renderTablePanel()}
            {renderConfigModal()}
        </div>
    )
}

export default FileBucketsView