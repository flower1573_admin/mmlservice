export const tableBaseColumns = [
    {
        title: '序号',
        dataIndex: 'number',
        key: 'number',
        render: (_, record, index) => index + 1,
        align: 'center',
    }, {
        title: 'IP地址',
        dataIndex: 'address',
        key: 'address',
    }, {
        title: '网关广播地址',
        dataIndex: 'broadcast',
        key: 'broadcast',
    }, {
        title: 'MAC地址',
        dataIndex: 'hwaddr',
        key: 'hwaddr',
    }, {
        title: '子网掩码',
        dataIndex: 'netmask',
        key: 'netmask',
    }, {
        title: '描述',
        dataIndex: 'description',
        key: 'description',
    }
]