import React, {useId} from "react";
import "./index.less"
import * as echarts from "echarts";
import {getServeInfo} from "@/api/systemManag/home";

export function HomeView() {
    const accessCanvasRef = useRef(null)
    const [serverInfo, setServerInfo] = useState({
        computerName: "--",
        userName: "--",
        arch: "--",
        version: "--",
        memTotal: "--",
        memUsed: "--",
        memPerc: "--",
        ipAddress: "--",
    })

    const [alarmTableList, setAlarmTableList] = useState([])

    useEffect(() => {
        initAccessCanvas();
        initServerInfo();
    }, []);

    const convertToGb = (data) => {
        try {
            const gbSize = data / (1024 * 1024 * 1024)
            return gbSize.toFixed(2) + " GB"
        } catch (e) {
            return "--"
        }
    }

    const getPerc = (totalValue, usedValue) => {
        try {
            const perc = usedValue / totalValue
            return perc.toFixed(2) + " %"
        } catch (e) {
            return "--"
        }
    }

    const initServerInfo = () => {
        getServeInfo().then(res => {
            console.log(res.inetInfo)
            const computerName = res.inetInfo.computerName
            const userName = res.inetInfo.userName
            const arch = res.osInfos.arch
            const version = res.osInfos.version
            const memTotal = convertToGb(res.memoryUsage.memTotal)
            const memUsed = convertToGb(res.memoryUsage.memUsed)
            const memPerc = getPerc(res.memoryUsage.memUsed, res.memoryUsage.memTotal)

            const ethernetInfo = res.ethernetList.find(item => item.address != "0.0.0.0")
            const ipAddress = ethernetInfo.address
            const params = {
                computerName: computerName,
                userName: userName,
                arch: arch,
                version: version,
                memTotal: memTotal,
                memUsed: memUsed,
                memPerc: memPerc,
                ipAddress: ipAddress,
            }
            setServerInfo(params)
        }).catch(err => {

        })
    }

    const initAccessCanvas = ()=> {
        const echartInstance = echarts.init(accessCanvasRef.current);

        const echartOptions = {
            grid: {
                x:8,
                y:32,
                x2:0,
                y2:0,
                containLabel: true
            },
            xAxis: {
                type: 'category',
                data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    data: [820, 932, 901, 934, 1290, 1330, 1320],
                    type: 'line',
                    smooth: true
                }
            ]
        };

        if (echartInstance){
            echartInstance.setOption(echartOptions)
        }
    }

    const renderRecentPanel = () => {
        return (
            <div className="recent-panel">
                <div className="panel-label">快捷入口</div>
            </div>
        )
    }

    const renderResourcePanel = () => {
        return (
            <div className="resource-panel">
                <Row className="panel-wrap" gutter={[8, 0]}>
                    <Col span={16}>
                        <div className="panel-item">
                            <div className="item-title">系统信息</div>
                            <div className="item-details">
                                <div className="info-item">
                                    <div className="info-item-label">主机</div>
                                    <div className="info-item-value">{serverInfo.computerName}</div>
                                </div>
                                <div className="info-item">
                                    <div className="info-item-label">账号</div>
                                    <div className="info-item-value">{serverInfo.userName}</div>
                                </div>
                                <div className="info-item">
                                    <div className="info-item-label">IP地址</div>
                                    <div className="info-item-value">{serverInfo.ipAddress}</div>
                                </div>
                                <div className="info-item">
                                    <div className="info-item-label">架构</div>
                                    <div className="info-item-value">{serverInfo.arch}</div>
                                </div>
                                <div className="info-item">
                                    <div className="info-item-label">版本</div>
                                    <div className="info-item-value">{serverInfo.version}</div>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col span={8}>
                        <div className="panel-item">
                            <div className="item-title">内存信息</div>
                            <div className="item-details">
                                <div className="info-item">
                                    <div className="info-item-label">使用率</div>
                                    <div className="info-item-value">{serverInfo.memPerc}</div>
                                </div>
                                <div className="info-item">
                                    <div className="info-item-label">总量</div>
                                    <div className="info-item-value">{serverInfo.memTotal}</div>
                                </div>
                                <div className="info-item">
                                    <div className="info-item-label">使用量</div>
                                    <div className="info-item-value">{serverInfo.memUsed}</div>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    const renderAccessPanel = () => {
        return (
            <div className="access-panel">
                <div className="panel-label">访问统计</div>
                <div className="canvas-cls" id="accessCanvas" ref={accessCanvasRef}>

                </div>
            </div>
        )
    }

    const renderAlarmPanel = () => {
        const alarmTableColumns = [
            {
                title: '序号',
                dataIndex: 'number',
                key: 'number',
            },{
                title: '名称',
                dataIndex: 'name',
                key: 'name',
            },{
                title: '时间',
                dataIndex: 'updateTime',
                key: 'updateTime',
            },
        ]
        return (
            <div className="alarm-panel">
                <div className="panel-label">历史告警</div>
                <div className="canvas-cls" id="accessCanvas" ref={accessCanvasRef}>
                    <Table
                        dataSource={alarmTableList}
                        columns={alarmTableColumns}
                        rowKey={row => row.id}
                        bordered
                    />
                </div>
            </div>
        )
    }

    return (
        <div className="system-manage-view-home-view-container">
            <div className="main-panel">
                {renderRecentPanel()}
                {renderResourcePanel()}
                {renderAlarmPanel()}
                {renderAccessPanel()}
            </div>
        </div>
    )
}

export default HomeView;