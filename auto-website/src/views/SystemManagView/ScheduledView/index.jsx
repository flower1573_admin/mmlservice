import React from "react";

import "./index.less"
import {tableBaseColumns} from "./moudle"
import {getTableList, createSchedule, deleteSchedule, updateSchedule, updateScheduleStatus, testSchedule} from "@/api/schedule/schedule";
const { TextArea } = Input;

export function LogManageView() {
    const {RangePicker} = DatePicker;

    let renderRef = useRef(true);
    const [filterParams, setFilterParams] = useState({
        timeRange: [],
        name: "",
        taskStatus: ""
    })
    const filterParamsRef  = useRef({
        timeRange: [],
        name: "",
        taskStatus: ""
    })

    const [tableList, setTableList] = useState([])
    const [tableCurrent, setTableCurrent] = useState(1)
    const [tableSize, setTableSize] = useState(15)
    const [tableTotal, setTableTotal] = useState(0)
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);

    const [visibleConfig, setVisibleConfig] = useState(false)
    const [configParams, setConfigParams] = useState({
        taskName: "",
        taskBean: "",
        taskMethod: "",
        taskParams: "",
        taskCron: "",
        taskStatus: "disable",
        remark: "",
    })
    const configParamsRef  = useRef({
        operateType: "",
        id: "",
        taskName: "",
        taskBean: "",
        taskMethod: "",
        taskParams: "",
        taskCron: "",
        taskStatus: "disable",
        remark: "",
    })

    useEffect(() => {
        if (renderRef.current) {
            renderRef.current = false;
            return;
        }
        initTableList();
    }, []);

    const initTableList = () => {
        const params = {
            page: tableCurrent,
            size: tableSize,
            name: filterParamsRef.current.name,
            taskStatus: filterParamsRef.current.taskStatus,
        }
        if (filterParamsRef.current.timeRange.length) {
            const startTime = filterParamsRef.current.timeRange[0].valueOf()
            const endTime = filterParamsRef.current.timeRange[1].valueOf()
            params.startTime = startTime
            params.endTime = endTime
        }
        getTableList(params).then(res => {
            const {data, total} = res
            setTableList(data)
            setTableTotal(total)
        }).catch(err => {
        })
    }


    const onPaginationChange = (page, size) => {
        if (tableSize != size) {
            setTableCurrent(1)
            setTableSize(size)
        } else {
            setTableCurrent(page)
        }
        initTableList()
    }

    const onTimeChange = (event) => {
        setFilterParams({
            ...filterParams,
            timeRange: event
        })
    }

    const clickFilter = () => {
        initTableList()
    }

    const clickReset = () => {
        setFilterParams({
            timeRange: []
        })
        initTableList()
    }

    const clickExport = () => {

    }

    const renderTablePanel = () => {
        const clickConfig = (event) => {
            const {id, taskName, taskBean, taskMethod, taskParams, taskCron, taskStatus, remark} = event
            configParamsRef.current = {
                operateType: "update",
                id: id,
                taskName: taskName,
                taskBean: taskBean,
                taskMethod: taskMethod,
                taskParams: taskParams,
                taskCron: taskCron,
                taskStatus: taskStatus,
                remark: remark,
            }
            setVisibleConfig(true)
        }
        const clickTest = (event) => {
            const params = {
                id: event.id
            }
            testSchedule(params).then(res=> {

            }).catch(err => {

            })
        }
        const clickDelete = (event) => {
            const params = {
                id: event.id
            }

            deleteSchedule(params).then(res=> {

            }).catch(err => {

            })
        }
        const onStatusChange = (row, event) => {
            const taskStatus = event ? "enable": "disable"
            const params = {
                id: row.id,
                taskStatus: taskStatus,
            }
            updateScheduleStatus(params).then(res=> {

            }).catch(err => {

            })
        }

        const onSelectChange = (event) => {
            setSelectedRowKeys(event);
        }

        const rowSelection =  {
            selectedRowKeys,
            onChange: onSelectChange,
        };

        const tableColumns = [
            ...tableBaseColumns,
            {
                title: '操作',
                key: 'operate',
                dataIndex: 'operate',
                align: 'center', //头部单元格和内容区水平居中
                render: (event, event2) => (
                    <div className="table-operate-column-wrap">
                        <div className="column-cls" onClick={tags => clickConfig(event2)}>编辑</div>
                        <div className="column-cls" onClick={tags => clickTest(event2)}>测试</div>
                        <div className="column-cls" onClick={tags => clickDelete(event2)}>删除</div>
                    </div>
                )
            }
        ]
        tableColumns[5] = {
            title: '状态',
            key: 'taskStatus',
            dataIndex: 'taskStatus',
            align: 'center',
            render: (event, event2) => (
                <div className="table-status-column-wrap">
                    <Switch checkedChildren="启用" unCheckedChildren="禁用" value={event2.taskStatus == "enable"} onChange={event => onStatusChange(event2, event)}/>
                </div>
            )
        }
        return (
            <div className="table-panel">
                <Table
                    pagination={false}
                    rowSelection={rowSelection}
                    dataSource={tableList}
                    columns={tableColumns}
                    rowKey={row => row.id}
                />
                <Pagination
                    current={tableCurrent}
                    pageSize={tableSize}
                    total={tableTotal}
                    className="pagination-cls"
                    showQuickJumper
                    pageSizeOptions={[15, 30, 50, 100, 500]}
                    onChange={onPaginationChange}
                    showTotal={(total) => `共 ${total} 条`}
                />
            </div>
        )
    }

    const renderFilterPanel = () => {
        const clickCreate = () => {
            configParamsRef.current = {
                ...configParamsRef.current,
                operateType: "create",
            }
            setVisibleConfig(true)
        }

        const onFilterChange = (type, event) => {
            console.log(event)
            const newParams = {
                ...filterParamsRef.current,
            }
            if (type == "name"){
                newParams.name = event.target.value
            }
            if (["taskStatus", "timeRange"].includes(type)){
                newParams[type] = event
            }
            filterParamsRef.current = newParams
            setFilterParams(newParams)
        }

        return (
            <div className="filter-panel">
                <div className="filter-left">
                    <Space>
                        <Button className="filter-item" onClick={clickCreate}>创建</Button>
                        <Button className="filter-item" onClick={clickFilter}>删除</Button>
                        <Button className="filter-item" onClick={clickFilter}>启用</Button>
                        <Button className="filter-item" onClick={clickFilter}>禁用</Button>
                    </Space>
                </div>
                <div className="filter-right">
                    <div className="filter-item">
                        <div className="label-cls">名称</div>
                        <Input className="filter-value-item" value={filterParams.name} onChange={event => onFilterChange("name", event)}/>
                    </div>
                    <div className="filter-item">
                        <div className="label-cls">类型</div>
                        <Select className="filter-value-item"
                            value={filterParams.type}
                            onSelect={event => onFilterChange("taskStatus", event)}
                            options={[
                                { value: 'enable', label: '启用' },
                                { value: 'disable', label: '禁用' },
                            ]}
                        />
                    </div>
                    <div className="filter-item">
                        <div className="label-cls">更新时间</div>
                        <RangePicker value={filterParams.timeRange} onChange={event => onFilterChange("timeRange", event)} showTime/>
                    </div>
                    <Button className="filter-item" onClick={clickFilter}>搜索</Button>
                    <Button className="filter-item" onClick={clickReset}>重置</Button>
                </div>
            </div>
        )
    }


    const renderConfigModal = () => {
        const clickCancel = () => {
            setVisibleConfig(false)
        }
        const clickConfirm = () => {
            const {operateType, id, taskName, taskBean, taskMethod, taskParams, taskCron, taskStatus, remark} = configParamsRef.current
            const params = {
                id: id,
                taskName: taskName,
                taskBean: taskBean,
                taskMethod: taskMethod,
                taskParams: taskParams,
                taskCron: taskCron,
                taskStatus: taskStatus,
                remark: remark,
            }

            if (operateType == "create"){
                createSchedule(params).then(res=> {
                    clickCancel()
                }).catch(err => {

                })
            } else {
                updateSchedule(params).then(res=> {
                    clickCancel()
                }).catch(err => {

                })
            }
        }
        const onInputChange = (key, event) => {
            const value = event.target.value
            const params = configParamsRef.current
            params[key] = value
        }
        return (
            <Modal
                title="配置"
                open={visibleConfig}
                onOk={clickConfirm}
                onCancel={clickCancel}
                keyboard={true}
                classNames="scheduled-config-modal"
                width="35%"
            >
                <Form
                    name="basic"
                    autoComplete="off"
                    labelCol={{ span: 2 }}
                >
                    <Form.Item
                        label="名称"
                        name="taskName"
                        rules={[{ required: true, message: '请输入名称' }]}
                    >
                        <Input defaultValue={configParamsRef.current.taskName} value={configParamsRef.current.taskName} onChange={event => onInputChange("taskName", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="类名"
                        name="taskBean"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Input defaultValue={configParamsRef.current.taskBean} value={configParamsRef.current.taskBean} onChange={event => onInputChange("taskBean", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="方法名"
                        name="taskMethod"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Input defaultValue={configParamsRef.current.taskMethod} value={configParamsRef.current.taskMethod} onChange={event => onInputChange("taskMethod", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="状态"
                        name="taskStatus"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Select className="filter-value-item"
                            value={configParamsRef.current.taskStatus}
                            defaultValue={configParamsRef.current.taskStatus}
                            onSelect={event => onInputChange("taskStatus", event)}
                            options={[
                                { value: 'enable', label: '启用' },
                                { value: 'disable', label: '禁用' },
                            ]}
                        />
                    </Form.Item>
                    <Form.Item
                        label="参数"
                        name="taskParams"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <TextArea
                            showCount
                            style={{ height: 100 }}
                            defaultValue={configParamsRef.current.taskParams}
                            value={configParamsRef.current.taskParams}
                            onChange={event => onInputChange("taskParams", event)}
                        />
                    </Form.Item>
                    <Form.Item
                        label="备注"
                        name="remark"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <TextArea
                            showCount
                            style={{ height: 100 }}
                            defaultValue={configParamsRef.current.remark}
                            value={configParamsRef.current.remark}
                            onChange={event => onInputChange("remark", event)}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        )
    }

    return <div className='system-manage-scheduled-view-container'>
        {renderFilterPanel()}
        {renderTablePanel()}
        {renderConfigModal()}
    </div>
}

export default LogManageView;