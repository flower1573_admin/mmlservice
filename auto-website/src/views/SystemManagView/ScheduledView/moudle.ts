export const tableBaseColumns = [
    {
        title: "序号",
        dataIndex: "number",
        key: "number",
        render: (_, record, index) => index + 1,
        align: "center",
    }, {
        title: "名称",
        dataIndex: "taskName",
        key: "taskName",
    }, {
        title: "类名",
        dataIndex: "taskBean",
        key: "taskBean",
    }, {
        title: "方法名",
        dataIndex: "taskMethod",
        key: "taskMethod",
    }, {
        title: "任务CRON",
        dataIndex: "taskCron",
        key: "taskCron",
    }, {
        title: "状态",
        dataIndex: "taskStatus",
        key: "taskStatus",
    }, {
        title: "备注",
        dataIndex: "remark",
        key: "remark",
    }, {
        title: "创建时间",
        dataIndex: "createTime",
        key: "createTime",
    }, {
        title: "更新时间",
        dataIndex: "updateTime",
        key: "updateTime",
    },
]