import React from "react";

import "./index.less"
import {tableBaseColumns} from "./moudle"
import {getDictionaryList, createDictionary, deleteDictionary, updateDictionary} from "@/api/systemManag/dictionary";

const {RangePicker} = DatePicker;
const { TextArea } = Input;

export function NavigatorView() {
    let renderRef = useRef(true);

    const [tableList, setTableList] = useState([])
    const [tableCurrent, setTableCurrent] = useState(1)
    const [tableSize, setTableSize] = useState(15)
    const [tableTotal, setTableTotal] = useState(0)
    const [visibleConfig, setVisibleConfig] = useState(false)
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);

    const configParamsRef  = useRef({
        operateType: "",
        id: "",
        dictionaryKey: "",
        dictionaryValue: "",
        remark: "",
    })

    const [filterParams, setFilterParams] = useState({
        dictionaryKey: ""
    });
    const filterParamsRef  = useRef({
        dictionaryKey: ""
    })

    useEffect(() => {
        initTableList();
    }, []);

    const initTableList = () => {
        const params = {
            dictionaryKey: filterParamsRef.current.dictionaryKey,
            size: tableSize,
            page: tableCurrent,
        }
        getDictionaryList(params).then(res => {
            const {data, total} = res
            setTableList(data)
            setTableTotal(total)
        }).catch(err => {
        })
    }

    const renderFilterPanel = () => {
        const clickCreate = () => {
            configParamsRef.current = {
                ...configParamsRef.current,
                operateType: "create",
            }
            setVisibleConfig(true)
        }

        const onFilterChange = (type, event) => {
            const value = event.target.value
            filterParamsRef.current = {
                ...filterParamsRef.current,
                dictionaryKey: value
            }
            setFilterParams({
                ...filterParams,
                dictionaryKey: value
            })
        }

        const clickFilter = () => {
            initTableList()
        }

        const clickReset = () => {
            filterParamsRef.current = {
                ...filterParamsRef.current,
                dictionaryKey: ""
            }
            setFilterParams({
                ...filterParams,
                dictionaryKey: ""
            })
            initTableList()
        }

        const clickDelete = (event) => {
            const idList = selectedRowKeys
            const params = {
                idList: idList
            }
            deleteDictionary(params).then(res=> {
                initTableList()
            }).catch(err => {

            })
        }

        const clickCreateCategory = ()=> {

        }

        return (
            <div className="filter-panel">
                <div className="filter-left">
                    <Space>
                        <Button className="filter-item" onClick={clickCreate}>创建</Button>
                        <Button className="filter-item" onClick={clickDelete}>删除</Button>
                    </Space>
                </div>
                <div className="filter-right">
                    <div className="filter-item">
                        <div className="label-cls">键</div>
                        <Input className="filter-value-item" value={filterParams.name} onChange={event => onFilterChange("name", event)}/>
                    </div>
                    <Button className="filter-item" onClick={clickFilter}>搜索</Button>
                    <Button className="filter-item" onClick={clickReset}>重置</Button>
                </div>
            </div>
        )
    }

    const renderTablePanel = () => {
        const onSelectChange = (event) => {
            setSelectedRowKeys(event);
        }

        const rowSelection =  {
            selectedRowKeys,
            onChange: onSelectChange,
        };

        const clickConfig = (event) => {
            const {id, dictionaryValue, dictionaryKey, remark} = event
            configParamsRef.current = {
                operateType: "update",
                id: id,
                dictionaryKey: dictionaryKey,
                dictionaryValue: dictionaryValue,
                remark: remark,
            }
            setVisibleConfig(true)
        }

        const clickLinkHref = (event) => {
            window.open(event, '_blank');
        }

        const onPaginationChange = (page, size) => {
            setTableCurrent(page)
            setTableSize(size)
        }

        const tableColumns = [
            ...tableBaseColumns,
            {
                title: '操作',
                key: 'operate',
                dataIndex: 'operate',
                align: 'center',
                render: (event, event2) => (
                    <div className="table-operate-column-wrap">
                        <div className="column-cls" onClick={tags => clickConfig(event2)}>编辑</div>
                    </div>
                )
            }
        ]
        return (
            <div className="table-panel">
                <Table
                    rowSelection={rowSelection}
                    dataSource={tableList}
                    columns={tableColumns}
                    rowKey={row => row.id}
                    pagination={false}
                />
                <Pagination
                    current={tableCurrent}
                    pageSize={tableSize}
                    total={tableTotal}
                    className="pagination-cls"
                    showQuickJumper
                    pageSizeOptions={[15, 30, 50, 100, 500]}
                    onChange={onPaginationChange}
                    showTotal={(total) => `共 ${total} 条`}
                />
            </div>
        )
    }

    const renderConfigModal = () => {
        const clickCancel = () => {
            setVisibleConfig(false)
        }
        const clickConfirm = () => {
            const {operateType, id, dictionaryKey, dictionaryValue, remark} = configParamsRef.current
            const params = {
                id: id,
                dictionaryKey: dictionaryKey,
                dictionaryValue: dictionaryValue,
                remark: remark,
            }

            if (operateType == "create"){
                createDictionary(params).then(res=> {
                    clickCancel()
                    initTableList()
                }).catch(err => {

                })
            } else {
                updateDictionary(params).then(res=> {
                    clickCancel()
                    initTableList()
                }).catch(err => {

                })
            }
        }
        const onInputChange = (key, event) => {
            const value = event.target.value
            const params = configParamsRef.current
            params[key] = value
        }
        return (
            <Modal
                title="配置"
                open={visibleConfig}
                onOk={clickConfirm}
                onCancel={clickCancel}
                keyboard={true}
                classNames="user-config-modal"
                width="30%"
            >
                <Form
                    name="basic"
                    autoComplete="off"
                    labelCol={{ span: 2 }}
                >
                    <Form.Item
                        label="键"
                        name="name"
                        rules={[{ required: true, message: '请输入名称' }]}
                    >
                        <Input defaultValue={configParamsRef.current.dictionaryKey} value={configParamsRef.current.dictionaryKey} onChange={event => onInputChange("dictionaryKey", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="值"
                        name="linkHref"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Input defaultValue={configParamsRef.current.dictionaryValue} value={configParamsRef.current.dictionaryValue} onChange={event => onInputChange("dictionaryValue", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="备注"
                        name="remark"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <TextArea
                            showCount
                            style={{ height: 100 }}
                            defaultValue={configParamsRef.current.remark}
                            value={configParamsRef.current.remark}
                            onChange={event => onInputChange("remark", event)}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        )
    }

    return <div className='systemmanag-dictionary-view'>
        {renderFilterPanel()}
        {renderTablePanel()}
        {renderConfigModal()}
    </div>
}

export default NavigatorView;