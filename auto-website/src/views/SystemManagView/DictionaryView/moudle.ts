export const tableBaseColumns = [
    {
        title: '序号',
        dataIndex: 'number',
        key: 'number',
        render: (_, record, index) => index + 1,
        align: 'center',
    }, {
        title: '键',
        dataIndex: 'dictionaryKey',
        key: 'dictionaryKey',
    }, {
        title: '值',
        dataIndex: 'dictionaryValue',
        key: 'dictionaryValue',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }, {
        title: '创建时间',
        dataIndex: 'createTime',
        key: 'createTime',
    }, {
        title: '修改时间',
        dataIndex: 'updateTime',
        key: 'updateTime',
    }
]