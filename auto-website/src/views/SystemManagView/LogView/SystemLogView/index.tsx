import React from "react";

import "./index.less"
import {tableBaseColumns} from "./moudle"
import {getDataList, collectData, exportData} from "@/api/logManage/systemLog";
import {AiOutlineExport} from "react-icons/ai";

export function LogManageView() {
    const [filterParams, setFilterParams] = useState({
        type: null,
        timeRange: []
    })
    const filterParamsRef = useRef({
        type: "",
        timeRange: []
    })
    const [tableList, setTableList] = useState([])
    const [tableCurrent, setTableCurrent] = useState(1)
    const [tableSize, setTableSize] = useState(15)
    const [tableTotal, setTableTotal] = useState(0)

    useEffect(() => {
        initTableList();
    }, []);

    const initTableList = () => {
        const { type, timeRange } = filterParamsRef.current
        const startTime = timeRange[0]
        const endTime = timeRange[1]
        const params = {
            size: tableSize,
            page: tableCurrent,
            startTime: startTime,
            endTime: endTime,
        }
        getDataList(params).then(res => {
            const {data, total} = res
            setTableList(data)
            setTableTotal(total)
        }).catch(err => {
        })
    }

    const renderFilterPanel = () => {
        const onFilterChange = (key, event) => {
            const params = {
                ...filterParamsRef.current,
                [key]: event
            }
            setFilterParams(params)
            filterParamsRef.current = params
        }

        const clickCollect = () => {
            const {type, timeRange}= filterParamsRef.current
            const params = {
                type: type,
                startTime: timeRange[0].valueOf(),
                endTime: timeRange[1].valueOf(),
            }
            collectData(params).then(res => {
                initTableList()
            }).catch(err => {
            })
        }

        const clickReset = () => {
            const params = {
                type: null,
                timeRange: []
            }
            setFilterParams(params)
            filterParamsRef.current = params
            initTableList()
        }

        return (
            <div className="filter-panel">
                <div className="filter-left">
                    <Space>
                        <Select className="filter-value-item"
                                allowClear
                                placeholder="请选择日志类型"
                                value={filterParams.type}
                                onSelect={event => onFilterChange("type", event)}
                                options={[
                                    {value: 'all', label: '全部'},
                                    {value: 'normal', label: '一般'},
                                    {value: 'success', label: '成功'},
                                    {value: 'warning', label: '警告'},
                                    {value: 'error', label: '错误'}
                                ]}
                        />
                        <DatePicker.RangePicker
                            value={filterParams.timeRange}
                            onChange={event => onFilterChange("timeRange", event)}
                            showTime
                            allowClear
                        />
                        <Button className="filter-item"
                            disabled={!(filterParams.type && filterParams.timeRange && filterParams.timeRange.length)}
                            onClick={clickCollect}
                        >
                            收集
                        </Button>
                        <Button className="filter-item"
                            onClick={clickReset}
                        >
                            重置
                        </Button>
                    </Space>
                </div>
            </div>
        )
    }

    const renderTablePanel = () => {
        const clickExport = (event) => {
            const params = {
                name: event.name
            }
            exportData(params).then(res=> {
                const {data, headers} = res
                const contentDisposition = res.headers["content-disposition"]
                if (contentDisposition) {
                    const match = contentDisposition.match(/filename="?([^";]*)"?/);
                    if (match) {
                        const fileName = decodeURI(match[1]);
                        const blob = new Blob([res.data])
                        const dom = document.createElement('a')
                        const downUrl = window.URL.createObjectURL(blob)
                        dom.href = downUrl
                        dom.download = decodeURIComponent(fileName)
                        dom.style.display = 'none'
                        document.body.appendChild(dom)
                        dom.click()
                        dom.parentNode.removeChild(dom)
                        window.URL.revokeObjectURL(downUrl)
                    }
                }
            }).catch(err => {

            })
        }

        const tableColumns = [
            ...tableBaseColumns,
            {
                title: '操作',
                key: 'operate',
                dataIndex: 'operate',
                align: 'center',
                render: (event, event2) => (
                    <div className="table-operate-column-wrap">
                        <AiOutlineExport onClick={tags => clickExport(event2)} className="operate-icon"/>
                    </div>
                )
            }
        ]

        const onPaginationChange = (page, size) => {
            if (tableSize != size) {
                setTableCurrent(1)
                setTableSize(size)
            } else {
                setTableCurrent(page)
            }
            initTableList()
        }

        return (
            <div className="table-panel">
                <Table
                    dataSource={tableList}
                    columns={tableColumns}
                    rowKey={row => row.id}
                    pagination={false}
                />
                <Pagination
                    current={tableCurrent}
                    pageSize={tableSize}
                    total={tableTotal}
                    className="pagination-cls"
                    showQuickJumper
                    pageSizeOptions={[15, 30, 50, 100, 500]}
                    onChange={onPaginationChange}
                    showTotal={(total) => `共 ${total} 条`}
                />
            </div>
        )
    }

    return <div className='system-log-view'>
        {renderFilterPanel()}
        {renderTablePanel()}
    </div>
}

export default LogManageView;