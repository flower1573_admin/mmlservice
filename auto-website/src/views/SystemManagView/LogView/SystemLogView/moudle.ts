export const tableBaseColumns = [
    {
        title: '序号',
        dataIndex: 'number',
        key: 'number',
        render: (_, record, index) => index + 1,
        align: 'center',
    }, {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '开始时间',
        dataIndex: 'startTime',
        key: 'startTime',
    }, {
        title: '结束时间',
        dataIndex: 'endTime',
        key: 'endTime',
    },
]