export const tableBaseColumns = [
    {
        title: '序号',
        dataIndex: 'number',
        key: 'number',
        render: (_, record, index) => index + 1,
        align: 'center',
    }, {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '类型',
        dataIndex: 'requestType',
        key: 'requestType',
    }, {
        title: '模块',
        dataIndex: 'requestMoudle',
        key: 'requestMoudle',
    }, {
        title: '客户端地址',
        dataIndex: 'userIp',
        key: 'userIp',
    }, {
        title: '指纹',
        dataIndex: 'id',
        key: 'id',
    }, {
        title: '操作时间',
        dataIndex: 'requestTime',
        key: 'requestTime',
    },
]