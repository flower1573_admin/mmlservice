import React from "react";
import menuList from "./moudle";
import "./index.less"

export function HomeView() {
    const navigate = useNavigate()

    const clikHeaderMenu = (event) =>{
        navigate(event)
    }

    const clikLoginIn = () => {
        navigate("/loginIn")
    }

    return (
        <div className="home-view-container">
            <div className="header-panel">
                <div className="header-name">
                    barbarossa
                </div>
                <div className="header-menu-wrap" id="homeHeader">
                    <div className="header-item" onClick={() => clikHeaderMenu("")}>首页</div>
                    <div className="header-item" onClick={() => clikHeaderMenu("systemManag/systemManagHomeView")}>系统</div>
                </div>
                <div>
                    <Button type="primary" className="header-login-register" onClick={clikLoginIn}>登录/注册</Button>
                </div>
            </div>
            <div className="main-panel">
                <Carousel>
                    <div className="banner-one">
                        <div className="banner-desc">
                            让你的工作可餐, 一款为开发者提升工作效率的工具集
                        </div>
                    </div>
                    <div className="banner-two">
                        <div className="banner-desc">
                            让你的工作可餐, 一款为开发者提升工作效率的工具集
                        </div>
                    </div>
                </Carousel>

                <div className="men-wrap">
                    <div className="menu-item">
                        价值
                    </div>
                    <div className="menu-item">
                        可靠
                    </div>
                    <div className="menu-item">
                        便捷
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomeView;