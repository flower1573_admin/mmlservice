import { ProChat } from '@ant-design/pro-chat';
import { useTheme } from 'antd-style';

import "./index.less"
import {sendMessage} from "@/api/chatRoom/chatRoom";

export function ChatRoom() {
    const socketRef = useRef(null)
    const socketMessageRef = useRef("")
    const [socketMessage, setSocketMessage] = useState([])

    useEffect(() => {
        initWebsocket();
    }, []);

    const initWebsocket = () => {
        if (socketRef.current == null) {
            socketRef.current = new WebSocket("ws://localhost:31986/ws");

            socketRef.current.onmessage = function(event){
                socketMessageRef.current = event.data
            }
        }
    }

    const renderChatPanel = () => {
        const theme = useTheme();
        return (
            <div className="chat-panel" >
                <ProChat
                    helloMessage = {
                        '欢迎在线客服，我是你的专属机器人'
                    }
                    request={async (event) => {
                        const currentEvent = event.pop()
                        const params = {
                            message: currentEvent.message
                        }
                        await sendMessage(params).then(res=> {
                        }).catch(err => {
                        }).finally(() => {
                        })
                        return new Response(socketMessageRef.current);
                    }}
                />
            </div>
        )
    }
    return (
        <div className="chat-room-view">
            {renderChatPanel()}
        </div>
    )
}

export default ChatRoom