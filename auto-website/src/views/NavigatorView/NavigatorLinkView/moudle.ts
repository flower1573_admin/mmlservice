export const tableBaseColumns = [
    {
        title: '序号',
        dataIndex: 'number',
        key: 'number',
        render: (_, record, index) => index + 1,
        align: 'center',
    }, {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '类别',
        dataIndex: 'categoryName',
        key: 'categoryName',
    }, {
        title: '链接',
        dataIndex: 'linkHref',
        key: 'linkHref',
    }, {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
    }
]