import React from "react";

import "./index.less"
import {tableBaseColumns} from "./moudle"
import {getNavigatorList, createNavigator, deleteNavigator, updateNavigator, exportNavigator} from "@/api/navigator/navigator";
import {getAllNavigatorList} from "@/api/navigator/navigatorCategory";

const {RangePicker} = DatePicker;
const { TextArea } = Input;

export function NavigatorView() {
    let renderRef = useRef(true);

    const [navigatorList, setNavigatorList] = useState([])
    const [tableList, setTableList] = useState([])
    const [tableCurrent, setTableCurrent] = useState(1)
    const [tableSize, setTableSize] = useState(15)
    const [tableTotal, setTableTotal] = useState(0)
    const [visibleConfig, setVisibleConfig] = useState(false)
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);

    const configParamsRef  = useRef({
        id: "",
        name: "",
        categoryId: "",
        linkHref: "",
        remark: "",
    })

    const [filterParams, setFilterParams] = useState({
        name: ""
    });
    const filterParamsRef  = useRef({
        name: ""
    })

    useEffect(() => {
        if (renderRef.current) {
            renderRef.current = false;
            return;
        }
        initTableList();
        initCategoryList();
    }, []);

    const initCategoryList = () => {
        const params = {}
        getAllNavigatorList(params).then(res => {
            const dataList = res.map(item => {
                const {id, name} = item
                return {
                    label: name,
                    value: id,
                }
            })
            setNavigatorList(dataList)
        }).catch(err => {
        })
    }

    const initTableList = () => {
        const params = {
            name: filterParamsRef.current.name,
            size: tableSize,
            page: tableCurrent,
        }
        getNavigatorList(params).then(res => {
            const {data, total} = res
            const dataList = data.map(item => {
                const {navigatorCategoryEntity} = item
                return {
                    ...item,
                    categoryName: navigatorCategoryEntity.name,
                }
            })
            setTableList(dataList)
            setTableTotal(total)
        }).catch(err => {
        })
    }

    const renderFilterPanel = () => {
        const clickCreate = () => {
            configParamsRef.current = {
                ...configParamsRef.current,
                operateType: "create",
            }
            setVisibleConfig(true)
        }

        const onFilterChange = (type, event) => {
            const value = event.target.value
            filterParamsRef.current = {
                ...filterParamsRef.current,
                name: value
            }
            setFilterParams({
                ...filterParams,
                name: value
            })
        }

        const clickFilter = () => {
            initTableList()
        }

        const clickReset = () => {
            filterParamsRef.current = {
                ...filterParamsRef.current,
                name: ""
            }
            setFilterParams({
                ...filterParams,
                name: ""
            })
            initTableList()
        }

        const clickDelete = (event) => {
            const idList = selectedRowKeys
            const params = {
                idList: idList
            }
            deleteNavigator(params).then(res=> {
                initTableList()
            }).catch(err => {

            })
        }

        const clickExport = (event) => {
            const idList = selectedRowKeys
            const params = {
                idList: idList
            }
            exportNavigator(params).then(res=> {
                const {data, headers} = res
                const contentDisposition = res.headers["content-disposition"]
                if (contentDisposition) {
                    const match = contentDisposition.match(/filename="?([^";]*)"?/);
                    if (match) {
                        const fileName = decodeURI(match[1]);
                        const blob = new Blob([res.data])
                        const dom = document.createElement('a')
                        const downUrl = window.URL.createObjectURL(blob)
                        dom.href = downUrl
                        dom.download = decodeURIComponent(fileName)
                        dom.style.display = 'none'
                        document.body.appendChild(dom)
                        dom.click()
                        dom.parentNode.removeChild(dom)
                        window.URL.revokeObjectURL(downUrl)
                    }
                }
            }).catch(err => {

            })
        }

        return (
            <div className="filter-panel">
                <div className="filter-left">
                    <Space>
                        <Button className="filter-item" onClick={clickCreate}>创建</Button>
                        <Button className="filter-item" onClick={clickDelete}>删除</Button>
                        <Button className="filter-item" onClick={clickExport}>导出</Button>
                    </Space>
                </div>
                <div className="filter-right">
                    <div className="filter-item">
                        <div className="label-cls">名称</div>
                        <Input className="filter-value-item" value={filterParams.name} onChange={event => onFilterChange("name", event)}/>
                    </div>
                    <Button className="filter-item" onClick={clickFilter}>搜索</Button>
                    <Button className="filter-item" onClick={clickReset}>重置</Button>
                </div>
            </div>
        )
    }

    const renderTablePanel = () => {
        const onSelectChange = (event) => {
            setSelectedRowKeys(event);
        }

        const rowSelection =  {
            selectedRowKeys,
            onChange: onSelectChange,
        };

        const clickConfig = (event) => {
            const {id, name, linkHref, remark, categoryId} = event
            configParamsRef.current = {
                operateType: "update",
                id: id,
                name: name,
                categoryId: categoryId,
                linkHref: linkHref,
                remark: remark,
            }
            console.log( configParamsRef.current)
            setVisibleConfig(true)
        }

        const clickLinkHref = (event) => {
            window.open(event.linkHref, '_blank');
        }

        const onPaginationChange = (page, size) => {
            setTableCurrent(page)
            setTableSize(size)
        }

        const tableColumns = [
            ...tableBaseColumns,
            {
                title: '操作',
                key: 'operate',
                dataIndex: 'operate',
                align: 'center',
                render: (event, event2) => (
                    <div className="table-operate-column-wrap">
                        <div className="column-cls" onClick={tags => clickConfig(event2)}>编辑</div>
                    </div>
                )
            }
        ]

        tableColumns[3] = {
            title: '链接',
            key: 'linkHref',
            dataIndex: 'linkHref',
            render: (event, row) => (
                <Button type="link" onClick={event => clickLinkHref(row)}>
                    <div>{row.linkHref}</div>
                </Button>
            ),
        }
        return (
            <div className="table-panel">
                <Table
                    rowSelection={rowSelection}
                    dataSource={tableList}
                    columns={tableColumns}
                    rowKey={row => row.id}
                    pagination={false}
                />
                <Pagination
                    current={tableCurrent}
                    pageSize={tableSize}
                    total={tableTotal}
                    className="pagination-cls"
                    showQuickJumper
                    pageSizeOptions={[15, 30, 50, 100, 500]}
                    onChange={onPaginationChange}
                    showTotal={(total) => `共 ${total} 条`}
                />
            </div>
        )
    }

    const renderConfigModal = () => {
        const clickCancel = () => {
            setVisibleConfig(false)
        }
        const clickConfirm = () => {
            const {operateType, id, name, category, linkHref, remark} = configParamsRef.current
            const params = {
                id: id,
                name: name,
                categoryId: category,
                linkHref: linkHref,
                remark: remark,
            }

            if (operateType == "create"){
                createNavigator(params).then(res=> {
                    clickCancel()
                    initTableList()
                }).catch(err => {

                })
            } else {
                updateNavigator(params).then(res=> {
                    clickCancel()
                    initTableList()
                }).catch(err => {

                })
            }
        }
        const onInputChange = (key, event) => {
            let value = ""
            if (key == "category") {
                value = event
            } else {
                value = event.target.value
            }
            const params = configParamsRef.current
            params[key] = value
        }
        return (
            <Modal
                title="配置"
                open={visibleConfig}
                onOk={clickConfirm}
                onCancel={clickCancel}
                keyboard={true}
                classNames="user-config-modal"
                width="30%"
            >
                <Form
                    name="basic"
                    autoComplete="off"
                    labelCol={{ span: 2 }}
                >
                    <Form.Item
                        label="名称"
                        name="name"
                        rules={[{ required: true, message: '请输入名称' }]}
                    >
                        <Input defaultValue={configParamsRef.current.name} value={configParamsRef.current.name} onChange={event => onInputChange("name", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="类型"
                        name="categoryId"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Select
                            defaultValue={configParamsRef.current.categoryId}
                            value={configParamsRef.current.categoryId}
                            onChange={event => onInputChange("categoryId", event)}
                            options={navigatorList}
                        />
                    </Form.Item>
                    <Form.Item
                        label="链接"
                        name="linkHref"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Input defaultValue={configParamsRef.current.linkHref} value={configParamsRef.current.linkHref} onChange={event => onInputChange("linkHref", event)}/>
                    </Form.Item>
                    <Form.Item
                        label="备注"
                        name="remark"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <TextArea
                            showCount
                            style={{ height: 100 }}
                            defaultValue={configParamsRef.current.remark}
                            value={configParamsRef.current.remark}
                            onChange={event => onInputChange("remark", event)}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        )
    }

    return <div className='navigator-link-view'>
        {renderFilterPanel()}
        {renderTablePanel()}
        {renderConfigModal()}
    </div>
}

export default NavigatorView;