import React from "react";
import * as echarts from "echarts";

import "./index.less"

export function SummaryView() {
    const echartCanvaRef = useRef(null)

    useEffect(() => {
        initEctartOptions();
    }, []);

    const initEctartOptions = () => {
        const echartInstance = echarts.init(echartCanvaRef.current);

        const echartOptions = {
            xAxis: {
                type: 'category',
                data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    data: [820, 932, 901, 934, 1290, 1330, 1320],
                    type: 'line',
                    smooth: true
                }
            ]
        };

        if (echartInstance){
            echartInstance.setOption(echartOptions)
        }
    }

    const renderResourcePanel = () => {
        return <div className="resource-panel">
            <div className="resource-item-wrap">
                <div className="item-label">
                    磁盘
                </div>
                <div className="item-value">

                </div>
            </div>
            <div className="resource-item-wrap">
                <div className="item-label">
                    内存
                </div>
                <div className="item-value">

                </div>
            </div>
            <div className="resource-item-wrap">
                <div className="item-label">
                    磁盘使用率
                </div>
                <div className="item-value">

                </div>
            </div>
            <div className="resource-item-wrap">
                <div className="item-label">
                    内存使用率
                </div>
                <div className="item-value">

                </div>
            </div>
        </div>
    }

    const renderLogPanel = () => {
        return <div className="log-panel">
            <div className="panel-title">
                访问趋势
            </div>
            <div className="log-cavcas" id="logCavcas" ref={echartCanvaRef}>

            </div>
        </div>
    }

    return <div className='summary-view-container'>
        {renderResourcePanel()}
        {renderLogPanel()}
    </div>
}

export default SummaryView;