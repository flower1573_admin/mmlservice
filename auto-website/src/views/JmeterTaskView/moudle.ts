export const tableBaseColumns = [
    {
        title: "序号",
        dataIndex: "number",
        key: "number",
        render: (_, record, index) => index + 1,
        align: "center",
    }, {
        title: "名称",
        dataIndex: "taskName",
        key: "taskName",
    }, {
        title: "状态",
        dataIndex: "taskStatus",
        key: "taskStatus",
    }, {
        title: "进度",
        dataIndex: "taskProgress",
        key: "taskProgress",
    }, {
        title: "开始时间",
        dataIndex: "startTime",
        key: "startTime",
    }, {
        title: "结束时间",
        dataIndex: "endTime",
        key: "endTime",
    }, {
        title: "描述",
        dataIndex: "description",
        key: "description",
    },
]