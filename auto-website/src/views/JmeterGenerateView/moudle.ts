export const tableBaseColumns = [
    {
        title: "序号",
        dataIndex: "number",
        key: "number",
        render: (_, record, index) => index + 1,
        align: "center",
    }, {
        title: "名称",
        dataIndex: "name",
        key: "name",
    },{
        title: "文件名称",
        dataIndex: "fileName",
        key: "fileName",
    }, {
        title: "备注",
        dataIndex: "remark",
        key: "remark",
    }, {
        title: "创建时间",
        dataIndex: "createTime",
        key: "createTime",
    },
]

export const fileTableBaseColumns = [
    {
        title: "序号",
        dataIndex: "number",
        key: "number",
        render: (_, record, index) => index + 1,
        align: "center",
    }, {
        title: "名称",
        dataIndex: "name",
        key: "name",
    }, {
        title: "域名",
        dataIndex: "domain",
        key: "domain",
    }, {
        title: "请求方法",
        dataIndex: "method",
        key: "method",
    }, {
        title: "请求路径",
        dataIndex: "url",
        key: "url",
    }, {
        title: "请求参数",
        dataIndex: "params",
        key: "params",
    },
]