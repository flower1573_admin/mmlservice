import React from "react";

import "./index.less"
import {tableBaseColumns, fileTableBaseColumns} from "./moudle"
import {getTableList, createSchedule, deleteSchedule, updateSchedule, updateScheduleStatus, testSchedule} from "@/api/schedule/schedule";
const { TextArea } = Input;
import { InboxOutlined } from '@ant-design/icons';
import {f} from "vite/dist/node/types.d-aGj9QkWt";
import {Button} from "antd";
import * as XLSX from 'xlsx';
import {generateUUID, isJsonString} from "@/utils/commonUtils";
import {getTaskList, generateScript, exportScript} from "@/api/jmeterGenerate/jmeterGenerate";

export function LogManageView() {
    const {RangePicker} = DatePicker;

    let renderRef = useRef(true);
    const [filterParams, setFilterParams] = useState({
        timeRange: [],
        name: "",
        taskStatus: ""
    })
    const filterParamsRef  = useRef({
        timeRange: [],
        name: "",
        taskStatus: ""
    })

    const [tableList, setTableList] = useState([])
    const [tableCurrent, setTableCurrent] = useState(1)
    const [tableSize, setTableSize] = useState(15)
    const [tableTotal, setTableTotal] = useState(0)
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);

    const [visibleConfig, setVisibleConfig] = useState(false)
    const [visibleExecute, setVisibleExecute] = useState(false)
    const [executeParams, setExecuteParams] = useState({
        type: "date",
        executeTime: "",
        executeCron: "",
    })
    const executeParamsRef = useRef({
        type: "date",
        executeTime: "",
        executeCron: "",
    })

    const [importStep, setImportStep] = useState(0)


    const [configParams, setConfigParams] = useState({
        name: "",
        type: "",
        threads: "",
        rampupPeriod: "",
        duration: "",
        remark: "",
        fileList: []
    })
    const configParamsRef  = useRef({
        name: "",
        type: "",
        threads: "",
        rampupPeriod: "",
        duration: "",
        remark: "",
        fileList: []
    })
    const [fileTableList, setFileTableList] = useState([])
    const fileTableListRef = useRef([])


    useEffect(() => {
        if (renderRef.current) {
            renderRef.current = false;
            return;
        }
        initTableList();
    }, []);

    const initTableList = () => {
        const params = {
            page: tableCurrent,
            size: tableSize,
            name: filterParamsRef.current.name,
        }

        getTaskList(params).then(res => {
            const {data, total} = res
            setTableList(data)
            setTableTotal(total)
        }).catch(err => {
        })
    }


    const onPaginationChange = (page, size) => {
        if (tableSize != size) {
            setTableCurrent(1)
            setTableSize(size)
        } else {
            setTableCurrent(page)
        }
        initTableList()
    }

    const onTimeChange = (event) => {
        setFilterParams({
            ...filterParams,
            timeRange: event
        })
    }

    const clickFilter = () => {
        initTableList()
    }

    const clickReset = () => {
        setFilterParams({
            timeRange: []
        })
        initTableList()
    }

    const clickExport = () => {

    }

    const renderTablePanel = () => {
        const clickConfig = (event) => {
            const {id, taskName, taskBean, taskMethod, taskParams, taskCron, taskStatus, remark} = event
            configParamsRef.current = {
                operateType: "update",
                id: id,
                taskName: taskName,
                taskBean: taskBean,
                taskMethod: taskMethod,
                taskParams: taskParams,
                taskCron: taskCron,
                taskStatus: taskStatus,
                remark: remark,
            }
            setVisibleConfig(true)
        }
        const clickExport = (event) => {
            const params = {
                fileName: event.fileName
            }
            exportScript(params).then(res=> {
                const {data, headers} = res
                const contentDisposition = res.headers["content-disposition"]
                if (contentDisposition) {
                    const match = contentDisposition.match(/filename="?([^";]*)"?/);
                    if (match) {
                        const fileName = decodeURI(match[1]);
                        const blob = new Blob([res.data])
                        const dom = document.createElement('a')
                        const downUrl = window.URL.createObjectURL(blob)
                        dom.href = downUrl
                        dom.download = decodeURIComponent(fileName)
                        dom.style.display = 'none'
                        document.body.appendChild(dom)
                        dom.click()
                        dom.parentNode.removeChild(dom)
                        window.URL.revokeObjectURL(downUrl)
                    }
                }
            }).catch(err => {

            })
        }
        const clickShare = (event) => {
            setVisibleExecute(true)
        }

        const onSelectChange = (event) => {
            setSelectedRowKeys(event);
        }

        const rowSelection =  {
            selectedRowKeys,
            onChange: onSelectChange,
        };

        const tableColumns = [
            ...tableBaseColumns,
            {
                title: '操作',
                key: 'operate',
                dataIndex: 'operate',
                align: 'center', //头部单元格和内容区水平居中
                render: (event, event2) => (
                    <div className="table-operate-column-wrap">
                        <div className="column-cls" onClick={tags => clickExport(event2)}>下载</div>
                    </div>
                )
            }
        ]
        return (
            <div className="table-panel">
                <Table
                    pagination={false}
                    rowSelection={rowSelection}
                    dataSource={tableList}
                    columns={tableColumns}
                    rowKey={row => row.id}
                />
                <Pagination
                    current={tableCurrent}
                    pageSize={tableSize}
                    total={tableTotal}
                    className="pagination-cls"
                    showQuickJumper
                    pageSizeOptions={[15, 30, 50, 100, 500]}
                    onChange={onPaginationChange}
                    showTotal={(total) => `共 ${total} 条`}
                />
            </div>
        )
    }

    const renderFilterPanel = () => {
        const clickImport = () => {
            setVisibleConfig(true)
        }
        const clickClear = () => {

        }

        const clickExportTemplate = () => {

        }

        return (
            <div className="filter-panel">
                <div className="filter-left">
                    <Space>
                        <Button className="filter-item" onClick={clickImport}>导入</Button>
                        <Button className="filter-item" onClick={clickExportTemplate}>模板</Button>
                        <Button className="filter-item" onClick={clickClear}>清除</Button>
                    </Space>
                </div>
                <div className="filter-right">
                </div>
            </div>
        )
    }


    const renderConfigModal = () => {
        const clickCancel = () => {
            setVisibleConfig(false)
        }
        const clickConfirm = () => {
            const {operateType, id, taskName, taskBean, taskMethod, taskParams, taskCron, taskStatus, remark} = configParamsRef.current
            const params = {
                id: id,
                taskName: taskName,
                taskBean: taskBean,
                taskMethod: taskMethod,
                taskParams: taskParams,
                taskCron: taskCron,
                taskStatus: taskStatus,
                remark: remark,
            }

            if (operateType == "create"){
                createSchedule(params).then(res=> {
                    clickCancel()
                }).catch(err => {

                })
            } else {
                updateSchedule(params).then(res=> {
                    clickCancel()
                }).catch(err => {

                })
            }
        }
        const onInputChange = (type, event) => {
            let changeValue = ""
            if (["type"].includes(type)){
                changeValue = event
            } else {
                changeValue = event.target.value
            }
            executeParamsRef.current = {
                ...executeParamsRef.current,
                [type]: changeValue
            }
            setExecuteParams({
                ...executeParams,
                [type]: changeValue
            })
            configParamsRef.current = {
                ...configParamsRef.current,
                [type]: changeValue
            }
        }

        const linisterOptions = [
            { label: '结果树', value: 'resultTree' },
            { label: '聚合报告', value: 'aggregateReport' },
        ]

        const stepsItems = [
            {
                title: '基本信息',
                current: 0,
            },
            {
                title: '文件解析',
                current: 1,
            },
            {
                title: '完成',
                current: 2,
            },
        ];

        const clickStep = (event) => {
            if (event == "prev" && [1, 2].includes(importStep)){
                setImportStep(importStep - 1)
            }
            if (event == "next" && [0, 1].includes(importStep)){
                setImportStep(importStep + 1)
            }
            if (event == "cancel"){
                setVisibleConfig(false)
            }
            if (event == "confirm"){
                const {name, type, threads,rampupPeriod,duration, remark } = configParamsRef.current
                const params = {
                    name: name,
                    type: type,
                    threads: threads,
                    rampupPeriod: rampupPeriod,
                    duration: duration,
                    remark: remark,
                    scriptList: fileTableListRef.current
                }
                generateScript(params).then(res => {
                    setImportStep(2)
                    setTimeout(() => {
                      setVisibleConfig(false)
                    }, 1000 *1)
                })
            }
        }

        const analyseExcel = (file) => {
            return new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.onloadend = (progressEvent) => {
                    const data = progressEvent.target!.result
                    const workbook = XLSX.read(data, { type: 'binary' });
                    const sheetNames = workbook.SheetNames;
                    const worksheet = workbook.Sheets[sheetNames[0]];
                    const jsonList = XLSX.utils.sheet_to_json(worksheet);
                    resolve(jsonList);
                };
                reader.readAsArrayBuffer(file);
            });
        }

        const toUpperCase = (event) => {
            if (event){
                return event.toUpperCase()
            }
            return "GET"
        }

        const trimString = (event) => {
            if (event){
                return event.trim()
            }
            return ""
        }

        const convertRequestBody = (requestMethod, requestBody) => {
            let requestMethodValue = ""
            if (requestMethod){
                requestMethodValue =  requestMethod.toUpperCase()
            }
            if (["GET"].includes(requestMethodValue) && isJsonString(requestBody)) {
                const paramsList = []
                const bodyJson = JSON.parse(requestBody)
                const keyList = Object.keys(bodyJson)
                for (let index in keyList) {
                    const paramsItem = keyList[index] + "=" + bodyJson[keyList[index]]
                    paramsList.push(paramsItem)
                }
                return "?" + paramsList.join("&")
            } else {
                if (isJsonString(requestBody)){
                    return requestBody
                }
            }
            return ""
        }

        const uploadProps = {
            name: 'file',
            multiple: false,
            showUploadList: false,
            beforeUpload: (file) => {
                return false;
            },
            onChange(file) {
                analyseExcel(file.file).then((res: any)=> {
                    const dataList = res.map(item => {
                        return {
                            id : generateUUID(),
                            name: trimString(item["名称"]),
                            domain: trimString(item["域名"]),
                            method: toUpperCase(item["请求方法"]),
                            url: trimString(item["请求路径"]),
                            params: convertRequestBody(item["请求方法"], item["请求参数"]),
                        }
                    })
                    setFileTableList(dataList)
                    fileTableListRef.current = dataList
                })
            },
        }

        const clickClearFile = () => {
            setFileTableList([])
            fileTableListRef.current = []
        }

        const tableColumns= [
            ...fileTableBaseColumns
        ]

        return (
            <Modal
                title="导入"
                open={visibleConfig}
                keyboard={true}
                className="jmeter-import-modal"
                width="60%"
                footer={() => (
                    <div className="operate-btn-group">
                        <Space>
                            <Button onClick={event => clickStep("prev")} className={[1].includes(importStep) ? "display-cls" : "hidden-cls"}>上一步</Button>
                            <Button onClick={event => clickStep("cancel")} className={[0, 1].includes(importStep) ? "display-cls" : "hidden-cls"}>取消</Button>
                            <Button type="primary" onClick={event => clickStep("confirm")} className={[1].includes(importStep) ? "display-cls" : "hidden-cls"}>确定</Button>
                            <Button onClick={event => clickStep("next")} className={[0].includes(importStep) ? "display-cls" : "hidden-cls"}>下一步</Button>
                        </Space>
                    </div>
                )}
            >
                <Steps current={importStep} labelPlacement="vertical" items={stepsItems} className="step-wrap"/>
                <div className={[0].includes(importStep) ? "display-cls" : "hidden-cls"}>
                    <Form
                        name="basic"
                        autoComplete="off"
                        labelCol={{ span: 2 }}
                    >
                        <Form.Item
                            label="名称"
                            name="name"
                            rules={[{ required: true, message: '请输入名称' }]}
                        >
                            <Input defaultValue={configParamsRef.current.name} value={configParamsRef.current.name} onChange={event => onInputChange("name", event)}/>
                        </Form.Item>
                        <Form.Item
                            label="类型"
                            name="type"
                        >
                            <Select className="filter-value-item"
                                    value={configParamsRef.current.type}
                                    defaultValue={configParamsRef.current.type}
                                    onSelect={event => onInputChange("type", event)}
                                    options={[
                                        { value: '1', label: '多文件' },
                                        { value: '2', label: '单文件单线程组' },
                                        { value: '3', label: '单文件多线程组' },
                                    ]}
                            />
                        </Form.Item>
                        <Form.Item
                            label="线程数"
                            name="taskMethod"
                            rules={[{ required: true, message: '请输入编码' }]}
                        >
                            <Input defaultValue={configParamsRef.current.threads} value={configParamsRef.current.threads} onChange={event => onInputChange("threads", event)}/>
                        </Form.Item>
                        <Form.Item
                            label="等待时间"
                            name="rampupPeriod"
                            rules={[{ required: true, message: '请输入编码' }]}
                        >
                            <Input defaultValue={configParamsRef.current.rampupPeriod} value={configParamsRef.current.rampupPeriod} onChange={event => onInputChange("rampupPeriod", event)}/>
                        </Form.Item>
                        <Form.Item
                            label="持续时间"
                            name="duration"
                            rules={[{ required: true, message: '请输入编码' }]}
                        >
                            <Input defaultValue={configParamsRef.current.duration} value={configParamsRef.current.duration} onChange={event => onInputChange("duration", event)}/>
                        </Form.Item>
                        <Form.Item
                            label="备注"
                            name="remark"
                            rules={[{ required: true, message: '请输入编码' }]}
                        >
                        <TextArea
                            showCount
                            style={{ height: 100 }}
                            defaultValue={configParamsRef.current.remark}
                            value={configParamsRef.current.remark}
                            onChange={event => onInputChange("remark", event)}
                        />
                        </Form.Item>
                    </Form>
                </div>
                <div className={[1].includes(importStep) ? "display-cls" : "hidden-cls"}>
                    <Space>
                        <Upload {...uploadProps}>
                            <Button>上传</Button>
                        </Upload>
                        <Button onClick={clickClearFile}>清除</Button>
                    </Space>
                    <Table className = "file-table"
                        pagination={false}
                        dataSource={fileTableList}
                        columns={tableColumns}
                        rowKey={row => row.id}
                    />
                </div>
                <div className={[2].includes(importStep) ? "display-cls" : "hidden-cls"}>
                    <Spin tip="Loading" size="large" spinning={[2].includes(importStep) } className="spin-cls">
                    </Spin>
                </div>
            </Modal>
        )
    }

    return <div className='jmeter-generate--container'>
        {renderFilterPanel()}
        {renderTablePanel()}
        {renderConfigModal()}
    </div>
}

export default LogManageView;