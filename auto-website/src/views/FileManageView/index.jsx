import React, { useId } from "react";

const {Header, Footer, Sider, Content} = Layout;
import { Menu, Item, useContextMenu } from 'react-contexify';
import 'react-contexify/dist/ReactContexify.css';
import {
  FolderOutlined,
  DownloadOutlined,
  ForkOutlined
} from '@ant-design/icons';
import "./index.less"
import {menuList, tableBaseColumns} from "./moudle";
import {
  getCategoryList,
  getFileRecordsList,
  getFilemultipart,
  uploadFilemultipart,
  completeFilemultipart,
  uploadSingleFile,
  downloadSingleFile,
  getFIleUrl,
  createCatalog,
  updateCatalog,
  deleteCatalog,
  deleteFile,
  getFileLink,
  getSystemInfo
} from "@/api/fileManage/fileManage";
import {getFileMd5} from "../../utils/fileUtils";
import { Button, message } from 'antd';
import lodash from 'lodash';
import moment from 'moment';

const {RangePicker} = DatePicker;
export function FileManageView() {
  const navigate = useNavigate()
  let renderRef = useRef(true);
  const [tableList, setTableList] = useState([])
  const [tableCurrent, setTableCurrent] = useState(1)
  const [tableSize, setTableSize] = useState(15)
  const [tableTotal, setTableTotal] = useState(0)

  const [categoryList, setCategoryList] = useState([])
  const [fileList, setFileList] = useState([])
  const fileUploadId = useRef("")

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);


  const [visibleCatelog, setVisibleCatelog] = useState(false)
  const [catelogParams, setCatelogParams] = useState({
    catelogName: "",
    catelogCode: "",
  })
  const catelogParamsRef = useRef({
    id:"",
    operateType: "",
    catelogName: "",
    catelogCode: "",
  })
  const [selectCatalog, setSelectCatalog] = useState("")
  const [selectedCatalogKeys, setSelectedCatalogKeys] = useState([])

  const [filterParams, setFilterParams] = useState({
    name: "",
    type: "",
    timeRange: []
  })
  const filterParamsRef = useRef({
    name: "",
    type: "",
    timeRange: []
  })



  useEffect(() => {
    initCategoryList();
    initSystemInfo()
  }, []);

  const initSystemInfo = () => {
    getSystemInfo().then(res=> {
      console.log(res)
    }).catch(err => {
      console.log(err)
    })
  }

  useEffect(() => {
    if (renderRef.current) {
      renderRef.current = false;
      return;
    }
    initFileRecordsList();
  }, [tableSize , tableCurrent]);

  const uploadProps = {
    showUploadList: false,
    beforeUpload: (file) => {
      return false;
    },
    onChange: (file) => {
      setFileList(file.fileList)
      executeUpload(file.fileList)
    }
  }

  const dewnloadFole = (event) => {
    const params = {
      fileName: event.name
    }
    downloadSingleFile(params).then(res => {
      const {data, headers} = res

      const contentDisposition = res.headers["content-disposition"]
      if (contentDisposition) {
        const match = contentDisposition.match(/filename="?([^";]*)"?/);
        if (match) {
          const fileName = decodeURI(match[1]);
          const blob = new Blob([res.data])
          const dom = document.createElement('a')
          const downUrl = window.URL.createObjectURL(blob)
          dom.href = downUrl
          dom.download = decodeURIComponent(fileName)
          dom.style.display = 'none'
          document.body.appendChild(dom)
          dom.click()
          dom.parentNode.removeChild(dom)
          window.URL.revokeObjectURL(downUrl)
        }
      }
    }).catch(err => {
      console.log(err)
    })
  }

  const clickFilter = () => {
    initFileRecordsList()
  }

  const clickReset = () => {
    const newParams = {
      "type": "",
      "name": "",
      timeRange: []
    }
    filterParamsRef.current = newParams
    setFilterParams(() => {
      return newParams
    })
    console.log(filterParams)
    initFileRecordsList()
  }

  const clickDeleteFIle = () => {
    const fileList = selectedRowKeys
    const params = {
      idList: fileList
    }
    deleteFile(params).then(res=> {
    initFileRecordsList()
    }).catch(err => {

    })
  }

  const getFileViewUrl = () => {
    const params = {
      fileName: "少时诵诗书所所.docx"
    }
    getFIleUrl(params).then(res => {
      console.log(res)
    }).catch(err => {

    })
  }

  const executeUpload = async (event) => {
    console.log(event[0].originFileObj)

    const file = event[0].originFileObj

    const fileSize = file.size
    const fileName = file.name
    const fileType = file.type
    const chunkSize = 1024 * 1024 * 5

    if (fileSize <= chunkSize) {
      uploadFile(file)
    } else {
      const chunkNum = Math.floor(fileSize / chunkSize);
      const fileMd5 = await getFileMd5(file)

      const multipartParams = {
        chunkNum: chunkNum,
        fileName: fileName,
        contentType: fileType,
      }
      await getFilemultipart(multipartParams).then(async (res) => {
        const {uploadId, parts} = res
        fileUploadId.current = uploadId

        for (let i = 0; i < parts.length; i++) {
          const partItem = parts[i]
          let startSize = (partItem.parkNum - 1) * chunkSize;
          //分片结束位置
          let endSize = partItem.parkNum === chunkNum ? fileSize : startSize + chunkSize;
          //获取当前分片的byte信息
          let chunkFile = file.slice(startSize, endSize);

          await uploadFilePear(partItem.uploadUrl, chunkFile, fileType);
        }

        completeChunkFile(chunkNum, fileName, fileType, fileMd5)
      })
    }
  }

  const uploadFile = (file) => {
    const formData = new FormData();
    formData.append('file', file);

    uploadSingleFile(formData).then(res => {
      initFileRecordsList()
    }).catch(err => {

    })
  }

  const uploadFilePear = async (uploadUrl, chunkFile, fileType) => {
    const params = {
      uploadUrl: uploadUrl,
      chunkFile: chunkFile,
      fileType: fileType
    }
    await uploadFilemultipart(params).then(res => {
    }).catch(err => {
    })
  }

  const completeChunkFile = (chunkNum, fileName, contentType, fileMd5) => {
    const params = {
      chunkNum: chunkNum,
      fileName: fileName,
      contentType: contentType,
      uploadId: fileUploadId.current,
      fileMd5: fileMd5,
    }
    completeFilemultipart(params).then(res => {

    }).catch(err => {

    })
  }


  const initCategoryList = () => {
    const params = {}
    getCategoryList(params).then(res => {
      const newDataList = res.map(item => {
        const {id,name, code} = item
        return {
          title: name,
          key: id,
          code: code
        }
      })
      console.log(newDataList)
      setCategoryList(newDataList)

    }).catch(err => {

    })
  }

  const initFileRecordsList = () => {

    const params = {
      page: tableCurrent,
      size: tableSize,
      name: filterParamsRef.current.name,
      type: filterParamsRef.current.type,
      startTime: "",
      endTime: "",
    }
    if (filterParamsRef.current.timeRange.length > 1){

      const startTime = filterParamsRef.current.timeRange[0].valueOf()
      const endTime = filterParamsRef.current.timeRange[1].valueOf()
      params.startTime =  startTime
      params.endTime =  endTime
    }
    getFileRecordsList(params).then(res => {
      setTableList(res.data)
      setTableTotal(res.total)
    }).catch(err => {

    })
  }

  const renderCatalogTree = () => {
    const contextMenu =  {
      menuId: "file_15188c77_bbd5_43be_8ddf_0de911485232",
      items: [
        { key: 'edit', name: '编辑'},
        { key: 'delete', name: '删除'},
      ]
    }

    const onRightClickMenu = (event) => {
      const { event: treeEvent, node: treeNode } =event
      setSelectCatalog(treeNode.key)
      setSelectedCatalogKeys([treeNode.key])

      show(event,{
        props: treeNode,
      });
    }

    const onClickMenu = (event) => {
      console.log(event.target.id)
      setSelectedCatalogKeys([event.target.id])
    }

    const { show } = useContextMenu({
      id: contextMenu.menuId,
    });

    const clickRightMenu = (event) => {
      const catelogData = categoryList.find(item => item.key == selectCatalog)
      catelogParamsRef.current = {
        id: catelogData.key,
        operateType: "update",
        catelogName: catelogData.title,
        catelogCode: catelogData.code,
      }

      if (event.key == "delete") {
        const params = {
          idList: [catelogData.key]
        }
        deleteCatalog(params).then(res => {
          initCategoryList()
        }).catch(err => {
        })
      } else {
        setVisibleCatelog(true)
      }
    }

    const ContextMenu = () => (
      <Menu id={contextMenu.menuId} className="tree-left-menu-wrap">
        {contextMenu.items.map((item) => (
          <Item key={item.key} onClick={() => clickRightMenu(item)}>{item.name}</Item>
        ))}
      </Menu>
    );
    return (
      <div>
        <Tree showIcon className="tree-wrap-cls"
          treeData={categoryList}
          onRightClick={onRightClickMenu}
          selectedKeys={selectedCatalogKeys}
              titleRender={(data) =>{
                return <div className="tree-node-wrap">
                  <FolderOutlined/>
                  <div
                    className= {selectedCatalogKeys.includes(data.key) ? "tree-node-text tree-node-selected-text" : "tree-node-text"}
                    onClick={onClickMenu}
                    id={data.key}
                  >
                    {data.title}
                    </div>
                </div>
              }}
        />
        <ContextMenu/>
      </div>
    )
  }

  const renderTablePanel = () => {
    const clickCopyLink = (event) => {
      const fileName = event.name
      const params = {
        fileName: fileName
      }
      getFileLink(params).then(res => {
        var tempInput = document.createElement("input");
        document.body.appendChild(tempInput);
        tempInput.value = res;
        tempInput.select();
        document.execCommand("copy");
        document.body.removeChild(tempInput);
        message.info('链接已复制到剪切板');
      }).catch(err => {

      })
    }
    const tableColumns = [
      ...tableBaseColumns,
      {
        title: '操作',
        key: 'operate',
        dataIndex: 'operate',
        align: 'center', //头部单元格和内容区水平居中
        render: (event, event2) => (
          <div className="table-operate-column-wrap">
            <div className="column-cls" onClick={tags => clickCopyLink(event2)}>分享</div>
            <div className="column-cls" onClick={tags => dewnloadFole(event2)}>下载</div>
          </div>
        )
      }
    ]
    const onPaginationChange = (page, size) => {
      setTableCurrent(page)
      setTableSize(size)
    }

    const onSelectChange = (newSelectedRowKeys) => {
      setSelectedRowKeys(newSelectedRowKeys);
    }

    const rowSelection =  {
      selectedRowKeys,
      onChange: onSelectChange,
    };

    const onFilterChange = (type, event) => {
      const newParams = {
          ...filterParamsRef.current,
      }
      if (type == "name"){
        newParams.name = event.target.value
      }
      if (["type", "timeRange"].includes(type)){
        newParams.timeRange = event
      }
      filterParamsRef.current = newParams
      setFilterParams(newParams)
    }

    return (
      <div className="table-panel">
        <div className="operate-wrap">
          <div className="filter-left">
            <div className="filter-item">
              <Upload {...uploadProps}>
                <Button>上传</Button>
              </Upload>
            </div>
            <Button className="filter-item" onClick ={clickDeleteFIle}>删除</Button>
            <Button className="filter-item" onClick={dewnloadFole}>下载</Button>
            <Button className="filter-item" onClick={clickConfigCatalog}>新建目录</Button>
          </div>
          <div className="filter-right">
            <div className="filter-item-wrap">
              <div className="item-label">类型</div>
              <Select className="filter-value-item"
                value={filterParams.type}
                onSelect={event => onFilterChange("type", event)}
                options={[
                  { value: 'picture', label: '图片' },
                  { value: 'document', label: '文档' },
                  { value: 'excel', label: '表格' },
                  { value: 'other', label: '其他' }
                ]}
              />
            </div>
            <div className="filter-item-wrap">
              <div className="item-label">名称</div>
              <Input className="filter-value-item" value={filterParams.name} onChange={event => onFilterChange("name", event)} onPressEnter={clickFilter}/>
            </div>
            <div className="filter-item-wrap">
              <div className="item-label">时间</div>
              <RangePicker value={filterParams.timeRange} onChange={event => onFilterChange("timeRange", event)} showTime />
            </div>
            <Button className="filter-item" onClick={clickFilter}>搜索</Button>
            <Button className="filter-item" onClick={clickReset}>重置</Button>
          </div>
        </div>
        <Table
          dataSource={tableList}
          columns={tableColumns}
          rowSelection={rowSelection}
          rowKey={row => row.id}
          pagination={false}
        />
        <Pagination
          current={tableCurrent}
          pageSize={tableSize}
          total={tableTotal}
          className="pagination-cls"
          showQuickJumper
          pageSizeOptions={[15, 30, 50, 100, 500]}
          onChange={onPaginationChange}
          showTotal={(total) => `共 ${total} 条`}
        />
      </div>
    )
  }

  const clickConfigCatalog = () => {
    setVisibleCatelog(true)
  }

  const clickCancel = () => {
    setVisibleCatelog(false)
  }

  const clickConfirm = () => {
    const  { catelogName, catelogCode, id }= catelogParamsRef.current
    const params = {
      catalogName: catelogName,
      catalogCode: catelogCode,
    }

    if (catelogParamsRef.current.operateType == "update") {
      params.id = id
      updateCatalog(params).then(res => {
        clickCancel()
        initCategoryList()
      }).catch(err => {

      })
    } else {
      createCatalog(params).then(res => {
        clickCancel()
        initCategoryList()
      }).catch(err => {

      })
    }
  }

  const renderCategoryModal = () => {
    const onFinishFailed =  () => {

    }

    const onInputChange = (key, event) => {
      const value = event.target.value
      const params = catelogParamsRef.current
      params[key] = value
      setCatelogParams(params)
    }
    return (
      <Modal
        title="配置目录"
        open={visibleCatelog}
        onOk={clickConfirm}
        onCancel={clickCancel}
        keyboard={true}
      >
        <Form
          name="basic"
          autoComplete="off"
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="名称"
            name="catelogName"
            rules={[{ required: true, message: '请输入名称' }]}
            >
            <Input defaultValue={catelogParamsRef.current.catelogName} value={catelogParamsRef.current.catelogName} onChange={event => onInputChange("catelogName", event)}/>
          </Form.Item>
          <Form.Item
            label="编码"
            name="catelogCode"
            rules={[{ required: true, message: '请输入编码' }]}
            >
            <Input defaultValue={catelogParamsRef.current.catelogCode} value={catelogParamsRef.current.catelogCode} onChange={event => onInputChange("catelogCode", event)}/>
          </Form.Item>
        </Form>
      </Modal>
    )
  }

  return (
    <div className="file-manage-view-container">
      <Layout className="main-layout">
        <Sider className="side-panel">
          {renderCatalogTree()}
        </Sider>
        <Layout className="right-layout">
          <Content className="content-panel">
            {renderTablePanel()}
          </Content>
        </Layout>
      </Layout>
      {renderCategoryModal()}
    </div>
  )
}

export default FileManageView;