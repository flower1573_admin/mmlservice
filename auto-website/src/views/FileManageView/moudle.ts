export const menuList = [
    {
        label: '文档',
        key: 'document',
    }, {
        label: '表格',
        key: 'excel',
    }, {
        label: '图片',
        key: 'picture',
    }, {
        label: '其他',
        key: 'others',
    }, {
        label: '最近文件',
        key: 'recent',
    }
]

export const tableBaseColumns = [
    {
        title: '序号',
        dataIndex: 'number',
        key: 'number',
        align: 'center', //头部单元格和内容区水平居中
        render: (_, record, index) => index + 1,
    }, {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
    }, {
        title: '类型',
        dataIndex: 'type',
        key: 'type',
    }, {
        title: '大小',
        dataIndex: 'size',
        key: 'size',
    }, {
        title: '修改者',
        dataIndex: 'updater',
        key: 'updater',
    }, {
        title: '创建时间',
        dataIndex: 'createTime',
        key: 'createTime',
        align: 'center', //头部单元格和内容区水平居中
    }
]