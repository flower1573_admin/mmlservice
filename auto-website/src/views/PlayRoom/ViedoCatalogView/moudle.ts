export const tableBaseColumns = [
    {
        title: "序号",
        dataIndex: "number",
        key: "number",
        render: (_, record, index) => index + 1,
        align: "center",
    }, {
        title: "名称",
        dataIndex: "name",
        key: "name",
    }, {
        title: "备注",
        dataIndex: "remark",
        key: "remark",
    }, {
        title: "创建时间",
        dataIndex: "createTime",
        key: "createTime",
    }, {
        title: "更新时间",
        dataIndex: "updateTime",
        key: "updateTime",
    }
]