import "./index.less"
import React from "react";

import {getDataList, createData, updateData, deleteData} from "@/api/playRoom/viedoCatalog";
import {tableBaseColumns} from "./moudle";

export function ViedoCatalogView() {
    const [filterParams, setFilterParams] = useState(() => {
        return {
            name: "",
            timeRange: [],
            tableCurrent: 1,
            tableSize: 15,
        }
    });
    const filterParamsRef = useRef( {
        name: "",
        timeRange: [],
        tableCurrent: 1,
        tableSize: 15,
    })
    const [tableList, setTableList] = useState([]);
    const [tableSelectedList, setTableSelectedList] = useState([]);
    const [tableTotal, setTableTotal] = useState(0);


    const [visibleConfig, setVisibleConfig] = useState(false);
    const configParamsRef = useRef({
        operateType: "",
        id: "",
        name: "",
        remark: "",
    });

    useEffect(() => {
        initDataList();
    }, []);

    const initDataList = () => {
        const {name, tableCurrent, tableSize, timeRange} = filterParamsRef.current
        const params = {
            name: name,
            page: tableCurrent,
            size: tableSize,
        } as any
        if (timeRange.length > 0){
            params.startTime = timeRange[0].valueOf()
            params.endTime = timeRange[1].valueOf()
        }
        getDataList(params).then((res: any) => {
            const {data, total} = res
            setTableList(data)
            setTableTotal(total)
        }).catch(err => {

        })
    }


    const renderFilterPanel = () => {
        const clickCreate = () => {
            configParamsRef.current = {
                ...configParamsRef.current,
                operateType: "create"
            }
            setVisibleConfig(true)
        }

        const clickDelete = () => {
            const params = {
                idList: tableSelectedList
            }
            deleteData(params).then(res => {
                initDataList()
            }).catch(err => {

            })
        }

        const clickExportTemplate = () => {

        }

        const onFilterChange = (key, event) => {
            const params = {
                ...filterParams,
                [key]: event
            }
            setFilterParams(params)
            filterParamsRef.current = params
        }

        const clickFilter = (type) => {
            if (type == "reset") {
                const params =  {
                    ...filterParams,
                    name: "",
                    timeRange: [],
                    tableCurrent: 1,
                    tableSize: 15,
                }
                setFilterParams(params)
                filterParamsRef.current = params
            }
            initDataList()
        }

        return (
            <div className="filter-panel">
                <div className="panel-left">
                    <Space>
                        <Button onClick={clickCreate}>添加</Button>
                        <Button onClick={clickDelete}>删除</Button>
                    </Space>
                </div>

                <div className="panel-right">
                    <Space>
                        <div>名称</div>
                        <Input value={filterParams.name} onChange={(event) => onFilterChange("name", event.target.value)} allowClear />
                        <div>更新时间</div>
                        <DatePicker.RangePicker value={filterParams.timeRange} onChange={event => onFilterChange("timeRange", event)} showTime allowClear/>
                        <Button onClick={() => clickFilter("filter")}>搜索</Button>
                        <Button onClick={() => clickFilter("reset")}>重置</Button>
                    </Space>
                </div>
            </div>
        )
    }

    const renderTablePanel = () => {
        const clickConfig = (event) => {
            const {id, name, remark} = event
            configParamsRef.current = {
                operateType: "update",
                id: id,
                name: name,
                remark: remark,
            }
            setVisibleConfig(true)
        }
        const tableColumns = [
            ...tableBaseColumns,
            {
                title: '操作',
                key: 'operate',
                dataIndex: 'operate',
                align: 'center',
                render: (event, event2) => (
                    <div className="table-operate-column-wrap">
                        <div className="column-cls" onClick={tags => clickConfig(event2)}>编辑</div>
                    </div>
                )
            }
        ]

        const rowSelection = () => {
            console.log(tableSelectedList)
            return {
                selectedRowKeys: tableSelectedList,
                onChange: event => {
                    console.log(event)
                    setTableSelectedList(event)
                },
            }
        }

        const onPaginationChange = (page, size) => {
            const params = {
                ...filterParams,
                tableCurrent: page,
                tableSize: size
            }
            setFilterParams()
            filterParamsRef.current = params
        }
        return (
            <div className="table-panel">
                <Table className="table-cls"
                       rowSelection={rowSelection()}
                       dataSource={tableList}
                       columns={tableColumns}
                       rowKey={row => row.id}
                       pagination={false}
                />
                <Pagination
                    current={filterParams.tableCurrent}
                    pageSize={filterParams.tableSize}
                    total={tableTotal}
                    className="pagination-cls"
                    showQuickJumper
                    pageSizeOptions={[15, 30, 50, 100, 500]}
                    onChange={onPaginationChange}
                    showTotal={(total) => `共 ${total} 条`}
                />
            </div>
        )
    }


    const renderConfigModal = () => {
        const clickCancel = () => {
            setVisibleConfig(false)
        }
        const clickConfirm = () => {
            const {operateType, id, name, remark} = configParamsRef.current
            const executeCreate = () => {
                const params = {
                    name: name,
                    remark: remark,
                }
                createData(params).then(res => {
                    clickCancel()
                    initDataList()
                }).catch(err => {

                })
            }

            const executeUpdate = () => {
                const params = {
                    id: id,
                    name: name,
                    remark: remark,
                }
                updateData(params).then(res => {
                    clickCancel()
                    initDataList()
                }).catch(err => {

                })
            }
            if (configParamsRef.current.operateType == "create") {
                executeCreate()
            } else {
                executeUpdate()
            }
        }
        const onInputChange = (key, event) => {
            const params = configParamsRef.current
            configParamsRef.current = {
                ...params,
                [key]: event
            }
        }
        return (
            <Modal
                title="配置"
                open={visibleConfig}
                onOk={clickConfirm}
                onCancel={clickCancel}
                keyboard={true}
                classNames="user-config-modal"
                width="30%"
            >
                <Form
                    name="basic"
                    autoComplete="off"
                    labelCol={{ span: 2 }}
                >
                    <Form.Item
                        label="名称"
                        name="name"
                        rules={[{ required: true, message: '请输入名称' }]}
                    >
                        <Input defaultValue={configParamsRef.current.name} value={configParamsRef.current.name} onChange={event => onInputChange("name", event.target.value)}/>
                    </Form.Item>
                    <Form.Item
                        label="备注"
                        name="remark"
                        rules={[{ required: true, message: '请输入编码' }]}
                    >
                        <Input.TextArea
                            showCount
                            style={{ height: 100 }}
                            defaultValue={configParamsRef.current.remark}
                            value={configParamsRef.current.remark}
                            onChange={event => onInputChange("remark", event.target.value)}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        )
    }

    return(
        <div className="viedo-catalog-view">
            {renderFilterPanel()}
            <Divider />
            {renderTablePanel()}
            {renderConfigModal()}
        </div>
    )
}

export default ViedoCatalogView