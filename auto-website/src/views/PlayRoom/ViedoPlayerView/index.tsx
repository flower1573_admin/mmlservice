import { useSearchParams } from 'react-router-dom';

import Player from 'xgplayer';
import 'xgplayer/dist/index.min.css';
import ReactPlayer from 'react-player'
import "./index.less"
import {getDataDetails} from "@/api/playRoom/viedoFile";

export function ViedoPlayerView() {
    const renderRef = useRef(true);
    const playerRef = useRef(null);
    const [ routerParams, setRouterParams ] = useSearchParams()


    useEffect(() => {
         initRouterParams();
     }, []);

    const initRouterParams = () => {
        const fileId = routerParams.get("id")
        const params = {
            fileId: fileId
        }
        getDataDetails(params).then(res => {
            const {fileLink} = res
            initPlayerCanvas(fileLink)
        }).catch(err => {

        })
    }

     const initPlayerCanvas = (fileLink) => {
         let player = new Player({
             id: 'playerCanva',
             url: fileLink,
             width: '100%',
             height: '100%',
             fitVideoSize: 'auto',// 自适应视频内容宽高
             volume: 0.5,// 默认音量
             autoplay: false, // 自动播放
             videoInit: true,// 初始化显示视频首帧,不可与autoplay同时设置,在移动端设置无效
             playbackRate: [0.5, 0.75, 1, 1.5, 2], // 设置倍数播放
             defaultPlaybackRate: 1, // 默认倍速
             lang: 'zh-cn', // 语言
             progressDot: [
                 {
                     time: 10, //展示标记的时间
                     text: '标记文字1', //鼠标hover在标记时展示的文字
                     duration: 8, //标记段长度（以时长计算）
                     style: { //标记样式
                         background: 'white'
                     }
                 }, {
                     time: 22,
                     text: '标记文字'
                 }, {
                     time: 56,
                     duration: 8,
                 }, {
                     time: 76,
                 }
             ], // 进度条标记
             keyShortcutStep: { //设置调整步长
                 currentTime: 10, //播放进度调整步长，默认10秒
                 volume: 0.2 //音量调整步长，默认0.1
             }, // 键盘快捷键
             errorTips: `请<span>刷新</span>试试`, // 错误提示
             pip: true,//画中画
         });
         player.emit('resourceReady', [
             {
                 name: '标清',
                 url: fileLink
             }
         ]);
     }


    const renderPlayerPanel = () => {
        return (
            <div className="player-panel" id="playerCanva" ref={playerRef}>

            </div>
        )
    }

    return <div className='viedo-player-view'>
        {renderPlayerPanel()}
    </div>
}

export default ViedoPlayerView