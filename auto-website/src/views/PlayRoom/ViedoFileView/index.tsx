import "./index.less"
import React from "react";
import {getDataList, deleteData, uploadData, getDataDetails} from "@/api/playRoom/viedoFile";
import {getAllDataList} from "@/api/playRoom/viedoCatalog";
import {tableBaseColumns, importModalBaseColumns} from "./moudle";
import {Button} from "antd";
import {getFileSize, getFileType} from "@/utils/fileUtils";
import {generateUUID} from "@/components/FileImport";
import axios from "axios";
import {generateScript} from "@/api/jmeterGenerate/jmeterGenerate";
export function ViedoFileView() {

    const [filterParams, setFilterParams] = useState(() => {
        return {
            name: "",
            timeRange: [],
            tableCurrent: 1,
            tableSize: 15,
        }
    });
    const filterParamsRef = useRef( {
        name: "",
        timeRange: [],
        tableCurrent: 1,
        tableSize: 15,
    })
    const [tableList, setTableList] = useState([]);
    const [tableSelectedList, setTableSelectedList] = useState([]);
    const [tableTotal, setTableTotal] = useState(0);

    const [importStep, setImportStep] = useState(0);
    const importBasicInfoRef = useRef( {
        name: "",
        remark: "",
        catalog: ""
    })

    const [visibleImport, setVisibleImport] = useState(false);
    const [fileList, setFileList] = useState([]);
    const [fileCatalogList, setFileCatalogList] = useState([]);
    const [importModalTableList, setImportModalTableList] = useState([]);
    const catalogRef = useRef( {
        catalogId: "",
        catalogName:  "",
    })

    useEffect(() => {
        initCaseFileList();
        initAllDataList();
    }, []);

    const initCaseFileList = () => {
        const {name, tableCurrent, tableSize, timeRange} = filterParamsRef.current
        const params = {
            name: name,
            page: tableCurrent,
            size: tableSize,
        } as any
        if (timeRange.length > 0){
            params.startTime = timeRange[0].valueOf()
            params.endTime = timeRange[1].valueOf()
        }
        getDataList(params).then((res: any) => {
            const {data, total} = res
            setTableList(data)
            setTableTotal(total)
        }).catch(err => {

        })
    }

    const initAllDataList = () => {
        const params = {

        } as any

        getAllDataList(params).then((res: any) => {
            const newDataList = res.map(item => {
                return {
                    label: item.name,
                    value: item.id,
                }
            })
            setFileCatalogList(newDataList)
        }).catch(err => {

        })
    }


    const renderFilterPanel = () => {
        const clickImport = () => {
            setVisibleImport(true)
        }

        const clickExport = () => {

        }

        const clickDelete = () => {
            const params = {
                idList: tableSelectedList
            }
            deleteData(params).then(res => {
                initCaseFileList()
            }).catch(err => {

            })
        }

        const clickExportTemplate = () => {

        }

        const onFilterChange = (key, event) => {
            const params = {
                ...filterParams,
                [key]: event
            }
            setFilterParams(params)
            filterParamsRef.current = params
        }

        const clickFilter = (type) => {
            if (type == "reset") {
                const params =  {
                    ...filterParams,
                    name: "",
                    timeRange: [],
                    tableCurrent: 1,
                    tableSize: 15,
                }
                setFilterParams(params)
                filterParamsRef.current = params
            }
            initCaseFileList()
        }

        return (
            <div className="filter-panel">
                <div className="panel-left">
                    <Space>
                        <Button onClick={clickImport}>上传</Button>
                        <Button onClick={clickExport}>导出</Button>
                        <Button onClick={clickDelete}>删除</Button>
                    </Space>
                </div>

                <div className="panel-right">
                    <Space>
                        <div>名称</div>
                        <Input value={filterParams.name} onChange={(event) => onFilterChange("name", event.target.value)} allowClear />
                        <div>更新时间</div>
                        <DatePicker.RangePicker value={filterParams.timeRange} onChange={event => onFilterChange("timeRange", event)} showTime allowClear/>
                        <Button onClick={() => clickFilter("filter")}>搜索</Button>
                        <Button onClick={() => clickFilter("reset")}>重置</Button>
                    </Space>
                </div>
            </div>
        )
    }

    const renderTablePanel = () => {
        const tableColumns = [
            ...tableBaseColumns,
            {
                title: '操作',
                key: 'operate',
                dataIndex: 'operate',
                align: 'center',
                render: (event, event2) => (
                    <div className="table-operate-column-wrap">
                        <div className="column-cls">
                            <Link target = "_blank" to={`/playRoom/viedoPlayerView?id=${event2.id}`}>播放</Link>
                        </div>
                    </div>
                )
            }
        ]

        const rowSelection = () => {
            return {
                selectedRowKeys: tableSelectedList,
                onChange: event => {
                    setTableSelectedList(event)
                },
            }
        }

        const onPaginationChange = (page, size) => {
            const params = {
                ...filterParams,
                tableCurrent: page,
                tableSize: size
            }
            setFilterParams()
            filterParamsRef.current = params
        }
        return (
            <div className="table-panel">
                <Table className="table-cls"
                       rowSelection={rowSelection()}
                       dataSource={tableList}
                       columns={tableColumns}
                       rowKey={row => row.id}
                       pagination={false}
                />
                <Pagination
                    current={filterParams.tableCurrent}
                    pageSize={filterParams.tableSize}
                    total={tableTotal}
                    className="pagination-cls"
                    showQuickJumper
                    pageSizeOptions={[15, 30, 50, 100, 500]}
                    onChange={onPaginationChange}
                    showTotal={(total) => `共 ${total} 条`}
                />
            </div>
        )
    }

    const renderImportModal = () => {
        const onCatalogChange = (event) => {
            const catalogName = fileCatalogList.find(item => item.value == event).label
            catalogRef.current = {
                catalogId: event,
                catalogName: catalogName
            }
        }

        const clickConfirm = () => {

        }

        const clickCancel = () => {
            setVisibleImport(false)
        }

        const clickImport = () => {
            const uploadList = []
            const {remark, catalog} = importBasicInfoRef.current
            const catalogId = catalog
            const catalogName = fileCatalogList.find(item => item.value == catalog).label
            for (let i = 0; i < importModalTableList.length; i++) {
                const fileItem = importModalTableList[i]
                const {id, file} = fileItem
                const formData = new FormData();
                formData.append('file', file.originFileObj);

                const fileRequest = new Promise((resolve, reject) => {
                    axios.post(`http://127.0.0.1:31986/viedoFile/uploadFile?catalogId=${catalogId}&catalogName=${catalogName}`, formData, {
                        headers: {
                            'Content-Type': "multipart/form-data"
                        }
                    } as any ).then((res) => {
                        resolve(res)
                    }).catch(err => {
                        resolve("error")
                    })
                })
                uploadList.push(fileRequest)
            }
            Promise.all(uploadList).then(res => {
                initCaseFileList()
            }).catch(err => {
            }).finally(() => {
                clickCancel()
            })
        }

        const clickDelete = () => {

        }


        const uploadProps = {
            showUploadList: false,
            maxCount: 8,
            multiple: true,
            fileList: fileList,
            beforeUpload: () => {
                return false;
            },
            onChange: (event) => {
                const fileList = event.fileList

                const newFileList = []
                for (let i = 0; i < fileList.length; i++) {
                    const file = fileList[i]
                    const {name, size}= file
                    const fileItem = {
                        key: generateUUID(),
                        id: generateUUID(),
                        name: name,
                        file: file,
                        type: getFileType(name),
                        size: getFileSize(size),
                        status: "waitting"
                    }
                    newFileList.push(fileItem)
                }
                setImportModalTableList([...newFileList, ...importModalTableList])
                setFileList([])
            }
        }

        const importModalColumns = [
            ...importModalBaseColumns
        ]

        const stepsItems = [
            {
                title: '基本信息',
                current: 0,
            },
            {
                title: '文件上传',
                current: 1,
            }
        ];

        const importBasicChange = (key, value) => {
            importBasicInfoRef.current = {
                ...importBasicInfoRef.current,
                [key]: value
            }
        }

        const clickStep = (event) => {
            if (event == "prev" && [1, 2].includes(importStep)){
                setImportStep(importStep - 1)
            }
            if (event == "next" && [0, 1].includes(importStep)){
                setImportStep(importStep + 1)
            }
            if (event == "cancel"){
                setVisibleImport(false)
            }
            if (event == "confirm"){
                clickImport()
            }
        }

        return (
            <Modal
                title="导入"
                open={visibleImport}
                keyboard={true}
                className="viedo-file-import-modal"
                width="45%"
                footer={() => (
                    <div className="operate-btn-group">
                        <Space>
                            <Button onClick={event => clickStep("prev")} className={[1].includes(importStep) ? "display-cls" : "hidden-cls"}>上一步</Button>
                            <Button onClick={event => clickStep("cancel")} className={[0, 1].includes(importStep) ? "display-cls" : "hidden-cls"}>取消</Button>
                            <Button type="primary" onClick={event => clickStep("confirm")} className={[1].includes(importStep) ? "display-cls" : "hidden-cls"}>确定</Button>
                            <Button onClick={event => clickStep("next")} className={[0].includes(importStep) ? "display-cls" : "hidden-cls"}>下一步</Button>
                        </Space>
                    </div>
                )}
            >
                <Steps current={importStep} labelPlacement="vertical" items={stepsItems} className="step-wrap"/>

                <div className="operate-panel" className={[0].includes(importStep) ? "import-basic" : "hidden-cls"}>
                    <Form
                        name="basic"
                        autoComplete="off"
                        labelCol={{ span: 2 }}
                    >
                        <Form.Item
                            label="名称"
                            name="name"
                            rules={[{ required: true, message: '请输入名称' }]}
                        >
                            <Input defaultValue={importBasicInfoRef.current.name} value={importBasicInfoRef.current.name} onChange={event => importBasicChange("name", event.target.value)}/>
                        </Form.Item>
                        <Form.Item
                            label="名称"
                            name="catalog"
                            rules={[{ required: true, message: '请输入名称' }]}
                        >
                            <Select className="catalog-select"
                                    onChange={event => importBasicChange("catalog", event)}
                                    options={fileCatalogList}
                            />
                        </Form.Item>
                        <Form.Item
                            label="备注"
                            name="remark"
                            rules={[{ required: true, message: '请输入编码' }]}
                        >
                            <Input.TextArea
                                showCount
                                style={{ height: 100 }}
                                defaultValue={importBasicInfoRef.current.remark}
                                value={importBasicInfoRef.current.remark}
                                onChange={event => importBasicChange("remark", event.target.value)}
                            />
                        </Form.Item>
                    </Form>
                </div>

                <div className={[1].includes(importStep) ? "import-step" : "hidden-cls"}>
                    <Space>
                        <Upload {...uploadProps}>
                            <Button>添加</Button>
                        </Upload>
                        <Button onClick={clickDelete}>删除</Button>
                    </Space>
                    <Table className="table-cls"
                           dataSource={importModalTableList}
                           columns={importModalColumns}
                           rowKey={row => row.id}
                           pagination={false}
                    />
                </div>
            </Modal>
        )
    }

    return(
        <div className="viedo-file-view">
            {renderFilterPanel()}
            <Divider />
            {renderTablePanel()}
            {renderImportModal()}
        </div>
    )
}

export default ViedoFileView