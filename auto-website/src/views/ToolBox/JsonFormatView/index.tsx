import React from "react";
import JSONEditor from 'jsoneditor';
import 'jsoneditor/dist/jsoneditor.css';

import "./index.less"

export function JsonFormatView() {
    const editorCanvasRef = useRef(null)
    const editorRef = useRef(null)

    useEffect(() => {
        initEditorCanvas();
    }, [])

    const initEditorCanvas = () => {
        const editorOptions: any = {
            mode: 'code',
            history: false,
            mainMenuBar: true, // 工具栏
        };

        if (!editorRef.current){
            editorRef.current = new JSONEditor(
                editorCanvasRef.current,
                editorOptions
            );
        }
    }

    return <div className='json-format-view'>
        <div className="editor-canvas"
             ref={editorCanvasRef}
        >
        </div>
    </div>
}

export default JsonFormatView