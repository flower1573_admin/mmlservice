import React from "react";

import {getEthernetInfo} from "@/api/systemManag/serveEthernet";
import AceEditor from 'react-ace';
import 'ace-builds/src-noconflict/theme-monokai';
import 'ace-builds/src-noconflict/mode-javascript';

import "./index.less"

export function DataFlushView() {
    const orginDataRef = useRef(null)
    const flushDataRef = useRef(null)


    const renderOperatePanel = () => {
        const clickFlush = () => {
            const orginData = orginDataRef.current.editor.getValue()
            const orginDataList = orginData.split("\n")

            const flushDataList = []
            for (const index in orginDataList) {
                if (!flushDataList.includes(orginDataList[index])) {
                    flushDataList.push(orginDataList[index])
                }
            }
            flushDataRef.current.editor.setValue(flushDataList.join("\n"))
        }
        const clickClear = () => {
            orginDataRef.current.editor.setValue("")
            flushDataRef.current.editor.setValue("")
        }
        return (
            <div className="operate-panel">
                <Button onClick={clickFlush} type="primary" className="operate-btn">清洗</Button>
                <Button onClick={clickClear} className="operate-btn">清除</Button>
            </div>
        )
    }

    const renderOrginDataPanel = () => {
        return (
            <div className="orgin-panel">
                <AceEditor className="editor-cls" ref={orginDataRef}
                    mode="javascript"
                    fontSize={16}
                    theme="monokai"
                    highlightActiveLine={true}
                    showPrintMargin={false}
                    setOptions={{
                        useWorker: false
                    }}
                />
            </div>
        )
    }

    const renderFlushDataPanel = () => {
        return (
            <div className="flush-panel">
                <AceEditor className="editor-cls" ref={flushDataRef}
                    mode="javascript"
                    fontSize={16}
                    theme="monokai"
                    highlightActiveLine={true}
                    readOnly={true}
                    showPrintMargin={false}
                    setOptions={{
                        useWorker: false
                    }}
                />
            </div>
        )
    }

    return <div className='data-flush-view-container'>
        {renderOperatePanel()}
        <Row gutter={[8, 8]} className="row-wrap">
            <Col span={12}>
                {renderOrginDataPanel()}
            </Col>
            <Col span={12} className="col-wrap">
                {renderFlushDataPanel()}
            </Col>
        </Row>
    </div>
}

export default DataFlushView;