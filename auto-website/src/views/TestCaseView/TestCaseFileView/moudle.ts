export const tableBaseColumns = [
    {
        title: "序号",
        dataIndex: "number",
        key: "number",
        render: (_, record, index) => index + 1,
        align: "center",
    }, {
        title: "名称",
        dataIndex: "name",
        key: "name",
    }, {
        title: "文件类型",
        dataIndex: "fileType",
        key: "fileType",
    }, {
        title: "文件大小",
        dataIndex: "fileSize",
        key: "fileSize",
    }, {
        title: "备注",
        dataIndex: "remark",
        key: "remark",
    }, {
        title: "创建时间",
        dataIndex: "createTime",
        key: "createTime",
    }
]