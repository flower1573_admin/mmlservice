import React from "react";

import SystemManagLayout from "@/layout/SystemManagLayout";
import SystemManagHomeView from "@/views/SystemManagView/HomeView";

import AduitLogView from "@/views/SystemManagView/LogView/AduitLogView/index";
import SystemLogView from "@/views/SystemManagView/LogView/SystemLogView/index";

import ServeCpuView from "@/views/SystemManagView/ServeCpuView";
import ServeDiskView from "@/views/SystemManagView/ServeDiskView";
import ServeEthernetView from "@/views/SystemManagView/ServeEthernetView";

import UserView from "@/views/SystemManagView/UserView";
import ScheduledView from "@/views/SystemManagView/ScheduledView";
import DictionaryView from "@/views/SystemManagView/DictionaryView";

import FileBucketsView from "@/views/SystemManagView/FileBucketsView";
import FileBrowserView from "@/views/SystemManagView/FileBrowserView";

const systemManageMen = [
    {
        id: "systemManage",
        element: <SystemManagLayout/>,
        path: "/systemManag",
        children: [
            {
                id: "systemManagHomeView",
                path: "systemManagHomeView",
                element: <SystemManagHomeView/>
            }, {
                id: "aduitLogView",
                path: "aduitLogView",
                element: <AduitLogView/>
            },{
                id: "systemLogView",
                path: "systemLogView",
                element: <SystemLogView/>
            }, {
                id: "serveCpuView",
                path: "serveCpuView",
                element: <ServeCpuView/>
            }, {
                id: "serveDiskView",
                path: "serveDiskView",
                element: <ServeDiskView/>
            }, {
                id: "serveEthernetView",
                path: "serveEthernetView",
                element: <ServeEthernetView/>
            }, {
                id: "userView",
                path: "userView",
                element: <UserView/>
            }, {
                id: "scheduledView",
                path: "scheduledView",
                element: <ScheduledView/>
            }, {
                id: "dictionaryView",
                path: "dictionaryView",
                element: <DictionaryView/>
            }, {
                id: "fileBucketsView",
                path: "fileBucketsView",
                element: <FileBucketsView/>
            }, {
                id: "fileBrowserView",
                path: "fileBrowserView",
                element: <FileBrowserView/>
            }
        ]
    },
]

export default systemManageMen