import AuthComponents from "../components/AuthComponents";
import React, {Suspense} from "react";
import CommonLayout from "@/layout/CommonLayout";
import HomeView from "@/views/HomeView";
import LoginView from "@/views/LoginView";
import SummaryView from "@/views/SummaryView";
import LogManageView from "@/views/LogManageView";
import FileManageView from "@/views/FileManageView";

const common = [
    {
        id: "app",
        path: "/",
        element: <HomeView/>,
    }, {
        id: "home",
        path: "/home",
        element: <HomeView/>,
    }, {
        id: "loginIn",
        path: "/loginIn",
        element: <LoginView/>,
    }, {
        id: "common",
        element: <CommonLayout/>,
        children: [
            {
                id: "fileManage",
                path: "fileManage",
                element: <FileManageView/>
            }
        ]
    },
]

export default common