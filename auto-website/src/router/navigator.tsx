import React from "react";

import NavigatorLayout from "@/layout/NavigatorLayout";
import NavigatorLinkView from "@/views/NavigatorView/NavigatorLinkView";
import NavigatorCategoryView from "@/views/NavigatorView/NavigatorCategoryView";


const navigator = [
    {
        id: "navigator",
        element: <NavigatorLayout/>,
        path: "/navigator",
        children: [
            {
                id: "navigatorLinkView",
                path: "navigatorLinkView",
                element: <NavigatorLinkView/>
            }, {
                id: "navigatorCategoryView",
                path: "navigatorCategoryView",
                element: <NavigatorCategoryView/>
            }
        ]
    },
]

export default navigator