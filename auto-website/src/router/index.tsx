import common from "./common.tsx"
import systemManageMen from "./systemManage.tsx"
import toolBox from "./toolBox.tsx"
import testBox from "./testBox.tsx"
import navigator from "./navigator.tsx"
import playRoom from "./playRoom.tsx"
import noteBook from "./noteBook.tsx"
import chatRoom from "./chatRoom.tsx"

const router = [
    ...common,
    ...systemManageMen,
    ...toolBox,
    ...testBox,
    ...navigator,
    ...playRoom,
    ...noteBook,
    ...chatRoom
];

export default router;
