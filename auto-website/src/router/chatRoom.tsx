import CommonLayout from "@/layout/CommonLayout";
import ChatRoomView from "@/views/ChatRoomView";

const chatRoom = [
    {
        id: "chatRoom",
        element: <CommonLayout/>,
        children: [
            {
                id: "chatRoomView",
                path: "chatRoomView",
                element: <ChatRoomView/>
            }
        ]
    },
]

export default chatRoom