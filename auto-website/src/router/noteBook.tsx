import React from "react";

import NotebookLayout from "@/layout/NotebookLayout";
import NotebookArticleView from "@/views/NoteBook/NotebookArticleView/index";
import NotebookTodoView from "@/views/NoteBook/NotebookTodoView/index";
import NoteBookArticleEditorView from "@/views/NoteBook/NoteBookArticleEditorView/index";
import NoteBookArticleDetailsView from "@/views/NoteBook/NoteBookArticleDetailsView/index";

const noteBook = [
    {
        id: "noteBook",
        element: <NotebookLayout/>,
        path: "/noteBook",
        children: [
            {
                id: "notebookArticleView",
                path: "notebookArticleView",
                element: <NotebookArticleView/>
            }, {
                id: "notebookTodoView",
                path: "notebookTodoView",
                element: <NotebookTodoView/>
            }, {
                id: "noteBookArticleEditorView",
                path: "noteBookArticleEditorView",
                element: <NoteBookArticleEditorView/>
            }, {
                id: "noteBookArticleDetailsView",
                path: "noteBookArticleDetailsView",
                element: <NoteBookArticleDetailsView/>
            }
        ]
    },
]

export default noteBook