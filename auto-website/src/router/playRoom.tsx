import React from "react";

import PlayRoomLayout from "@/layout/PlayRoomLayout";
import ViedoPlayView from "@/views/PlayRoom/ViedoPlayerView";
import ViedoCatalogView from "@/views/PlayRoom/ViedoCatalogView";
import ViedoFileView from "@/views/PlayRoom/ViedoFileView";


const playRoom = [
    {
        id: "playRoom",
        element: <PlayRoomLayout/>,
        path: "/playRoom",
        children: [
            {
                id: "viedoPlayerView",
                path: "viedoPlayerView",
                element: <ViedoPlayView/>
            }, {
                id: "viedoCatalogView",
                path: "viedoCatalogView",
                element: <ViedoCatalogView/>
            }, {
                id: "viedoFileView",
                path: "viedoFileView",
                element: <ViedoFileView/>
            }
        ]
    },
]

export default playRoom