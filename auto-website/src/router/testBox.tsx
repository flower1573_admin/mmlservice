import React from "react";

import TestBoxLayout from "@/layout/TestBoxLayout";
import JmeterGenerateView from "@/views/JmeterGenerateView";
import JmeterPlanView from "@/views/JmeterPlanView";
import JmeterTaskView from "@/views/JmeterTaskView";
import TestCaseFileView from "@/views/TestCaseView/TestCaseFileView";

const testBox = [
    {
        id: "testBox",
        element: <TestBoxLayout/>,
        path: "/testBox",
        children: [
            {
                id: "jmeterGenerateView",
                path: "jmeterGenerateView",
                element: <JmeterGenerateView/>
            }, {
                id: "jmeterPlanView",
                path: "jmeterPlanView",
                element: <JmeterPlanView/>
            }, {
                id: "jmeterTaskView",
                path: "jmeterTaskView",
                element: <JmeterTaskView/>
            }, {
                id: "testCaseFileView",
                path: "testCaseFileView",
                element: <TestCaseFileView/>
            }
        ]
    },
]

export default testBox