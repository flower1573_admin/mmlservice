import React from "react";

import ToolBoxLayout from "@/layout/ToolBoxLayout";
import DataFlushView from "@/views/ToolBox/DataFlushView/index";
import JsonFormatView from "@/views/ToolBox/JsonFormatView";


const toolBox = [
    {
        id: "toolBox",
        element: <ToolBoxLayout/>,
        path: "/toolBox",
        children: [
            {
                id: "dataFlushView",
                path: "dataFlushView",
                element: <DataFlushView/>
            },
            {
                id: "jsonFormatView",
                path: "jsonFormatView",
                element: <JsonFormatView/>
            }
        ]
    },
]

export default toolBox