import React, {Fragment, useEffect, useState} from "react";
import {RouterProvider, createHashRouter} from "react-router-dom";
import {Provider} from "react-redux";

import {ThemeProvider} from 'styled-components'
import {App as AntdProvider, theme} from 'antd'
import zhCN from 'antd/locale/zh_CN'

import router from "./router";
import store from "./store";
import ConfigProvider from "antd/es/config-provider";

function App() {
    const {i18n} = useTranslation()
    const [locale, setLocale] = useState(zhCN)
    const isDark = false
    const colorScheme = "light"
    const elementSize = "middle"

    useEffect(() => {
        const cancelSub = () => {
            i18n.changeLanguage("zh")
        }
        return () => cancelSub()
    }, [i18n])

  return (
    <ConfigProvider
      locale={locale}
      componentSize={elementSize}
      theme={{
        cssVar: true,
        hashed: false,
        algorithm: isDark ? theme.darkAlgorithm : theme.defaultAlgorithm
      }}>
      <ThemeProvider theme={{isDark}}>
        <AntdProvider component={false}>
            <Provider store={store}>
              <RouterProvider router={createHashRouter(router)} fallbackElement={<Fragment/>}/>
            </Provider>
        </AntdProvider>
      </ThemeProvider>
    </ConfigProvider>
  )
}

export default App
