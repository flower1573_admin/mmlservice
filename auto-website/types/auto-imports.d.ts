/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// noinspection JSUnusedGlobalSymbols
// Generated by unplugin-auto-import
export {}
declare global {
  const Alert: typeof import('antd')['Alert']
  const AuthComponents: typeof import('../src/components/AuthComponents/index')['default']
  const Button: typeof import('antd')['Button']
  const Card: typeof import('antd')['Card']
  const Carousel: typeof import('antd')['Carousel']
  const Checkbox: typeof import('antd')['Checkbox']
  const CodeMirror: typeof import('../src/components/CodeMirror/index.less')['default']
  const Col: typeof import('antd')['Col']
  const DatePicker: typeof import('antd')['DatePicker']
  const Divider: typeof import('antd')['Divider']
  const FileImport: typeof import('../src/components/FileImport/index')['default']
  const Form: typeof import('antd')['Form']
  const HightRisk: typeof import('../src/components/HightRisk/index')['default']
  const Input: typeof import('antd')['Input']
  const Layout: typeof import('antd')['Layout']
  const Link: typeof import('react-router-dom')['Link']
  const Menu: typeof import('antd')['Menu']
  const Message: typeof import('../src/components/MessageComponents/index')['Message']
  const MessageComponents: typeof import('../src/components/MessageComponents/index')['default']
  const Modal: typeof import('antd')['Modal']
  const NavLink: typeof import('react-router-dom')['NavLink']
  const Navigate: typeof import('react-router-dom')['Navigate']
  const Outlet: typeof import('react-router-dom')['Outlet']
  const Pagination: typeof import('antd')['Pagination']
  const Route: typeof import('react-router-dom')['Route']
  const Routes: typeof import('react-router-dom')['Routes']
  const Row: typeof import('antd')['Row']
  const Select: typeof import('antd')['Select']
  const Space: typeof import('antd')['Space']
  const Spin: typeof import('antd')['Spin']
  const Statistic: typeof import('antd')['Statistic']
  const Steps: typeof import('antd')['Steps']
  const Switch: typeof import('antd')['Switch']
  const Table: typeof import('antd')['Table']
  const Tag: typeof import('antd')['Tag']
  const Tree: typeof import('antd')['Tree']
  const Upload: typeof import('antd')['Upload']
  const closeModal: typeof import('../src/components/HightRisk/index')['closeModal']
  const createRef: typeof import('react')['createRef']
  const forwardRef: typeof import('react')['forwardRef']
  const generateUUID: typeof import('../src/components/FileImport/index')['generateUUID']
  const initReactI18next: typeof import('react-i18next')['initReactI18next']
  const lazy: typeof import('react')['lazy']
  const memo: typeof import('react')['memo']
  const message: typeof import('antd')['message']
  const openModal: typeof import('../src/components/HightRisk/index')['openModal']
  const startTransition: typeof import('react')['startTransition']
  const useCallback: typeof import('react')['useCallback']
  const useContext: typeof import('react')['useContext']
  const useDebugValue: typeof import('react')['useDebugValue']
  const useDeferredValue: typeof import('react')['useDeferredValue']
  const useEffect: typeof import('react')['useEffect']
  const useHref: typeof import('react-router-dom')['useHref']
  const useId: typeof import('react')['useId']
  const useImperativeHandle: typeof import('react')['useImperativeHandle']
  const useInRouterContext: typeof import('react-router-dom')['useInRouterContext']
  const useInsertionEffect: typeof import('react')['useInsertionEffect']
  const useLayoutEffect: typeof import('react')['useLayoutEffect']
  const useLinkClickHandler: typeof import('react-router-dom')['useLinkClickHandler']
  const useLocation: typeof import('react-router-dom')['useLocation']
  const useMemo: typeof import('react')['useMemo']
  const useNavigate: typeof import('react-router-dom')['useNavigate']
  const useNavigationType: typeof import('react-router-dom')['useNavigationType']
  const useOutlet: typeof import('react-router-dom')['useOutlet']
  const useOutletContext: typeof import('react-router-dom')['useOutletContext']
  const useParams: typeof import('react-router-dom')['useParams']
  const useReducer: typeof import('react')['useReducer']
  const useRef: typeof import('react')['useRef']
  const useResolvedPath: typeof import('react-router-dom')['useResolvedPath']
  const useRoutes: typeof import('react-router-dom')['useRoutes']
  const useSearchParams: typeof import('react-router-dom')['useSearchParams']
  const useState: typeof import('react')['useState']
  const useSyncExternalStore: typeof import('react')['useSyncExternalStore']
  const useTransition: typeof import('react')['useTransition']
  const useTranslation: typeof import('react-i18next')['useTranslation']
}
